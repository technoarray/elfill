import React, {Component} from 'react';
import { StackNavigator } from "react-navigation";

import MainScreen from './components/drawer/MainScreen';
import SignupScreen from './components/drawer/SignupScreen';
import LoginScreen from './components/drawer/LoginScreen';
import ForgetPasswordScreen from './components/drawer/ForgetPasswordScreen';
import VerifyOtpScreen from './components/drawer/VerifyOtpScreen';
import StartScreen from './components/drawer/StartScreen';

import SellerDashboard from './components/drawer/SellerDashboard';
import ViewPaymentScreen from './components/drawer/ViewPaymentScreen';

import Stationlistseller from './components/drawer/Stationlistseller';
import AddStationScreen from './components/drawer/AddStationScreen';
import EditStationScreen from './components/drawer/EditStationScreen';
import ViewStationScreen from './components/drawer/ViewStationScreen';

import AddserviceScreen from './components/drawer/AddserviceScreen';
import EditProfile from './components/drawer/EditProfile';
import Editservices from './components/drawer/Editservices';
import ViewServiceScreen from './components/drawer/ViewServiceScreen';

import PaymentMethod from './components/drawer/PaymentMethod';
import AvailableServices from './components/drawer/AvailableServices';
import BookingCalender from './components/drawer/BookingCalender';
import HelpScreen from './components/drawer/HelpScreen';
import AvailableSubServices from './components/drawer/AvailableSubServices';

import HomeScreen from './components/drawer/HomeScreen';
import SingleBookScreen from './components/drawer/SingleBookScreen';
import Thankyou from './components/drawer/ThankyouScreen';
import CalendarDetail from './components/drawer/CalendarDetail';
import RegistrationTc from './components/drawer/RegistrationTc'

import El_car_charge from './components/drawer/El_car_charge'
import El_car_park from './components/drawer/El_car_park'
import El_car_cable from './components/drawer/El_car_cable'
import El_car_bed from './components/drawer/El_car_bed'
import El_car_rent from './components/drawer/El_car_rent'

import El_bike_charge from './components/drawer/El_bike_charge'
import El_bike_park from './components/drawer/El_bike_park'
import El_bike_cable from './components/drawer/El_bike_cable'
import El_bike_bed from './components/drawer/El_bike_bed'
import El_bike_rent from './components/drawer/El_bike_rent'

import Gadget_charge from './components/drawer/Gadget_charge'
import Gadget_cable from './components/drawer/Gadget_cable'
import Gadget_bed from './components/drawer/Gadget_bed'
import Gadget_rent from './components/drawer/Gadget_rent'
import AccomodationStations from './components/drawer/AccomodationStations'
import AccomodationServices from './components/drawer/AccomodationServices'
import SelectionScreen from './components/drawer/SelectionScreen'

const AppNavigator = StackNavigator({
  MainScreen: {
      screen: MainScreen,
      navigationOptions: {
          header: null
      }
  },
  SignupScreen: {
      screen: SignupScreen,
      navigationOptions: {
          header: null
      }
  },
  LoginScreen: {
      screen: LoginScreen,
      navigationOptions: {
          header: null
      }
  },
  ForgetPasswordScreen: {
      screen: ForgetPasswordScreen,
      navigationOptions: {
          header: null
      }
  },
  VerifyOtpScreen: {
      screen: VerifyOtpScreen,
      navigationOptions: {
          header: null
      }
  },
  StartScreen: {
      screen: StartScreen,
      navigationOptions: {
          header: null
      }
  },
  SellerDashboard: {
      screen: SellerDashboard,
      navigationOptions: {
          header: null
      }
  },
  Stationlistseller: {
      screen: Stationlistseller,
      navigationOptions: {
          header: null
      }
  },
  AddserviceScreen: {
      screen: AddserviceScreen,
      navigationOptions: {
          header: null
      }
  },
  AddStationScreen: {
      screen: AddStationScreen,
      navigationOptions: {
          header: null
      }
  },
  EditStationScreen:{
    screen: EditStationScreen,
    navigationOptions: {
        header: null
    }
  },

    ViewStationScreen: {
        screen: ViewStationScreen,
        navigationOptions: {
            header: null
        }
    },

  EditProfile: {
      screen: EditProfile,
      navigationOptions: {
          header: null
      }
  },

  Editservices: {
      screen: Editservices,
      navigationOptions: {
          header: null
      }
  },

  ViewServiceScreen: {
      screen: ViewServiceScreen,
      navigationOptions: {
          header: null
      }
  },

  ViewPaymentScreen:{
    screen: ViewPaymentScreen,
    navigationOptions: {
        header: null
    }
  },
  PaymentMethod: {
      screen: PaymentMethod,
      navigationOptions: {
          header: null
      }
  },
  AvailableServices: {
      screen: AvailableServices,
      navigationOptions: {
          header: null
      }
  },
  BookingCalender: {
      screen: BookingCalender,
      navigationOptions: {
          header: null
      }
  },
  HelpScreen: {
      screen: HelpScreen,
      navigationOptions: {
          header: null
      }
  },

  HomeScreen: {
      screen: HomeScreen,
      navigationOptions: {
          header: null
      }
  },

  SingleBookScreen: {
      screen: SingleBookScreen,
      navigationOptions: {
          header: null
      }
  },
  AvailableSubServices:{
    screen:AvailableSubServices,
    navigationOptions:{
      header:null
    }
  },
  Thankyou:{
    screen:Thankyou,
    navigationOptions:{
      header:null
    }
  },
  CalendarDetail:{
    screen:CalendarDetail,
    navigationOptions:{
      header:null
    }
  },
  RegistrationTc:{
    screen:RegistrationTc,
    navigationOptions:{
      header:null
    }
  },
  El_car_charge:{
    screen:El_car_charge,
    navigationOptions:{
      header:null
    }
  },
  El_car_park:{
    screen:El_car_park,
    navigationOptions:{
      header:null
    }
  },
  El_car_bed:{
    screen:El_car_bed,
    navigationOptions:{
      header:null
    }
  },
  El_car_cable:{
    screen:El_car_cable,
    navigationOptions:{
      header:null
    }
  },
  El_car_rent:{
    screen:El_car_rent,
    navigationOptions:{
      header:null
    }
  },
  El_bike_charge:{
    screen:El_bike_charge,
    navigationOptions:{
      header:null
    }
  },
  El_bike_park:{
    screen:El_bike_park,
    navigationOptions:{
      header:null
    }
  },
  El_bike_bed:{
    screen:El_bike_bed,
    navigationOptions:{
      header:null
    }
  },
  El_bike_cable:{
    screen:El_bike_cable,
    navigationOptions:{
      header:null
    }
  },
  El_bike_rent:{
    screen:El_bike_rent,
    navigationOptions:{
      header:null
    }
  },
  Gadget_charge:{
    screen:Gadget_charge,
    navigationOptions:{
      header:null
    }
  },
  Gadget_cable:{
    screen:Gadget_cable,
    navigationOptions:{
      header:null
    }
  },
  Gadget_bed:{
    screen:Gadget_bed,
    navigationOptions:{
      header:null
    }
  },
  Gadget_rent:{
    screen:Gadget_rent,
    navigationOptions:{
      header:null
    }
  },
  AccomodationStations:{
    screen:AccomodationStations,
    navigationOptions:{
      header:null
    }
  },
  AccomodationServices:{
    screen:AccomodationServices,
    navigationOptions:{
      header:null
    }
  },
  SelectionScreen:{
    screen:SelectionScreen,
    navigationOptions:{
      header:null
    }
  },
}, {
    //initialRouteName: isEmpty(currentUser) ? 'StartScreen' : 'LoginScreen',
      initialRouteName: 'MainScreen',
   //initialRouteName: 'PaymentMethod',

    headerMode: 'none',

});

export default AppNavigator;
