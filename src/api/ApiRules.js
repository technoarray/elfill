class Constant {
    static Google_apiKey='AIzaSyAEIW3Lg6K5vpN9GTIFT691Cc9A7crUB_E';

    static BASE_URL = "https://www.talentim.net/fa_mytasks/slim_api/public/";
    static image_path="https://www.talentim.net/fa_mytasks/efil/files/";

    static URL_register = Constant.BASE_URL + 'users/signup';
    static URL_login = Constant.BASE_URL + 'users/login';
    static URL_updateProfile = Constant.BASE_URL + 'users/update';

    static URL_socialLogin = Constant.BASE_URL + 'users/social_login';
    static URL_forgotPassword = Constant.BASE_URL + 'users/forgetPassword';
    static URL_verificationCode = Constant.BASE_URL + 'users/verification_Code';
    static URL_Changetype = Constant.BASE_URL + 'users/change_usertype';

    // seller Api
    static URL_serviceStation = Constant.BASE_URL + 'seller/serviceStation/all';
    static URL_serviceStationEdit = Constant.BASE_URL + 'seller/serviceStation/edit';
    static URL_serviceStationAdd = Constant.BASE_URL + 'seller/serviceStation/add';
    static URL_serviceStationDelete = Constant.BASE_URL + 'seller/serviceStation/delete';

    static URL_service = Constant.BASE_URL + 'seller/services/all';
    static URL_serviceEdit = Constant.BASE_URL + 'seller/services/edit';
    static URL_serviceAdd = Constant.BASE_URL + 'seller/services/add';
    static URL_serviceUpdate=Constant.BASE_URL + 'seller/service/update';
    static URL_serviceDelete = Constant.BASE_URL + 'seller/services/delete';

    static URL_stationList = Constant.BASE_URL + 'seller/station/all';
    static URL_subCategoryList = Constant.BASE_URL + 'seller/subcategory';
    static URL_otherCategoryList=Constant.BASE_URL + 'seller/othercategory';
    static URL_devicesList = Constant.BASE_URL + 'seller/devices/list';
    static URL_buyService = Constant.BASE_URL + 'buyer/station/buyservice'

    static URL_calender = Constant.BASE_URL + 'seller/calender/order_date'
    static URL_calenderDetails = Constant.BASE_URL + 'seller/calender/services'


    //buyer api
    static URL_allStationList=Constant.BASE_URL + 'buyer/station/locationlist';
    static URL_allStationListByCat=Constant.BASE_URL + 'buyer/station/locationbycat';
    static URL_userStationList=Constant.BASE_URL + 'buyer/station/location';
    static URL_servicsCatList=Constant.BASE_URL + 'buyer/station/category';
    static URL_servicesSubCatList=Constant.BASE_URL + 'buyer/station/subcategory';
    static URL_servicesToBuyer=Constant.BASE_URL + 'buyer/station/service';
    static URL_signupData= Constant.BASE_URL + 'users/signup_data';
    static URL_accomodationStation=Constant.BASE_URL + 'buyer/station/accomodation'
    static URL_accomodationServices=Constant.BASE_URL + 'buyer/station/serviceaccomodation'

    static URL_payment = Constant.BASE_URL + 'seller/payment/all';
    static URL_sellerDetails=Constant.BASE_URL + 'buyer/seller/details';
    static URL_mybooking=Constant.BASE_URL + 'buyer/booking';

    }

    var WebServices = {

      callWebService: function(url, formBody) {
        //console.log(formBody)

          return fetch(url, {
                  method: 'POST',
                  headers: {
                      //'Accept': 'application/json',
                      'Accept':'application/json; charset=utf-8',
                      'Content-Type':'multipart/form-data;charset=UTF-8'
                      //'CallToken': body.CallToken ? body.CallToken : '',
                  },
                  body: formBody
              })
              .then(response => response.text()) // Convert to text instead of res.json()
              .then((text) => {
                  return text;
              })
              .then(response => JSON.parse(response)) // Parse the text.
              .then((jsonRes) => {
                console.log(jsonRes)
                  return jsonRes; //main output
              });
      },

        callWebService_GET: function(url, body) {
            return fetch(url, { // Use your url here
                    method: 'GET',
                    headers: {
                        'Accept': 'application/json',
                        'Content-Type': 'application/json',
                        //'CallToken': body.CallToken ? body.CallToken : '',
                    },
                })
                .then(response => response.text()) // Convert to text instead of res.json()
                .then((text) => {
                  return text;
                })
                .then(response => JSON.parse(response)) // Parse the text.
                .then((jsonRes) => {

                    // if ((!jsonRes) || (jsonRes.ResponseCode != '200')) {
                    //     onError(jsonRes);
                    // } else {
                    //     onRequestComplete(jsonRes);
                    // }

                    return jsonRes;
                });
        },

}
module.exports = {
    Constant,
    WebServices
}
