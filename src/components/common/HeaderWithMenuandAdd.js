import React, { Component } from 'react';
import {StyleSheet,Text,View,Image,TouchableOpacity,WebView,Platform, ActivityIndicator,StatusBar,Modal,ScrollView} from 'react-native';
import { DrawerActions } from 'react-navigation';
import LinearGradient from 'react-native-linear-gradient';
import { AppStyles, AppSizes, AppColors } from '../../themes/'
import * as commonFunctions from '../../utils/CommonFunctions'

/* Images */
var logo = require( '../../themes/Images/logo.png')
var menu_icon = require( '../../themes/Images/menu_icon_196.png')
var plus= require( '../../themes/Images/add.png')

const MyStatusBar = ({backgroundColor, ...props}) => (
  <View style={[styles.statusBar, { backgroundColor }]}>
    <StatusBar translucent backgroundColor={backgroundColor} {...props} />
  </View>
);
export default class Header extends React.Component {
  constructor (props) {
    super(props);
    this.state = {
    routename: '',
  },
  _that = this;
  }

  addBtn(){
    _that.props.navigation.navigate('AddStationScreen');
  }

    render() {
      const routename=this.props.info.screens;
      const type=this.props.info.type;

      if(routename=='HomeScreen'){
        headerTitle=<View style={styles.logoimageContainer}>
        <View style={styles.logoWrapper}>
            <Image style={styles.image} source={logo}/>
        </View>
        </View>;
      }
      else {
        headerTitle=<Text style={styles.headertitle}>{this.props.info.title}</Text>;
      }

     if(type!=''){
       headerAdd=<TouchableOpacity style={styles.qusWrapper} onPress={this.addBtn}>
               <Image source={plus} style={styles.ImageStylemain} />
         </TouchableOpacity>;
     }
     else {
       headerAdd=<Text></Text>;
     }

      return (
      <LinearGradient colors={['#3995f7','#3995f7','#3995f7','#3995f7']}>

        <MyStatusBar barStyle="light-content"  backgroundColor="#0f5491"/>
          <View style={styles.appBar} >
              <View style={{ flex:1,flexDirection: 'row', justifyContent: 'space-between', alignItems: 'center', }}>
                <View style={styles.imageContainer}>
                  <TouchableOpacity style={styles.menuWrapper} onPress={ () =>this.props.navigation.openDrawer()}>
                    <Image style={styles.image} source={menu_icon}/>
                    </TouchableOpacity>
                </View>

                <View style={{ marginTop:0,width:'60%',justifyContent: 'center',alignItems:'center' }}>
                    {headerTitle}
                </View>

                <View style={{width:'15%',flexDirection: 'row',justifyContent: 'flex-end'}}>
                {headerAdd}
              </View>

              </View>
          </View>
      </LinearGradient>
    )
    }
}

const styles = StyleSheet.create({
  statusBar: {
    height: AppSizes.statusBarHeight,
  },
  appBar: {
      height: AppSizes.navbarHeight,
      justifyContent:'center',
      paddingTop:(Platform.OS === 'ios') ? AppSizes.ResponsiveSize.Padding(0) :AppSizes.ResponsiveSize.Padding(4),
      paddingBottom:(Platform.OS === 'ios') ? AppSizes.ResponsiveSize.Padding(10) :AppSizes.ResponsiveSize.Padding(0),
    },
  headertitle:{
    color:'#ffffff',
    fontSize:AppSizes.ResponsiveSize.Sizes(20),
    fontWeight:'300',
    letterSpacing:1,
  },
  imageContainer:{
    width:'20%'
  },
  logoimageContainer:{
    width:'50%'
  },
  logoWrapper:{
    width:'100%',
    height:'100%',
  },
  menuWrapper: {
    width:'40%',
    height:'40%',
    marginLeft:'30%',
  },
  qusWrapper: {
    width:'40%',
    height:'40%',
    marginRight:'30%',
    marginTop:'15%'
  },
  image: {
    flex: 1,
    width: undefined,
    height: undefined,
    resizeMode:'contain'
  },

    buttonStyle: {
      width: '100%',
      height: '100%',
      justifyContent: 'center',
      alignItems: 'center'
    },
    TextStyle:{
      color:'#000',
      textAlign:'center',
      fontSize: AppSizes.ResponsiveSize.Sizes(18),
    },

  ImageStylemain: {
    height: AppSizes.screen.width/17,
    width: AppSizes.screen.width/17,
    resizeMode : 'contain',
  },
  Alert_Main_View:{
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: '#ffffff',
    height:'40%',
    width: '80%',
    borderRadius:20,
    flexDirection:'column',
    justifyContent: 'center',
    alignItems: 'center',

  },
        Alert_Title:{
        fontSize: AppSizes.ResponsiveSize.Sizes(25),
        color: "#000",
        textAlign: 'center',
        padding: 10,
        height: '28%'
      },
        Alert_Message:{
          fontSize: AppSizes.ResponsiveSize.Sizes(18),
          color: "#000",
          textAlign: 'center',
          padding: 10,
        },
        buttonStyle: {
          width: '100%',
          height: '100%',
          justifyContent: 'center',
          alignItems: 'center'
        },
        TextStyle:{
          color:'#000',
          textAlign:'center',
          fontSize: AppSizes.ResponsiveSize.Sizes(18),
        },

});
