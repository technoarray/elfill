import React from 'react'
import { Text, View, StyleSheet, Image } from 'react-native'
import { AppSizes } from '../../themes/'

type Props = {
    children: string
}

const Loader = (props: Props) => {
    const { isVisible } = props
    return (
      	<View style={styles.noLoadingStyle}>
		    { (isVisible) ?
		    	<View style={styles.loadingStyle}>
					    <View style={{ alignItems: 'center', backgroundColor: 'transparent', height: 100, overflow: 'hidden', width: 100, }}>
					        <Image source={require( '../../themes/Images/loading.gif')} style={{width: 100, height: 100 }}/>
					    </View>
				</View>
		    :
		    <View/> }
		</View>
    )
}

export default Loader

const styles =StyleSheet.create({
	loadingStyle : {
		position: 'absolute', 
		width: AppSizes.screen.width, 
		height: AppSizes.screen.height, 
		alignItems: 'center', 
      	justifyContent: 'center', 
      	backgroundColor:'transparent',
	},
	noLoadingStyle : {
		position: 'absolute', 
		width: AppSizes.screen.width, 
		height: AppSizes.screen.height, 
		alignItems: 'center', 
      	justifyContent: 'center', 
	}
})