import React, { Component } from 'react';
import {StyleSheet,Text,View,Image,TouchableOpacity,WebView,Platform, ActivityIndicator,StatusBar,Modal,ScrollView} from 'react-native';
import { DrawerActions } from 'react-navigation';
import LinearGradient from 'react-native-linear-gradient';
import { AppStyles, AppSizes, AppColors } from '../../themes/'
import * as commonFunctions from '../../utils/CommonFunctions'

/* Images */
var left_arrow = require( '../../themes/Images/left-arrow.png')

const MyStatusBar = ({backgroundColor, ...props}) => (
  <View style={[styles.statusBar, { backgroundColor }]}>
    <StatusBar translucent backgroundColor={backgroundColor} {...props} />
  </View>
);
export default class HeaderBeforeLogin extends React.Component {
  constructor (props) {
    super(props);
    this.state = {
    routename: '',

  },
  _that = this;
  }


  menu_click() {
      _that.props.navigation.navigate('LoginScreen');
  }



    render() {
      const routename=this.props.info.screens;
      if(routename=='ForgetScreen'){
        headerTitle=<Text style={styles.headertitle}></Text>;
      }
      else {
        headerTitle=<Text style={styles.headertitle}>{this.props.info.title}</Text>;
      }
      return (
      <LinearGradient colors={['#ffffff','#ffffff','#ffffff','#ffffff']}>

        <MyStatusBar barStyle="dark-content"  backgroundColor="#ffffff"/>
          <View style={styles.appBar} >
              <View style={{ flex:1,flexDirection: 'row', justifyContent: 'space-between', alignItems: 'center', }}>
                <View style={styles.imageContainer}>
                  <TouchableOpacity style={styles.menuWrapper} onPress={this.menu_click}>
                    <Image style={styles.image} source={left_arrow}/>
                    </TouchableOpacity>
                </View>

                <View style={{ marginTop:0,width:'60%',justifyContent: 'center',alignItems:'center' }}>
                    {headerTitle}
                </View>

                <View style={{width:'15%',flexDirection: 'row',justifyContent: 'flex-end'}}>

                </View>

              </View>
          </View>


      </LinearGradient>
    )
    }
}

const styles = StyleSheet.create({
  statusBar: {
    height: AppSizes.statusBarHeight,
  },
  appBar: {
      height: AppSizes.navbarHeight,
      justifyContent:'center',
    },
  headertitle:{
    color:AppColors.primary,
    fontSize:AppSizes.ResponsiveSize.Sizes(25),
    fontWeight:'500',
    letterSpacing:1,
  },
  imageContainer:{
    width:'20%'
  },
  menuWrapper: {
    width:'35%',
    height:'35%',
    marginLeft:'20%',
  },

  image: {
    flex: 1,
    width: undefined,
    height: undefined,
    resizeMode:'contain'
  },

    buttonStyle: {
      width: '100%',
      height: '100%',
      justifyContent: 'center',
      alignItems: 'center'
    },
    TextStyle:{
      color:'#000',
      textAlign:'center',
      fontSize: AppSizes.ResponsiveSize.Sizes(18),
    },

});
