import React, { Component } from 'react';
import {StyleSheet,Text,View,Image,TouchableOpacity,Platform,Alert,Button} from 'react-native';
import LinearGradient from 'react-native-linear-gradient';
import { AppStyles, AppSizes, AppColors } from '../../themes/'


export default class CommonButton extends Component {
  render() {
    return (

        <LinearGradient colors={['#3995f7','#3995f7','#3995f7','#3995f7']} style={styles.btncontainer}>
          <View style={{ width: this.props.width}}>
              <Text style={styles.textbtn}>{this.props.label}</Text>
          </View>
        </LinearGradient>

    );
  }
}

const styles = StyleSheet.create({
textbtn:{
  color:'#ffffff',
  fontSize:AppSizes.ResponsiveSize.Sizes(14),
  fontWeight:'500',
  textAlign:'center'
},
btncontainer:{

  justifyContent: 'center',
  height:AppSizes.screen.width/8,
  borderWidth:0,
  flexDirection:'row',
  alignItems: 'center',
  justifyContent:'center',
}
});
