import React, { Component } from 'react';
import {StyleSheet,Text,View,Image,TouchableOpacity,Platform,Alert,Button} from 'react-native';
import LinearGradient from 'react-native-linear-gradient';
import { AppStyles, AppSizes, AppColors } from '../../themes/'


export default class CommonButton extends Component {
  render() {
    return (

        <View  style={styles.btncontainer}>
          <View style={{ width: this.props.width}}>
              <Text style={styles.textbtn}>{this.props.label}</Text>
          </View>
        </View>

    );
  }
}

const styles = StyleSheet.create({
textbtn:{
  color:'#ffffff',
  fontSize:AppSizes.ResponsiveSize.Sizes(14),
  fontWeight:'500',
  textAlign:'center'
},
btncontainer:{
  borderColor:'#fff',
  justifyContent: 'center',
  height:AppSizes.screen.width/8,
  borderWidth:1,
  flexDirection:'row',
  alignItems: 'center',
  justifyContent:'center',
}
});
