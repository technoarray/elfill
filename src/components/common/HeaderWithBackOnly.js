import React, { Component } from 'react';
import {StyleSheet,Text,View,Image,TouchableOpacity,WebView,Platform, ActivityIndicator,StatusBar,Modal,ScrollView} from 'react-native';
import { NavigationActions,StackActions } from 'react-navigation';
import LinearGradient from 'react-native-linear-gradient';
import { AppStyles, AppSizes, AppColors } from '../../themes/'
import * as commonFunctions from '../../utils/CommonFunctions'

/* Images */
var logo = require( '../../themes/Images/logo.png')
var left_arrow = require( '../../themes/Images/left-arrow.png')

const MyStatusBar = ({backgroundColor, ...props}) => (
  <View style={[styles.statusBar, { backgroundColor }]}>
    <StatusBar translucent backgroundColor={backgroundColor} {...props} />
  </View>
);
export default class Header extends React.Component {
  constructor (props) {
    super(props);
    this.state = {
    routename: '',
  },
  _that = this;
  }

  menu_click() {
    const routename=_that.props.info.screens;

      if(routename!="EditProfile"){
        _that.props.navigation.goBack(null);
      }
      else{
        const resetAction = StackActions.reset({
              index: 0,
              actions: [NavigationActions.navigate({ routeName: 'StartScreen' })],
          });
       _that.props.navigation.dispatch(resetAction);
      }
  }
    render() {
      const routename=this.props.info.screens;
      const type=this.props.info.type;


      if(routename=='HomeScreen'){
        headerTitle=<View style={styles.logoimageContainer}>
        <View style={styles.logoWrapper}>
            <Image style={styles.image} source={logo}/>
        </View>
        </View>;
      }
      else {
        headerTitle=<Text style={styles.headertitle}>{this.props.info.title}</Text>;
      }

      return (
      <LinearGradient colors={['#3995f7','#3995f7','#3995f7','#3995f7']}>

        <MyStatusBar barStyle="light-content"  backgroundColor="#3995f7"/>
          <View style={styles.appBar} >
              <View style={{ flex:1,flexDirection: 'row', justifyContent: 'space-between', alignItems: 'center', }}>
                <View style={styles.imageContainer}>
                <TouchableOpacity style={styles.menuWrapper} onPress={this.menu_click}>
                  <Image style={styles.image} source={left_arrow}/>
                  </TouchableOpacity>
                </View>

                <View style={{ marginTop:0,width:'60%',justifyContent: 'center',alignItems:'center' }}>
                    {headerTitle}
                </View>

                <View style={{width:'15%',flexDirection: 'row',justifyContent: 'flex-end',}}>

                </View>

              </View>
          </View>


      </LinearGradient>
    )
    }
}

const styles = StyleSheet.create({
  statusBar: {
    height: AppSizes.statusBarHeight,
  },
  appBar: {
      height: AppSizes.navbarHeight,
      justifyContent:'center',
      paddingTop:(Platform.OS === 'ios') ? AppSizes.ResponsiveSize.Padding(0) :AppSizes.ResponsiveSize.Padding(4),
      paddingBottom:(Platform.OS === 'ios') ? AppSizes.ResponsiveSize.Padding(10) :AppSizes.ResponsiveSize.Padding(0),
    },
  headertitle:{
    color:'#ffffff',
    fontSize:AppSizes.ResponsiveSize.Sizes(20),
    fontWeight:'300',
    letterSpacing:1,
  },
  imageContainer:{
    width:'20%'
  },
  logoimageContainer:{
    width:'50%'
  },
  logoWrapper:{
    width:'100%',
    height:'100%',
  },
  menuWrapper: {
    width:'35%',
    height:'35%',
    marginLeft:'30%',
  },
  qusWrapper: {
    width:'40%',
    height:'40%',
    marginRight:'30%',
    marginTop:'15%'
  },
  image: {
    flex: 1,
    width: undefined,
    height: undefined,
    resizeMode:'contain'
  },

    buttonStyle: {
      width: '100%',
      height: '100%',
      justifyContent: 'center',
      alignItems: 'center'
    },
    TextStyle:{
      color:'#000',
      textAlign:'center',
      fontSize: AppSizes.ResponsiveSize.Sizes(18),
    },

        ImageStylemain: {
          height: AppSizes.screen.width/17,
          width: AppSizes.screen.width/17,
          resizeMode : 'contain',
        },

});
