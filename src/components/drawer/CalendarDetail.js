import React, { Component } from 'react';
import {StyleSheet,Text,View,Image,TouchableOpacity,Platform,Alert,ScrollView,TextInput} from 'react-native';
import { AppStyles, AppSizes, AppColors } from '../../themes/'
import { NavigationActions } from 'react-navigation'
import Spacer from '../common/Spacer'
import * as commonFunctions from '../../utils/CommonFunctions'
import Header from '../common/HeaderWithBack'
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view'
import CommonButton from '../common/CommonButton'
import { Fonts } from '../../utils/Fonts';
var Constant = require('../../api/ApiRules').Constant;
var WebServices = require('../../api/ApiRules').WebServices;

let _that
export default class CalendarDetail extends Component {
  constructor (props) {
      super(props)
      this.state = {
        isVisible: false,
        item:[]
    },
    _that = this;
  }

  componentWillMount(){
    var item=this.props.navigation.state.params.item
      _that.setState({
        item:item
      })
  }

  render() {
    const headerProp = {
      title: 'Calender Details',
      screens: 'CalendarDetail',
      type:''
    };
    return (
      <View style={styles.container}>
      <Header info={headerProp} navigation={_that.props.navigation}/>

        <View style={styles.content}>
        <KeyboardAwareScrollView innerRef={() => {return [this.refs.name,this.refs.email, this.refs.password]}} >

          <View style={styles.container2}>
            <View style={styles.SectionStyle}>
                <View style={styles.inputfield}>
                    <Text style={styles.inputtext}>Order ID</Text>
                    <Text style={styles.smalltext}>{this.state.item.order_id}</Text>
                </View>
            </View>

              <View style={styles.SectionStyle}>
                  <View style={styles.inputfield}>
                      <Text style={styles.inputtext}>Name</Text>
                      <Text style={styles.smalltext}>{this.state.item.name}</Text>
                  </View>
              </View>
              <View style={styles.SectionStyle}>
                  <View style={styles.inputfield}>
                      <Text style={styles.inputtext}>Station Name</Text>
                      <Text style={styles.smalltext}>{this.state.item.station_name}</Text>
                  </View>
              </View>
              <View style={styles.SectionStyle}>
                  <View style={styles.inputfield}>
                      <Text style={styles.inputtext}>Category</Text>
                      <Text style={styles.smalltext}>{this.state.item.cat_name}</Text>
                  </View>
              </View>
              <View style={styles.SectionStyle}>
                  <View style={styles.inputfield}>
                      <Text style={styles.inputtext}>Subcategory</Text>
                      <Text style={styles.smalltext}>{this.state.item.subcat_name}</Text>
                  </View>
              </View>

              {this.state.item.address ?
              <View style={styles.SectionStyle}>
                  <View style={styles.inputfield}>
                      <Text style={styles.inputtext}>Address </Text>
                      <Text style={styles.smalltext}></Text>
                  </View>
              </View>
              : null }

              {this.state.item.telephone ?
              <View style={styles.SectionStyle}>
                  <View style={styles.inputfield}>
                      <Text style={styles.inputtext}>Contact Number </Text>
                      <Text style={styles.smalltext}>{this.state.item.telephone}</Text>
                  </View>
              </View>
              : null }

              {this.state.item.zip_code ?
              <View style={styles.SectionStyle}>
                  <View style={styles.inputfield}>
                      <Text style={styles.inputtext}>Zip Code</Text>
                      <Text style={styles.smalltext}>{this.state.item.zip_code}</Text>
                  </View>
              </View>
            : null }

            {this.state.item.country ?
              <View style={styles.SectionStyle}>
                  <View style={styles.inputfield}>
                      <Text style={styles.inputtext}>Country</Text>
                      <Text style={styles.smalltext}>{this.state.item.country}</Text>
                  </View>
              </View>
            : null }

          </View>
        </KeyboardAwareScrollView>
        </View>
    </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    paddingTop:  0,
    backgroundColor:'#fff'
  },

  content: {
      flex: 1,
      flexDirection: 'column',
      height:'100%',
      padding:'5%',
      //backgroundColor:'#000'
  },

  container2: {
    height:'100%',
    flexDirection: 'column',
    //backgroundColor:'blue'
  },
  container3: {
    flex: 1,
    height:AppSizes.screen.width/8,
    alignItems: 'center',//replace with flex-end or center
    justifyContent: 'flex-end',
    //backgroundColor:'green'


  },

headerTitle:{
  fontSize:AppSizes.ResponsiveSize.Sizes(25),
  color:'#000000',
  fontWeight:'bold',
  fontFamily:Fonts.RobotoBold,
  paddingBottom:AppSizes.ResponsiveSize.Padding(1),
},
headersubTitle:{
  fontSize:AppSizes.ResponsiveSize.Sizes(14),
  color:'#000000',
  fontWeight:'600',
  fontFamily:Fonts.RobotoBold,
  paddingTop:AppSizes.ResponsiveSize.Padding(2),
},
headerBorder :{
  borderBottomColor: '#000000',
  borderBottomWidth: 3,
  width:'10%',
},
footertext :{
 color :'#000000',
 fontSize:AppSizes.ResponsiveSize.Sizes(12),
 fontWeight:'400',
 fontFamily:Fonts.RobotoRegular,
},
SectionStyle: {
  width:AppSizes.ResponsiveSize.width,

},
smalltext:{
  width:'50%',
  fontSize:AppSizes.ResponsiveSize.Sizes(13),
  color:'#333',
  fontWeight:'400',
  fontFamily:Fonts.RobotoRegular,
},

inputtext:{marginBottom:10,
  width:'50%',
  marginTop:AppSizes.ResponsiveSize.Padding(3),
  fontSize:AppSizes.ResponsiveSize.Sizes(16),
  fontWeight:'600',
   fontFamily:Fonts.RobotoBold,
  marginBottom:AppSizes.ResponsiveSize.Padding(1)
},
highlight:{
  backgroundColor:'green',
  padding:AppSizes.ResponsiveSize.Padding(1),
  paddingTop:AppSizes.ResponsiveSize.Padding(.10),
  paddingBottom:AppSizes.ResponsiveSize.Padding(.10),
  borderRadius:15,
  color:'#fff',
  flexDirection:'row'
},
highlightbox:{

  flexDirection:'row',
  flexWrap:'wrap',
},
inputfield:{
  flexDirection: 'row',
  justifyContent: 'space-between',
  alignItems: 'center',
},
booktext:{
  fontSize:AppSizes.ResponsiveSize.Sizes(20),
  fontWeight:'600',
  color:'#000',
  fontFamily:Fonts.RobotoBold,
  marginBottom:AppSizes.ResponsiveSize.Padding(2)
},
bookbox:{
  width:'100%',
  borderColor:'#fff',
  alignItems:'center',

},

});
