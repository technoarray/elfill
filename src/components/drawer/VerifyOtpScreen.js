import React, { Component } from 'react';
import {AsyncStorage,StyleSheet,Text,View,Image,TouchableOpacity,Platform,ScrollView,Alert,ImageBackground,StatusBar,TextInput} from 'react-native';
import { AppStyles, AppSizes, AppColors } from '../../themes/'
import { NavigationActions, StackActions  } from 'react-navigation';
import Spacer from '../common/Spacer'
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view'
import Header from '../common/HeaderBeforeLogin'
import CommonButton from '../common/CommonButton'
import * as commonFunctions from '../../utils/CommonFunctions'
import Spinner from 'react-native-loading-spinner-overlay';
import { Fonts } from '../../utils/Fonts';
var Constant = require('../../api/ApiRules').Constant;
var WebServices = require('../../api/ApiRules').WebServices;

const bg_image = require('../../themes/Images/bg_image.png')
const left_arrow = require('../../themes/Images/left-arrow.png')
const password_icon = require('../../themes/Images/password.png')

const MyStatusBar = ({backgroundColor, ...props}) => (
  <View style={[styles.statusBar, { backgroundColor }]}>
    <StatusBar translucent backgroundColor={backgroundColor} {...props} />
  </View>
);
let _that
export default class VerifyOtpScreen extends Component {
  constructor (props) {
    super(props)
    this.state = {
      uid:'',
      isVisible: false,
      verification_Code : '',
      verifyerr:'',
  },
  _that = this;
}

componentWillMount() {
  AsyncStorage.getItem('UserData').then((UserData) => {
        var data = JSON.parse(UserData);
        this.setState({uid:data.id})
        console.log(this.state.uid)
    })
}

arrow_click = () => {
      _that.props.navigation.navigate('MainScreen');
}
signup_click=() =>{
  _that.props.navigation.navigate('SignupScreen');
}

submit_click(){
  _that.validationAndApiParameter()

}

validationAndApiParameter() {
  const { uid,verification_Code,isVisible } = this.state
  var error=0;

      if ((verification_Code.length <= 0)) {
        _that.setState({verifyerr:'Please enter verification code'})
        error=1;
      }
      else{
          _that.setState({verifyerr:''})
      }

      if(error==0){
        const data = new FormData();
        data.append('uid', uid);
        data.append('verification_code', verification_Code);

        console.log(data);

        _that.setState({isVisible: true});
        this.postToApiCalling('POST', 'verify', Constant.URL_verificationCode, data);
      }
    }


    postToApiCalling(method, apiKey, apiUrl, data) {

       new Promise(function(resolve, reject) {
            if (method == 'POST') {
                resolve(WebServices.callWebService(apiUrl, data));
            } else {
                resolve(WebServices.callWebService_GET(apiUrl, data));
            }
        }).then((jsonRes) => {
          _that.setState({ isVisible: false })

            if ((!jsonRes) || (jsonRes.code == 0)) {

            setTimeout(()=>{
                Alert.alert('Error',jsonRes.message);
            },200);

            } else {
                if (jsonRes.code == 1) {
                    _that.apiSuccessfullResponse(apiKey, jsonRes)
                }
            }
        }).catch((error) => {
            console.log("ERROR" + error);
            _that.setState({ isVisible: false })

            setTimeout(()=>{
                Alert.alert("Server issue");
            },200);
        });
    }

    apiSuccessfullResponse(apiKey, jsonRes) {
      if (apiKey == 'verify') {
        AsyncStorage.setItem('onScreen','VerifyOtpScreen')
        AsyncStorage.setItem("loggedIn", JSON.stringify(true)).done();

        const resetAction = StackActions.reset({
          index: 0,
          actions: [NavigationActions.navigate({ routeName: 'StartScreen' })],
        });
        _that.props.navigation.dispatch(resetAction);
      }
    }

  render() {

    return (
      <View style={styles.wrapper}>
      <MyStatusBar barStyle="light-content"  backgroundColor="#45295a"/>

        <ImageBackground source={bg_image} style={{width: '100%', height: '100%'}}>
        <View style={styles.appBar} >
              <View style={styles.imageContainer}>
                <TouchableOpacity style={styles.menuWrapper} onPress={this.arrow_click}>
                  <Image style={styles.image} source={left_arrow}/>
                  </TouchableOpacity>
              </View>

        </View>
        <View style={{flex:1}}>
        <KeyboardAwareScrollView innerRef={() => {return [this.refs.email, this.refs.password]}} >


          <View style={styles.container1}>
            <Text style={styles.headerTitle}>OTP Verification</Text>
            <View style={styles.headerBorder}/>
            <Text style={styles.headersubTitle}>We have sent the confirmation code to your email.</Text>
            <Text style={styles.headersubTitle}>Please check your inbox.</Text>
          </View>

          <View style={styles.container2}>

            <View style={styles.SectionStyle}>
                <Image source={password_icon} style={styles.ImageStyle} />
                <TextInput
                  autoCapitalize={'none'}
                  autoCorrect={false}
                  underlineColorAndroid="transparent"
                  returnKeyType={ "next"}
                  selectionColor={"#FFFFFF"}
                  autoFocus={ false}
                  placeholder="Enter Verification Code"
                  placeholderTextColor="#fff"
                  style={styles.textInput}
                  ref="verification_Code"
                  keyboardType={ 'default'}
                  onChangeText={verification_Code=> this.setState({verification_Code})}
                />
            </View>
            { !(this.state.verifyerr) ? null :
              <Text style={styles.error}>{this.state.verifyerr}</Text>
            }

            <TouchableOpacity onPress={this.submit_click} style={styles.btncon}>
              <CommonButton label='Submit' width='100%'/>
            </TouchableOpacity>

          </View>
          </KeyboardAwareScrollView>
            </View>

          <View style={styles.container3}>
            <TouchableOpacity onPress={this.signup_click}>
              <Text style={styles.footertext}>Dont have an account?
              <Text style={{fontWeight:'bold'}}> Sign Up </Text></Text>
            </TouchableOpacity>
          </View>

        </ImageBackground>
        <Spinner visible={this.state.isVisible}  />

     </View>

    );
  }
}

const styles = StyleSheet.create({
  wrapper: {
    flex: 1,
    backgroundColor:'#ffffff'
  },
  statusBar: {
    height: AppSizes.statusBarHeight,
  },

  appBar: {
      height: AppSizes.navbarHeight,
      justifyContent:'flex-end',
      alignItems:'flex-start',
      paddingLeft:AppSizes.ResponsiveSize.Padding(5),
    },
    imageContainer:{
      width:'20%'
    },
    menuWrapper: {
      width:'60%',
      height:'60%',
    },
   image: {
      flex: 1,
      width: undefined,
      height: undefined,
      resizeMode:'contain'
    },

  container1: {
    flex: 3,
    height:AppSizes.screen.width/4,
    paddingLeft:AppSizes.ResponsiveSize.Padding(5),
    //backgroundColor:'red'
  },

  container2: {
    flex: 10,
    height:AppSizes.screen.width+20,
    flexDirection: 'column',
    alignItems: 'center',//replace with flex-end or center
    justifyContent: 'center',
    //backgroundColor:'blue'
  },
  container3: {
    flex: .1,
    //height:AppSizes.screen.width/5,
    alignItems: 'center',//replace with flex-end or center
    justifyContent: 'center',
    //backgroundColor:'red'
  },
  headerTitle:{
    fontSize:AppSizes.ResponsiveSize.Sizes(25),
    color:'#ffffff',
    fontWeight:'bold',
    paddingBottom:AppSizes.ResponsiveSize.Padding(1),
    fontFamily:Fonts.RobotoBold,
  },
  headersubTitle:{
    fontSize:AppSizes.ResponsiveSize.Sizes(14),
    color:'#ffffff',
    fontWeight:'600',
    fontFamily:Fonts.RobotoBold,

    paddingTop:AppSizes.ResponsiveSize.Padding(2),
  },
  headerBorder :{
    borderBottomColor: '#ffffff',
    borderBottomWidth: 3,
    width:'10%',
 },
 footertext :{
   color :'#ffffff',
   fontSize:AppSizes.ResponsiveSize.Sizes(12),
   fontWeight:'400',
   fontFamily:Fonts.RobotoRegular,

 },
 SectionStyle: {
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    borderBottomWidth: 1,
    borderBottomColor: '#ffffff',
    borderRadius: 5 ,
    marginLeft: AppSizes.ResponsiveSize.Padding(10),
    marginRight: AppSizes.ResponsiveSize.Padding(10),
    marginBottom: AppSizes.ResponsiveSize.Padding(3),
    height:40,
},

ImageStyle: {
    height: AppSizes.screen.width/21,
    width: AppSizes.screen.width/21,
    resizeMode : 'contain',
},
textInput: {
  flex:1,
  marginLeft: AppSizes.ResponsiveSize.Padding(2),
  fontSize: AppSizes.ResponsiveSize.Sizes(12),
  color:'#ffffff',
  fontFamily:Fonts.RobotoRegular,

  },
  btncon:{
    marginLeft: AppSizes.ResponsiveSize.Padding(10),
    marginRight: AppSizes.ResponsiveSize.Padding(10),
    marginTop: AppSizes.ResponsiveSize.Padding(5),
  },
  error:{
    color:'red',
    fontFamily:Fonts.RobotoRegular,

  }
});
