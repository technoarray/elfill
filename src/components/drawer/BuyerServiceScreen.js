import React, { Component } from 'react';
import {StyleSheet,Text,View,Image,TouchableOpacity,Platform,Alert,ScrollView,TextInput} from 'react-native';
import { AppStyles, AppSizes, AppColors } from '../../themes/'
import { NavigationActions } from 'react-navigation'
import Spacer from '../common/Spacer'
import * as commonFunctions from '../../utils/CommonFunctions'
import Header from '../common/HeaderWithBack'
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view'
import CommonButton from '../common/CommonButton'
import { Fonts } from '../../utils/Fonts';
var Constant = require('../../api/ApiRules').Constant;

/* Images */
const staion = require('../../themes/Images/station.png')
/*const left_arrow = require('../../themes/Images/left-arrow.png')
const profile_icon = require('../../themes/Images/profile.png')
const email_icon = require('../../themes/Images/email.png')
const password_icon = require('../../themes/Images/password.png')
const device_icon = require('../../themes/Images/car.png')
const modal_icon = require('../../themes/Images/modal.png')
const mobile_icon = require('../../themes/Images/mobile.png*/

export default class BuyerServiceScreen extends Component {
  constructor (props) {
      super(props)
      this.state = {
      isVisible: false,
    },
    _that = this;
  }


  render() {
    const headerProp = {
      title: 'Service Detail',
      screens: '',
      type:''
    };
    return (
    <View style={styles.container}>
      <Header info={headerProp} navigation={_that.props.navigation}/>
        <View style={styles.content}>
          <ScrollView>
            <View style={[styles.container1,styles.shadow]}>
                <View style={styles.containerimage}>
                    <Image source={staion} style={styles.ImageStyle} />
                </View>
                <View style={styles.containercontent}>
                      <Text style={styles.title}>Midtown East, Manhattan, NY</Text>
                      <View style={styles.border} />
                      <Text style={styles.contentbox}>
                          Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s
                      </Text>
                      <View style={styles.tagbox}>
                          <View style={styles.tag}>
                              <Text style={styles.tagtext}>
                                  Location:-Panchkula,Haryana
                              </Text>
                          </View>
                          <View style={styles.tag}>
                              <Text style={styles.tagtext}>
                                  Country:-India
                              </Text>
                          </View>
                          <View style={styles.tag}>
                              <Text style={styles.tagtext}>
                                  Zip Code:-134205
                              </Text>
                          </View>
                          <View style={styles.tag}>
                              <Text style={styles.tagtext}>
                                  State:-Haryana
                              </Text>
                          </View>

                      </View>
                      <View style={styles.tagbtn}>
                          <View style={styles.boxbtn}>
                            <View style={{ width: this.props.width}}>
                                <Text style={styles.boxtextbtn}>Edit</Text>
                            </View>
                          </View>
                          <View style={styles.boxbtn}>
                            <View style={{ width: this.props.width}}>
                                <Text style={styles.boxtextbtn}>Book</Text>
                            </View>
                          </View>
                      </View>

                </View>
            </View>
          </ScrollView>
        </View>
    </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    paddingTop:  0,
    backgroundColor:'#fff'
  },

  content: {
      flex: 1,
      flexDirection: 'column',
      height:'100%',
      padding:'5%',
      //backgroundColor:'#000',
      paddingBottom:0
  },
  container1: {
    //flex: 1,
    //height:AppSizes.screen.width/4,
    padding:AppSizes.ResponsiveSize.Padding(3),
    backgroundColor:'red',
  },
  containerimage:{
    //backgroundColor:'green',
    //flex:2
    height:AppSizes.screen.height/2.8,
    paddingTop:AppSizes.ResponsiveSize.Padding(4),
  },

  ImageStyle: {
  height:'100%',
  width:'100%',
  },
containercontent:{
  //backgroundColor:'green',
  //height:AppSizes.screen.height/2,
  paddingTop:AppSizes.ResponsiveSize.Padding(2)
},
title:{
  fontSize:AppSizes.ResponsiveSize.Sizes(16),
  color:'#333',
  fontWeight:'600',
  fontFamily:Fonts.RobotoBold,
},
border:{
  width:30,
  height:2,
  backgroundColor:'#333',
  marginTop:AppSizes.ResponsiveSize.Padding(1),
  marginBottom:AppSizes.ResponsiveSize.Padding(1)
},
contentbox:{
  fontSize:AppSizes.ResponsiveSize.Sizes(12),
  color:'#333',
  fontWeight:'400',
  lineHeight: AppSizes.ResponsiveSize.Sizes(10 * 1.80),
fontFamily:Fonts.RobotoRegular,
},
tagbox:{
  flexDirection:'row',
  flexWrap:'wrap',
  paddingTop:AppSizes.ResponsiveSize.Padding(3),


},

tag:{
  backgroundColor:'#eee',
  padding:AppSizes.ResponsiveSize.Padding(1.5),
  paddingTop:AppSizes.ResponsiveSize.Padding(.50),
  paddingBottom:AppSizes.ResponsiveSize.Padding(.50),
  borderRadius:6,
  marginRight:AppSizes.ResponsiveSize.Padding(1),
  marginBottom:AppSizes.ResponsiveSize.Padding(1)
},
tagtext:{
  fontSize:AppSizes.ResponsiveSize.Sizes(10),
  color:'#000',
  fontFamily:Fonts.RobotoRegular,
},
shadow:{
    borderWidth:1,
    borderRadius: 2,
    borderColor: '#fff',
    justifyContent:'center',
    backgroundColor:'#fff',
    borderColor: '#ddd',
    borderBottomWidth: 0,
    shadowColor: '#999',
    shadowOffset: { width: 0, height: 2 },
    shadowOpacity: 0.8,
    shadowRadius: 2,
    elevation: 0,
    zIndex:0,
    //backgroundColor:'red',
    //paddingTop:AppSizes.ResponsiveSize.Padding(4),

 },
tagbtn:{
  flexDirection:'row',
  flexWrap:'wrap',
  paddingTop:AppSizes.ResponsiveSize.Padding(1),
  //backgroundColor:'green'
},
 boxtextbtn:{
   color:'#ffffff',
   fontSize:AppSizes.ResponsiveSize.Sizes(12),
   fontWeight:'400',
   fontFamily:Fonts.RobotoRegular,
   textAlign:'center'
 },
 boxbtn:{
   backgroundColor:'#452959',
   padding:AppSizes.ResponsiveSize.Padding(1.5),
   paddingTop:AppSizes.ResponsiveSize.Padding(1),
   paddingBottom:AppSizes.ResponsiveSize.Padding(1),
   borderRadius:6,
   marginRight:AppSizes.ResponsiveSize.Padding(1),
   marginBottom:AppSizes.ResponsiveSize.Padding(1),
   width:'45%'
 }
});
