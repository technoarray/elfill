import React, { Component } from 'react';
import {AsyncStorage,StyleSheet,Text,View,Image,PixelRatio,TouchableOpacity,Platform,Alert,ScrollView,TextInput} from 'react-native';
import { AppStyles, AppSizes, AppColors } from '../../themes/'
import { NavigationActions } from 'react-navigation'
import Spinner from 'react-native-loading-spinner-overlay';
import * as commonFunctions from '../../utils/CommonFunctions'
import Header from '../common/HeaderWithBack'
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view'
import CommonButton from '../common/CommonButton'
import ImagePicker from 'react-native-image-picker';
import Picker from 'react-native-q-picker';
import {RadioGroup, RadioButton} from 'react-native-flexi-radio-button';
import { pickerData } from '../data/pickerData';
import CheckBox from 'react-native-check-box';
import { Fonts } from '../../utils/Fonts';
var Constant = require('../../api/ApiRules').Constant;
var WebServices = require('../../api/ApiRules').WebServices;

/* Images */
const bg_image = require('../../themes/Images/bg_image.png')
const left_arrow = require('../../themes/Images/left-arrow.png')
const arrow_right = require('../../themes/Images/right_arrow.png')
const arrow_down = require('../../themes/Images/down_arrow.png')
const profile_icon = require('../../themes/Images/profile.png')
const email_icon = require('../../themes/Images/email.png')
const password_icon = require('../../themes/Images/password.png')
const device_icon = require('../../themes/Images/car.png')
const modal_icon = require('../../themes/Images/modal.png')
const mobile_icon = require('../../themes/Images/mobile.png')

var numberchargeitems = [];

for( var i = 1; i <=20; i++){
  var numbers = {
      name: i+"",
      id: i
  };
  numberchargeitems.push(numbers);
}

let _that
export default class AddserviceScreen extends Component {
  constructor (props) {
      super(props)
      this.state = {
      isVisible: false,
      uid:'',
      status:'1',
      diviceType:[],
      station_id:this.props.navigation.state.params.station_id,
      cat_id:'2',
      subcat_id:'3',
      coffee:pickerData.coffee,
      selectedDeviceType:'',
      selecteddriver_licence_no:'',
      selectedupload_licence:'',
      selectedinsurance:'',
      selecteddescription:'',
      avatarSource:null,
      avatarerr:'',
      deviceTypeError:'',
      descriptionError:'',
      rentError:'',
      car_charge:false,
      car_park:false,
      car_bed:false,
      car_cable:false,
      car_rent:false,
      bike_charge:false,
      bike_park:false,
      bike_bed:false,
      bike_cable:false,
      bike_rent:false,
      gadget_charge:false,
      gadget_bed:false,
      gadget_cable:false,
      gadget_rent:false,
      device_listing:false,
      label:'Select Device'
    },
    _that = this;
  }

  componentWillMount(){
    AsyncStorage.getItem('UserData').then((UserData) => {
      var data = JSON.parse(UserData);
        uid=data.id
        if(uid != ''){
          _that.setState({
            uid:uid,
          })
        }
        this.validationAndApiParameter('device_list')
    })
  }

  onSelect(index, value){
    this.setState({status: value})
  }

  selectPhotoTapped() {
    const options = {
      quality: 1.0,
      maxWidth: 500,
      maxHeight: 500,
      storageOptions: {
        skipBackup: true,
      },
    };
      ImagePicker.showImagePicker(options, (response) => {
      console.log('Response = ', response);

      if (response.didCancel) {
        console.log('User cancelled image picker');
      } else if (response.error) {
        console.log('ImagePicker Error: ', response.error);
      } else if (response.customButton) {
        console.log('User tapped custom button: ', response.customButton);
      } else {
        const source = { uri: response.uri };

        this.setState({
          avatarSource: source,
          avatarerr:''
        });
      }
    });
   }

  validationAndApiParameter(apikey,val) {
    const { cat_id,subcat_id,station_id,selecteddescription,avatarSource,status,selectedDeviceType,reantperday,selecteddriver_licence_no,selectedupload_licence,selectedinsurance} = this.state
    var error=0
    if(apikey=='device_list'){
      this.setState({device_listing:false,diviceType:[],label:'Select Device'})
      const data = new FormData();
      data.append('subcat_id',subcat_id);
      data.append('other_catid',0)

      console.log(data);
      _that.setState({isVisible: true});
      _that.postToApiCalling('POST',apikey, Constant.URL_devicesList,data);
    }
    if (apikey== 'saveService') {
      if (avatarSource===null) {
       console.log('error');
        _that.setState({avatarerr:'Please select photo'})
       error=1;
      }
       if(selectedDeviceType==''){
         console.log('error');
         _that.setState({deviceTypeError:'Please select device type'})
         error=1;
       }
       if(reantperday==''){
         console.log('error');
         _that.setState({rentError:'Please enter an amount'})
         error=1;
       }
       if(selecteddescription==''){
         console.log('error');
        _that.setState({descriptionError:'Please enter an amount'})
        error=1;
       }
       if(error==0){
        const data = new FormData();
        data.append('uid', this.state.uid);
        data.append('station_id',station_id);
        data.append('cat_id',cat_id);
        data.append('subcat_id',subcat_id);
        data.append('other_cat_id',0)
        data.append('device_id',selectedDeviceType)
        data.append('reantperday',reantperday);
        data.append('driver_licence_no',selecteddriver_licence_no)
        data.append('upload_driver_licence',selectedupload_licence)
        data.append('insurance',selectedinsurance)
        data.append('description', selecteddescription);
        data.append('service_image', {
          uri:  avatarSource.uri,
          type: 'image/jpeg',
          name: 'serviceImage.jpg'
        });
        data.append('service_status', status);

        console.log('Data:',data);
        _that.setState({isVisible: true});
        this.postToApiCalling('POST', 'saveService', Constant.URL_serviceAdd, data);
       }
     }
   }

  postToApiCalling(method, apiKey, apiUrl, data) {
      //console.log(data);
      new Promise(function(resolve, reject) {
        if (method == 'POST') {
          resolve(WebServices.callWebService(apiUrl, data));
        } else {
          resolve(WebServices.callWebService_GET(apiUrl, data));
        }
      }).then((jsonRes) => {
        console.log(jsonRes);
        _that.setState({ isVisible: false })
        if ((!jsonRes) || (jsonRes.code == 0)) {
            setTimeout(()=>{
              Alert.alert('Login Error',jsonRes.message);
            },200);
        }
        else{
          if(jsonRes.code == 1){
            _that.apiSuccessfullResponse(apiKey, jsonRes)
          }
        }
      }).catch((error) => {
          console.log("ERROR" + error);
          _that.setState({ isVisible: false })
          setTimeout(()=>{
              Alert.alert("Server issue"+error);
          },200);
      });
  }

  apiSuccessfullResponse(apiKey, jsonRes) {
      if(apiKey=='saveService'){
        console.log(jsonRes);
        this.handleOnPress()
      }
      if(apiKey=='device_list'){
        console.log(jsonRes);
        var deviceData=jsonRes.result
        console.log(deviceData.length);
        if(deviceData.length>0){
          this.setState({device_listing:true})
        }
        deviceData.map(ddata=> this.state.diviceType.push({name:ddata.name,id:parseInt(ddata.id)}));
      }
  }

  handleOnPress(){
    var selected=this.props.navigation.state.params.selected
    if(selected.indexOf('el_car_charge')!=-1){
      selected.splice(selected.indexOf('el_car_charge'), 1);
      _that.props.navigation.navigate('El_car_charge',{selected:selected,station_id:this.state.station_id})
    }
    else if (selected.indexOf('el_car_park')!=-1) {
      selected.splice(selected.indexOf('el_car_park'), 1);
      _that.props.navigation.navigate('El_car_park',{selected:selected,station_id:this.state.station_id})
    }
    else if (selected.indexOf('el_car_bed')!=-1) {
      selected.splice(selected.indexOf('el_car_bed'), 1);
      _that.props.navigation.navigate('El_car_bed',{selected:selected,station_id:this.state.station_id})
    }
    else if (selected.indexOf('el_car_cable')!=-1) {
      selected.splice(selected.indexOf('el_car_cable'), 1);
      _that.props.navigation.navigate('El_car_cable',{selected:selected,station_id:this.state.station_id})
    }
    else if (selected.indexOf('el_car_rent')!=-1) {
      selected.splice(selected.indexOf('el_car_rent'), 1);
      _that.props.navigation.navigate('El_car_rent',{selected:selected,station_id:this.state.station_id})
    }
    else if (selected.indexOf('el_bike_charge')!=-1) {
      selected.splice(selected.indexOf('el_bike_charge'), 1);
      _that.props.navigation.navigate('El_bike_charge',{selected:selected,station_id:this.state.station_id})
    }
    else if (selected.indexOf('el_bike_park')!=-1) {
      selected.splice(selected.indexOf('el_bike_park'), 1);
      _that.props.navigation.navigate('El_bike_park',{selected:selected,station_id:this.state.station_id})
    }
    else if (selected.indexOf('el_bike_bed')!=-1) {
      selected.splice(selected.indexOf('el_bike_bed'), 1);
      _that.props.navigation.navigate('El_bike_bed',{selected:selected,station_id:this.state.station_id})
    }
    else if (selected.indexOf('el_bike_cable')!=-1) {
      selected.splice(selected.indexOf('el_bike_cable'), 1);
      _that.props.navigation.navigate('El_bike_cable',{selected:selected,station_id:this.state.station_id})
    }
    else if (selected.indexOf('el_bike_rent')!=-1) {
      selected.splice(selected.indexOf('el_bike_rent'), 1);
      _that.props.navigation.navigate('El_bike_rent',{selected:selected,station_id:this.state.station_id})
    }
    else if (selected.indexOf('gadget_charge')!=-1) {
      selected.splice(selected.indexOf('gadget_charge'), 1);
      _that.props.navigation.navigate('Gadget_charge',{selected:selected,station_id:this.state.station_id})
    }
    else if (selected.indexOf('gadget_bed')!=-1) {
      selected.splice(selected.indexOf('gadget_bed'), 1);
      _that.props.navigation.navigate('Gadget_bed',{selected:selected,station_id:this.state.station_id})
    }
    else if (selected.indexOf('gadget_cable')!=-1) {
      selected.splice(selected.indexOf('gadget_cable'), 1);
      _that.props.navigation.navigate('Gadget_cable',{selected:selected,station_id:this.state.station_id})
    }
    else if (selected.indexOf('gadget_rent')!=-1) {
      selected.splice(selected.indexOf('gadget_rent'), 1);
      _that.props.navigation.navigate('Gadget_rent',{selected:selected,station_id:this.state.station_id})
    }
    else{
      _that.props.navigation.navigate('StartScreen')
    }
  }

  backMain(){
    _that.props.navigation.navigate('AddserviceScreen')
  }

  render() {
    const headerProp = {
      title: 'El-Car Rent',
      screens: 'Serviceslistseller',
      type:''
    };

    return (
      <View style={styles.container}>
        <Header info={headerProp} navigation={_that.props.navigation}/>

          <View style={styles.content}>
            <KeyboardAwareScrollView innerRef={() => {return [this.refs.name,this.refs.email, this.refs.password]}} >
              <ScrollView>
                <View style={styles.container2}>

                <View style={{alignItems:'center',marginBottom:AppSizes.ResponsiveSize.Padding(3)}}>
                  <TouchableOpacity onPress={this.selectPhotoTapped.bind(this)} style={{height:120,width:120}}>
                  <View style={[styles.avatar,  styles.avatarContainer,{ marginBottom: 20 },]}>
                    {this.state.avatarSource === null ? (
                      <Text>Select a Photo</Text>
                    ) : (
                      <Image style={styles.avatar} source={this.state.avatarSource} />
                    )}
                  </View>
                  </TouchableOpacity>
                </View>
                { !(this.state.avatarerr) ? null :
                  <Text style={styles.error}>{this.state.avatarerr}</Text>
                }

                {this.state.device_listing?
                  <View style={[styles.SectionStyle,styles.subsec]}>
                      <View style={styles.inputfield}>
                          <Text style={[styles.inputtext,styles.inputmar]}>Device Type</Text>
                          <Picker PickerData={this.state.diviceType}
                           label={this.state.label}
                           color={'#000000'}
                           getTxt={(val,label)=>{this.setState({selectedDeviceType: val});this.setState({deviceTypeError:'',label:''})}}/>
                      </View>
                      <Text style={styles.error}>{this.state.deviceTypeError}</Text>
                  </View>
                :
                  null
                }

                <View >
                  <View style={[styles.SectionStyle,styles.subsec]}>
                      <View style={styles.inputfield}>
                          <Text style={[styles.inputtext,styles.inputmar]}>Rent per day</Text>
                          <TextInput
                            autoCorrect={false}
                            underlineColorAndroid="transparent"
                            selectionColor={"#000000"}
                            autoFocus={ false}
                            ref="reantperday"
                            placeholder="Enter parking fee per lot"
                            placeholderTextColor="#808080"
                            style={styles.textInput}
                            keyboardType={ 'number-pad'}
                            onChangeText={reantperday => this.setState({reantperday})}
                          />
                      </View>
                  </View>
                  {this.state.rentError==''?null:<Text style={styles.error}>{this.state.rentError}</Text>}
                  <View style={[styles.SectionStyle,styles.subsec]}>
                      <View style={styles.inputfield}>
                          <Text style={[styles.inputtext,styles.inputmar]}>Drivers Licence Number</Text>
                          <Picker PickerData={this.state.coffee}
                           label={'Please select '}
                           color={'#000000'}
                           getTxt={(val,label) => this.setState({selecteddriver_licence_no:label})}/>
                      </View>
                  </View>
                  <View style={[styles.SectionStyle,styles.subsec]}>
                      <View style={styles.inputfield}>
                          <Text style={[styles.inputtext,styles.inputmar]}>Upload copy of Drivers Licence</Text>
                          <Picker PickerData={this.state.coffee}
                           label={'Please select '}
                           color={'#000000'}
                           getTxt={(val,label) => this.setState({selectedupload_licence:label})}/>
                      </View>
                  </View>
                  <View style={[styles.SectionStyle,styles.subsec]}>
                      <View style={styles.inputfield}>
                          <Text style={[styles.inputtext,styles.inputmar]}>Insurance/Deposit</Text>
                          <TextInput
                            autoCorrect={false}
                            underlineColorAndroid="transparent"
                            selectionColor={"#000000"}
                            autoFocus={ false}
                            ref="selectedinsurance"
                            placeholder="Amount"
                            placeholderTextColor="#808080"
                            style={styles.textInput}
                            keyboardType={ 'number-pad'}
                            onChangeText={selectedinsurance => this.setState({selectedinsurance})}
                          />
                      </View>
                  </View>
                </View>

                  <View style={styles.SectionStyle}>
                      <View style={styles.inputfield}>
                          <Text style={styles.inputtext}>Description</Text>
                          <TextInput
                            autoCapitalize={'none'}
                            autoCorrect={false}
                            value={this.state.description}
                            multiline={true}
                            numberOfLines={4}
                            underlineColorAndroid="transparent"
                            returnKeyType={ "next"}
                            selectionColor={"#000000"}
                            autoFocus={ false}
                            placeholder="Description"
                            placeholderTextColor="#808080"
                            style={[styles.textInput,{height:80}]}
                            ref="selecteddescription"
                            keyboardType={ 'default'}
                            onFocus={ () => this.setState({descriptionError:''}) }
                            onChangeText={selecteddescription=> this.setState({selecteddescription})}
                          />
                      </View>
                      {this.state.descriptionError==''?null:<Text style={styles.error}>{this.state.descriptionError}</Text>}
                  </View>
                  <View style={styles.SectionStyle}>
                    <View style={styles.inputfield}>
                      <Text style={styles.inputtext}>Status</Text>
                        <RadioGroup
                          color='#000000'
                          selectedIndex={1}
                          style={styles.SectionRadioGroup}
                          onSelect = {(index, value) => this.onSelect(index, value)}
                        >
                          <RadioButton value={'0'}>
                            <Text style={{color:'#000000'}}>InActive</Text>
                          </RadioButton>

                          <RadioButton value={'1'}>
                            <Text style={{color:'#000000'}}>Active</Text>
                          </RadioButton>
                        </RadioGroup>
                      </View>
                  </View>
                </View>
              </ScrollView>
            </KeyboardAwareScrollView>
            <View style={[styles.SectionStyle,{marginBottom:10,marginLeft:10,marginTop:15,alignItems:'center'}]}>
              <View style={styles.textSubHead}>
                <CheckBox
                  style={{width:'100%'}}
                  onClick={()=>{this.setState({confirm:!this.state.confirm})}}
                  isChecked={this.state.confirm}
                  rightText={"Confirm Submition"}
                  checkedCheckBoxColor={'#463057'}
                  uncheckedCheckBoxColor={'#463057'}
                />
              </View>
            </View>
            <View style={styles.bottombtn}>
              {this.state.confirm?
                <View style={styles.bottombtn}>
                  <TouchableOpacity onPress={()=>this.backMain()} style={[styles.SectionStyle,{marginTop:AppSizes.ResponsiveSize.Padding(3),width:'50%',borderRightWidth:1,borderColor:'#fff'}]}>
                    <CommonButton label='Back to main' width='50%'/>
                  </TouchableOpacity>
                  <TouchableOpacity onPress={()=>this.validationAndApiParameter('saveService')} style={[styles.SectionStyle,{marginTop:AppSizes.ResponsiveSize.Padding(3),width:'50%',opacity:1}]}>
                    <CommonButton label='Next' width='50%'/>
                  </TouchableOpacity>
                </View>
              :
                <TouchableOpacity onPress={()=>this.backMain()} style={[styles.SectionStyle,{marginTop:AppSizes.ResponsiveSize.Padding(3),width:'100%'}]}>
                  <CommonButton label='Back to main' width='50%'/>
                </TouchableOpacity>
              }
            </View>
          </View>

        <Spinner visible={this.state.isVisible}  />
      </View>
    );
  }
}


const styles = StyleSheet.create({
  container: {
    flex: 1,
    paddingTop:  0,
  },
  content: {
      flex: 1,
      flexDirection: 'column',
      height:'70%',
      backgroundColor:'#ffffff'
  },
  container1: {
    flex: 3,
    height:AppSizes.screen.width/4,
    paddingLeft:AppSizes.ResponsiveSize.Padding(5),
    //backgroundColor:'red'
  },
  container2: {
    flex: 10,
    height:'70%',
    paddingLeft:'4%',
    paddingRight:'4%',
    paddingTop:'4%',
    flexDirection: 'column',
    //backgroundColor:'blue'
  },
  container3: {
    flex: 3,
    height:AppSizes.screen.width/8,
    alignItems: 'center',//replace with flex-end or center
    justifyContent: 'flex-end',
  },
  avatarContainer: {
     borderColor: '#9B9B9B',
     borderWidth: 1 / PixelRatio.get(),
     justifyContent: 'center',
     alignItems: 'center',
  },
  avatar: {
   borderRadius: 75,
   width: 120,
   height: 120,
  },
  headerTitle:{
   fontSize:AppSizes.ResponsiveSize.Sizes(25),
   color:'#000000',
   fontWeight:'bold',
   fontFamily:Fonts.RobotoBold,
   paddingBottom:AppSizes.ResponsiveSize.Padding(1),
  },
  headersubTitle:{
   fontSize:AppSizes.ResponsiveSize.Sizes(14),
   color:'#000000',
   fontWeight:'600',
   fontFamily:Fonts.RobotoBold,
   paddingTop:AppSizes.ResponsiveSize.Padding(2),
  },
  headerBorder :{
   borderBottomColor: '#000000',
   borderBottomWidth: 3,
   width:'10%',
  },
  footertext :{
   color :'#000000',
   fontSize:AppSizes.ResponsiveSize.Sizes(12),
   fontWeight:'400',
   fontFamily:Fonts.RobotoRegular,
  },
  SectionStyle: {
   width:AppSizes.ResponsiveSize.width,
  },
  ImageStyle: {
   height: AppSizes.screen.width/21,
   width: AppSizes.screen.width/21,
   resizeMode : 'contain',
  },
  textInput: {
   flex:1,
   paddingLeft:AppSizes.ResponsiveSize.Padding(3),
   fontSize: AppSizes.ResponsiveSize.Sizes(12),
   color:'#000000',
   backgroundColor:'#eee',
   height:40,
   fontFamily:Fonts.RobotoRegular,
  },
  inputfield:{
   width:AppSizes.ResponsiveSize.width,
  },
  inputtext:{marginBottom:10,
   fontSize:AppSizes.ResponsiveSize.Sizes(14),
   fontWeight:'600',
   fontFamily:Fonts.RobotoBold,
  },
  error:{
   color:'red',
   fontSize:AppSizes.ResponsiveSize.Sizes(12),
   fontFamily:Fonts.RobotoRegular,
  },
  SectionRadioGroup:{
    flexDirection:'row',
  },
  facilityView:{
    alignItems:'center',
    margin:AppSizes.ResponsiveSize.Padding(2)
  },
  facilityText:{
    fontSize:25,
    color:'#000',
    fontFamily:Fonts.RobotoRegular,
  },
  checkboxSection :{
    marginBottom: AppSizes.ResponsiveSize.Padding(2),
    height:AppSizes.screen.width/15,
    width:AppSizes.screen.width/3
    //backgroundColor:'red',
  },
  SectionRadioView: {
    borderBottomWidth: 1,
    borderBottomColor: '#ffffff',
    borderRadius: 5 ,
    width:AppSizes.screen.width*90/100,
    marginLeft: AppSizes.ResponsiveSize.Padding(5),
    marginRight: AppSizes.ResponsiveSize.Padding(5),
    marginBottom: (Platform.OS === 'ios') ? AppSizes.ResponsiveSize.Padding(1) :AppSizes.ResponsiveSize.Padding(2),

  },
  textHead:{
    width:'100%',
    flexDirection:'row',
    justifyContent:'space-between',
    backgroundColor:'#b7b7b7',
    padding:10,
  },
  textSubHead:{
    width:'100%',
    flexDirection:'row',
    justifyContent:'space-between',
  },
  subsec:{
    //backgroundColor:'red',
    paddingTop:AppSizes.ResponsiveSize.Padding(5),

},
inputmar:{
  marginBottom:15
},
bottombtn:{
  flexDirection:'row'
}
});
