import React, { Component } from 'react';
import {AsyncStorage,StyleSheet,Text,View,Image,PixelRatio,TouchableOpacity,Platform,Alert,ScrollView,TextInput} from 'react-native';
import { AppStyles, AppSizes, AppColors } from '../../themes/'
import { NavigationActions } from 'react-navigation'
import Spacer from '../common/Spacer'
import Spinner from 'react-native-loading-spinner-overlay';
import * as commonFunctions from '../../utils/CommonFunctions'
import Header from '../common/HeaderWithBack'
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view'
import CommonButton from '../common/CommonButton'
import Geocoder from 'react-native-geocoder';
import {RadioGroup, RadioButton} from 'react-native-flexi-radio-button'
import ImagePicker from 'react-native-image-picker';
import { Fonts } from '../../utils/Fonts';
var Constant = require('../../api/ApiRules').Constant;
var WebServices = require('../../api/ApiRules').WebServices;

/* Images */
const bg_image = require('../../themes/Images/bg_image.png')
const left_arrow = require('../../themes/Images/left-arrow.png')
const profile_icon = require('../../themes/Images/profile.png')
const email_icon = require('../../themes/Images/email.png')
const password_icon = require('../../themes/Images/password.png')
const device_icon = require('../../themes/Images/car.png')
const modal_icon = require('../../themes/Images/modal.png')
const mobile_icon = require('../../themes/Images/mobile.png')

let _that
export default class EditStationScreen extends Component {
  constructor (props) {
      super(props)
      this.state = {
        isVisible: false,
        item:[],
        uid:'',
        station_name:'',
        station_id:'',
        location:'',
        state:'',
        city:'',
        country:'',
        zip:'',
        description:'',
        stationerr:'',
        locationerr:'',
        stateerr:'',
        countryerr:'',
        ziperr:'',
        cityerr:'',
        descriptionerr:'',
        status:1,
        avatarSource:null,
        imageSource:null,
        avatarerr:''
    },
    _that = this;
  }

  componentWillMount(){
    AsyncStorage.getItem('UserData').then((UserData) => {
      var data = JSON.parse(UserData);
        uid=data.id
        if(uid != ''){
          _that.setState({
            uid:uid,
          })
        }
    })

    var item=this.props.navigation.state.params.item
      _that.setState({
        station_id:item.id,
        imageSource:item.service_station_image,
        station_name:item.station_name,
        location:item.location,
        state:item.state,
        country:item.country,
        zip:item.zip,
        city:item.city,
        description:item.description,
        status:item.status,
      })
  }

  onSelect(index, value){
    this.setState({status: value})
  }

  selectPhotoTapped() {
    const options = {
      quality: 1.0,
      maxWidth: 500,
      maxHeight: 500,
      storageOptions: {
        skipBackup: true,
      },
    };
      ImagePicker.showImagePicker(options, (response) => {
      console.log('Response = ', response);

      if (response.didCancel) {
        console.log('User cancelled image picker');
      } else if (response.error) {
        console.log('ImagePicker Error: ', response.error);
      } else if (response.customButton) {
        console.log('User tapped custom button: ', response.customButton);
      } else {
        const source = { uri: response.uri };

        this.setState({
          avatarSource: response.uri,
          avatarerr:''
        });
      }
    });
   }

  submit_click(){
    _that.validationAndApiParameter()
  }

  validationAndApiParameter() {
        const { method,imageSource,avatarSource,uid,station_name, location, state,city, country,zip,description,station_id,status,isVisible } = this.state

        var error=0;

        if (station_name.length <= 0) {
          _that.setState({stationerr:'Please enter service station name'})
          error=1;
        }
        else{
            _that.setState({stationerr:''})
        }
        if (location.length <= 0) {
          _that.setState({locationerr:'Please enter location'})
          error=1;
        }
        else{
            _that.setState({locationerr:''})
        }
        if (state.length <= 0) {
          _that.setState({stateerr:'Please enter state'})
          error=1;
        }
        else{
            _that.setState({stateerr:''})
        }
        if (country.length <= 0) {
          _that.setState({countryerr:'Please enter country'})
          error=1;
        }
        else{
            _that.setState({countryerr:''})
        }
        if (zip.length <= 0) {
          _that.setState({ziperr:'Please enter zipcode'})
          error=1;
        }
        else{
            _that.setState({ziperr:''})
        }
        if (city.length <= 0) {
          _that.setState({cityerr:'Please enter city'})
          error=1;
        }
        else{
            _that.setState({cityerr:''})
        }
        if (description.length <= 0) {
          _that.setState({descriptionerr:'Please enter description'})
          error=1;
        }
        else{
            _that.setState({descriptionerr:''})
        }
        if (imageSource===null) {
          _that.setState({avatarerr:'Please select photo'})
          error=1;
        }
        else{
            _that.setState({avatarerr:''})
        }


        if(error==0){
          var latitude=longitude='';
          var address=location+','+city+','+state+','+country+' '+zip;
          //console.log(address);
          Geocoder.fallbackToGoogle(Constant.Google_apiKey);

            Geocoder.geocodeAddress(address).then(res => {
              //console.log(res)
              var position=res[0].position
               latitude= position.lat;
               longitude= position.lng;

               const data = new FormData();
                data.append('uid', uid);
                data.append('id', station_id);
                data.append('station_name', station_name);
                data.append('location', location);
                data.append('state', state);
                data.append('city', city);
                data.append('country', country);
                data.append('zip', zip);
                data.append('description', description);
                data.append('latitude', latitude);
                data.append('longitude', longitude);
                data.append('service_station_image', '');
                data.append('station_status', status);
                if(avatarSource!== null){
                  data.append('station_image', {
                    uri:  avatarSource,
                    type: 'image/jpeg', // or photo.type
                    name: 'stationImage.jpg'
                  });
                }
                else{
                  data.append('station_image', imageSource);
                }
                console.log(data);

                _that.setState({isVisible: true});
                this.postToApiCalling('POST', 'station_edit', Constant.URL_serviceStationEdit, data);
              })

        }
      }

      postToApiCalling(method, apiKey, apiUrl, data) {

         new Promise(function(resolve, reject) {
              if (method == 'POST') {
                  resolve(WebServices.callWebService(apiUrl, data));
              } else {
                  resolve(WebServices.callWebService_GET(apiUrl, data));
              }
          }).then((jsonRes) => {
            _that.setState({ isVisible: false })

              if ((!jsonRes) || (jsonRes.code == 0)) {

              setTimeout(()=>{
                  Alert.alert('Error',jsonRes.message);
              },200);

              } else {
                  if (jsonRes.code == 1) {
                      _that.apiSuccessfullResponse(apiKey, jsonRes)
                  }
              }
          }).catch((error) => {
              console.log("ERROR" + error);
              _that.setState({ isVisible: false })

              setTimeout(()=>{
                  Alert.alert("Server issue");
              },200);
          });
      }

      apiSuccessfullResponse(apiKey, jsonRes) {
          if (apiKey == 'station_edit') {
            stationlist=jsonRes.result
            _that.props.navigation.navigate('Stationlistseller',{stationlist:stationlist});
          }
      }


  render() {
    const headerProp = {
      title: 'Edit Service Station',
      screens: 'Stationlistseller',
      type:''
    };
    var image='';
    if(this.state.avatarSource !== null){
      image=this.state.avatarSource;
    }
    else if(this.state.imageSource !== null){
      image=Constant.image_path+this.state.imageSource;
    }

    return (
      <View style={styles.container}>
      <Header info={headerProp} navigation={_that.props.navigation}/>

        <View style={styles.content}>
        <KeyboardAwareScrollView innerRef={() => {return [this.refs.name,this.refs.email, this.refs.password]}} >
            <ScrollView>
          <View style={styles.container2}>

            <TouchableOpacity onPress={this.selectPhotoTapped.bind(this)} style={{alignItems:'center'}}>
            <View style={[styles.avatar,  styles.avatarContainer,{ marginBottom: 20 },]}>
              {this.state.imageSource === null ? (
                <Text>Select a Photo</Text>
              ) : (
                <Image style={styles.avatar} source={{uri: image}} />
              )}
            </View>
            </TouchableOpacity>
            { !(this.state.avatarerr) ? null :
              <Text style={styles.error}>{this.state.avatarerr}</Text>
            }

            <View style={styles.SectionStyle}>
                <View style={styles.inputfield}>
                    <Text style={styles.inputtext}>Service Station Name</Text>
                    <TextInput
                      autoCapitalize={'none'}
                      autoCorrect={false}
                      underlineColorAndroid="transparent"
                      returnKeyType={ "next"}
                      value={this.state.station_name}
                      selectionColor={"#000000"}
                      autoFocus={ false}
                      placeholder="Service Station Name"
                      placeholderTextColor="#808080"
                      style={styles.textInput}
                      ref="station_name"
                      keyboardType={ 'email-address'}
                      onChangeText={station_name=> this.setState({station_name})}
                    />
                </View>
                { !(this.state.stationerr) ? null :
                  <Text style={styles.error}>{this.state.stationerr}</Text>
                }

            </View>

            <View style={styles.SectionStyle}>
                <View style={styles.inputfield}>
                    <Text style={styles.inputtext}>Location</Text>
                    <TextInput
                      autoCapitalize={'none'}
                      autoCorrect={false}
                      value={this.state.location}
                      underlineColorAndroid="transparent"
                      returnKeyType={ "next"}
                      selectionColor={"#000000"}
                      autoFocus={ false}
                      placeholder="Location"
                      placeholderTextColor="#808080"
                      style={styles.textInput}
                      ref="location"
                      keyboardType={ 'email-address'}
                      onChangeText={location=> this.setState({location})}
                    />
                </View>
                { !(this.state.locationerr) ? null :
                  <Text style={styles.error}>{this.state.locationerr}</Text>
                }
            </View>
            <View style={styles.SectionStyle}>
                <View style={styles.inputfield}>
                    <Text style={styles.inputtext}>City</Text>
                    <TextInput
                      autoCapitalize={'none'}
                      autoCorrect={false}
                      value={this.state.city}
                      underlineColorAndroid="transparent"
                      returnKeyType={ "next"}
                      selectionColor={"#000000"}
                      autoFocus={ false}
                      placeholder="City"
                      placeholderTextColor="#808080"
                      style={styles.textInput}
                      ref="city"
                      keyboardType={ 'email-address'}
                      onChangeText={city=> this.setState({city})}
                    />
                </View>
                { !(this.state.cityerr) ? null :
                  <Text style={styles.error}>{this.state.cityerr}</Text>
                }
            </View>
            <View style={styles.SectionStyle}>
                <View style={styles.inputfield}>
                    <Text style={styles.inputtext}>State</Text>
                    <TextInput
                      autoCapitalize={'none'}
                      autoCorrect={false}
                      value={this.state.state}
                      underlineColorAndroid="transparent"
                      returnKeyType={ "next"}
                      selectionColor={"#000000"}
                      autoFocus={ false}
                      placeholder="State"
                      placeholderTextColor="#808080"
                      style={styles.textInput}
                      ref="state"
                      keyboardType={ 'email-address'}
                      onChangeText={state=> this.setState({state})}
                    />
                </View>
                { !(this.state.stateerr) ? null :
                  <Text style={styles.error}>{this.state.stateerr}</Text>
                }
            </View>

            <View style={styles.SectionStyle}>
                <View style={styles.inputfield}>
                    <Text style={styles.inputtext}>Country</Text>
                    <TextInput
                      autoCapitalize={'none'}
                      autoCorrect={false}
                      underlineColorAndroid="transparent"
                      value={this.state.country}
                      returnKeyType={ "next"}
                      selectionColor={"#000000"}
                      autoFocus={ false}
                      placeholder="Country"
                      placeholderTextColor="#808080"
                      style={styles.textInput}
                      ref="country"
                      keyboardType={ 'email-address'}
                      onChangeText={country=> this.setState({country})}
                    />
                </View>
                { !(this.state.countryerr) ? null :
                  <Text style={styles.error}>{this.state.countryerr}</Text>
                }
            </View>
            <View style={styles.SectionStyle}>
                <View style={styles.inputfield}>
                    <Text style={styles.inputtext}>Zip Code</Text>
                    <TextInput
                      autoCapitalize={'none'}
                      autoCorrect={false}
                      value={this.state.zip}
                      underlineColorAndroid="transparent"
                      returnKeyType={ "next"}
                      selectionColor={"#000000"}
                      autoFocus={ false}
                      placeholder="Zip Code"
                      placeholderTextColor="#808080"
                      style={styles.textInput}
                      ref="zip"
                      keyboardType={ 'email-address'}
                      onChangeText={zip=> this.setState({zip})}
                    />
                </View>
                { !(this.state.ziperr) ? null :
                  <Text style={styles.error}>{this.state.ziperr}</Text>
                }
            </View>
            <View style={styles.SectionStyle}>
                <View style={styles.inputfield}>
                    <Text style={styles.inputtext}>Description</Text>
                    <TextInput
                      autoCapitalize={'none'}
                      autoCorrect={false}
                      value={this.state.description}
                      multiline={true}
                      numberOfLines={4}
                      underlineColorAndroid="transparent"
                      returnKeyType={ "next"}
                      selectionColor={"#000000"}
                      autoFocus={ false}
                      placeholder="Description"
                      placeholderTextColor="#808080"
                      style={[styles.textInput,{height:80}]}
                      ref="description"
                      keyboardType={ 'email-address'}
                      onChangeText={description=> this.setState({description})}
                    />
                </View>
                { !(this.state.descriptionerr) ? null :
                  <Text style={styles.error}>{this.state.descriptionerr}</Text>
                }
            </View>
            <View style={styles.SectionStyle}>
              <View style={styles.inputfield}>
                <Text style={styles.inputtext}>Status</Text>
                  <RadioGroup
                    color='#000000'
                    selectedIndex={this.state.status}
                    style={styles.SectionRadioGroup}
                    onSelect = {(index, value) => this.onSelect(index, value)}
                  >
                    <RadioButton value={'0'}>
                      <Text style={{color:'#000000'}}>InActive</Text>
                    </RadioButton>

                    <RadioButton value={'1'}>
                      <Text style={{color:'#000000'}}>Active</Text>
                    </RadioButton>
                  </RadioGroup>
                </View>
            </View>



            </View>

                </ScrollView>
            </KeyboardAwareScrollView>

            <TouchableOpacity onPress={this.submit_click} style={[styles.SectionStyle,{marginTop:AppSizes.ResponsiveSize.Padding(3)}]}>
              <CommonButton label='Save' width='100%'/>
            </TouchableOpacity>

        </View>
        <Spinner visible={this.state.isVisible}  />
    </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    paddingTop:  0,
  },

  content: {
      flex: 1,
      flexDirection: 'column',
      height:'70%',
      backgroundColor:'#ffffff'
  },
  container1: {
    flex: 3,

    height:AppSizes.screen.width/4,
    paddingLeft:AppSizes.ResponsiveSize.Padding(5),
    //backgroundColor:'red'
  },

  container2: {
    flex: 10,
    height:'70%',
    paddingLeft:'4%',
    paddingRight:'4%',
    paddingTop:'4%',
    flexDirection: 'column',

    //backgroundColor:'blue'
  },
  container3: {
    flex: 3,
    height:AppSizes.screen.width/8,
    alignItems: 'center',//replace with flex-end or center
    justifyContent: 'flex-end',
 },

 avatarContainer: {
     borderColor: '#9B9B9B',
     borderWidth: 1 / PixelRatio.get(),
     justifyContent: 'center',
     alignItems: 'center',
   },
   avatar: {
     borderRadius: 75,
     width: 120,
     height: 120,
 },

headerTitle:{
  fontSize:AppSizes.ResponsiveSize.Sizes(25),
  color:'#000000',
  fontWeight:'bold',
  paddingBottom:AppSizes.ResponsiveSize.Padding(1),
  fontFamily:Fonts.RobotoBold,
},
headersubTitle:{
  fontSize:AppSizes.ResponsiveSize.Sizes(14),
  color:'#000000',
  fontWeight:'600',
  paddingTop:AppSizes.ResponsiveSize.Padding(2),
  fontFamily:Fonts.RobotoBold,
},
headerBorder :{
  borderBottomColor: '#000000',
  borderBottomWidth: 3,
  width:'10%',
},
footertext :{
 color :'#000000',
 fontSize:AppSizes.ResponsiveSize.Sizes(12),
 fontWeight:'400',
 fontFamily:Fonts.RobotoRegular,
},
SectionStyle: {
  width:AppSizes.ResponsiveSize.width,

},

ImageStyle: {
  height: AppSizes.screen.width/21,
  width: AppSizes.screen.width/21,
  resizeMode : 'contain',
},
textInput: {
flex:1,
paddingLeft:AppSizes.ResponsiveSize.Padding(3),
fontSize: AppSizes.ResponsiveSize.Sizes(12),
color:'#000000',
backgroundColor:'#eee',
height:40,
fontFamily:Fonts.RobotoBold,
},
inputfield:{
  width:AppSizes.ResponsiveSize.width,

},
inputtext:{marginBottom:10,
  marginTop:AppSizes.ResponsiveSize.Padding(3),
  fontSize:AppSizes.ResponsiveSize.Sizes(14),
  fontWeight:'600',
  fontFamily:Fonts.RobotoBold,
  marginBottom:AppSizes.ResponsiveSize.Padding(1)
},
error:{
  color:'red',
  fontSize:AppSizes.ResponsiveSize.Sizes(12),
  textAlign:'center',
  fontFamily:Fonts.RobotoRegular,
},
SectionRadioGroup:{
  flexDirection:'row',
},
});
