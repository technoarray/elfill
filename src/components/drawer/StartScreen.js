import {
  AsyncStorage,
    StyleSheet,
    Dimensions,
    Button,
    Image,
    View,
    Text,
    ScrollView,
    TouchableOpacity,
    TouchableHighlight
} from 'react-native';
import { AppStyles, AppSizes, AppColors } from '../../themes/'
import React from 'react';

import { DrawerNavigator } from 'react-navigation';

import HomeScreen from './HomeScreen';
import AccountScreen from './AccountScreen';
import AboutScreen from './AboutScreen';
import LogoutScreen from './LogoutScreen';
import SideMenu from './SideMenu';
import Serviceslist from './Serviceslist';
import Serviceslistseller from './Serviceslistseller';
import SellerDashboard from './SellerDashboard';
import SettingScreen from './SettingScreen';
import TermConditionScreen from './TermConditionScreen';
import PrivacyPolicyScreen from './PrivacyPolicyScreen';
import ThankyouScreen from './ThankyouScreen';
import MyBookings from './MyBookings';
import BookingDetails from './BookingDetails';
import PaymentsScreen from './PaymentsScreen';
import CalendarScreen from './CalendarScreen';

export default DrawerNavigator({
    HomeScreen: { screen: HomeScreen},
    SellerDashboard: { screen: SellerDashboard},
    MyBookings: { screen: MyBookings},
    BookingDetails: { screen: BookingDetails},
    AboutScreen: { screen: AboutScreen},
    SettingScreen: { screen: SettingScreen},
    PaymentsScreen:{screen:PaymentsScreen},
    CalendarScreen:{screen:CalendarScreen},
    Serviceslistseller:{screen:Serviceslistseller},
    TermConditionScreen : {screen: TermConditionScreen},
    PrivacyPolicyScreen : {screen: PrivacyPolicyScreen},
    LogoutScreen: { screen: LogoutScreen},
    ThankyouScreen: { screen: ThankyouScreen},
}, {
  contentComponent: SideMenu,
  drawerWidth: AppSizes.screen.width-50
});
