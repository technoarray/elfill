import React, { Component } from 'react';
import {StyleSheet,Text,View,Image,TouchableOpacity,Platform,Alert,ScrollView,TextInput} from 'react-native';
import { AppStyles, AppSizes, AppColors } from '../../themes/'
import { NavigationActions } from 'react-navigation'
import Spacer from '../common/Spacer'
import * as commonFunctions from '../../utils/CommonFunctions'
import Header from '../common/Header'
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view'
import CommonButton from '../common/CommonButton'
import LinearGradient from 'react-native-linear-gradient';
import { Fonts } from '../../utils/Fonts';
var Constant = require('../../api/ApiRules').Constant;

/* Images */
const bg_image = require('../../themes/Images/bg_image.png')
var left= require( '../../themes/Images/right.png')
var plus= require( '../../themes/Images/plus.png')

export default class AccountScreen extends Component {
  constructor (props) {
      super(props)
      this.state = {
      isVisible: false,
      Modal_Visibility: false
    },
    _that = this;
  }

  ServiceviewBtn=()=>{
     _that.props.navigation.navigate('SingleBookScreen');
  }

  updateAlert = () => {
        this.setState({Modal_Visibility: !this.state.Modal_Visibility});
  }

  // website_link() {
  //   Alert.alert(
  //       'eThera',
  //       'You will be redirected to your default browser, to visit our Website',
  //       [
  //         {text: 'Cancel', onPress: () => console.log('Cancel Button Pressed'), style: 'cancel'},
  //         {text: 'OK', onPress: () => Linking.openURL(Constant.SITE_URL)},
  //
  //       ]
  //
  //     )
  // }

  render() {
    const headerProp = {
      title: 'Serviceslist',
      screens: 'Serviceslist',
    };
    return (
      <View style={styles.container}>
      <Header info={headerProp} navigation={_that.props.navigation}/>
        <View style={styles.content}>
        <ScrollView contentContainerStyle={styles.contentContainer}>
            <View style={styles.container1}>
            <TouchableOpacity activeOpacity={.6} onPress={this.ServiceviewBtn}>
                      <LinearGradient colors={['#3995f7','#483158','#544658','#5a585b']} style={[styles.linearGradient,styles.servicebox]}>
                          <View style={styles.titleblk}>
                            <Text style={styles.title}>Service one</Text>
                            <Text style={styles.smalltext}>Panchkula,Haryana</Text>
                          </View>
                          <View style={styles.sideicon}>
                              <Image source={left} style={[styles.ImageStyle,styles.sidearrow]} />
                          </View>
                        </LinearGradient>
              </TouchableOpacity>
                        <LinearGradient colors={['#3995f7','#483158','#544658','#5a585b']} style={[styles.linearGradient,styles.servicebox]}>
                            <View style={styles.titleblk}>
                              <Text style={styles.title}>Service one</Text>
                              <Text style={styles.smalltext}>Panchkula,Haryana</Text>
                            </View>
                            <View style={styles.sideicon}>
                                <Image source={left} style={[styles.ImageStyle,styles.sidearrow]} />
                            </View>
                          </LinearGradient>
                          <LinearGradient colors={['#3995f7','#483158','#544658','#5a585b']} style={[styles.linearGradient,styles.servicebox]}>
                              <View style={styles.titleblk}>
                                <Text style={styles.title}>Service one</Text>
                                <Text style={styles.smalltext}>Panchkula,Haryana</Text>
                              </View>
                              <View style={styles.sideicon}>
                                  <Image source={left} style={[styles.ImageStyle,styles.sidearrow]} />
                              </View>
                            </LinearGradient>
                            <LinearGradient colors={['#3995f7','#483158','#544658','#5a585b']} style={[styles.linearGradient,styles.servicebox]}>
                                <View style={styles.titleblk}>
                                  <Text style={styles.title}>Service one</Text>
                                  <Text style={styles.smalltext}>Panchkula,Haryana</Text>
                                </View>
                                <View style={styles.sideicon}>
                                    <Image source={left} style={[styles.ImageStyle,styles.sidearrow]} />
                                </View>
                              </LinearGradient>
                              <LinearGradient colors={['#3995f7','#483158','#544658','#5a585b']} style={[styles.linearGradient,styles.servicebox]}>
                                  <View style={styles.titleblk}>
                                    <Text style={styles.title}>Service one</Text>
                                    <Text style={styles.smalltext}>Panchkula,Haryana</Text>
                                  </View>
                                  <View style={styles.sideicon}>
                                      <Image source={left} style={[styles.ImageStyle,styles.sidearrow]} />
                                  </View>
                                </LinearGradient>
                                <LinearGradient colors={['#3995f7','#483158','#544658','#5a585b']} style={[styles.linearGradient,styles.servicebox]}>
                                    <View style={styles.titleblk}>
                                      <Text style={styles.title}>Service one</Text>
                                      <Text style={styles.smalltext}>Panchkula,Haryana</Text>
                                    </View>
                                    <View style={styles.sideicon}>
                                        <Image source={left} style={[styles.ImageStyle,styles.sidearrow]} />
                                    </View>
                                  </LinearGradient>
                                  <LinearGradient colors={['#3995f7','#483158','#544658','#5a585b']} style={[styles.linearGradient,styles.servicebox]}>
                                      <View style={styles.titleblk}>
                                        <Text style={styles.title}>Service one</Text>
                                        <Text style={styles.smalltext}>Panchkula,Haryana</Text>
                                      </View>
                                      <View style={styles.sideicon}>
                                          <Image source={left} style={[styles.ImageStyle,styles.sidearrow]} />
                                      </View>
                                    </LinearGradient>
                                    <LinearGradient colors={['#3995f7','#483158','#544658','#5a585b']} style={[styles.linearGradient,styles.servicebox]}>
                                        <View style={styles.titleblk}>
                                          <Text style={styles.title}>Service one</Text>
                                          <Text style={styles.smalltext}>Panchkula,Haryana</Text>
                                        </View>
                                        <View style={styles.sideicon}>
                                            <Image source={left} style={[styles.ImageStyle,styles.sidearrow]} />
                                        </View>
                                      </LinearGradient>
                                      <LinearGradient colors={['#3995f7','#483158','#544658','#5a585b']} style={[styles.linearGradient,styles.servicebox]}>
                                          <View style={styles.titleblk}>
                                            <Text style={styles.title}>Service one</Text>
                                            <Text style={styles.smalltext}>Panchkula,Haryana</Text>
                                          </View>
                                          <View style={styles.sideicon}>
                                              <Image source={left} style={[styles.ImageStyle,styles.sidearrow]} />
                                          </View>
                                        </LinearGradient>
            </View>
  </ScrollView>
        </View>
    </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    paddingTop:  0,
  },

  content: {
      flex: 1,
      padding:'5%',
      paddingBottom:0,
      //backgroundColor:'#000'
  },
  container1: {
    flex: 1,

    flexDirection: 'column',
  },
  servicebox:{
    flexDirection:'row',
    padding:AppSizes.ResponsiveSize.Sizes(15),
    borderRadius: 5,
    marginBottom:AppSizes.ResponsiveSize.Padding(2)
  },
  titleblk:{
    width:'90%',
    height:'100%'
  },
  sideicon:{
      width:'10%',
      alignItems: 'center',
      paddingTop:AppSizes.ResponsiveSize.Padding(2),

  },
  ImageStyle: {
    height: AppSizes.screen.width/15,
    width: AppSizes.screen.width/15,
    resizeMode : 'contain',
  },

  title:{
    fontSize:AppSizes.ResponsiveSize.Sizes(20),
    paddingBottom: AppSizes.ResponsiveSize.Sizes(5),
    color:'#fff',
    fontWeight:'700',
    fontFamily:Fonts.RobotoBold,
  },
  smalltext:{
    fontSize:AppSizes.ResponsiveSize.Sizes(10),
    color:'#fff',
    fontWeight:'400',
    fontFamily:Fonts.RobotoRegular,
  },
  contentContainer: {
      paddingVertical: 20,
      height:'85%',
        //backgroundColor:'green'
    },
    containerend:{
      flex:2,
      justifyContent:'flex-end',
      //backgroundColor:'red',
      alignItems:'flex-end'
    },

    sideiconmain:{
        width:'10%',
        alignItems: 'center',
        paddingTop:AppSizes.ResponsiveSize.Padding(2),

        //backgroundColor:'gray'
    },

    ImageStylemain: {
      height: AppSizes.screen.width/8,
      width: AppSizes.screen.width/8,
      resizeMode : 'contain',
    },
});
