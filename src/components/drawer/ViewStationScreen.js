import React, { Component } from 'react';
import {StyleSheet,Text,View,Image,TouchableOpacity,Platform,Alert,ScrollView,TextInput} from 'react-native';
import { AppStyles, AppSizes, AppColors } from '../../themes/'
import { NavigationActions } from 'react-navigation'
import Spacer from '../common/Spacer'
import * as commonFunctions from '../../utils/CommonFunctions'
import Header from '../common/HeaderWithBack'
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view'
import CommonButton from '../common/CommonButton'
import { Fonts } from '../../utils/Fonts';
var Constant = require('../../api/ApiRules').Constant;

/* Images */
var no_image= require( '../../themes/Images/no_station.png')


export default class ViewStationScreen extends Component {
  constructor (props) {
      super(props)
      this.state = {
      isVisible: false,
      item:[]
    },
    _that = this;
  }

  componentWillMount(){
    var item=this.props.navigation.state.params.item
      _that.setState({
        item:item
      })
  }

  editBtn=(item)=>{
     _that.props.navigation.navigate('EditStationScreen',{item:item});
  }


  render() {
    const headerProp = {
      title: 'Station Details',
      screens: 'Stationlistseller',
      type:''
    };


    return (
    <View style={styles.container}>
      <Header info={headerProp} navigation={_that.props.navigation}/>
        <View style={styles.content}>
          <ScrollView style={{height:'100%',paddingTop:0}}>
            <View style={[styles.container1,styles.shadow]}>
                <View style={styles.containerimage}>
                {this.state.item.service_station_image != '' ?
                <Image
                 source={{uri: Constant.image_path+this.state.item.service_station_image}}
                 style={styles.ImageStyle} />
                  :
                  <Image source={no_image} style={styles.ImageStyle} />
                }

                </View>
                <View style={styles.containercontent}>
                      <Text style={styles.title}>{this.state.item.station_name}</Text>
                      <View style={styles.border} />
                      <Text style={styles.contentbox}>
                        {this.state.item.description}
                      </Text>
                      <View style={styles.tagbox}>
                          <View style={styles.tag}>
                              <Text style={styles.tagtext}>
                                  Location:- {this.state.item.location}
                              </Text>
                          </View>
                          <View style={styles.tag}>
                              <Text style={styles.tagtext}>
                                  Country:- {this.state.item.country}
                              </Text>
                          </View>
                          <View style={styles.tag}>
                              <Text style={styles.tagtext}>
                                  Zip Code:- {this.state.item.zip}
                              </Text>
                          </View>
                          <View style={styles.tag}>
                              <Text style={styles.tagtext}>
                                  State:- {this.state.item.state}
                              </Text>
                          </View>

                      </View>
                      <View style={styles.tagbtn}>
                          <View style={styles.boxbtn}>
                          <TouchableOpacity activeOpacity={.6} onPress={() => this.editBtn(this.state.item)} >
                            <View style={{ width: this.props.width}}>
                                <Text style={styles.boxtextbtn}>Edit</Text>
                            </View>
                            </TouchableOpacity>
                          </View>

                      </View>

                </View>
            </View>
          </ScrollView>
        </View>
    </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    paddingTop:  0,
    backgroundColor:'#fff'
  },

  content: {
      flex: 1,
      flexDirection: 'column',
      //height:'100%',

      //backgroundColor:'red',
      paddingBottom:0,
      height:'100%'
  },
  container1: {
    //flex: 1,
    height:'100%',
    width:'100%',
  },
  containerimage:{
    //backgroundColor:'green',
    //flex:2,
    width:'100%',

    paddingTop:AppSizes.ResponsiveSize.Padding(0),
  },

  ImageStyle: {
  height:'100%',
  height:AppSizes.screen.height/2.9,
  resizeMode:'cover'
  },
containercontent:{
  //backgroundColor:'green',
  //height:AppSizes.screen.height/2,
  padding:AppSizes.ResponsiveSize.Padding(6),

},
title:{
  fontSize:AppSizes.ResponsiveSize.Sizes(22),
  color:'#3995f7',
  fontWeight:'600',
  marginBottom:3,
  fontFamily:Fonts.RobotoBold,
},
border:{
  width:30,
  height:2,
  backgroundColor:'#333',
  marginTop:AppSizes.ResponsiveSize.Padding(1),
  marginBottom:AppSizes.ResponsiveSize.Padding(1)
},
contentbox:{
  fontSize:AppSizes.ResponsiveSize.Sizes(15),
  color:'#333',
  fontWeight:'400',
  lineHeight: AppSizes.ResponsiveSize.Sizes(10 * 1.80),
  marginTop:8,
  marginBottom:8,
  fontFamily:Fonts.RobotoRegular,

},
tagbox:{
  flexDirection:'row',
  flexWrap:'wrap',
  paddingTop:AppSizes.ResponsiveSize.Padding(3),
},

tag:{
  backgroundColor:'#eee',
  padding:AppSizes.ResponsiveSize.Padding(2),
  paddingTop:AppSizes.ResponsiveSize.Padding(2),
  paddingBottom:AppSizes.ResponsiveSize.Padding(2),
  borderRadius:6,
  marginRight:AppSizes.ResponsiveSize.Padding(2),
  marginBottom:AppSizes.ResponsiveSize.Padding(2)
},
tagtext:{
  fontSize:AppSizes.ResponsiveSize.Sizes(12),
  color:'#000',
},
shadow:{
    borderWidth:0,
    borderRadius: 2,
    borderColor: '#fff',
    justifyContent:'center',
    backgroundColor:'#fff',
    borderColor: '#ddd',
    borderBottomWidth: 0,
    shadowColor: '#999',
    shadowOffset: { width: 0, height: 2 },
    shadowOpacity: 0.8,
    shadowRadius: 2,
    elevation: 0,
    zIndex:0,
    height:'100%'
    //backgroundColor:'red',
    //paddingTop:AppSizes.ResponsiveSize.Padding(4),

 },
tagbtn:{
  flexDirection:'row',
  flexWrap:'wrap',
  paddingTop:AppSizes.ResponsiveSize.Padding(1),
  marginBottom:AppSizes.ResponsiveSize.Padding(2),
  //backgroundColor:'green'
},
 boxtextbtn:{
   color:'#ffffff',
   fontSize:AppSizes.ResponsiveSize.Sizes(12),
   fontWeight:'400',
   textAlign:'center',
   fontFamily:Fonts.RobotoRegular,
 },
 boxbtn:{
   backgroundColor:'#3995f7',
   padding:AppSizes.ResponsiveSize.Padding(3),
   paddingTop:AppSizes.ResponsiveSize.Padding(3),
   paddingBottom:AppSizes.ResponsiveSize.Padding(3),
   borderRadius:3,
   marginTop:10,
   marginRight:AppSizes.ResponsiveSize.Padding(1),
   marginBottom:AppSizes.ResponsiveSize.Padding(1),
   width:'45%'
 }
});
