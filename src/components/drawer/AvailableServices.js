import React, { Component } from 'react';
import {AsyncStorage,StyleSheet,Alert,Text,View,Image,TouchableOpacity,Platform,WebView,ImageBackground,ScrollView,Dimensions} from 'react-native';
import { AppStyles, AppSizes, AppColors } from '../../themes/'
import { NavigationActions, StackActions  } from 'react-navigation';
import Spacer from '../common/Spacer'
import Header from '../common/HeaderWithBack';
import Spinner from 'react-native-loading-spinner-overlay';
import * as commonFunctions from '../../utils/CommonFunctions'
import LinearGradient from 'react-native-linear-gradient';
import MapView, { Marker, ProviderPropType } from 'react-native-maps';
import Geocoder from 'react-native-geocoder';
import { Fonts } from '../../utils/Fonts';
var Constant = require('../../api/ApiRules').Constant;
var WebServices = require('../../api/ApiRules').WebServices;

const bg_image = require('../../themes/Images/bg_image.png')
const all = require('../../themes/Images/station_icon.png')
const list_car = require('../../themes/Images/list_car.png')
const car = require('../../themes/Images/car.png')
const arrow = require('../../themes/Images/double_arrow.png')
const bike = require('../../themes/Images/bike.png')
//const parking = require('../../themes/Images/parking.png')
const gadget = require('../../themes/Images/laptop.png')

const { width, height } = Dimensions.get('window');
const ASPECT_RATIO = width / height;
const LATITUDE = 37.78825;
const LONGITUDE = -122.4324;
const LATITUDE_DELTA = 0.29;
const LONGITUDE_DELTA = LATITUDE_DELTA * ASPECT_RATIO;
const SPACE = 0.01;
const LAT= 30.7279571
const LANG= 76.8553046

let _that
export default class HomeScreen extends Component {
  constructor (props) {
      super(props)
      this.state = {
        uid:'',
      isVisible: false,
      stationList:[],
      isMapReady: false,
      region: {
          latitude: LATITUDE,
          longitude: LONGITUDE,
          latitudeDelta: LATITUDE_DELTA,
          longitudeDelta: LONGITUDE_DELTA
      },
      image:all,
      cat:'',
      sub_cat:'',
      bike:'0',
      gadget:'0',
      type_id:'',
      name:this.props.navigation.state.params.name,
      cat:this.props.navigation.state.params.cat,
      sub_cat:this.props.navigation.state.params.sub_cat,
      cat_id:this.props.navigation.state.params.cat_id,
      subcat_id:this.props.navigation.state.params.subcat_id,
      other_cat_id:this.props.navigation.state.params.other_cat_id,
      empty:true,
      othername:''
    },
    _that = this;
  }

  componentDidMount() {
    if(this.state.sub_cat=='car'){
      this.setState({image:car})
      this.setState({type_id:'1'})
    }
    else if (this.state.sub_cat=='Bike') {
      this.setState({image:bike})
      this.setState({type_id:'2'})
    }
    else if (this.state.sub_cat=='Gadgets') {
      this.setState({image:gadget})
      this.setState({type_id:'4'})
    }

    if(this.state.cat=='electric'){
      this.setState({othername:'charging station'})
    }
    else if (this.state.cat=='rentals') {
      this.setState({othername:'rental station'})
    }
    else if (this.state.cat=='parking') {
      this.setState({othername:'parking station'})
    }
    AsyncStorage.getItem('UserData').then((UserData) => {
      var data = JSON.parse(UserData);
        uid=data.id
        if(uid != ''){
          _that.setState({
            uid:uid
          })
        }
    })

    try {
    Geocoder.fallbackToGoogle('AIzaSyBX6rKXe6Jsk6ZynShEZiNfDfyhZWgmXsQ');
     navigator.geolocation.getCurrentPosition(
      (position) => {
        var region = {
          lat: position.coords.latitude,
          lng:  position.coords.longitude,
        };
        _that.validationAndApiParameter('ServiceList',uid,region.lat,region.lng)
        this.setState({
          region: {
            latitude: position.coords.latitude,
            longitude: position.coords.longitude,
            latitudeDelta: LATITUDE_DELTA,
            longitudeDelta: LONGITUDE_DELTA,
            accuracy: position.coords.accuracy
          }
        });
        Geocoder.geocodePosition(region).then(res => {
          //console.log(res)
          var address=res[0].formattedAddress
          _that.setState({
            latitude: position.coords.latitude,
            longitude: position.coords.longitude,
            address: address,
          });
        })
        this.watchID = navigator.geolocation.watchPosition((position) => {
          const newRegion = {
            latitude: position.coords.latitude,
            longitude: position.coords.longitude,
            latitudeDelta: LATITUDE_DELTA,
            longitudeDelta: LONGITUDE_DELTA,
            accuracy: position.coords.accuracy
          }
          this.setState({newRegion});
        })
      },
      (error) => console.log('error '+error.message),
      {enableHighAccuracy: false, timeout: 50000, maximumAge: 10000}
    );
  }
  catch(err) {
      console.log(err);
  }
}

  ServicelistBtn(name,id,lat,lgt,image,data){
     _that.props.navigation.navigate('AvailableSubServices',{name:name,id:id,uid:this.state.uid,cat_id:this.state.cat_id,subcat_id:this.state.subcat_id,type_id:this.state.type_id,lat:lat,lgt:lgt,cat:this.state.cat,sub_cat:this.state.sub_cat});
  }

  validationAndApiParameter(apikey,id,lat,lng) {
    if(apikey=='ServiceList'){
      const data = new FormData();
      data.append('uid',id);
      data.append('cat_id',this.state.cat_id);
      data.append('subcat_id',this.state.subcat_id)
      data.append('latitude',lat);
      data.append('longitude',lng);
      data.append('other_cat_id',this.state.other_cat_id)
      console.log('Data',data);
      _that.setState({isVisible: true});
      this.postToApiCalling('POST',apikey, Constant.URL_userStationList, data);
    }
  }

  postToApiCalling(method, apiKey, apiUrl, data) {
    new Promise(function(resolve, reject) {
      if (method == 'POST') {
        resolve(WebServices.callWebService(apiUrl, data));
      } else {
        resolve(WebServices.callWebService_GET(apiUrl, data));
      }
    }).then((jsonRes) => {
      //console.log(jsonRes);
      _that.setState({ isVisible: false })
      if ((!jsonRes) || (jsonRes.code == 0)) {
          setTimeout(()=>{
            Alert.alert('Login Error',jsonRes.message);
          },200);
      }
      else{
        if (jsonRes.code == 1) {
          _that.apiSuccessfullResponse(apiKey, jsonRes)
        }
      }
    }).catch((error) => {
        console.log("ERROR" + error);
        _that.setState({ isVisible: false })
        setTimeout(()=>{
            Alert.alert("Server issue"+error);
        },200);
    });
  }

  apiSuccessfullResponse(apiKey, jsonRes) {
      if (apiKey == 'ServiceList') {
          stationData=jsonRes.result;
          //console.log('Response',stationData);
          if (stationData.length==0) {
            _that.setState({empty:false})
          }
          else{
            _that.setState({empty:true})
          }
          _that.setState({stationList:stationData})
      }
  }
  convert(data){
    var numb = data
    numb = parseFloat(numb).toFixed(2);
    return numb;
  }

  render() {
    const headerProp = {
      title: 'Stations list',
      screens: 'Serviceslist',
      type:''
    };
    return (

      <View style={styles.container}>
        <Header info={headerProp} navigation={_that.props.navigation} />

       <View style={styles.content}>
      {/*  <View style={styles.mapcontainer}>
             <MapView style={styles.map}
                    region={this.state.region}
                    provider='google'
                    mapType='standard'
                    showsCompass={true}
                    showsPointsOfInterest
                    showsUserLocation={true} >
                    {this.state.stationList.map((marker) => (
                      <Marker
                        coordinate={{latitude:Number(marker.latitude),longitude:Number(marker.longitude)}}
                        key={marker.id}
                        opacity={0.8}
                        image={this.state.image}
                      />
                    ))}
            </MapView>
           <WebView
                source={{uri: 'https://leemansinussolutions.com/efil/map/map.php'}}
              />
          </View>*/}

          <View style={styles.formcontainer}>

          {this.state.empty?
            <View style={{paddingTop:AppSizes.ResponsiveSize.Padding(3),alignItems:'center',width:'100%',height:'100%'}}>
              <ScrollView>
                {this.state.stationList.map(data => (
                  <TouchableOpacity activeOpacity={.6} key={data.id} onPress={()=>this.ServicelistBtn(data.station_name,data.id,Number(data.latitude),Number(data.longitude),data.service_station_image,data)} style={[styles.containerin,styles.shadow1]}>

                    <View style={{width:'100%',marginBottom:15,marginTop:8,flexDirection:'row',paddingLeft:'2%',paddingRight:'2%'}}>
                      <View style={{width:'60%'}}>
                        <Text style={styles.texttitle}>********</Text>
                        <Text style={styles.text1} ellipsizeMode={'tail'} numberOfLines={2}>{data.description}</Text>
                      </View>
                      <View style={{width:'40%'}}>
                        <Text style={styles.textmiles}>{this.convert(data.distance_mile)} Mi</Text>
                      </View>
                    </View>
                  </TouchableOpacity>
                ))}
              </ScrollView>
            </View>
          :
            <View style={{alignItems:'center',justifyContent:'center',height:'100%'}}>
             <Text style={{color:'#3995f7',fontSize:25,textAlign:'center'}}>No {this.state.othername} found nearby
             </Text>
            </View>
          }


      </View>
     </View>
     <Spinner visible={this.state.isVisible}  />
    </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    paddingTop:  0,
  },

  content: {
      flex: 1,
      height:'70%',
      width:'100%'
  },
  mapcontainer:{
    flex:1.5,
  },
  formcontainer:{
    flex:1.5,

  },
   image: {
     flex: 1,
     width: undefined,
     height: undefined,
     resizeMode:'contain'
   },
   innercontainer:{
     width: '100%',
     height: '100%',
     resizeMode:'contain',
   },

   containerin: {
    height:100,
    paddingTop: AppSizes.ResponsiveSize.Padding(1),
    paddingBottom: AppSizes.ResponsiveSize.Padding(1),
    flexDirection: 'row',
    alignItems: 'center',
    width:'100%',
    borderBottomColor:'#999',
    borderBottomWidth:1,
    marginBottom:10,
    marginLeft:5,
    paddingLeft:AppSizes.ResponsiveSize.Padding(2),
    paddingRight:AppSizes.ResponsiveSize.Padding(2)
  },

  texttitle: {
    marginLeft: AppSizes.ResponsiveSize.Padding(4),
    paddingBottom: AppSizes.ResponsiveSize.Padding(2),
    fontSize:AppSizes.ResponsiveSize.Sizes(30),
    color:'#000',
    width:'90%',
    fontFamily:Fonts.RobotoRegular,
    //fontFamily: "Proxima Nova Semibold",
  },
  textmiles:{
    marginRight: AppSizes.ResponsiveSize.Padding(5),
    paddingBottom: AppSizes.ResponsiveSize.Padding(2),
    fontSize:AppSizes.ResponsiveSize.Sizes(17),
    fontWeight:'500',
    backgroundColor:'#3995f7',
    color:'#fff',
    width:'100%',
    borderRadius:6,
    textAlign:'center',
    fontFamily:Fonts.RobotoBold,
  },
  text1: {
    marginLeft: AppSizes.ResponsiveSize.Padding(4),
    fontSize:AppSizes.ResponsiveSize.Sizes(16),
    lineHeight:22,
    color:'#747474',
    fontWeight:'400',
    fontFamily:Fonts.RobotoRegular,
  },
  photo: {
    flex:1,
    width: undefined,
    height: undefined,
    borderRadius:30,
},
map: {
    ...StyleSheet.absoluteFillObject,
},
});
