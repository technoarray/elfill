import React, { Component } from 'react';
import {AsyncStorage,StyleSheet,Alert,Text,View,Image,TouchableOpacity,Platform,WebView,ImageBackground,ScrollView,Dimensions} from 'react-native';
import { AppStyles, AppSizes, AppColors } from '../../themes/'
import { NavigationActions, StackActions  } from 'react-navigation';
import Spacer from '../common/Spacer'
import Header from '../common/Header'
import Spinner from 'react-native-loading-spinner-overlay';
import * as commonFunctions from '../../utils/CommonFunctions'
import LinearGradient from 'react-native-linear-gradient';
import MapView, { Marker, ProviderPropType } from 'react-native-maps';
import Geocoder from 'react-native-geocoder';
import { Fonts } from '../../utils/Fonts';
var Constant = require('../../api/ApiRules').Constant;
var WebServices = require('../../api/ApiRules').WebServices;

const bg_image = require('../../themes/Images/bg_image.png')
const all = require('../../themes/Images/station_icon.png')
//const list_car = require('../../themes/Images/list_car.png')
const arrow = require('../../themes/Images/double_arrow.png')
//const bike = require('../../themes/Images/bike.png')
//const parking = require('../../themes/Images/parking.png')
//const gadget = require('../../themes/Images/laptop.png')

const { width, height } = Dimensions.get('window');
const ASPECT_RATIO = width / height;
const LATITUDE = 37.78825;
const LONGITUDE = -122.4324;
const LATITUDE_DELTA = 0.29;
const LONGITUDE_DELTA = LATITUDE_DELTA * ASPECT_RATIO;
const SPACE = 0.01;
const LAT= 30.7279571
const LANG= 76.8553046

let _that
export default class HomeScreen extends Component {
  constructor (props) {
    super(props)
    this.state = {
      uid:'',
      isVisible: false,
      stationList:[],
      isMapReady: false,
      region: {
        latitude: LATITUDE,
        longitude: LONGITUDE,
        latitudeDelta: LATITUDE_DELTA,
        longitudeDelta: LONGITUDE_DELTA
      },
      image:all,
      cat:'',
      sub_cat:'',
      bike:'0',
      gadget:'0',
      cat_id:'',
      subcat_id:''
    },
    _that = this;
  }

  componentDidMount() {
    AsyncStorage.getItem('UserData').then((UserData) => {
      var data = JSON.parse(UserData);
        uid=data.id
        if(uid != ''){
          _that.setState({
            uid:uid
          })
        }
    })

    try {
    Geocoder.fallbackToGoogle('AIzaSyBX6rKXe6Jsk6ZynShEZiNfDfyhZWgmXsQ');
     navigator.geolocation.getCurrentPosition(
      (position) => {
        var region = {
          lat: position.coords.latitude,
          lng:  position.coords.longitude,
        };
        _that.validationAndApiParameter('ServiceList',uid,region.lat,region.lng)
        this.setState({
          region: {
            latitude: position.coords.latitude,
            longitude: position.coords.longitude,
            latitudeDelta: LATITUDE_DELTA,
            longitudeDelta: LONGITUDE_DELTA,
            accuracy: position.coords.accuracy
          }
        });
        Geocoder.geocodePosition(region).then(res => {
          //console.log(res)
          var address=res[0].formattedAddress
          _that.setState({
            latitude: position.coords.latitude,
            longitude: position.coords.longitude,
            address: address,
          });
        })
        this.watchID = navigator.geolocation.watchPosition((position) => {
          const newRegion = {
            latitude: position.coords.latitude,
            longitude: position.coords.longitude,
            latitudeDelta: LATITUDE_DELTA,
            longitudeDelta: LONGITUDE_DELTA,
            accuracy: position.coords.accuracy
          }
          this.setState({newRegion});
        })
      },
      (error) => console.log('error '+error.message),
      {enableHighAccuracy: false, timeout: 50000, maximumAge: 10000}
    );
  }
  catch(err) {
      console.log(err);
  }
}
    /*try {
      Geocoder.fallbackToGoogle('AIzaSyBX6rKXe6Jsk6ZynShEZiNfDfyhZWgmXsQ');
      console.log('hello');

       navigator.geolocation.getCurrentPosition(
        (position) => {
          console.log('hello world');
          var region = {
            lat: position.coords.latitude,
            lng:  position.coords.longitude,
          };
          _that.validationAndApiParameter('ServiceList',uid,region.lat,region.lng)
          this.setState({
            region: {
              latitude: position.coords.latitude,
              longitude: position.coords.longitude,
              latitudeDelta: LATITUDE_DELTA,
              longitudeDelta: LONGITUDE_DELTA,
              accuracy: position.coords.accuracy
            }
          });
          console.log('Position',region)
          Geocoder.geocodePosition(region).then(res => {
            //console.log(res)
            var address=res[0].formattedAddress
            _that.setState({
              latitude: position.coords.latitude,
              longitude: position.coords.longitude,
              address: address,
            });
          })
        },
        (error) => console.log('error '+error.message),
        {enableHighAccuracy: false, timeout: 50000, maximumAge: 10000}
      );

      this.watchID = navigator.geolocation.watchPosition((position) => {
        const newRegion = {
          latitude: position.coords.latitude,
          longitude: position.coords.longitude,
          latitudeDelta: LATITUDE_DELTA,
          longitudeDelta: LONGITUDE_DELTA,
          accuracy: position.coords.accuracy
        }
        this.setState({newRegion});
      })

    }
    catch(err) {
        console.log(err);
    }*/

  componentWillMount() {
    AsyncStorage.getItem('user_type').then((type) => {
        var type = JSON.parse(type);
        if(type=="seller"){
          _that.props.navigation.navigate('SellerDashboard');
        }
    })
  }

  ServicelistBtn(name){
    if(this.state.cat_id=='1' && name=='car'){
      this.setState({subcat_id:'1'}),
      this.setState({sub_cat:name}),
      this.setState({bike:'0'}),
      this.setState({gadget:'0'}),
      this.setState({sub_cat:'car'})
      _that.props.navigation.navigate('AvailableServices',{name:name,cat:this.state.cat,sub_cat:'car',cat_id:this.state.cat_id,subcat_id:'1'});
    }
    else if (this.state.cat_id=='1' && name=='bike') {
      this.setState({sub_cat:name})
      this.setState({subcat_id:'2'}),
      this.setState({bike:'1'}),
      this.setState({gadget:'0'})
    }
    else if (this.state.cat_id=='1' && name=='gadget') {
      this.setState({sub_cat:name}),
      this.setState({bike:'0'}),
      this.setState({subcat_id:'11'}),
      this.setState({gadget:'1'})
    }
    else if (this.state.cat_id=='2' && name=='car') {
      this.setState({bike:'0'}),
      this.setState({gadget:'0'}),
      this.setState({sub_cat:name}),
      this.setState({subcat_id:'3'})
      _that.props.navigation.navigate('AvailableServices',{name:name,cat:this.state.cat,sub_cat:'car',cat_id:this.state.cat_id,subcat_id:'3'});
    }
    else if (this.state.cat_id=='2' && name == 'bike') {
      this.setState({sub_cat:name}),
      this.setState({bike:'1'}),
      this.setState({subcat_id:'4'}),
      this.setState({gadget:'0'})
    }
    else if (this.state.cat_id=='2' && name == 'gadget') {
      this.setState({sub_cat:name}),
      this.setState({bike:'0'}),
      this.setState({subcat_id:'12'}),
      this.setState({gadget:'1'})
    }
    else if (this.state.cat_id=='3' && name == 'car') {
      this.setState({bike:'0'}),
      this.setState({sub_cat:name}),
      this.setState({subcat_id:'13'})
      _that.props.navigation.navigate('AvailableServices',{name:name,cat:this.state.cat,sub_cat:'car',cat_id:this.state.cat_id,subcat_id:'13'});
    }
    else if (this.state.cat_id=='3' && name == 'bike') {
      this.setState({sub_cat:name}),
      this.setState({bike:'1'})
      this.setState({subcat_id:'14'})
    }
  }
  handleOnPress(name){
    if (name=='elbike' || name=='scooters' ||name=='skateboards' ||name=='3wheelers') {
      _that.props.navigation.navigate('AvailableServices',{name:name,cat:this.state.cat,sub_cat:this.state.sub_cat,cat_id:this.state.cat_id,subcat_id:this.state.subcat_id});
    }
    else if (name=='laptops' || name=='drone' ||name=='mobiles' ||name=='pc' ||name=='dronefilling' ) {
      _that.props.navigation.navigate('AvailableServices',{name:name,cat:this.state.cat,sub_cat:this.state.sub_cat,cat_id:this.state.cat_id,subcat_id:this.state.subcat_id});
    }
  }

  validationAndApiParameter(apikey,id,lat,lng) {
    if(apikey=='ServiceList'){
      const data = new FormData();
      data.append('uid',id);
      data.append('latitude',lat);
      data.append('longitude',lng);
      //console.log('Data',data);
      _that.setState({isVisible: true});
      this.postToApiCalling('POST',apikey, Constant.URL_allStationList, data);
    }
  }

  postToApiCalling(method, apiKey, apiUrl, data) {
    new Promise(function(resolve, reject) {
      if (method == 'POST') {
        resolve(WebServices.callWebService(apiUrl, data));
      } else {
        resolve(WebServices.callWebService_GET(apiUrl, data));
      }
    }).then((jsonRes) => {
      //console.log(jsonRes);
      _that.setState({ isVisible: false })
      if ((!jsonRes) || (jsonRes.code == 0)) {
          setTimeout(()=>{
            Alert.alert('Login Error',jsonRes.message);
          },200);
      }
      else{
        if (jsonRes.code == 1) {
          _that.apiSuccessfullResponse(apiKey, jsonRes)
        }
      }
    }).catch((error) => {
        console.log("ERROR" + error);
        _that.setState({ isVisible: false })
        setTimeout(()=>{
            Alert.alert("Server issue"+error);
        },200);
    });
  }

  apiSuccessfullResponse(apiKey, jsonRes) {
      if (apiKey == 'ServiceList') {
          stationData=jsonRes.result;
          //console.log('Response',stationData);
          _that.setState({stationList:stationData})
      }
  }
  convert(data){
    var numb = data
    numb = parseFloat(numb).toFixed(2);
    return numb;
  }

  render() {
    const headerProp = {
      title: 'HOME',
      screens: 'HomeScreen',
        };
    return (

      <View style={styles.container}>
        <Header info={headerProp} navigation={_that.props.navigation} />

       <View style={styles.content}>
        <View style={styles.mapcontainer}>
          <MapView style={styles.map}
                    region={this.state.region}
                    provider='google'
                    mapType='standard'
                    showsCompass={true}
                    showsPointsOfInterest
                    showsUserLocation={true} >
                    {this.state.stationList.map((marker) => (
                      <Marker
                        coordinate={{latitude:Number(marker.latitude),longitude:Number(marker.longitude)}}
                        key={marker.id}
                        title={marker.station_name}
                        opacity={0.8}
                        image={this.state.image}
                      />
                    ))}
            </MapView>
            {/*  <WebView
                source={{uri: 'https://leemansinussolutions.com/efil/map/map.php'}}
              />*/}
          </View>

          <View style={styles.formcontainer}>
          <ImageBackground source={bg_image} style={styles.innercontainer}>
          <View style={[styles.toptab,styles.shadow]}>
            <TouchableOpacity style={styles.headingbtn1} onPress={()=> {this.setState({cat:'electric'}),this.setState({sub_cat:''}),this.setState({bike:'0'}),this.setState({gadget:'0'}),this.setState({cat_id:'1'})}}>
              <Text style={styles.tab}>Electric Filling</Text>
            </TouchableOpacity>
            <TouchableOpacity style={[styles.headingbtn,styles.borderlr]}  onPress={()=> {this.setState({cat:'rentals'}),this.setState({sub_cat:''}),this.setState({bike:'0'}),this.setState({gadget:'0'}),this.setState({cat_id:'2'})}}>
              <Text style={styles.tab}>Rentals</Text>
            </TouchableOpacity>
            <TouchableOpacity style={styles.headingbtn}  onPress={()=> {this.setState({cat:'parkings'}),this.setState({sub_cat:''}),this.setState({bike:'0'}),this.setState({gadget:'0'}),this.setState({cat_id:'3'})}}>
              <Text style={styles.tab}>Parking</Text>
            </TouchableOpacity>
          </View>
          <View style={{padding:AppSizes.ResponsiveSize.Padding(3),paddingBottom:50,flexDirection:'row'}}>
           <View style={{width:'40%'}}>
             {this.state.cat == 'electric'?
               <View>
                 <TouchableOpacity style={styles.containerin} onPress={()=>{this.ServicelistBtn('car')}}>
                   <Text style={styles.texttitle}>Car</Text>
                 </TouchableOpacity>
                 <TouchableOpacity style={styles.containerin} onPress={()=>{this.ServicelistBtn('bike')}}>
                   <Text style={styles.texttitle}>Bike</Text>
                   {this.state.bike == '1'?
                    <Image source={arrow} style={{height:10,width:10}}/>
                   :
                    null
                   }
                 </TouchableOpacity>
                 <TouchableOpacity style={styles.containerin} onPress={()=>{this.ServicelistBtn('gadget')}}>
                   <Text style={styles.texttitle}>Gadgets</Text>
                   {this.state.gadget == '1'?
                    <Image source={arrow} style={{height:10,width:10}}/>
                   :
                    null
                   }
                 </TouchableOpacity>
               </View>
             :
               null
             }
             {this.state.cat == 'rentals'?
               <View>
               <TouchableOpacity style={styles.containerin} onPress={()=>{this.ServicelistBtn('car')}}>
                 <Text style={styles.texttitle}>Car</Text>
               </TouchableOpacity>
               <TouchableOpacity style={styles.containerin} onPress={() => {this.ServicelistBtn('bike')}}>
                 <Text style={styles.texttitle}>Bike</Text>
                 {this.state.bike == '1'?
                  <Image source={arrow} style={{height:10,width:10}}/>
                 :
                  null
                 }
               </TouchableOpacity>
               <TouchableOpacity style={styles.containerin} onPress={()=> {this.ServicelistBtn('gadget')}}>
                 <Text style={styles.texttitle}>Gadgets</Text>
                 {this.state.gadget == '1'?
                  <Image source={arrow} style={{height:10,width:10}}/>
                 :
                  null
                 }
               </TouchableOpacity>
               </View>
             :
               null
             }
             {this.state.cat == 'parkings'?
               <View>
               <TouchableOpacity style={styles.containerin} onPress={()=>{this.ServicelistBtn('car')}}>
                 <Text style={styles.texttitle}>Car</Text>
               </TouchableOpacity>
               <TouchableOpacity style={styles.containerin} onPress={() => {this.ServicelistBtn('bike')}}>
                 <Text style={styles.texttitle}>Bike</Text>
                 {this.state.bike == '1'?
                  <Image source={arrow} style={{height:10,width:10}}/>
                 :
                  null
                 }
               </TouchableOpacity>
               </View>
             :
               null
             }
           </View>
           <View style={{width:'60%'}}>
             {this.state.sub_cat == 'bike'?
               <View>
                 <TouchableOpacity style={styles.subCatContainer} onPress={()=>this.handleOnPress('elbike')}>
                   <Text style={styles.texttitle}>ElBikes</Text>
                 </TouchableOpacity>
                 <TouchableOpacity style={styles.subCatContainer} onPress={()=>this.handleOnPress('scooters')}>
                   <Text style={styles.texttitle}>Scooters</Text>
                 </TouchableOpacity>
                 <TouchableOpacity style={styles.subCatContainer} onPress={()=>this.handleOnPress('skateboards')}>
                   <Text style={styles.texttitle}>SkateBoards</Text>
                 </TouchableOpacity>
                 <TouchableOpacity style={styles.subCatContainer} onPress={()=>this.handleOnPress('3wheelers')}>
                   <Text style={styles.texttitle}>3 Wheelers</Text>
                 </TouchableOpacity>
               </View>
             :
               null
             }
             {this.state.sub_cat == 'gadget'?
               <View>
                 <TouchableOpacity style={styles.subCatContainer} onPress={()=>this.handleOnPress('laptops')}>
                   <Text style={styles.texttitle}>Laptops</Text>
                 </TouchableOpacity>
                 <TouchableOpacity style={styles.subCatContainer} onPress={()=>this.handleOnPress('drone')}>
                   <Text style={styles.texttitle}>Drones</Text>
                 </TouchableOpacity>
                 <TouchableOpacity style={styles.subCatContainer} onPress={()=>this.handleOnPress('mobiles')}>
                   <Text style={styles.texttitle}>Mobile Phones</Text>
                 </TouchableOpacity>
                 <TouchableOpacity style={styles.subCatContainer} onPress={()=>this.handleOnPress('pc')}>
                   <Text style={styles.texttitle}>Pc</Text>
                 </TouchableOpacity>
                 <TouchableOpacity style={styles.subCatContainer} onPress={()=>this.handleOnPress('dronefilling')}>
                   <Text style={styles.texttitle}>Filling for drones</Text>
                 </TouchableOpacity>
               </View>
             :
               null
             }
           </View>
          </View>
        </ImageBackground>
      </View>
     </View>
     <Spinner visible={this.state.isVisible}  />
    </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    paddingTop:  0,
  },

  content: {
      flex: 1,
      height:'70%',
      width:'100%'
  },
  mapcontainer:{
    flex:1,
  },
  formcontainer:{
    flex:1,
  },
  buttonContainer:{
     justifyContent: 'center',
     marginBottom:AppSizes.ResponsiveSize.Padding(3),
     width: '100%',
     borderWidth:1,
     borderColor:'#302334',
     flexDirection:'row',
     alignItems: 'center',
     justifyContent:'center',
     backgroundColor:'#302334',
   },
   buttonimgContainer:{
     width: '40%',
     height:AppSizes.screen.width/6,
     backgroundColor:'red',
   },
   buttontextContainer:{
     width: '60%',
     marginLeft:AppSizes.ResponsiveSize.Padding(3),
     justifyContent:'center',
     alignItems: 'flex-start',
     backgroundColor:'blue',
   },
   btnimg:{
     flex: 1,
     width: undefined,
     height: undefined,
     resizeMode:'contain',
   },
   imageWrapper: {
     width:'100%',
     height:AppSizes.screen.width/8,
   },
   image: {
     flex: 1,
     width: undefined,
     height: undefined,
     resizeMode:'contain'
   },
   innercontainer:{
     width: '100%',
     height: '100%',
     resizeMode:'contain',
   },
   containerin: {
    height:30,
    padding: AppSizes.ResponsiveSize.Padding(2),
    flexDirection: 'row',
    alignItems: 'center',
    width:'100%',
    paddingBottom:10,
    //backgroundColor:'green',
    marginBottom:10
  },
  texttitle: {
    marginLeft: AppSizes.ResponsiveSize.Padding(4),
    paddingBottom: AppSizes.ResponsiveSize.Padding(2),
    fontSize:AppSizes.ResponsiveSize.Sizes(18),
    fontWeight:'500',
    color:'#ffffff',
    width:'60%'
  },
  textmiles:{
    marginLeft: AppSizes.ResponsiveSize.Padding(4),
    paddingBottom: AppSizes.ResponsiveSize.Padding(2),
    fontSize:AppSizes.ResponsiveSize.Sizes(18),
    fontWeight:'500',
    color:'#ffffff',
    width:'40%'
  },
  text1: {
    marginLeft: AppSizes.ResponsiveSize.Padding(4),
     fontSize:AppSizes.ResponsiveSize.Sizes(12),
    color:'#ffffff',
  },
  photo: {
    flex: 1,
    width: undefined,
    height: undefined,
},
map: {
    ...StyleSheet.absoluteFillObject,
},
tab:{
  color:'#fff',
  fontSize:AppSizes.ResponsiveSize.Sizes(20),
  paddingLeft:AppSizes.ResponsiveSize.Padding(4),
  paddingRight:AppSizes.ResponsiveSize.Padding(4),
  //backgroundColor:'green',
  paddingTop:AppSizes.ResponsiveSize.Padding(2),
  paddingBottom:AppSizes.ResponsiveSize.Padding(2),
  textAlign:'center'

},
border:{
  height:2,
  backgroundColor:'#fff'
},
mainm:{
  paddingBottom:70,
  //backgroundColor:'red',
  width:'100%',
  alignItems:'center'
},
text:{
  marginLeft: AppSizes.ResponsiveSize.Padding(4),
  fontSize:AppSizes.ResponsiveSize.Sizes(15),
  color:'#ffffff',
},
subCatContainer:{
   height:30,
   padding: AppSizes.ResponsiveSize.Padding(2),
   flexDirection: 'row',
   alignItems: 'center',
   width:'100%',
   paddingBottom:10,
   //backgroundColor:'green',
   marginBottom:10
},
headingbtn:{
  //backgroundColor:'red',
  width:'30%',
  height:50,
  justifyContent:'center',
  alignItems:'center',
},
headingbtn1:{
  //backgroundColor:'red',
  height:50,
  justifyContent:'center',
  alignItems:'center',
  width:'40%',
},
shadow:{
  borderWidth:1,
  borderRadius: 2,
  borderColor: '#fff',
  justifyContent:'center',
  borderColor: '#fff',
  borderWidth: 0,
  shadowColor: '#fff',
  shadowOffset: { width: 2, height: 1 },
  shadowOpacity: 1,
  shadowRadius: 2,
  elevation: 10,
  zIndex:0,
  backgroundColor:'#1e0730',
},
toptab:{
flexDirection:'row',
alignItems:'center',
width:'100%'
},
borderlr:{
borderLeftWidth:1,
  borderRightWidth:1,
  borderColor:'#463057'
}
});
