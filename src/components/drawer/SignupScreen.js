import React, { Component } from 'react';
import {StyleSheet,Text,View,Image,TouchableOpacity,Platform,ScrollView,Alert,ImageBackground,StatusBar,TextInput,Keyboard,AsyncStorage} from 'react-native';
import { AppStyles, AppSizes, AppColors } from '../../themes/'
import { NavigationActions } from 'react-navigation'
import Spacer from '../common/Spacer'
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view'
import Header from '../common/HeaderBeforeLogin'
import CommonButton from '../common/CommonButton1'
import * as commonFunctions from '../../utils/CommonFunctions'
import {RadioGroup, RadioButton} from 'react-native-flexi-radio-button'
import Picker from 'react-native-q-picker';
import CheckBox from 'react-native-check-box';
import Spinner from 'react-native-loading-spinner-overlay';
import Geocoder from 'react-native-geocoder';

import { Fonts } from '../../utils/Fonts';

var Constant = require('../../api/ApiRules').Constant;
var WebServices = require('../../api/ApiRules').WebServices;

const bg_image = require('../../themes/Images/bg_image.png')
const left_arrow = require('../../themes/Images/left-arrow.png')
const profile_icon = require('../../themes/Images/profile.png')
const email_icon = require('../../themes/Images/email.png')
const password_icon = require('../../themes/Images/password.png')
const device_icon = require('../../themes/Images/car.png')
const modal_icon = require('../../themes/Images/modal.png')
const mobile_icon = require('../../themes/Images/mobile.png')

const MyStatusBar = ({backgroundColor, ...props}) => (
  <View style={[styles.statusBar, { backgroundColor }]}>
    <StatusBar translucent backgroundColor={backgroundColor} {...props} />
  </View>
);
let _that
let seller_services=[]

export default class SignupScreen extends Component {
  constructor (props) {
    super(props)
    this.state = {
    uid:'',
    user_name:'',
    email:'',
    password:'',
    mobile_number:'',
    user_type:'',
    devicetype:'',
    show_buyer_section:false,
    show_seller_section:false,
    gadgets_data:[],
    deviceitems:[],
    device_type: '',
    modal_type: '',
    scooter:false,
    car:false,
    gadgets:false,
    parking_lots:false,
    isChecked:true,
    isVisible: false,
    Modal_Visibility: false,
    laptop:false,
    drone:false,
    filling:false,
    mobile:false,
    pc:false,
    email : '',
    password : '',
    nameerr :'',
    emailerr :'',
    pwderr :'',
    typeerr :'',
  },
  _that = this;
}

  componentDidMount() {
    try {
    Geocoder.fallbackToGoogle('AIzaSyBX6rKXe6Jsk6ZynShEZiNfDfyhZWgmXsQ');
     navigator.geolocation.getCurrentPosition(
      (position) => {
        var region = {
          lat: position.coords.latitude,
          lng:  position.coords.longitude,
        };
        Geocoder.geocodePosition(region).then(res => {
          console.log(res)
          var address=res[0].formattedAddress
          _that.setState({
            latitude: position.coords.latitude,
            longitude: position.coords.longitude,
            address: address,
          });
          var country=res[0].country
          _that.setState({country:country})
          var zipcode=res[0].postalCode
          _that.setState({zipcode:zipcode})
        })
        _that.validationAndApiParameter('signup_data')
      },
      (error) => console.log('error '+error.message),
      {enableHighAccuracy: false, timeout: 50000, maximumAge: 10000}
    );
  }
  catch(err) {
      console.log(err);
  }
}


componentWillMount(){
  if(Platform.OS === 'ios'){
    this.setState({devicetype:'1'})
  }
  else{
    this.setState({devicetype:'0'})
  }

  AsyncStorage.getItem('onScreen').then((on) => {
    var data = on;
    console.log(data);
    if(data=='FilledForm'){
      _that.props.navigation.navigate('RegistrationTc')
    }
    else if (data=='AcceptedTc') {
      _that.props.navigation.navigate('VerifyOtpScreen')
    }
  })
}

onSelect(index, value){
  this.setState({typeerr:''})
  this.setState({user_type: value})
  if(value=='buyer'){
    seller_services=[]
      this.setState({
        show_buyer_section: true,
        show_seller_section: false,
        scooter:false,
        car:false,
        parking_lots:false,
        gadgets:false,
        laptop:false,
        drone:false,
        filling:false,
        mobile:false,
        pc:false
      })
  }
  else if(value=='seller'){
    this.setState({user_type: value})
    this.setState({
      show_buyer_section: false,
      show_seller_section: true,
      device_type:'',
      scooter:false,
      car:false,
      parking_lots:false,
      gadgets:false,
      laptop:false,
      drone:false,
      filling:false,
      mobile:false,
      pc:false
    })
  }
  else if(value=='both'){
    this.setState({user_type:'buyer'})
    this.setState({
      show_buyer_section: true,
      show_seller_section: true,
      scooter:false,
      device_type:'',
      scooter:false,
      car:false,
      parking_lots:false,
      gadgets:false,
      laptop:false,
      drone:false,
      filling:false,
      mobile:false,
      pc:false
    })
  }
}

arrow_click = () => {
      _that.props.navigation.navigate('MainScreen');
  }

login_click=() =>{
  _that.props.navigation.navigate('LoginScreen');
  this.setState({user_type: ''})
  this.setState({show_buyer_section: false});
  this.setState({show_seller_section: false});
  this.setState({nameerr:''});
  this.setState({emailerr:''});
  this.setState({pwderr:''});
  this.setState({typeerr:''});
}

submit_click(){
  _that.validationAndApiParameter('signup')
}

validationAndApiParameter(apiKey) {

  const { method, devicetoken, devicetype, user_name,email, password, mobile_number,user_type,device_type,modal_type,scooter,car,parking_lots,gadgets,isVisible } = this.state
  var error=0;

  if(apiKey == 'signup'){
    if ((user_name.length <= 0)) {
      _that.setState({nameerr:'Please enter your name'})
      error=1;
    }
    else{
      _that.setState({nameerr:''})
    }

    if ((email.indexOf(' ') >= 0 || email.length <= 0)) {
      _that.setState({emailerr:'Please enter Email address'})
      error=1;
    }else if (!commonFunctions.validateEmail(email)) {
      _that.setState({emailerr:'Please enter valid Email address'})
      error=1;
    }
    else{
        _that.setState({emailerr:''})
    }

    if ((password.indexOf(' ') >= 0 || password.length <= 0)) {
      _that.setState({pwderr:'Please enter password'})
      error=1;
    }
    else{
        _that.setState({pwderr:''})
    }

    if ((user_type.length <= 0)) {
      _that.setState({typeerr:'Please enter type'})
      error=1;
    }
    else{
      _that.setState({typeerr:''})
    }
    if(error==0) {
      Keyboard.dismiss()
      if(car==true){
        seller_services.push('car')
      }
      if(scooter==true){
        seller_services.push('scooter')
      }
      if(parking_lots==true){
        seller_services.push('parking_lots')
      }
      if(gadgets==true){
        seller_services.push('gadgets')
      }
      var json_seller_services = seller_services.toString();
      //console.log(json_seller_services);
      const data = new FormData();
      data.append('device_token', devicetoken);
      data.append('device_type', devicetype);
      data.append('name', user_name);
      data.append('email', email);
      data.append('password', password);
      data.append('telephone', mobile_number);
      data.append('user_type', user_type);
      data.append('buyer_device_type', device_type);
      data.append('buyer_device_modal', modal_type);
      data.append('seller_services', json_seller_services);
      data.append('address',_that.state.address)
      data.append('zip_code',_that.state.zipcode)
      data.append('country',_that.state.country)

      //console.log(data);
      _that.setState({isVisible: true});
      this.postToApiCalling('POST', apiKey, Constant.URL_register, data);
    }
  }
  else if(apiKey == 'signup_data'){
    const data1 = new FormData();
    data1.append('uid',5)
    this.postToApiCalling('POST', apiKey, Constant.URL_signupData,data1);
  }
}

postToApiCalling(method, apiKey, apiUrl, data) {

   new Promise(function(resolve, reject) {
        if (method == 'POST') {
            resolve(WebServices.callWebService(apiUrl, data));
        } else {
            resolve(WebServices.callWebService_GET(apiUrl, data));
        }
    }).then((jsonRes) => {
      _that.setState({ isVisible: false })
      //console.log('response',jsonRes)
      if ((!jsonRes) || (jsonRes.code == 0)) {
        //_that.setState({ isVisible: false })
        setTimeout(()=>{
            Alert.alert(jsonRes.message);
        },200);
      } else {
         if (jsonRes.code == 1) {
            _that.apiSuccessfullResponse(apiKey, jsonRes)
          }
        }
    }).catch((error) => {
        console.log("ERROR" + error);
        _that.setState({ isVisible: false })

        setTimeout(()=>{
            Alert.alert("Server issue" + error);
        },200);
    });
  }

    apiSuccessfullResponse(apiKey, jsonRes) {
        if (apiKey == 'signup') {
            jdata=jsonRes.data;
            seller_services=[]
            //AsyncStorage.setItem("loggedIn", JSON.stringify(true)).done();
            AsyncStorage.setItem("user_type", JSON.stringify(jdata.user_type));
            AsyncStorage.setItem('UserData', JSON.stringify(jsonRes.data));
            AsyncStorage.setItem('onScreen','FilledForm')
            _that.props.navigation.navigate('VerifyOtpScreen');
        }
        if (apiKey == 'signup_data'){
          buyer_data=jsonRes.buyer_data
          gadgets_data=jsonRes.gadgets_data
          //console.log(jsonRes);

          buyer_data.map(data => this.state.deviceitems.push({name:data.name,id:parseInt(data.id)}))
          gadgets_data.map(data => this.state.gadgets_data.push({name:data.name,id:parseInt(data.id)}))
        }
    }

  render() {

    return (
      <View style={styles.wrapper}>
      <MyStatusBar barStyle="light-content"  backgroundColor="#00469a"/>

        <ImageBackground source={bg_image} style={{width: '100%', height: '100%'}}>
        <View style={styles.appBar} >
              <View style={styles.imageContainer}>
                <TouchableOpacity style={styles.menuWrapper} onPress={this.arrow_click}>
                  <Image style={styles.image} source={left_arrow}/>
                  </TouchableOpacity>
              </View>

        </View>

        <View style={{flex:1}}>
        <KeyboardAwareScrollView innerRef={() => {return [this.refs.user_name,this.refs.email, this.refs.password]}} >


        <View style={styles.container1}>
          <Text style={styles.headerTitle}>Sign up</Text>
          <View style={styles.headerBorder}/>
        </View>

          <View style={styles.container2}>

          <View style={styles.SectionStyle}>
              <Image source={profile_icon} style={styles.ImageStyle} />
              <TextInput
                autoCapitalize={'none'}
                autoCorrect={false}
                underlineColorAndroid="transparent"
                returnKeyType={ "next"}
                selectionColor={"#FFFFFF"}
                autoFocus={ false}
                placeholder="Full Name"
                placeholderTextColor="#fff"
                style={styles.textInput}
                ref="name"
                keyboardType={ 'default'}
                onFocus={ () => this.setState({nameerr:''}) }
                onChangeText={user_name=> this.setState({user_name})}
              />
          </View>
          { !(this.state.nameerr) ? null :
            <Text style={styles.error}>{this.state.nameerr}</Text>
          }

          <View style={styles.SectionStyle}>
              <Image source={email_icon} style={styles.ImageStyle} />
              <TextInput
                autoCapitalize={'none'}
                autoCorrect={false}
                underlineColorAndroid="transparent"
                returnKeyType={ "next"}
                selectionColor={"#FFFFFF"}
                autoFocus={ false}
                placeholder="Email Address"
                placeholderTextColor="#fff"
                style={styles.textInput}
                ref="email"
                keyboardType={ 'email-address'}
                onFocus={ () => this.setState({emailerr:''}) }
                onChangeText={email=> this.setState({email})}
              />
          </View>
          { !(this.state.emailerr) ? null :
            <Text style={styles.error}>{this.state.emailerr}</Text>
          }

          <View style={styles.SectionStyle}>
              <Image source={password_icon} style={styles.ImageStyle} />
              <TextInput
                autoCapitalize={'none'}
                autoCorrect={false}
                underlineColorAndroid="transparent"
                returnKeyType={ "next"}
                selectionColor={"#FFFFFF"}
                autoFocus={ false}
                placeholder="Password"
                placeholderTextColor="#fff"
                style={styles.textInput}
                keyboardType={ 'default'}
                ref="password"
                secureTextEntry={true}
                onFocus={ () => this.setState({pwderr:''}) }
                onChangeText={password=> this.setState({password})}
              />
          </View>
          { !(this.state.pwderr) ? null :
            <Text style={styles.error}>{this.state.pwderr}</Text>
          }

          <View style={styles.SectionStyle}>
              <Image source={mobile_icon} style={styles.ImageStyle} />
              <TextInput
                autoCapitalize={'none'}
                autoCorrect={false}
                underlineColorAndroid="transparent"
                returnKeyType={ "next"}
                selectionColor={"#FFFFFF"}
                autoFocus={ false}
                placeholder="Mobile Number"
                placeholderTextColor="#fff"
                style={styles.textInput}
                ref="mobile"
                keyboardType={ 'number-pad'}
                onChangeText={mobile_number=> this.setState({mobile_number})}
              />
          </View>

          <View style={styles.SectionRadioView}>
              <RadioGroup
                color='#ffffff'
                style={styles.SectionRadioGroup}
                onSelect = {(index, value) => this.onSelect(index, value)}
              >
                <RadioButton value={'buyer'}>
                  <Text style={{color:'#ffffff'}}>Buyer</Text>
                </RadioButton>

                <RadioButton value={'seller'}>
                  <Text style={{color:'#ffffff'}}>Seller</Text>
                </RadioButton>

                <RadioButton value={'both'}>
                  <Text style={{color:'#ffffff'}}>Both</Text>
                </RadioButton>
              </RadioGroup>

          </View>
          { !(this.state.typeerr) ? null :
            <Text style={styles.error}>{this.state.typeerr}</Text>
          }

          { (!this.state.show_buyer_section) ? null :
            <View style={{width:AppSizes.screen.width}}>
            <View style={styles.SectionRadioView}>
                <Picker PickerData={this.state.deviceitems}
                 label={'Select Device'}
                 color={'#ffffff'}

                 getTxt={(val,label)=>this.setState({device_type: label})}/>
            </View>

            {/*<View style={styles.SectionRadioView}>
                <Picker PickerData={this.state.modalitems}
                 label={'Select Device Modal'}
                 color={'#ffffff'}
                 getTxt={(val,label)=>this.setState({modal_type: label})}/>
            </View>*/}
          </View>
          }

          { (!this.state.show_seller_section) ? null :
            <View style={{width:AppSizes.screen.width}}>
              <View style={styles.SectionRadioView}>
                <CheckBox
                  style={styles.checkboxSection}
                  checkBoxColor={"#ffffff"}
                  onClick={()=>{this.setState({scooter:!this.state.scooter});}}
                  isChecked={this.state.scooter}
                  rightText={"El-scooter"}
                  rightTextStyle={{color:"#ffffff"}}
                />
                <CheckBox
                  style={styles.checkboxSection}
                  checkBoxColor="#ffffff"
                  onClick={()=>{this.setState({car:!this.state.car})}}
                  isChecked={this.state.car}
                  rightText={"El-car"}
                  rightTextStyle={{color:"#ffffff"}}
                />
                <CheckBox
                  style={styles.checkboxSection}
                  checkBoxColor="#ffffff"
                  onClick={()=>{this.setState({parking_lots:!this.state.parking_lots});}}
                  isChecked={this.state.parking_lots}
                  rightText={"Parking lots"}
                  rightTextStyle={{color:"#ffffff"}}
                />
                <CheckBox
                  style={styles.checkboxSection}
                  checkBoxColor="#ffffff"
                  onClick={()=>{this.setState({gadgets:!this.state.gadgets});}}
                  isChecked={this.state.gadgets}
                  rightText={"Gadgets"}
                  rightTextStyle={{color:"#ffffff"}}
                />
              </View>
            </View>
          }

          {/*this.state.gadgets?
            <View style={{width:AppSizes.screen.width}}>
              <View style={styles.SectionRadioView}>
                <CheckBox
                  style={styles.checkboxSection}
                  checkBoxColor="#ffffff"
                  rightText={"Laptops"}
                  rightTextStyle={{color:"#ffffff"}}
                  onClick={()=>{this.setState({laptop:!this.state.laptop});}}
                  isChecked={this.state.laptop}
                />
                <CheckBox
                  style={styles.checkboxSection}
                  checkBoxColor="#ffffff"
                  rightText={"Drone"}
                  rightTextStyle={{color:"#ffffff"}}
                  onClick={()=>{this.setState({drone:!this.state.drone});}}
                  isChecked={this.state.drone}
                />
                <CheckBox
                  style={styles.checkboxSection}
                  checkBoxColor="#ffffff"
                  rightText={"Filling to Drone"}
                  rightTextStyle={{color:"#ffffff"}}
                  onClick={()=>{this.setState({filling:!this.state.filling});}}
                  isChecked={this.state.filling}
                />
                <CheckBox
                  style={styles.checkboxSection}
                  checkBoxColor="#ffffff"
                  rightText={"Mobile Phones"}
                  rightTextStyle={{color:"#ffffff"}}
                  onClick={()=>{this.setState({mobile:!this.state.mobile});}}
                  isChecked={this.state.mobile}
                />
                <CheckBox
                  style={styles.checkboxSection}
                  checkBoxColor="#ffffff"
                  rightText={"PC"}
                  rightTextStyle={{color:"#ffffff"}}
                  onClick={()=>{this.setState({pc:!this.state.pc});}}
                  isChecked={this.state.pc}
                />
              </View>
            </View>
            :
            null
          */}

          <TouchableOpacity onPress={this.submit_click} style={styles.btncon}>
            <CommonButton label='Signup' width='100%'/>
          </TouchableOpacity>
          </View>
          </KeyboardAwareScrollView>
            </View>

          <View style={styles.container3}>
            <TouchableOpacity onPress={this.login_click}>
              <Text style={styles.footertext}>Already Have an account?
                <Text style={{fontWeight:'bold'}}> Login </Text></Text>
            </TouchableOpacity>
          </View>

        </ImageBackground>
        <Spinner visible={this.state.isVisible}  />
     </View>

    );
  }
}

const styles = StyleSheet.create({
  wrapper: {
    flex: 1,
    backgroundColor:'#ffffff'
  },
  statusBar: {
    height: AppSizes.statusBarHeight,
  },

  appBar: {
      height: AppSizes.navbarHeight,
      justifyContent:'flex-end',
      alignItems:'flex-start',
      paddingLeft:AppSizes.ResponsiveSize.Padding(5),
    },
    imageContainer:{
      width:'20%'
    },
    menuWrapper: {
      width:'60%',
      height:'60%',
    },
   image: {
      flex: 1,
      width: undefined,
      height: undefined,
      resizeMode:'contain'
    },

    container1: {
      flex: 3,
      height:AppSizes.screen.width/4,
      paddingLeft:AppSizes.ResponsiveSize.Padding(5),
      //backgroundColor:'red'
    },

    container2: {
      flex: 10,
      //height:'70%',
      flexDirection: 'column',
      alignItems: 'center',//replace with flex-end or center
      justifyContent: 'center',
      //backgroundColor:'blue'
    },
    container3: {
      flex: .1,
      //height:AppSizes.screen.width/5,
      alignItems: 'center',//replace with flex-end or center
      justifyContent: 'center',
      //backgroundColor:'red'
    },
    headerTitle:{
      fontSize:AppSizes.ResponsiveSize.Sizes(35),
      color:'#ffffff',
      fontWeight:'bold',
      paddingBottom:AppSizes.ResponsiveSize.Padding(3),
        fontFamily:Fonts.RobotoBold
    },
  headersubTitle:{
    fontSize:AppSizes.ResponsiveSize.Sizes(14),
    color:'#ffffff',
    fontWeight:'600',
    paddingTop:AppSizes.ResponsiveSize.Padding(2),
      fontFamily:Fonts.RobotoBold
  },
  headerBorder :{
    borderBottomColor: '#ffffff',
    borderBottomWidth: 3,
    width:'10%',
 },
 footertext :{
   color :'#ffffff',
   fontSize:AppSizes.ResponsiveSize.Sizes(12),
   fontWeight:'400',
     fontFamily:Fonts.RobotoRegular
 },
 SectionStyle: {
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    borderBottomWidth: 1,
    borderBottomColor: '#ffffff',
    borderRadius: 5 ,
    marginLeft: AppSizes.ResponsiveSize.Padding(5),
    marginRight: AppSizes.ResponsiveSize.Padding(5),
    marginBottom: (Platform.OS === 'ios') ? AppSizes.ResponsiveSize.Padding(1) :AppSizes.ResponsiveSize.Padding(2),
    height:AppSizes.screen.width/8,
},

SectionRadioView: {
  borderBottomWidth: 1,
  borderBottomColor: '#ffffff',
  borderRadius: 5 ,
  width:AppSizes.screen.width*90/100,
  marginLeft: AppSizes.ResponsiveSize.Padding(5),
  marginRight: AppSizes.ResponsiveSize.Padding(5),
  marginBottom: (Platform.OS === 'ios') ? AppSizes.ResponsiveSize.Padding(1) :AppSizes.ResponsiveSize.Padding(2),
    fontFamily:Fonts.RobotoRegular
},
SectionRadioGroup:{
  flexDirection:'row',
},
ImageStyle: {
    height: AppSizes.screen.width/21,
    width: AppSizes.screen.width/21,
    resizeMode : 'contain',
},
textInput: {
  flex:1,
  marginLeft: AppSizes.ResponsiveSize.Padding(2),
  fontSize: AppSizes.ResponsiveSize.Sizes(14),
  color:'#ffffff',
    fontFamily:Fonts.RobotoRegular
},
btncon:{
    marginLeft: AppSizes.ResponsiveSize.Padding(5),
    marginRight: AppSizes.ResponsiveSize.Padding(5),
    marginTop: AppSizes.ResponsiveSize.Padding(2),
    height:AppSizes.screen.width/5,
  },
checkboxSection :{
  marginBottom: AppSizes.ResponsiveSize.Padding(2),
  height:AppSizes.screen.width/15,
  fontFamily:Fonts.RobotoRegular
},
error:{
  color:'red',
    fontFamily:Fonts.RobotoRegular
},


});
