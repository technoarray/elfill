import React, { Component } from 'react';
import {StyleSheet,Text,View,Image,TouchableOpacity,Platform,Alert,ScrollView} from 'react-native';
import { AppStyles, AppSizes, AppColors } from '../../themes/'
import { NavigationActions } from 'react-navigation'
import Spacer from '../common/Spacer'
import * as commonFunctions from '../../utils/CommonFunctions'
import Header from '../common/HeaderWithBackOnly'

import { Fonts } from '../../utils/Fonts';

var Constant = require('../../api/ApiRules').Constant;
var logo = require('../../themes/Images/logoc.png')
var map = require('../../themes/Images/map.png')


/* Images */

let _that

export default class AboutScreen extends Component {
  constructor (props) {
      super(props)
      this.state = {
      isVisible: false,
      Modal_Visibility: false
    },
    _that = this;
  }



  render() {
    const headerProp = {
      title: 'About Us',
      screens: 'AboutScreen',
    };
    return (
      <View style={styles.container}>
      <Header info={headerProp} navigation={_that.props.navigation}/>

        <View style={styles.content}>

          <View style={styles.logobox}>
                <Image style={styles.logo} source={logo} />
          </View>
          <View style={styles.box3}>
            <ScrollView showsVerticalScrollIndicator={false}>
              <Text allowFontScaling={false}  style={styles.textcontent}>
                Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industrys standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.
                  Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industrys standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.
              </Text>

            </ScrollView>
          </View>
          <View style={styles.box4}>
                  <Image style={styles.map} source={map} />
          </View>
        </View>
    </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    paddingTop:0,
    //backgroundColor:'green'
  },

  content: {
      flex: 1,
      flexDirection: 'column',
      height:'100%',
      backgroundColor:'#ffffff'
  },
  title:{
    color:AppColors.primary,
    textAlign:'center',
    fontSize:AppSizes.ResponsiveSize.Sizes(20),
    fontWeight:'600',
    fontFamily:Fonts.RobotoBold,
  },
  texturl :{
    textAlign:'center',
    fontWeight:'bold',
    fontSize:AppSizes.ResponsiveSize.Sizes(18),
    color:AppColors.primary,
    fontFamily:Fonts.RobotoBold,
  },
  box3: {
    //backgroundColor: 'red',
    padding:20,
    height:'60%'
  },
  image: {
    flex: 1,
    width: undefined,
    height: undefined,
    resizeMode:'contain'
  },
 textcontent:{
   color:AppColors.contentColor,
   textAlign:'center',
   fontSize:AppSizes.ResponsiveSize.Sizes(13),
   lineHeight: AppSizes.ResponsiveSize.Sizes(13 * 1.70),
   fontWeight:'300',
   fontFamily:Fonts.RobotoRegular,
  },
  about_padding : {
    paddingTop:'5%'
  },
  logobox:{
    width:'100%',
    backgroundColor:'#ddd',
    alignItems:'center',
    justifyContent:'center',
    height:AppSizes.screen.height/6,
  },
  logo:{
    width:AppSizes.screen.width/2,
    height:AppSizes.screen.height/10
  },
  map:{
    width:'100%',
    height:'100%'
  },
  box4:{
    justifyContent:'flex-end',
    flex:1
  }
});
