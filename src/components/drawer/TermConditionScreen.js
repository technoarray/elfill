import React, { Component } from 'react';
import {StyleSheet,Text,View,Image,TouchableOpacity,Platform,Alert,ScrollView} from 'react-native';
import { AppStyles, AppSizes, AppColors } from '../../themes/'
import { NavigationActions } from 'react-navigation'
import Spacer from '../common/Spacer'
import * as commonFunctions from '../../utils/CommonFunctions'
import Header from '../common/HeaderWithBackOnly'
import { Fonts } from '../../utils/Fonts';
var Constant = require('../../api/ApiRules').Constant;

/* Images */
var map = require('../../themes/Images/password.png')
var logo = require('../../themes/Images/logoc.png')

let _that

export default class TermConditionScreen extends Component {
  constructor (props) {
      super(props)
      this.state = {
      isVisible: false,
      Modal_Visibility: false
    },
    _that = this;
  }



  render() {
    const headerProp = {
      title: 'Terms & Condition',
      screens: 'TermConditionScreen',
    };
    return (
      <View style={styles.container}>
      <Header info={headerProp} navigation={_that.props.navigation}/>

        <View style={styles.content}>

            {/*<View style={[styles.box2,{height:'30%'}]}>
              <View style={{width:'100%',height:'80%',paddingTop:'5%'}}>
                <Image source={map} style={styles.image}/>
              </View>
            </View> */}
              <View style={styles.logobox}>
                    <Image style={styles.logo} source={logo} />
              </View>

            <View style={styles.box3}>
            <ScrollView>
              <Text style={styles.textcontent}>
                Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industrys standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.
               </Text>

              </ScrollView>
            </View>

        </View>
    </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    paddingTop:  0,
  },

  content: {
      flex: 1,
      flexDirection: 'column',
      height:'100%',
      backgroundColor:'#ffffff'
  },
  title:{
    color:AppColors.primary,
    textAlign:'center',
    fontSize:AppSizes.ResponsiveSize.Sizes(20),
    fontWeight:'600',
      fontFamily:Fonts.RobotoBold,
  },
  texturl :{
    textAlign:'center',
    fontWeight:'bold',
    fontSize:AppSizes.ResponsiveSize.Sizes(18),
    color:AppColors.primary,
    fontFamily:Fonts.RobotoBold,
  },

  box1: {
  //  backgroundColor: '#fff'
  },
  box2: {
  //  backgroundColor: '#fff'
  },
  box3: {
  //  backgroundColor: '#fff'
  paddingLeft:20,
  paddingRight:20,
  },
  image: {
    flex: 1,
    width: undefined,
    height: undefined,
    resizeMode:'contain'
  },

 textcontent:{
   color:AppColors.contentColor,
   textAlign:'center',
   fontSize:AppSizes.ResponsiveSize.Sizes(13),
   lineHeight: AppSizes.ResponsiveSize.Sizes(13 * 1.70),
   fontWeight:'300',
   fontFamily:Fonts.RobotoRegular,
},

about_padding : {
  paddingTop:'5%'
},
logobox:{
  width:'100%',
  backgroundColor:'#ddd',
  alignItems:'center',
  justifyContent:'center',
  height:AppSizes.screen.height/4,
  marginBottom:AppSizes.ResponsiveSize.Padding(3)
},
logo:{
  width:AppSizes.screen.width/2,
  height:AppSizes.screen.height/10

}

});
