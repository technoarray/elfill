import React, { Component } from 'react';
import {StyleSheet,Text,View,Image,TouchableOpacity,Platform,Alert,ScrollView,TextInput} from 'react-native';
import { AppStyles, AppSizes, AppColors } from '../../themes/'
import { NavigationActions } from 'react-navigation'
import Spacer from '../common/Spacer'
import * as commonFunctions from '../../utils/CommonFunctions'
import Header from '../common/HeaderWithBack'
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view'
import CommonButton from '../common/CommonButton'
import LinearGradient from 'react-native-linear-gradient';

var Constant = require('../../api/ApiRules').Constant;

/* Images */
var mobile= require( '../../themes/Images/mobile.png')


export default class PaymentsScreen extends Component {
  constructor (props) {
      super(props)
      this.state = {
      isVisible: false,
      uid:'',
      dataSource: [],
    },
    _that = this;
  }


  componentWillMount(){
    paymentlist=this.props.navigation.state.params.paymentlist
    _that.setState({
       dataSource: paymentlist,
     });
     console.log(paymentlist);
  }

  viewBtn=(item)=>{
     _that.props.navigation.navigate('ViewPaymentScreen',{item:item});
  }


  render() {
    const headerProp = {
      title: 'Payment',
      screens: 'PaymentsScreen',
      type:''
    };
    //console.log(this.state.dataSource)
    return (
      <View style={styles.container}>
      <Header info={headerProp} navigation={_that.props.navigation}/>

        <View style={styles.content}>
        {this.state.dataSource && this.state.dataSource.length > 0 ?
        <ScrollView>
            <View style={styles.container1}>
            {
              this.state.dataSource.map((item, index) => (
                <LinearGradient colors={['#3995f7','#483158','#544658','#5a585b']} key={item.id} style={[styles.linearGradient,styles.servicebox]}>
                      <TouchableOpacity activeOpacity={.6} onPress={() => this.viewBtn(item)} style={styles.titleblk}>
                        <Text style={styles.title}>{item.cat_name}</Text>
                        <Text style={styles.smalltext}>#{item.order_id},     ${item.price}.00</Text>
                      </TouchableOpacity>
                </LinearGradient>
              ))
           }
            </View>

  </ScrollView>
  :
  <Text style={{textAlign:'center'}}>No Record Found</Text>
 }
        </View>
    </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    paddingTop:  0,
  },

  content: {
      flex: 1,
      padding:'5%',
      backgroundColor:'#ffffff'
  },
  container1: {
    flex: 1,

    flexDirection: 'column',
  },
  servicebox:{
    flexDirection:'row',
    padding:AppSizes.ResponsiveSize.Sizes(15),
    paddingBottom:AppSizes.ResponsiveSize.Sizes(20),
    borderRadius: 5,
    marginBottom:AppSizes.ResponsiveSize.Padding(2)
  },
  titleblk:{
    width:'100%',
    height:'100%'

  },
  sideicon:{
      width:'30%',
      alignItems: 'center',
      flexDirection:'row',
      justifyContent: 'center',
  },
  ImageStyle: {
    height: AppSizes.screen.width/15,
    width: AppSizes.screen.width/15,
    resizeMode : 'contain',
  },
  title:{
    fontSize:AppSizes.ResponsiveSize.Sizes(20),
    paddingBottom: AppSizes.ResponsiveSize.Sizes(5),
    color:'#fff',
    fontWeight:'700',
  },
  smalltext:{
    fontSize:AppSizes.ResponsiveSize.Sizes(10),
    color:'#fff',
    fontWeight:'400'
  },
  contentContainer: {
      paddingVertical: 20,
    },

    icon:{

      padding:AppSizes.ResponsiveSize.Padding(5),
    }

});
