import React, { Component } from 'react';
import {AsyncStorage,StyleSheet,Text,View,Image,TouchableOpacity,Platform,Alert,ScrollView,TextInput} from 'react-native';
import { AppStyles, AppSizes, AppColors } from '../../themes/'
import { NavigationActions } from 'react-navigation'
import Spacer from '../common/Spacer'
import * as commonFunctions from '../../utils/CommonFunctions'
import Header from '../common/Header'
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view'
import CommonButton from '../common/CommonButton'
import LinearGradient from 'react-native-linear-gradient';
import Spinner from 'react-native-loading-spinner-overlay';
import { Fonts } from '../../utils/Fonts';
var Constant = require('../../api/ApiRules').Constant;
var WebServices = require('../../api/ApiRules').WebServices;

/* Images */
var mobile= require( '../../themes/Images/mobile.png')

let _that
export default class PaymentsScreen extends Component {
  constructor (props) {
      super(props)
      this.state = {
      isVisible: false,
      uid:'',
      dataSource: [],
    },
    _that = this;
  }


  componentWillMount(){
    AsyncStorage.getItem('UserData').then((UserData) => {
      var data = JSON.parse(UserData);
        uid=data.id
        //console.log(uid)
        if(uid != ''){
          _that.setState({uid:uid})
          _that.validationAndApiParameter(uid,'paymentlist')
        }
    })
  }

  validationAndApiParameter(uid,apiKey){
    if(apiKey=='paymentlist'){
      const data = new FormData();
      data.append('uid', uid);
       console.log(data);
      _that.setState({isVisible: true});

     _that.postToApiCalling('POST', apiKey, Constant.URL_payment, data);
    }
  }

  postToApiCalling(method, apiKey, apiUrl, data) {

     new Promise(function(resolve, reject) {
          if (method == 'POST') {
              resolve(WebServices.callWebService(apiUrl, data));
          } else {
              resolve(WebServices.callWebService_GET(apiUrl, data));
          }
      }).then((jsonRes) => {
        _that.setState({ isVisible: false })
        console.log(jsonRes);
          if ((!jsonRes) || (jsonRes.code == 0)) {
          setTimeout(()=>{
              Alert.alert('Error',jsonRes.message);
          },200);
          } else {
              _that.apiSuccessfullResponse(apiKey, jsonRes)
          }
      }).catch((error) => {
          console.log("ERROR" + error);
          _that.setState({ isVisible: false })

          setTimeout(()=>{
              Alert.alert("Server issue");
          },200);
      });
  }

  apiSuccessfullResponse(apiKey, jsonRes){
    if(apiKey =='paymentlist'){
      console.log(jsonRes)
      paymentlist=jsonRes.result
      this.setState({dataSource:paymentlist})
    }
  }

  viewBtn=(item)=>{
     _that.props.navigation.navigate('ViewPaymentScreen',{item:item});
  }


  render() {
    const headerProp = {
      title: 'Payment',
      screens: 'PaymentsScreen',
      type:''
    };
    //console.log(this.state.dataSource)
    return (
      <View style={styles.container}>
      <Header info={headerProp} navigation={_that.props.navigation}/>

        <View style={styles.content}>
        {this.state.dataSource && this.state.dataSource.length > 0 ?
        <ScrollView>
            <View style={styles.container1}>
            {
              this.state.dataSource.map((item, index) => (
                <LinearGradient colors={['#3995f7','#3995f7','#3995f7','#3995f7']} key={item.id} style={[styles.linearGradient,styles.servicebox]}>
                      <TouchableOpacity activeOpacity={.6} onPress={() => this.viewBtn(item)} style={styles.titleblk}>
                        <Text style={styles.title}>{item.cat_name}</Text>
                        <Text style={styles.smalltext}>#{item.order_id},     ${item.price}.00</Text>
                      </TouchableOpacity>
                </LinearGradient>
              ))
           }
            </View>

  </ScrollView>
  :
  <Text style={{textAlign:'center'}}>No Record Found</Text>
 }
        </View>
        <Spinner visible={this.state.isVisible}  />
    </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    paddingTop:  0,
  },

  content: {
      flex: 1,
      padding:'5%',
      backgroundColor:'#ffffff'
  },
  container1: {
    flex: 1,

    flexDirection: 'column',
  },
  servicebox:{
    flexDirection:'row',
    padding:AppSizes.ResponsiveSize.Sizes(15),
    paddingBottom:AppSizes.ResponsiveSize.Sizes(20),
    borderRadius: 5,
    marginBottom:10
  },
  titleblk:{
    width:'100%',
    height:'100%'

  },
  sideicon:{
      width:'30%',
      alignItems: 'center',
      flexDirection:'row',
      justifyContent: 'center',
  },
  ImageStyle: {
    height: AppSizes.screen.width/15,
    width: AppSizes.screen.width/15,
    resizeMode : 'contain',
  },
  title:{
    fontSize:AppSizes.ResponsiveSize.Sizes(20),
    paddingBottom: AppSizes.ResponsiveSize.Sizes(5),
    color:'#fff',
    fontWeight:'700',
    fontFamily:Fonts.RobotoBold,
  },
  smalltext:{
    fontSize:AppSizes.ResponsiveSize.Sizes(10),
    color:'#fff',
    fontWeight:'400',
    fontFamily:Fonts.RobotoRegular,
  },
  contentContainer: {
      paddingVertical: 20,
    },

    icon:{

      padding:AppSizes.ResponsiveSize.Padding(5),
    }

});
