import React, { Component } from 'react';
import {AsyncStorage,StyleSheet,Text,View,Image,TouchableOpacity,Platform,Alert,ScrollView,TextInput} from 'react-native';
import { AppStyles, AppSizes, AppColors } from '../../themes/'
import { NavigationActions } from 'react-navigation'
import Spacer from '../common/Spacer'
import * as commonFunctions from '../../utils/CommonFunctions'
import Header from '../common/Header'
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view'
import CommonButton from '../common/CommonButton'
import LinearGradient from 'react-native-linear-gradient';
import Spinner from 'react-native-loading-spinner-overlay';

var Constant = require('../../api/ApiRules').Constant;
var WebServices = require('../../api/ApiRules').WebServices;

/* Images */
const bg_image = require('../../themes/Images/bg_image.png')
var left= require( '../../themes/Images/right.png')
var payment= require( '../../themes/Images/payments.png')
var service= require( '../../themes/Images/services.png')
var servicestation= require( '../../themes/Images/service-station.png')
var calendar = require('../../themes/Images/calendar.png')

let _that
export default class SellerDashboard extends Component {
  constructor (props) {
      super(props)
      this.state = {
      isVisible: false,
      selectedDates:[]
    },
    _that = this;
  }

  componentWillMount(){
    AsyncStorage.getItem('UserData').then((UserData) => {
      var data = JSON.parse(UserData);
        uid=data.id
        //console.log(uid)
        if(uid != ''){
          _that.setState({uid:uid})

        }
    })
  }

  validationAndApiParameter(apiKey) {
    const { uid,isVisible } = this.state
       if(apiKey=='stationlist'){
         const data = new FormData();
         data.append('uid', uid);
          console.log(data);
         _that.setState({isVisible: true});

        _that.postToApiCalling('POST', apiKey, Constant.URL_serviceStation, data);
      }
      else if(apiKey=='serviceslist'){
        const data = new FormData();
        data.append('uid', uid);
         console.log(data);
        _that.setState({isVisible: true});

       _that.postToApiCalling('POST', apiKey, Constant.URL_service, data);
      }
      // else if(apiKey=='paymentlist'){
      //   const data = new FormData();
      //   data.append('uid', uid);
      //    console.log(data);
      //   _that.setState({isVisible: true});
      //
      //  _that.postToApiCalling('POST', apiKey, Constant.URL_payment, data);
      // }
      else if(apiKey=='calenderlist'){
        const data = new FormData();
        data.append('uid', uid);
         console.log(data);
        _that.setState({isVisible: true});

       _that.postToApiCalling('POST', apiKey, Constant.URL_calender, data);
      }

    }

    postToApiCalling(method, apiKey, apiUrl, data) {

       new Promise(function(resolve, reject) {
            if (method == 'POST') {
                resolve(WebServices.callWebService(apiUrl, data));
            } else {
                resolve(WebServices.callWebService_GET(apiUrl, data));
            }
        }).then((jsonRes) => {
          _that.setState({ isVisible: false })
          console.log(jsonRes);
            if ((!jsonRes) || (jsonRes.code == 0)) {
            setTimeout(()=>{
                Alert.alert('Error',jsonRes.message);
            },200);
            } else {
                _that.apiSuccessfullResponse(apiKey, jsonRes)
            }
        }).catch((error) => {
            console.log("ERROR" + error);
            _that.setState({ isVisible: false })

            setTimeout(()=>{
                Alert.alert("Server issue");
            },200);
        });
    }

    apiSuccessfullResponse(apiKey, jsonRes) {
        if (apiKey == 'stationlist') {
          //console.log(jsonRes)
          stationlist=jsonRes.result
          _that.props.navigation.navigate('Stationlistseller',{stationlist:stationlist});
        }
        else if(apiKey =='serviceslist'){
          //console.log(jsonRes)
          servicelist=jsonRes.result
          _that.props.navigation.navigate('Serviceslistseller',{servicelist:servicelist});
        }
        // else if(apiKey =='paymentlist'){
        //   //console.log(jsonRes)
        //   paymentlist=jsonRes.result
        //   _that.props.navigation.navigate('PaymentsScreen',{paymentlist:paymentlist});
        // }
        else if(apiKey =='calenderlist'){
          if(jsonRes.code==1){
            //console.log(jsonRes.result);
            selectedDates=jsonRes.result
          }
          else{
            selectedDates=[];
          }
          _that.props.navigation.navigate('Calendar',{selectedDates:selectedDates});
        }
    }

  // payBtn=()=>{
  //      _that.validationAndApiParameter('paymentlist')
  // }
  servicesBtn=()=>{
    _that.validationAndApiParameter('serviceslist')
     //_that.props.navigation.navigate('Serviceslistseller');
  }
  stationBtn=()=>{
      _that.validationAndApiParameter('stationlist')

  }
  calendarbtn = () =>{
    _that.validationAndApiParameter('calenderlist')
    //_that.props.navigation.navigate('Calendar',{selectedDates:this.state.selectedDates})
  }


  render() {
    const headerProp = {
      title: 'Dashboard',
      screens: 'SideMenu',
    };
    return (
      <View style={styles.container}>
        <Header info={headerProp} navigation={_that.props.navigation}/>
        <View style={styles.content}>
          <ScrollView contentContainerStyle={styles.contentContainer}>
            <View style={styles.container1}>
              <TouchableOpacity activeOpacity={.6} onPress={this.stationBtn} style={[styles.servicebox,styles.shadow]}>
                <View style={styles.sideicon}>
                  <Image source={servicestation} style={styles.ImageStyle} />
                </View>
                <View style={styles.titleblk}>
                  <Text style={styles.title}>SERVICE STATION</Text>
                  <Text style={styles.smalltext}>More</Text>
                </View>
                {/*  <LinearGradient colors={['#3995f7','#483158','#544658','#5a585b']} style={[styles.linearGradient,styles.numberbox]}>
                    <Text style={styles.numberboxtext}>3</Text>
                </LinearGradient>*/}
              </TouchableOpacity>
              <TouchableOpacity activeOpacity={.6} onPress={this.servicesBtn} style={[styles.servicebox,styles.shadow]}>
                <View style={styles.sideicon}>
                    <Image source={service} style={styles.ImageStyle} />
                </View>
                <View style={styles.titleblk}>
                  <Text style={styles.title}>SERVICES</Text>
                  <Text style={styles.smalltext}>More</Text>
                </View>
                {/*<LinearGradient colors={['#3995f7','#483158','#544658','#5a585b']} style={[styles.linearGradient,styles.numberbox]}>
                      <Text style={styles.numberboxtext}>3</Text>
                  </LinearGradient>*/}
              </TouchableOpacity>
              {/*<TouchableOpacity activeOpacity={.6} onPress={this.payBtn} style={[styles.servicebox,styles.shadow]}>

                      <View style={styles.sideicon}>
                          <Image source={payment} style={styles.ImageStyle} />
                      </View>
                      <View style={styles.titleblk} >
                        <Text style={styles.title}>PAYMENTS</Text>
                        <Text style={styles.smalltext}>More</Text>
                      </View>

                      <LinearGradient colors={['#3995f7','#483158','#544658','#5a585b']} style={[styles.linearGradient,styles.numberbox]}>
                          <Text style={styles.numberboxtext}>3</Text>
                      </LinearGradient>
                  </TouchableOpacity>*/}

              {/*<TouchableOpacity activeOpacity={.6} onPress={this.calendarbtn} style={[styles.servicebox,styles.shadow]}>
                <View style={styles.sideicon}>
                    <Image source={calendar} style={styles.ImageStyle} />
                </View>
                <View style={styles.titleblk} >
                  <Text style={styles.title}>CALENDAR</Text>
                  <Text style={styles.smalltext}>More</Text>
                </View>
                <LinearGradient colors={['#3995f7','#483158','#544658','#5a585b']} style={[styles.linearGradient,styles.numberbox]}>
                        <Text style={styles.numberboxtext}>3</Text>
                    </LinearGradient>
              </TouchableOpacity>*/}

            </View>
          </ScrollView>
        </View>
        <Spinner visible={this.state.isVisible}  />
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    paddingTop:  0,
  },

  content: {
      flex: 1,
      padding:'5%',
      paddingBottom:0,
      //backgroundColor:'#000'
  },
  container1: {
    flex: 1,

    flexDirection: 'column',
  },
  servicebox:{
    flexDirection:'row',
    padding:AppSizes.ResponsiveSize.Sizes(15),
    borderRadius: 5,
    marginBottom:AppSizes.ResponsiveSize.Padding(2),
    backgroundColor:'red'
  },
  titleblk:{
    width:'65%',

    justifyContent:'center',
    //backgroundColor:'red'
  },
  sideicon:{
      width:'35%',
      alignItems: 'center',
      paddingTop:AppSizes.ResponsiveSize.Padding(2),
      //backgroundColor:'red'
  },
  ImageStyle: {
    height: AppSizes.screen.width/5,
    width: AppSizes.screen.width/5,
    resizeMode : 'contain',
  },
  numberbox:{
      height:18,
      width:18,
      borderRadius:30,
      textAlign:'center',
      alignItems:'center',
      position:'absolute',
      right:20,
      top:20,
      paddingTop:AppSizes.ResponsiveSize.Padding(1)
  },
  numberboxtext:{
    color:'#fff',
    fontSize:AppSizes.ResponsiveSize.Sizes(10),
  },
  title:{
    fontSize:AppSizes.ResponsiveSize.Sizes(15),
    paddingBottom: AppSizes.ResponsiveSize.Sizes(5),
    color:'#4a2e59',
    fontWeight:'700',
    //textTransform:'uppercase'

  },
  smalltext:{
    fontSize:AppSizes.ResponsiveSize.Sizes(10),
    color:'#575459',
    fontWeight:'600',
    position:'absolute',
    bottom:0,
    right:0
  },
  contentContainer: {
      paddingVertical: 0,
      height:'100%',
    //backgroundColor:'green'
    },
    shadow:{
     borderWidth:1,
     borderRadius: 2,
     borderColor: '#fff',
     justifyContent:'center',
     backgroundColor:'#fff',
     borderColor: '#ddd',
     borderBottomWidth: 1,
     shadowColor: '#999',
     shadowOffset: { width: 0, height: 2 },
     shadowOpacity: 0.8,
     shadowRadius: 2,
     elevation: 0,
     //backgroundColor:'red',

    },

});
