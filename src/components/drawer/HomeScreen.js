import React, { Component } from 'react';
import {AsyncStorage,StyleSheet,Alert,Text,View,Image,TouchableOpacity,Platform,WebView,ImageBackground,ScrollView,Dimensions} from 'react-native';
import { AppStyles, AppSizes, AppColors } from '../../themes/'
import { NavigationActions, StackActions  } from 'react-navigation';
import Spacer from '../common/Spacer'
import Header from '../common/Header'
import Spinner from 'react-native-loading-spinner-overlay';
import * as commonFunctions from '../../utils/CommonFunctions'
import LinearGradient from 'react-native-linear-gradient';
import MapView, { Marker, ProviderPropType } from 'react-native-maps';
import Geocoder from 'react-native-geocoder';
import { pickerData } from '../data/pickerData';
import { Fonts } from '../../utils/Fonts';

var Constant = require('../../api/ApiRules').Constant;
var WebServices = require('../../api/ApiRules').WebServices;

const bg_image = require('../../themes/Images/bg_image.png')
const car = require('../../themes/Images/car_home.png')
const arrow = require('../../themes/Images/double_arrow.png')
const bike = require('../../themes/Images/bike_home.png')
const electric = require('../../themes/Images/station_home.png')
const rental = require('../../themes/Images/rental_home.png')
const parking = require('../../themes/Images/parking_home.png')
const bed = require('../../themes/Images/bed.png')
const cable = require('../../themes/Images/charging-c.png')
const gadget = require('../../themes/Images/gadget_home.png')
const single = require('../../themes/Images/single_bed.png')
const double = require('../../themes/Images/doublebed.png')
const king = require('../../themes/Images/kingbed.png')
const house = require('../../themes/Images/house.png')

const all = require('../../themes/Images/common_icon.png')
const bed_map_icon = require('../../themes/Images/bed_station_icon.png')
const charging_map_icon = require('../../themes/Images/station_charging_icon.png')
const parking_map_icon = require('../../themes/Images/station_parking_icon.png')
const rent_map_icon = require('../../themes/Images/station_rent_icon.png')
const cable_map_icon = require('../../themes/Images/station_cable_icon.png')

var help_audio=require('../../themes/sound/HomeHelp.mp3')


const { width, height } = Dimensions.get('window');
const ASPECT_RATIO = width / height;
const LATITUDE = 37.78825;
const LONGITUDE = -122.4324;
const LATITUDE_DELTA = 0.29;
const LONGITUDE_DELTA = LATITUDE_DELTA * ASPECT_RATIO;
const SPACE = 0.01;
const LAT= 30.7279571
const LANG= 76.8553046

let _that
export default class HomeScreen extends Component {
  constructor (props) {
    super(props)
    this.state = {
      uid:'',
      isVisible: false,
      Modal_Visibility: false,
      stationList:[],
      isMapReady: false,
      region: {
        latitude: LATITUDE,
        longitude: LONGITUDE,
        latitudeDelta: LATITUDE_DELTA,
        longitudeDelta: LONGITUDE_DELTA
      },
      image:all,
      cat:'',
      sub_cat:'',
      cat_id:'',
      subcat_id:'',
      other_cat_id:'',
      tab_el:false,
      tab_rent:false,
      tab_parking:false,
      bike:'0',
      gadget:'0',
      tabSelected:false,
      subCatSelected:false,
      fab:false
    },
    _that = this;
  }

  componentDidMount() {
    AsyncStorage.getItem('UserData').then((UserData) => {
      var data = JSON.parse(UserData);
        uid=data.id
        if(uid != ''){
          _that.setState({
            uid:uid
          })
        }
    })

    try {
    Geocoder.fallbackToGoogle('AIzaSyBX6rKXe6Jsk6ZynShEZiNfDfyhZWgmXsQ');
     navigator.geolocation.getCurrentPosition(
      (position) => {
        var region = {
          lat: position.coords.latitude,
          lng:  position.coords.longitude,
        };
        _that.validationAndApiParameter('ServiceList',uid,region.lat,region.lng)
        this.setState({
          region: {
            latitude: position.coords.latitude,
            longitude: position.coords.longitude,
            latitudeDelta: LATITUDE_DELTA,
            longitudeDelta: LONGITUDE_DELTA,
            accuracy: position.coords.accuracy
          }
        });
        Geocoder.geocodePosition(region).then(res => {
          //console.log(res)
          var address=res[0].formattedAddress
          _that.setState({
            latitude: position.coords.latitude,
            longitude: position.coords.longitude,
            address: address,
          });
        })
        this.watchID = navigator.geolocation.watchPosition((position) => {
          const newRegion = {
            latitude: position.coords.latitude,
            longitude: position.coords.longitude,
            latitudeDelta: LATITUDE_DELTA,
            longitudeDelta: LONGITUDE_DELTA,
            accuracy: position.coords.accuracy
          }
          this.setState({newRegion});
        })
      },
      (error) => console.log('error '+error.message),
      {enableHighAccuracy: false, timeout: 50000, maximumAge: 10000}
    );
  }
  catch(err) {
      console.log(err);
  }
}
  componentWillMount() {
    AsyncStorage.getItem('user_type').then((type) => {
        var type = JSON.parse(type);
        if(type=="seller"){
          _that.props.navigation.navigate('SellerDashboard');
        }
    })
  }

  updateAlert = (visible) => {
        this.setState({Modal_Visibility: !this.state.Modal_Visibility});
  }

  ServicelistBtn(name,id){
    if(this.state.cat_id=='1' && name=='Car'){
      this.setState({subcat_id:id})
      this.setState({sub_cat:name})
      _that.props.navigation.navigate('AvailableServices',{name:name,cat:this.state.cat,sub_cat:'car',cat_id:this.state.cat_id,subcat_id:id,other_cat_id:''});
      this.setState({tab_el:false}),
      this.setState({tab_rent:false}),
      this.setState({tab_parking:false})
      this.setState({tabSelected:false})
      this.setState({cat:''})
      this.setState({cat_id:''})
    }
    else if (this.state.cat_id=='1' && name=='Bike') {
      this.setState({sub_cat:name})
      this.setState({subcat_id:id})
      this.setState({bike:'1'})
      this.setState({gadget:'0'})
      this.setState({subCatSelected:true})
    }
    else if (this.state.cat_id=='1' && name=='Gadgets') {
      this.setState({sub_cat:name})
      this.setState({subcat_id:id})
      this.setState({bike:'0'})
      this.setState({gadget:'1'})
      this.setState({subCatSelected:true})
    }
    else if (this.state.cat_id=='2' && name=='Car') {
      this.setState({sub_cat:name})
      this.setState({subcat_id:id})
      _that.props.navigation.navigate('AvailableServices',{name:name,cat:this.state.cat,sub_cat:'car',cat_id:this.state.cat_id,subcat_id:id,other_cat_id:''});
      this.setState({cat:''})
      this.setState({cat_id:''})
      this.setState({tab_el:false}),
      this.setState({tabSelected:false})
      this.setState({tab_rent:false}),
      this.setState({tab_parking:false})
    }
    else if (this.state.cat_id=='2' && name == 'Bike') {
      this.setState({sub_cat:name})
      this.setState({subcat_id:id})
      this.setState({bike:'1'})
      this.setState({gadget:'0'})
      this.setState({subCatSelected:true})
    }
    else if (this.state.cat_id=='2' && name == 'Gadgets') {
      this.setState({sub_cat:name})
      this.setState({subcat_id:id})
      this.setState({bike:'0'})
      this.setState({gadget:'1'})
      this.setState({subCatSelected:true})
    }
    else if (this.state.cat_id=='3' && name == 'Car') {
      this.setState({sub_cat:name})
      this.setState({subcat_id:id})
      _that.props.navigation.navigate('AvailableServices',{name:name,cat:this.state.cat,sub_cat:'car',cat_id:this.state.cat_id,subcat_id:id,other_cat_id:''});
      this.setState({tab_el:false}),
      this.setState({tab_rent:false}),
      this.setState({tab_parking:false})
      this.setState({cat:''})
      this.setState({cat_id:''})
      this.setState({tabSelected:false})
    }
    else if (this.state.cat_id=='3' && name == 'Bike') {
      this.setState({sub_cat:name})
      this.setState({subcat_id:id})
      this.setState({bike:'1'})
      this.setState({gadget:'0'})
      this.setState({subCatSelected:true})
    }
    else if (this.state.cat_id=='5' && name == 'Car') {
      this.setState({sub_cat:name})
      this.setState({subcat_id:id})
      _that.props.navigation.navigate('AvailableServices',{name:name,cat:this.state.cat,sub_cat:'car',cat_id:this.state.cat_id,subcat_id:id,other_cat_id:''});
      this.setState({tab_el:false}),
      this.setState({tab_rent:false}),
      this.setState({tab_parking:false})
      this.setState({cat:''})
      this.setState({cat_id:''})
      this.setState({tabSelected:false})
    }
    else if (this.state.cat_id=='5' && name == 'Bike') {
      this.setState({sub_cat:name})
      this.setState({subcat_id:id})
      _that.props.navigation.navigate('AvailableServices',{name:name,cat:this.state.cat,sub_cat:'car',cat_id:this.state.cat_id,subcat_id:id,other_cat_id:''});
      this.setState({tab_el:false}),
      this.setState({tab_rent:false}),
      this.setState({tab_parking:false})
      this.setState({cat:''})
      this.setState({cat_id:''})
      this.setState({tabSelected:false})
    }
    else if (this.state.cat_id=='5' && name == 'Gadgets') {
      this.setState({sub_cat:name})
      this.setState({subcat_id:id})
      _that.props.navigation.navigate('AvailableServices',{name:name,cat:this.state.cat,sub_cat:'car',cat_id:this.state.cat_id,subcat_id:id,other_cat_id:''});
      this.setState({tab_el:false}),
      this.setState({tab_rent:false}),
      this.setState({tab_parking:false})
      this.setState({cat:''})
      this.setState({cat_id:''})
      this.setState({tabSelected:false})
    }
  }
  handleOnPress(name,id){
    this.setState({tab_el:false}),
    this.setState({tab_rent:false}),
    this.setState({tab_parking:false})
    this.setState({sub_cat:''})
    this.setState({subcat_id:''})
    this.setState({cat:''})
    this.setState({cat_id:''})
    this.setState({tabSelected:false})
    this.setState({bike:'0'})
    this.setState({gadget:'0'})
    _that.props.navigation.navigate('AvailableServices',{name:name,cat:this.state.cat,sub_cat:this.state.sub_cat,cat_id:this.state.cat_id,subcat_id:this.state.subcat_id,other_cat_id:id});
  }

  validationAndApiParameter(apikey,id,lat,lng) {
    if(apikey=='ServiceList'){
      const data = new FormData();
      data.append('uid',id);
      data.append('latitude',lat);
      data.append('longitude',lng);
      //console.log('Data',data);
      _that.setState({isVisible: true});
      this.postToApiCalling('POST',apikey, Constant.URL_allStationList, data);
    }
    else if (apikey=='getStationByCat') {
      const data=new FormData();
      data.append('uid',this.state.uid)
      data.append('latitude',this.state.region.latitude)
      data.append('longitude',this.state.region.longitude)
      data.append('cat_id',id)
      console.log(data);
      _that.setState({isVisible: true});
      this.postToApiCalling('POST',apikey, Constant.URL_allStationListByCat, data);
    }
  }

  postToApiCalling(method, apiKey, apiUrl, data) {
    new Promise(function(resolve, reject) {
      if (method == 'POST') {
        resolve(WebServices.callWebService(apiUrl, data));
      } else {
        resolve(WebServices.callWebService_GET(apiUrl, data));
      }
    }).then((jsonRes) => {
      //console.log(jsonRes);
      _that.setState({ isVisible: false })
      if ((!jsonRes) || (jsonRes.code == 0)) {
          setTimeout(()=>{
            Alert.alert('Login Error',jsonRes.message);
          },200);
      }
      else{
        if (jsonRes.code == 1) {
          _that.apiSuccessfullResponse(apiKey, jsonRes)
        }
      }
    }).catch((error) => {
        console.log("ERROR" + error);
        _that.setState({ isVisible: false })
        setTimeout(()=>{
            Alert.alert("Server issue"+error);
        },200);
    });
  }

  apiSuccessfullResponse(apiKey, jsonRes) {
      if (apiKey == 'ServiceList') {
          stationData=jsonRes.result;
          //console.log('Response',stationData);
          _that.setState({stationList:stationData})
      }
      else if (apiKey=='getStationByCat') {
        stationData=jsonRes.result
        console.log(stationData);
        if(this.state.cat_id=='1'){
          this.setState({image:charging_map_icon})//charging
        }
        else if (this.state.cat_id=='2') {
          this.setState({image:rent_map_icon})//rentals
        }
        else if (this.state.cat_id=='3') {
          this.setState({image:parking_map_icon})//parking
        }
        else if (this.state.cat_id=='5') {
          this.setState({image:cable_map_icon})//cable
        }
        else if (this.state.cat_id=='6') {
          this.setState({image:bed_map_icon})//bed
        }
        _that.setState({stationList:stationData})
      }
  }
  convert(data){
    var numb = data
    numb = parseFloat(numb).toFixed(2);
    return numb;
  }

    handleHeadingPress(name){
      if(name == 'electric'){
        this.setState({fab:false})
        this.setState({tabSelected:true})
        this.setState({cat:'electric'}),
        this.setState({sub_cat:''}),
        this.setState({bike:'0'}),
        this.setState({gadget:'0'}),
        this.setState({cat_id:'1'}),
        this.setState({tab_el:true}),
        this.setState({tab_rent:false}),
        this.setState({tab_parking:false})
        this.setState({subCatSelected:false})
        this.setState({tab_cable:false})
        this.setState({tab_bed:false})
        this.validationAndApiParameter('getStationByCat','1')
      }
      else if (name == 'rentals') {
        this.setState({fab:false})
        this.setState({tabSelected:true})
        this.setState({cat:'rentals'}),
        this.setState({sub_cat:''}),
        this.setState({bike:'0'}),
        this.setState({gadget:'0'}),
        this.setState({cat_id:'2'}),
        this.setState({tab_el:false}),
        this.setState({tab_rent:true}),
        this.setState({tab_parking:false})
        this.setState({subCatSelected:false})
        this.setState({tab_cable:false})
        this.setState({tab_bed:false})
        this.validationAndApiParameter('getStationByCat','2')
      }
      else if (name== 'parking') {
        this.setState({fab:false})
        this.setState({tabSelected:true})
        this.setState({cat:'parkings'}),
        this.setState({sub_cat:''}),
        this.setState({bike:'0'}),
        this.setState({gadget:'0'}),
        this.setState({cat_id:'3'}),
        this.setState({tab_el:false}),
        this.setState({tab_rent:false}),
        this.setState({tab_parking:true})
        this.setState({subCatSelected:false})
        this.setState({tab_cable:false})
        this.setState({tab_bed:false})
        this.validationAndApiParameter('getStationByCat','3')
      }
      else if (name== 'bed') {
        this.setState({fab:false})
        this.setState({tabSelected:true})
        this.setState({cat:'bed'}),
        this.setState({sub_cat:''}),
        this.setState({bike:'0'}),
        this.setState({gadget:'0'}),
        this.setState({cat_id:'6'}),
        this.setState({tab_el:false}),
        this.setState({tab_rent:false}),
        this.setState({tab_parking:false})
        this.setState({subCatSelected:false})
        this.setState({tab_cable:false})
        this.setState({tab_bed:true})
        this.validationAndApiParameter('getStationByCat','6')
      }
      else if (name== 'cable') {
        this.setState({fab:false})
        this.setState({tabSelected:true})
        this.setState({cat:'cable'}),
        this.setState({sub_cat:''}),
        this.setState({bike:'0'}),
        this.setState({gadget:'0'}),
        this.setState({cat_id:'5'}),
        this.setState({tab_el:false}),
        this.setState({tab_rent:false}),
        this.setState({tab_parking:false})
        this.setState({tab_cable:true})
        this.setState({tab_bed:false})
        this.setState({subCatSelected:false})
        this.validationAndApiParameter('getStationByCat','5')
      }
    }

    handleAccomodation(selected){
      if(selected=='Single Bed'){
        _that.props.navigation.navigate('AccomodationStations',{name:selected,cat:this.state.cat,cat_id:this.state.cat_id});
      }
      else if (selected=='Double Bed') {
        _that.props.navigation.navigate('AccomodationStations',{name:selected,cat:this.state.cat,cat_id:this.state.cat_id});
      }
      else if (selected=='Kingsize Bed') {
        _that.props.navigation.navigate('AccomodationStations',{name:selected,cat:this.state.cat,cat_id:this.state.cat_id});
      }
      else if (selected=='House/Flat') {
        _that.props.navigation.navigate('AccomodationStations',{name:selected,cat:this.state.cat,cat_id:this.state.cat_id});
      }
    }

  render() {
    var popup_data=<Text>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.</Text>;

    const headerProp = {
      title: 'HOME',
      screens: 'HomeScreen',
      qus_content:popup_data,
      qus_audio:help_audio
        };
    return (

      <View style={styles.container}>
        <Header info={headerProp} navigation={_that.props.navigation} updateAlert={this.updateAlert}/>

        <View style={styles.content}>
          <View style={[this.state.tabSelected?styles.mapcontainer1:styles.mapcontainer]}>
            <MapView style={styles.map}
              region={this.state.region}
              provider='google'
              mapType='standard'
              showsCompass={true}
              followsUserLocation={true}
              showsMyLocationButton={true}
              loadingEnabled={true}
              showsPointsOfInterest
              showsUserLocation={true} >
              {this.state.stationList.map((marker) => (
                <Marker
                  coordinate={{latitude:Number(marker.latitude),longitude:Number(marker.longitude)}}
                  key={marker.id}
                  icon={this.state.image}
                  title={marker.station_name}
                  opacity={0.8}
                />
              ))}
            </MapView>
            <View style={styles.bottombar}>

                <View style={styles.bottomdivider}>
                  <TouchableOpacity onPress={()=> this.handleHeadingPress('bed')} style={[styles.fabContainer,styles.half,styles.rightborder,this.state.tab_bed?{backgroundColor:'#0064cf'}:{backgroundColor:'#3995f7'}]}>
                  <View style={styles.side_iconm}>
                      <Image source={bed} style={styles.fab1}/>
                  </View>
                  <View style={styles.side_text}>
                      <Text style={styles.tabtext}>Accomodation</Text>
                  </View>
                  </TouchableOpacity>
                  <TouchableOpacity onPress={()=> this.handleHeadingPress('electric')} style={[styles.fabContainer,styles.half,this.state.tab_el?{backgroundColor:'#0064cf'}:{backgroundColor:'#3995f7'}]}>
                    <View style={styles.side_iconm}>
                      <Image source={electric} style={styles.fab1}/>
                    </View>
                      <View style={styles.side_text}>
                    <Text style={styles.tabtext}>Charging</Text>
                      </View>
                  </TouchableOpacity>
                </View>
                <View style={styles.bottomdivider}>
                  <TouchableOpacity onPress={()=> this.handleHeadingPress('parking')} style={[styles.fabContainer,styles.halfless,styles.rightborder,this.state.tab_parking?{backgroundColor:'#0064cf'}:{backgroundColor:'#3995f7'}]}>
                    <View style={styles.side_iconm}>
                      <Image source={parking} style={styles.fab1}/>
                    </View>
                    <View style={styles.side_text}>
                      <Text style={styles.tabtext}>Parking</Text>
                    </View>
                  </TouchableOpacity>
                  <TouchableOpacity onPress={()=> this.handleHeadingPress('rentals')} style={[styles.fabContainer,styles.halfless,styles.rightborder,this.state.tab_rent?{backgroundColor:'#0064cf'}:{backgroundColor:'#3995f7'}]}>
                    <View style={styles.side_iconm}>
                      <Image source={rental} style={styles.fab1}/>
                    </View>
                    <View style={styles.side_text}>
                      <Text style={styles.tabtext}>Rent</Text>
                    </View>
                  </TouchableOpacity>
                  <TouchableOpacity onPress={()=> this.handleHeadingPress('cable')} style={[styles.fabContainer,styles.halfless,this.state.tab_cable?{backgroundColor:'#0064cf'}:{backgroundColor:'#3995f7'}]}>
                    <View style={styles.side_iconm}>
                      <Image source={cable} style={styles.fab1}/>
                    </View>
                    <View style={styles.side_text}>
                      <Text style={styles.tabtext}>Cable</Text>
                    </View>
                  </TouchableOpacity>
                </View>

            </View>
          </View>

          <View style={[this.state.tabSelected?styles.formcontainer1:styles.formcontainer]}>
            <ImageBackground source={bg_image} style={styles.innercontainer}>
              <View style={{padding:AppSizes.ResponsiveSize.Padding(3),paddingBottom:10,flexDirection:'row',paddingTop:20}}>
                <View style={[this.state.subCatSelected?{width:'40%'}:{width:'100%'}]}>
                 {this.state.cat == 'electric'?
                   <ScrollView style={{marginBottom:10}} contentContainerStyle={{paddingBottom:10}} showsVerticalScrollIndicator={false}>
                     <TouchableOpacity style={[styles.containerin,styles.shadow1]} onPress={()=>{this.ServicelistBtn('Car','1')}}>
                        <View style={styles.side_icon}>
                          <Image source={car} style={{height:20,width:20}}/>
                        </View>
                       <Text style={styles.texttitle}>Car</Text>
                     </TouchableOpacity>
                     <TouchableOpacity style={[styles.containerin,styles.shadow1]} onPress={()=>{this.ServicelistBtn('Bike','2')}}>
                       <View style={styles.side_icon}>
                         <Image source={bike} style={{height:20,width:20}}/>
                       </View>
                       <Text style={styles.texttitle}>Bike</Text>
                       {this.state.bike == '1'?
                        <Image source={arrow} style={{height:10,width:10}}/>
                       :
                        null
                       }
                     </TouchableOpacity>
                     <TouchableOpacity style={[styles.containerin,styles.shadow1]} onPress={()=>{this.ServicelistBtn('Gadgets','5')}}>
                       <View style={styles.side_icon}>
                         <Image source={gadget} style={{height:20,width:20}}/>
                       </View>
                       <Text style={styles.texttitle}>Gadgets</Text>
                       {this.state.gadget == '1'?
                        <Image source={arrow} style={{height:10,width:10}}/>
                       :
                        null
                       }
                     </TouchableOpacity>
                   </ScrollView>
                 :
                   null
                 }
                 {this.state.cat == 'rentals'?
                   <ScrollView style={{marginBottom:10}} contentContainerStyle={{paddingBottom:10}} showsVerticalScrollIndicator={false}>
                     <TouchableOpacity style={[styles.containerin,styles.shadow1]} onPress={()=>{this.ServicelistBtn('Car','3')}}>
                     <View style={styles.side_icon}>
                       <Image source={car} style={{height:20,width:20}}/>
                     </View>
                       <Text style={styles.texttitle}>Car</Text>
                     </TouchableOpacity>
                     <TouchableOpacity style={[styles.containerin,styles.shadow1]} onPress={()=>{this.ServicelistBtn('Bike','4')}}>
                     <View style={styles.side_icon}>
                       <Image source={bike} style={{height:20,width:20}}/>
                     </View>
                       <Text style={styles.texttitle}>Bike</Text>
                       {this.state.bike == '1'?
                        <Image source={arrow} style={{height:10,width:10}}/>
                       :
                        null
                       }
                     </TouchableOpacity>
                     <TouchableOpacity style={[styles.containerin,styles.shadow1]} onPress={()=>{this.ServicelistBtn('Gadgets','6')}}>
                     <View style={styles.side_icon}>
                       <Image source={gadget} style={{height:20,width:20}}/>
                     </View>
                       <Text style={styles.texttitle}>Gadgets</Text>
                       {this.state.gadget == '1'?
                        <Image source={arrow} style={{height:10,width:10}}/>
                       :
                        null
                       }
                     </TouchableOpacity>
                   </ScrollView>
                 :
                   null
                 }
                 {this.state.cat == 'parkings'?
                   <ScrollView style={{marginBottom:10}} contentContainerStyle={{paddingBottom:10}} showsVerticalScrollIndicator={false}>
                     <TouchableOpacity style={[styles.containerin,styles.shadow1]} onPress={()=>{this.ServicelistBtn('Car','7')}}>
                     <View style={styles.side_icon}>
                       <Image source={car} style={{height:20,width:20}}/>
                     </View>
                       <Text style={styles.texttitle}>Car</Text>
                     </TouchableOpacity>
                     <TouchableOpacity style={[styles.containerin,styles.shadow1]} onPress={()=>{this.ServicelistBtn('Bike','8')}}>
                     <View style={styles.side_icon}>
                       <Image source={bike} style={{height:20,width:20}}/>
                     </View>
                       <Text style={styles.texttitle}>Bike</Text>
                       {this.state.bike == '1'?
                        <Image source={arrow} style={{height:10,width:10,}}/>
                       :
                        null
                       }
                     </TouchableOpacity>
                   </ScrollView>
                 :
                   null
                 }
                 {this.state.cat == 'bed'?
                 <ScrollView style={{marginBottom:10}} contentContainerStyle={{paddingBottom:10}} showsVerticalScrollIndicator={false}>
                   <TouchableOpacity style={[styles.containerin,styles.shadow1]} onPress={()=>{this.handleAccomodation('Single Bed')}}>
                     <View style={styles.side_icon}>
                       <Image source={single} style={{height:20,width:20}}/>
                     </View>
                     <Text style={styles.texttitle}>Single Bed</Text>
                   </TouchableOpacity>
                   <TouchableOpacity style={[styles.containerin,styles.shadow1]} onPress={()=>{this.handleAccomodation('Double Bed')}}>
                     <View style={styles.side_icon}>
                       <Image source={double} style={{height:20,width:20}}/>
                     </View>
                     <Text style={styles.texttitle}>Double Bed</Text>
                   </TouchableOpacity>
                   <TouchableOpacity style={[styles.containerin,styles.shadow1]} onPress={()=>{this.handleAccomodation('Kingsize Bed')}}>
                     <View style={styles.side_icon}>
                       <Image source={king} style={{height:20,width:20}}/>
                     </View>
                     <Text style={styles.texttitle}>Kingsize Bed</Text>
                   </TouchableOpacity>
                   <TouchableOpacity style={[styles.containerin,styles.shadow1]} onPress={()=>{this.handleAccomodation('House/Flat')}}>
                     <View style={styles.side_icon}>
                       <Image source={house} style={{height:20,width:20}}/>
                     </View>
                     <Text style={styles.texttitle}>House/Flat</Text>
                   </TouchableOpacity>
                 </ScrollView>
                 :
                   null
                 }
                 {this.state.cat == 'cable'?
                   <ScrollView style={{marginBottom:10}} contentContainerStyle={{paddingBottom:10}} showsVerticalScrollIndicator={false}>
                     <TouchableOpacity style={[styles.containerin,styles.shadow1]} onPress={()=>{this.ServicelistBtn('Car','13')}}>
                       <View style={styles.side_icon}>
                         <Image source={car} style={{height:20,width:20}}/>
                       </View>
                       <Text style={styles.texttitle}>Car</Text>
                     </TouchableOpacity>
                     <TouchableOpacity style={[styles.containerin,styles.shadow1]} onPress={()=>{this.ServicelistBtn('Bike','12')}}>
                       <View style={styles.side_icon}>
                         <Image source={bike} style={{height:20,width:20}}/>
                       </View>
                       <Text style={styles.texttitle}>Bike</Text>
                     </TouchableOpacity>
                     <TouchableOpacity style={[styles.containerin,styles.shadow1]} onPress={()=>{this.ServicelistBtn('Gadgets','14')}}>
                       <View style={styles.side_icon}>
                         <Image source={gadget} style={{height:20,width:20}}/>
                       </View>
                       <Text style={styles.texttitle}>Gadgets</Text>
                     </TouchableOpacity>
                   </ScrollView>
                 :
                   null
                 }
                </View>
                <View style={{width:'60%'}}>
                 {this.state.sub_cat == 'Bike' && this.state.cat=='electric'?
                   <View style={{height:'100%',alignItems:'center',paddingLeft:10,paddingRight:10}}>
                     <TouchableOpacity style={[styles.subCatContainer,styles.shadow1]} onPress={()=>this.handleOnPress('ElBike','5')}>
                       <Text style={styles.texttitle1}>ElBike</Text>
                     </TouchableOpacity>
                     <TouchableOpacity style={[styles.subCatContainer,styles.shadow1]} onPress={()=>this.handleOnPress('Scooters','1')}>
                       <Text style={styles.texttitle1}>Scooters</Text>
                     </TouchableOpacity>
                     <TouchableOpacity style={[styles.subCatContainer,styles.shadow1]} onPress={()=>this.handleOnPress('Skateboards','3')}>
                       <Text style={styles.texttitle1}>Skateboards</Text>
                     </TouchableOpacity>
                     <TouchableOpacity style={[styles.subCatContainer,styles.shadow1]} onPress={()=>this.handleOnPress('3wheelers','6')}>
                       <Text style={styles.texttitle1}>3wheelers</Text>
                     </TouchableOpacity>
                   </View>
                 :
                 this.state.sub_cat == 'Bike' && this.state.cat=='rentals'?
                   <View style={{height:'100%',alignItems:'center',paddingLeft:10,paddingRight:10}}>
                     <TouchableOpacity style={[styles.subCatContainer,styles.shadow1]} onPress={()=>this.handleOnPress('ElBike','7')}>
                       <Text style={styles.texttitle1}>ElBike</Text>
                     </TouchableOpacity>
                     <TouchableOpacity style={[styles.subCatContainer,styles.shadow1]} onPress={()=>this.handleOnPress('Scooters','2')}>
                       <Text style={styles.texttitle1}>Scooters</Text>
                     </TouchableOpacity>
                     <TouchableOpacity style={[styles.subCatContainer,styles.shadow1]} onPress={()=>this.handleOnPress('Skateboards','4')}>
                       <Text style={styles.texttitle1}>Skateboards</Text>
                     </TouchableOpacity>
                     <TouchableOpacity style={[styles.subCatContainer,styles.shadow1]} onPress={()=>this.handleOnPress('3wheelers','13')}>
                       <Text style={styles.texttitle1}>3wheelers</Text>
                     </TouchableOpacity>
                   </View>
                  :
                  this.state.sub_cat == 'Bike' && this.state.cat=='parkings'?
                    <View style={{height:'100%',alignItems:'center',paddingLeft:10,paddingRight:10}}>
                      <TouchableOpacity style={[styles.subCatContainer,styles.shadow1]} onPress={()=>this.handleOnPress('ElBike','16')}>
                        <Text style={styles.texttitle1}>ElBike</Text>
                      </TouchableOpacity>
                      <TouchableOpacity style={[styles.subCatContainer,styles.shadow1]} onPress={()=>this.handleOnPress('Scooters','15')}>
                        <Text style={styles.texttitle1}>Scooters</Text>
                      </TouchableOpacity>
                      <TouchableOpacity style={[styles.subCatContainer,styles.shadow1]} onPress={()=>this.handleOnPress('Skateboards','14')}>
                        <Text style={styles.texttitle1}>Skateboards</Text>
                      </TouchableOpacity>
                      <TouchableOpacity style={[styles.subCatContainer,styles.shadow1]} onPress={()=>this.handleOnPress('3wheelers','17')}>
                        <Text style={styles.texttitle1}>3wheelers</Text>
                      </TouchableOpacity>
                    </View>
                  :
                   null
                 }
                 {this.state.sub_cat == 'Gadgets' && this.state.cat=='electric'?
                   <View style={{height:'100%',alignItems:'center',paddingLeft:10,paddingRight:10}}>
                     <TouchableOpacity style={[styles.subCatContainer,styles.shadow1]} onPress={()=>this.handleOnPress('Laptops','18')}>
                       <Text style={styles.texttitle1}>Laptops</Text>
                     </TouchableOpacity>
                     <TouchableOpacity style={[styles.subCatContainer,styles.shadow1]} onPress={()=>this.handleOnPress('Drone','19')}>
                       <Text style={styles.texttitle1}>Drone</Text>
                     </TouchableOpacity>
                     <TouchableOpacity style={[styles.subCatContainer,styles.shadow1]} onPress={()=>this.handleOnPress('Mobile Phones','20')}>
                       <Text style={styles.texttitle1}>Mobile Phones</Text>
                     </TouchableOpacity>
                     <TouchableOpacity style={[styles.subCatContainer,styles.shadow1]} onPress={()=>this.handleOnPress('PC','21')}>
                       <Text style={styles.texttitle1}>PC</Text>
                     </TouchableOpacity>
                     <TouchableOpacity style={[styles.subCatContainer,styles.shadow1]} onPress={()=>this.handleOnPress('Filling for drones','22')}>
                       <Text style={styles.texttitle1}>Filling for drones</Text>
                     </TouchableOpacity>
                   </View>
                 :
                 this.state.sub_cat == 'Gadgets' && this.state.cat=='rentals'?
                   <View style={{height:'100%',alignItems:'center',paddingLeft:10,paddingRight:10}}>
                     <TouchableOpacity style={[styles.subCatContainer,styles.shadow1]} onPress={()=>this.handleOnPress('Laptops','24')}>
                       <Text style={styles.texttitle1}>Laptops</Text>
                     </TouchableOpacity>
                     <TouchableOpacity style={[styles.subCatContainer,styles.shadow1]} onPress={()=>this.handleOnPress('Drone','27')}>
                       <Text style={styles.texttitle1}>Drone</Text>
                     </TouchableOpacity>
                     <TouchableOpacity style={[styles.subCatContainer,styles.shadow1]} onPress={()=>this.handleOnPress('Mobile Phones','25')}>
                       <Text style={styles.texttitle1}>Mobile Phones</Text>
                     </TouchableOpacity>
                     <TouchableOpacity style={[styles.subCatContainer,styles.shadow1]} onPress={()=>this.handleOnPress('PC','26')}>
                       <Text style={styles.texttitle1}>PC</Text>
                     </TouchableOpacity>
                     <TouchableOpacity style={[styles.subCatContainer,styles.shadow1]} onPress={()=>this.handleOnPress('Filling for drones','23')}>
                       <Text style={styles.texttitle1}>Filling for drones</Text>
                     </TouchableOpacity>
                   </View>
                  :
                   null
                 }
                </View>
              </View>
            </ImageBackground>
          </View>
        </View>
        <Spinner visible={this.state.isVisible}  />
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    paddingTop:  0,
  },
  content: {
      flex: 1,
      height:'70%',
      width:'100%'
  },
  mapcontainer1:{
    flex:2
  },
  mapcontainer:{
    height:'100%'
  },
  formcontainer:{
    paddingTop:AppSizes.ResponsiveSize.Padding(3)
  },
  formcontainer1:{
    flex:1,

  },
   image: {
     flex: 1,
     width: undefined,
     height: undefined,
     resizeMode:'contain'
   },
   innercontainer:{
     width: '100%',
     height: '100%',
     resizeMode:'contain',

   },
   containerin: {
    height:50,
    padding: AppSizes.ResponsiveSize.Padding(2),
    flexDirection: 'row',
    alignItems: 'center',
    width:'100%',
    padding:10,
    alignItems:'center',
    borderColor:'#fff',
    borderWidth:1,
    borderRadius:3,
    marginBottom:10
  },
  texttitle: {
    marginLeft: AppSizes.ResponsiveSize.Padding(2),
    fontSize:AppSizes.ResponsiveSize.Sizes(18),
    fontWeight:'500',
    color:'#ffffff',
    width:'60%',
    fontFamily:Fonts.RobotoBold
    //backgroundColor:'red',
  },
  texttitle1: {
    marginLeft: AppSizes.ResponsiveSize.Padding(2),
    fontSize:AppSizes.ResponsiveSize.Sizes(18),
    fontWeight:'500',
    color:'#ffffff',
    width:'70%',
    fontFamily:Fonts.RobotoBold
    //backgroundColor:'red',
  },
map: {
    ...StyleSheet.absoluteFillObject,
    height:'100%'
},
subCatContainer:{
   padding: AppSizes.ResponsiveSize.Padding(2),
   flexDirection: 'row',
   alignItems: 'center',
   width:'100%',
   paddingTop:10,
   paddingBottom:10,
   justifyContent:'center',
   //backgroundColor:'green',
   marginBottom:10
},
shadow:{
  borderRadius: 2,
  borderColor: '#fff',
  justifyContent:'center',
  borderColor: '#fff',
  borderWidth: 0,
  shadowColor: '#fff',
  shadowOffset: { width: 2, height: 1 },
  shadowOpacity: 1,
  shadowRadius: 2,
  elevation: 10,
  zIndex:0,
  backgroundColor:'#4a3554',
},

side_icon:{
  marginRight:AppSizes.ResponsiveSize.Padding(3),
  borderRadius:50,
  backgroundColor:'#3995f7',
  padding:8
},
  bottombar:{
    width:'100%',
    justifyContent:'flex-end',
    flexDirection:'column',
    position:'absolute',
    bottom:0,
    backgroundColor: '#3995f7',
  },
  fab:{
    height:50,
    width:50
  },


  fabContainers:{
    width: 50,
    height: 50,
    alignItems: 'center',
    justifyContent: 'center',
    right: 20,
    bottom: 9,
    backgroundColor: '#3995f7',
    borderRadius: 30,
    elevation: 8,
    //position:'absolute'
  },

  fabContainer1:{
    //width: 40,
    //height: 40,
    alignItems: 'center',
    justifyContent: 'center',
    //right: 25,
    //bottom: 80,
    backgroundColor: '#3995f7',
    borderRadius: 30,
    elevation: 8,
    //position:'absolute'
  },
  fabContainer2:{
    //width: 40,
    //height: 40,
    alignItems: 'center',
    justifyContent: 'center',
    //right: 25,
    //bottom: 130,
    backgroundColor: '#3995f7',
    borderRadius: 30,
    elevation: 8,
    //position:'absolute'
  },
  fabContainer3:{
    //width: 40,
    //height: 40,
    alignItems: 'center',
    justifyContent: 'center',
    //right: 25,
    //bottom: 180,
    backgroundColor: '#3995f7',
    borderRadius: 30,
    elevation: 8,
    //position:'absolute'
  },
  fabContainer4:{
    //width: 40,
    //height: 40,
    alignItems: 'center',
    justifyContent: 'center',
    //right: 25,
    //bottom: 230,
    backgroundColor: '#3995f7',
    borderRadius: 30,
    elevation: 8,
    //position:'absolute'
  },
  fabContainer5:{
    //width: 40,
    //height: 40,
    alignItems: 'center',
    justifyContent: 'center',
    //right: 25,
    //bottom: 280,
    backgroundColor: '#3995f7',
    borderRadius: 30,
    elevation: 8,
    //position:'absolute'
  },
  fabText1:{
    //width: 120,
    //height: 25,
    textAlign: 'center',
    right: 50,
    bottom:7,
    backgroundColor: '#fff',
    elevation: 8,
    borderRadius: 10,
    position:'absolute',
    color:'#000',
    fontSize:AppSizes.ResponsiveSize.Sizes(16),
    fontFamily:Fonts.RobotoRegular
  },
  fabText2:{
    width: 80,
    height: 25,
    textAlign: 'center',
    right: 0,
    bottom:0,
    backgroundColor: '#fff',
    elevation: 8,
    borderRadius: 10,
    position:'absolute',
    color:'#000',
    fontSize:20,
    fontFamily:Fonts.RobotoRegular
  },
  fabContainer6:{
    width: 40,
    height: 40,
    alignItems: 'center',
    justifyContent: 'center',
    right: 27,
    bottom: 63,
    backgroundColor: '#3995f7',
    borderRadius: 30,
    elevation: 8,
    position:'absolute',
    fontFamily:Fonts.RobotoRegular
  },
  fabContainer7:{
    width: 40,
    height: 40,
    alignItems: 'center',
    justifyContent: 'center',
    right: 27,
    bottom: 107,
    backgroundColor: '#3995f7',
    borderRadius: 30,
    elevation: 8,
    position:'absolute',
    fontFamily:Fonts.RobotoRegular
  },
  fabContainer8:{
    width: 40,
    height: 40,
    alignItems: 'center',
    justifyContent: 'center',
    right: 27,
    bottom: 151,
    backgroundColor: '#3995f7',
    borderRadius: 30,
    elevation: 8,
    position:'absolute',
    fontFamily:Fonts.RobotoRegular
  },
  fabContainer9:{
    width: 40,
    height: 40,
    alignItems: 'center',
    justifyContent: 'center',
    right: 27,
    bottom: 195,
    backgroundColor: '#3995f7',
    borderRadius: 30,
    elevation: 8,
    position:'absolute',
    fontFamily:Fonts.RobotoRegular
  },
  fabContainer10:{
    width: 40,
    height: 40,
    alignItems: 'center',
    justifyContent: 'center',
    right: 27,
    bottom: 240,
    backgroundColor: '#3995f7',
    borderRadius: 30,
    elevation: 8,
    position:'absolute',
    fontFamily:Fonts.RobotoRegular
  },
  fabContainer:{
    position:'relative',
    height: 48,
    alignItems: 'center',
    justifyContent: 'center',
    //backgroundColor: '#000',
    flexDirection:'row'
    //position:'absolute'
  },
  bottomdivider:{
    width:'100%',
    flexDirection:'row',


  },
  tabtext:{
    //backgroundColor:'green',

  color:'#fff',
    fontSize:AppSizes.ResponsiveSize.Sizes(15),

    width:'100%',
    fontFamily:Fonts.RobotoBold
  },
  rightborder:{
    borderRightWidth:1,
    borderRightColor:'#fff',

  },

  half:{
    width:'50%',
    borderBottomWidth:1,
    borderBottomColor:'#fff'
  },
  halfless:{
    width:'33%',

    borderBottomWidth:1,
    borderBottomColor:'#fff'
  },
  fab1:{
    height:30,
    width:30,
    //left:10,
    //position:'absolute'
  },
  side_iconm:{
    width:50,
    //backgroundColor:'red',
    alignItems:'center'
  },
  side_text:{
    width:'70%',

    //backgroundColor:'green'
  }

});
