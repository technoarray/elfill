import React, { Component } from 'react';
import {StyleSheet,Text,View,Image,TouchableOpacity,Platform,Alert,ScrollView,TextInput} from 'react-native';
import { AppStyles, AppSizes, AppColors } from '../../themes/';
import { NavigationActions } from 'react-navigation';
import Spacer from '../common/Spacer';
import Spinner from 'react-native-loading-spinner-overlay';
import * as commonFunctions from '../../utils/CommonFunctions';
import Header from '../common/HeaderWithBack';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view';
import CommonButton from '../common/CommonButton';
import LinearGradient from 'react-native-linear-gradient';
import { Fonts } from '../../utils/Fonts';
var Constant = require('../../api/ApiRules').Constant;
var WebServices = require('../../api/ApiRules').WebServices;

/* Images */
const bg_image = require('../../themes/Images/bg_image.png')
var left= require( '../../themes/Images/right.png')
var plus= require( '../../themes/Images/plus.png')

let _that
export default class AvailableServices extends Component {
  constructor (props) {
      super(props)
      this.state = {
      station_id:this.props.navigation.state.params.id,
      isVisible: false,
      servicesData:[],
      Modal_Visibility: false,
      code:false,
    },
    _that = this;
  }

  componentDidMount(){
    _that.validationAndApiParameter('services')
  }

  ServiceviewBtn(seller_id,station_id,cat_id){
     _that.props.navigation.navigate('AvailableSubServices',{seller_id:seller_id,station_id:station_id,cat_id:cat_id});
  }

  validationAndApiParameter(apikey) {
    if(apikey=='services'){
      const data = new FormData();
      data.append('uid',this.state.uid);
      data.append('station_id',this.state.station_id);
      //console.log('Data',data);
      _that.setState({isVisible: true});
      this.postToApiCalling('POST',apikey, Constant.URL_servicsCatList, data);
    }
  }

  postToApiCalling(method, apiKey, apiUrl, data) {
    new Promise(function(resolve, reject) {
      if (method == 'POST') {
        resolve(WebServices.callWebService(apiUrl, data));
      } else {
        resolve(WebServices.callWebService_GET(apiUrl, data));
      }
    }).then((jsonRes) => {
      _that.setState({ isVisible: false })
      if ((!jsonRes) || (jsonRes.code == 0)) {
          setTimeout(()=>{
            Alert.alert('Login Error',jsonRes.message);
          },200);
      }
      else if(jsonRes.code == 2){
        this.setState({code:true})
      }
      else{
        if (jsonRes.code == 1) {
          _that.apiSuccessfullResponse(apiKey, jsonRes)
        }
      }
    }).catch((error) => {
        console.log("ERROR" + error);
        _that.setState({ isVisible: false })
        setTimeout(()=>{
            Alert.alert("Server issue"+error);
        },200);
    });
  }

  apiSuccessfullResponse(apiKey, jsonRes) {
      if (apiKey == 'services') {
          servicesData=jsonRes.result;
          //console.log('response:',servicesData);
          _that.setState({servicesData:servicesData})
      }
  }

  render() {
    const headerProp = {
      title: 'Serviceslist',
      screens: 'Serviceslist',
        type:''
    };
    return (
      <View style={styles.container}>
      <Header info={headerProp} navigation={_that.props.navigation}/>

        <View style={styles.content}>
        <ScrollView contentContainerStyle={styles.contentContainer}>
            {this.state.code ?
                <View style={styles.container1}>
                  <Text style={{color:'black',padding:AppSizes.ResponsiveSize.Padding(1)}}>No Record Found</Text>
                </View>
            :
                <View style={styles.container1}>
                    {this.state.servicesData.map(data => (
                      <TouchableOpacity activeOpacity={.6} onPress={this.ServiceviewBtn.bind(this,data.uid,data.station_id,data.cat_id)} key={data.service_id}>
                                <LinearGradient colors={['#3995f7','#483158','#544658','#5a585b']} style={[styles.linearGradient,styles.servicebox]}>
                                    <View style={styles.titleblk}>
                                      <Text style={styles.title}>{data.name}</Text>
                                    </View>
                                    <View style={styles.sideicon}>
                                        <Image source={left} style={[styles.ImageStyle,styles.sidearrow]} />
                                    </View>
                                  </LinearGradient>
                        </TouchableOpacity>
                    ))}
                </View>
            }
  </ScrollView>

        </View>
        <Spinner visible={this.state.isVisible}  />
    </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    paddingTop:  0,
  },

  content: {
      flex: 1,
      padding:'5%',
      paddingBottom:0,
      //backgroundColor:'#000'
  },
  container1: {
    flex: 1,
    flexDirection: 'column',
  },
  servicebox:{
    flexDirection:'row',
    padding:AppSizes.ResponsiveSize.Sizes(15),
    borderRadius: 5,
    marginBottom:AppSizes.ResponsiveSize.Padding(2)
  },
  titleblk:{
    width:'90%',
    height:'100%'
  },
  sideicon:{
      width:'12%',
      alignItems: 'center',
      paddingTop:AppSizes.ResponsiveSize.Padding(1),

  },
  ImageStyle: {
    height: AppSizes.screen.width/15,
    width: AppSizes.screen.width/15,
    resizeMode : 'contain',
  },

  title:{
    fontSize:AppSizes.ResponsiveSize.Sizes(20),
    paddingBottom: AppSizes.ResponsiveSize.Sizes(5),
    color:'#fff',
    fontWeight:'700',
    fontFamily:Fonts.RobotoBold,
  },
  smalltext:{
    fontSize:AppSizes.ResponsiveSize.Sizes(10),
    color:'#fff',
    fontWeight:'400',
    fontFamily:Fonts.RobotoRegular,
  },
  contentContainer: {
      paddingVertical: 20,
      height:'85%',
        //backgroundColor:'green'
    },
    containerend:{
      flex:2,
      justifyContent:'flex-end',
      //backgroundColor:'red',
      alignItems:'flex-end'
    },

    sideiconmain:{
        width:'10%',
        alignItems: 'center',
        paddingTop:AppSizes.ResponsiveSize.Padding(2),

        //backgroundColor:'gray'
    },

    ImageStylemain: {
      height: AppSizes.screen.width/8,
      width: AppSizes.screen.width/8,
      resizeMode : 'contain',
    },

});
