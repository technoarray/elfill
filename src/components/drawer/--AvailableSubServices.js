import React, { Component } from 'react';
import {AsyncStorage,StyleSheet,Alert,Text,View,Image,TouchableOpacity,Platform,WebView,ImageBackground,ScrollView,Dimensions} from 'react-native';
import { AppStyles, AppSizes, AppColors } from '../../themes/'
import { NavigationActions, StackActions  } from 'react-navigation';
import Spacer from '../common/Spacer'
import Header from '../common/HeaderWithBack';
import Spinner from 'react-native-loading-spinner-overlay';
import * as commonFunctions from '../../utils/CommonFunctions'
import LinearGradient from 'react-native-linear-gradient';
import MapView, {ProviderPropType} from 'react-native-maps';
import Geocoder from 'react-native-geocoder';
import PolyLine from '@mapbox/polyline';
import Picker from 'react-native-q-picker';
import CommonButton from '../common/CommonButton'

var Constant = require('../../api/ApiRules').Constant;
var WebServices = require('../../api/ApiRules').WebServices;

const bg_image = require('../../themes/Images/bg_image.png')
const all = require('../../themes/Images/station_icon.png')
const list_car = require('../../themes/Images/list_car.png')
const car = require('../../themes/Images/car.png')
const arrow = require('../../themes/Images/double_arrow.png')
const bike = require('../../themes/Images/bike.png')
//const parking = require('../../themes/Images/parking.png')
const gadget = require('../../themes/Images/laptop.png')

const { width, height } = Dimensions.get('window');
const ASPECT_RATIO = width / height;
const LATITUDE = 37.78825;
const LONGITUDE = -122.4324;
const LATITUDE_DELTA = 0.29;
const LONGITUDE_DELTA = LATITUDE_DELTA * ASPECT_RATIO;
const SPACE = 0.01;
const LAT= 30.7279571
const LANG= 76.8553046

let _that
export default class HomeScreen extends Component {
  constructor (props) {
      super(props)
      this.state = {
        uid:this.props.navigation.state.params.uid,
        station_id:this.props.navigation.state.params.id,
        isVisible: false,
        cat_id:this.props.navigation.state.params.cat_id,
        subcat_id:this.props.navigation.state.params.subcat_id,
        type_id:this.props.navigation.state.params.type_id,
        lat:this.props.navigation.state.params.lat,
        lgt:this.props.navigation.state.params.lgt,
        stationData:[],
        deviceList:[],
        selectedDeviceType:'',
        price:'',
        description:'',
        numberofslots:'',
        outlet_type:'',
        chargedata:[],
        selectedCharge:'',
        private_facility:'',
        public_facility:'',
        power_bank:'',
        other_facility:'',
        effect:'',
        phases:'',
        ampere:'',
        parking_fee:'',
        accomodation:'',
        reg_no:'',
        coffee:'',
        dlnumber:'',
        upload_licence:'',
        insurance:'',
        chargeError:'',
        deviceTypeError:'',
        uploadError:'',
        stationList:[],
        isMapReady: false,
        region: {
          latitude: LATITUDE,
          longitude: LONGITUDE,
          latitudeDelta: LATITUDE_DELTA,
          longitudeDelta: LONGITUDE_DELTA
        },
        image:all,
        cat:this.props.navigation.state.params.cat,
        sub_cat:this.props.navigation.state.params.sub_cat,
        concat: null,
        latitude: null,
        longitude: null,
        error: null,
        concat: null,
        coords:[],
        x: 'false',
        cordLatitude:this.props.navigation.state.params.lat,
        cordLongitude:this.props.navigation.state.params.lgt,
        empty:true,
        name:this.props.navigation.state.params.name
    },
    this.mergeLot = this.mergeLot.bind(this);
    _that = this;
  }

  componentDidMount() {
    if(this.state.sub_cat=='Car'){
      this.setState({image:car})
    }
    else if (this.state.sub_cat=='Bike') {
      this.setState({image:bike})
    }
    else if (this.state.sub_cat=='Gadgets') {
      this.setState({image:gadget})
    }
    AsyncStorage.getItem('UserData').then((UserData) => {
      var data = JSON.parse(UserData);
        uid=data.id
        if(uid != ''){
          _that.setState({
            uid:uid
          })
        }
    })
    if(this.state.type_id == '1'){
      _that.validationAndApiParameter('chargeType')
    }
    else {
      _that.validationAndApiParameter('stationList')
    }

    try {
    Geocoder.fallbackToGoogle('AIzaSyBX6rKXe6Jsk6ZynShEZiNfDfyhZWgmXsQ');
     navigator.geolocation.getCurrentPosition(
      (position) => {
        var region = {
          lat: position.coords.latitude,
          lng:  position.coords.longitude,
        };
        this.setState({
          region: {
            latitude: position.coords.latitude,
            longitude: position.coords.longitude,
            latitudeDelta: LATITUDE_DELTA,
            longitudeDelta: LONGITUDE_DELTA,
            accuracy: position.coords.accuracy
          }
        });
        this.setState({
           latitude: position.coords.latitude,
           longitude: position.coords.longitude,
           error: null,
         });
        this.mergeLot();
        Geocoder.geocodePosition(region).then(res => {
          //console.log(res)
          var address=res[0].formattedAddress
          _that.setState({
            latitude: position.coords.latitude,
            longitude: position.coords.longitude,
            address: address,
          });
        })
        this.watchID = navigator.geolocation.watchPosition((position) => {
          const newRegion = {
            latitude: position.coords.latitude,
            longitude: position.coords.longitude,
            latitudeDelta: LATITUDE_DELTA,
            longitudeDelta: LONGITUDE_DELTA,
            accuracy: position.coords.accuracy
          }
          this.setState({newRegion});
        })
      },
      (error) => console.log('error '+error.message),
      {enableHighAccuracy: false, timeout: 50000, maximumAge: 10000}
    );
    }
    catch(err) {
        console.log(err);
    }
  }

  onSelect(index, value){
    this.setState({status: value})
  }

selectPhotoTapped() {
  const options = {
    quality: 1.0,
    maxWidth: 500,
    maxHeight: 500,
    storageOptions: {
      skipBackup: true,
    }
  };
    ImagePicker.showImagePicker(options, (response) => {
    console.log('Response = ', response);

    if (response.didCancel) {
      console.log('User cancelled image picker');
    } else if (response.error) {
      console.log('ImagePicker Error: ', response.error);
    } else if (response.customButton) {
      console.log('User tapped custom button: ', response.customButton);
    } else {
      const source = { uri: response.uri };

      this.setState({
        avatarSource: source,
        avatarerr:''
      });
    }
  });
 }

 async getDirections(startLoc, destinationLoc) {
          try {
              let resp = await fetch(`https://maps.googleapis.com/maps/api/directions/json?origin=${ startLoc }&destination=${ destinationLoc }&key=AIzaSyBX6rKXe6Jsk6ZynShEZiNfDfyhZWgmXsQ`)
              console.log(resp);
              let respJson = await resp.json();
              let points = PolyLine.decode(respJson.routes[0].overview_polyline.points);
              let coords = points.map((point, index) => {
                  return  {
                      latitude : point[0],
                      longitude : point[1]
                  }
              })
              this.setState({coords: coords})
              this.setState({x: "true"})
              return coords
          } catch(error) {
            console.log('error',error)
              this.setState({x: "error"})
              return error
          }
      }

      mergeLot(){

        if (this.state.latitude != null && this.state.longitude!=null)
         {
           let concatLot = this.state.latitude +","+this.state.longitude
           let destination=this.state.lat +","+this.state.lgt
           console.log(concatLot);
           console.log(destination);
           this.setState({
             concat: concatLot
           }, () => {
             this.getDirections(concatLot,destination);
           });
         }
       }

  submit_click=() =>{
    _that.validationAndApiParameter('buy');
  }

  onClick(val,label,apiKey){
    if(apiKey=='deviceType'){
      this.setState({deviceTypeError:''})
      this.setState({selectedDeviceType:''});
      this.setState({selectedDevice:label})
      this.setState({selectedDeviceType: val});
      var type=this.state.stationData
      for (var i = 0; i < type.length; i++) {
        if(type[i].device_id==val){
          this.setState({price:type[i].price})
          this.setState({description:type[i].description})
          this.setState({numberofslots:type[i].no_of_charging_spot})
          this.setState({outlet_type:type[i].outlet_type})
          this.setState({private_facility:type[i].private_home_facility})
          this.setState({public_facility:type[i].public_facility})
          this.setState({power_bank:type[i].portable_power_bank})
          this.setState({other_facility:type[i].other_facility})
          this.setState({effect:type[i].effect})
          this.setState({phases:type[i].phases})
          this.setState({ampere:type[i].ampere})
          this.setState({parking_fee:type[i].parking_fee})
          this.setState({reg_no:type[i].car_reg_no})
          this.setState({accomodation:type[i].accomodation})
          this.setState({coffee:type[i].coffee})
          this.setState({dlnumber:type[i].driver_licence_no})
          this.setState({upload_licence:type[i].upload_driver_licence})
          this.setState({insurance:type[i].insurance})
          this.setState({service_id:type[i].id})
          this.setState({seller_id:type[i].uid})
        }
      }
    }
    else if(apiKey == 'charge'){
      var data=[]
      this.setState({chargeError:''})
        this.setState({deviceTypeError:''})
      this.state.deviceList.length=0
      this.setState({selectedDeviceType:''});
      this.setState({selectedCharge:label})
      var type=this.state.stationData
      for (var i = 0; i < type.length; i++) {
        if(type[i].charge_type==label){
          this.state.deviceList.push({name:type[i].device_name,id:parseInt(type[i].device_id)})
        }
      }
    }
  }

  validationAndApiParameter(apikey,id,lat,lng) {
    if(apikey=='stationList'){
      const data = new FormData();
      data.append('station_id',this.state.station_id);
      data.append('cat_id',this.state.cat_id);
      data.append('subcat_id',this.state.subcat_id)

      _that.setState({isVisible: true});
      this.postToApiCalling('POST',apikey, Constant.URL_servicesToBuyer, data);
    }
    if(apikey=='chargeType'){
      const data = new FormData();
      data.append('station_id',this.state.station_id);
      data.append('cat_id',this.state.cat_id);
      data.append('subcat_id',this.state.subcat_id)

      _that.setState({isVisible: true});
      this.postToApiCalling('POST',apikey, Constant.URL_servicesToBuyer, data);
    }
    if(apikey =='buy'){
      var error=0
      if(this.state.type_id == '2' && this.state.cat_id != '2'){
        if(this.state.selectedDeviceType==''){
          error=1
          this.setState({deviceTypeError:'Field is required'})
        }
      }
      else if (this.state.type_id=='1' && this.state.cat_id != '2') {
        if(this.state.selectedDeviceType==''){
          error=1
          this.setState({deviceTypeError:'Field is required'})
        }
        if(this.state.selectedCharge==''){
          error=1
          this.setState({deviceTypeError:'Field is required'})
        }
      }
      else if (this.state.type_id=='3' && this.state.cat_id != '2') {
        if(this.state.selectedDeviceType==''){
          error=1
          this.setState({deviceTypeError:'Field is required'})
        }
      }
      else if (this.state.cat_id == '2') {
        if(this.state.type_id == '2'){
          if(this.state.selectedDeviceType==''){
            error=1
            this.setState({deviceTypeError:'Field is required'})
          }
        }
        else if (this.state.type_id=='1') {
          if(this.state.selectedDeviceType==''){
            error=1
            this.setState({deviceTypeError:'Field is required'})
          }
          if(this.state.selectedCharge==''){
            error=1
            this.setState({deviceTypeError:'Field is required'})
          }
        }
        else if (this.state.type_id=='3') {
          if(this.state.selectedDeviceType==''){
            error=1
            this.setState({deviceTypeError:'Field is required'})
          }
        }
        if(this.state.upload_licence==''){
          error=1
          this.setState({deviceTypeError:'Field is required'})
        }
      }
      if(error==0){
        const data =[];
        data.push(this.state.uid);//0
        data.push(this.state.seller_id);//1
        data.push(this.state.station_id);//2
        data.push(this.state.type_id);//3
        data.push(this.state.cat_id);//4
        data.push(this.state.subcat_id);//5
        data.push(this.state.selectedDeviceType);//6
        data.push(this.state.selectedCharge);//7
        data.push(this.state.outlet_type);//8
        data.push(this.state.parking_fee);//9
        data.push(this.state.reg_no);//10
        data.push(this.state.accomodation);//11
        data.push(this.state.coffee);//12
        data.push(this.state.effect);//13
        data.push(this.state.phases);//14
        data.push(this.state.ampere);//15
        data.push(this.state.private_facility);//16
        data.push(this.state.public_facility);//17
        data.push(this.state.power_bank);//18
        data.push(this.state.other_facility);//19
        data.push(this.state.dlnumber);//20
        data.push(this.state.upload_licence);//21
        data.push(this.state.insurance);//22
        data.push(this.state.price);//23
        data.push(this.state.description);//24
        data.push(this.state.numberofslots);//25
        data.push(this.state.selectedDevice);//26
        data.push(this.state.service_id)//27
        data.push(this.state.lat),
        data.push(this.state.lag)
        //console.log(data);

        _that.props.navigation.navigate('PaymentMethod',{data:data})
      }
    }
  }

  postToApiCalling(method, apiKey, apiUrl, data) {
    new Promise(function(resolve, reject) {
      if (method == 'POST') {
        resolve(WebServices.callWebService(apiUrl, data));
      } else {
        resolve(WebServices.callWebService_GET(apiUrl, data));
      }
    }).then((jsonRes) => {
      console.log(jsonRes);
      _that.setState({ isVisible: false })
      if ((!jsonRes) || (jsonRes.code == 0)) {
          setTimeout(()=>{
            Alert.alert('Login Error',jsonRes.message);
          },200);
      }
      else if(jsonRes.code == 1){
        this.setState({empty:false})
        _that.apiSuccessfullResponse(apiKey, jsonRes)
      }
      else if (jsonRes.code == 2) {
        this.setState({empty:true})
      }
    }).catch((error) => {
        console.log("ERROR" + error);
        _that.setState({ isVisible: false })
        setTimeout(()=>{
            Alert.alert("Server issue"+error);
        },200);
    });
  }

  apiSuccessfullResponse(apiKey, jsonRes) {
    if (apiKey == 'stationList') {
        stationData=jsonRes.result;
        this.setState({stationData:stationData})
        //console.log(stationData);
        stationData.map(sdata=> this.state.deviceList.push({name:sdata.device_name,id:parseInt(sdata.device_id)}));
    }
    else if (apiKey == 'chargeType') {
      var data1=[]
        stationData=jsonRes.result;
        this.setState({stationData:stationData})
        //console.log(stationData);
        stationData.map(data => data1.push(data.charge_type))
        console.log(data1);
        var unique=data1.filter((v, i, a) => a.indexOf(v) === i);
        console.log(unique);
        for (var i = 0; i < unique.length; i++) {
          this.state.chargedata.push({name:unique[i],id:i})
        }
        console.log(this.state.chargedata);
    }
  }
  convert(data){
    var numb = data
    numb = parseFloat(numb).toFixed(2);
    return numb;
  }

  render() {
    const headerProp = {
      title: 'Serviceslist',
      screens: 'Serviceslist',
      type:''
    };
    return (

      <View style={styles.container}>
        <Header info={headerProp} navigation={_that.props.navigation} />

       <View style={styles.content}>
        <View style={styles.mapcontainer}>
          <MapView style={styles.map}
                    region={this.state.region}
                    provider='google'
                    mapType='standard'
                    showsCompass={true}
                    showsPointsOfInterest >
                    {!!this.state.latitude && !!this.state.longitude &&
                      <MapView.Marker
                       pinColor={'#3995f7'}
                       coordinate={{"latitude":this.state.latitude,"longitude":this.state.longitude}}
                       title={"Your Location"}
                       showsUserLocation={true}
                     />}

                     {!!this.state.cordLatitude && !!this.state.cordLongitude &&
                       <MapView.Marker
                        pinColor={'#3995f7'}
                        coordinate={{"latitude":this.state.cordLatitude,"longitude":this.state.cordLongitude}}
                        title={this.state.name}
                        showsUserLocation={true}
                      />}

                     {!!this.state.latitude && !!this.state.longitude && this.state.x == 'true' &&
                     <MapView.Polyline
                          coordinates={this.state.coords}
                          strokeWidth={3}
                          lineCap={'round'}
                          lineJoin={'round'}
                          strokeColor="#3995f7"/>
                      }

                      {!!this.state.latitude && !!this.state.longitude && this.state.x == 'error' &&
                      <MapView.Polyline
                        coordinates={[
                            {latitude: this.state.latitude, longitude: this.state.longitude},
                            {latitude: this.state.cordLatitude, longitude: this.state.cordLongitude},
                        ]}
                        lineCap={'round'}
                        lineJoin={'round'}
                        strokeWidth={3}
                        strokeColor="#3995f7"/>
                       }
            </MapView>
            {/*  <WebView
                source={{uri: 'https://leemansinussolutions.com/efil/map/map.php'}}
              />*/}
          </View>

          <View style={styles.formcontainer}>
          {this.state.empty?
            <View style={{alignItems:'center',justifyContent:'center',height:'100%'}}>
             <Text style={{color:'#000',fontSize:25}}>No service station found
             </Text>
            </View>
          :
            <View>
              <ScrollView showsVerticalScrollIndicator={false}>
              <View style={styles.container2}>
                {this.state.upload_licence== 'yes' && this.state.cat_id == '2'?
                  <View style={styles.inputfield}>
                    <Text style={styles.inputtext}>Upload copy of Drivers Licence</Text>
                    <TouchableOpacity onPress={this.selectPhotoTapped.bind(this)} style={{alignItems:'center'}}>
                      <View style={[styles.avatar,  styles.avatarContainer,{ marginBottom: 20 },]}>
                        {this.state.avatarSource === null ? (
                          <Text>Upload photo of Driver Licence</Text>
                        ) : (
                          <Image style={styles.avatar} source={this.state.avatarSource} />
                        )}
                      </View>
                    </TouchableOpacity>
                  </View>
                :
                  null
                }
                {this.state.cat_id == '1' && this.state.subcat_id=='1'?
                <View>
                  <View style={styles.SectionStyle}>
                    <View style={styles.inputfield}>
                        <Text style={styles.inputtext}>Charge</Text>
                        <Picker PickerData={this.state.chargedata}
                         label={'Charge'}
                         color={'#000000'}
                         getTxt={(val,label)=> this.onClick(val,label,'charge')}/>
                    </View>
                    <Text style={styles.error}>{this.state.chargeError}</Text>
                  </View>
                </View>
                :
                  null
                }

                {this.state.selectedCharge=='Fast charge' ?
                  <View>
                    <View>
                        <View style={styles.inputfield}>
                            <Text style={styles.inputtext}>Device Type</Text>
                            <Picker PickerData={this.state.deviceList}
                             label={'Please select category'}
                             color={'#000000'}
                             getTxt={(val,label)=> this.onClick(val,label,'deviceType')}/>
                         <Text style={styles.error}>{this.state.deviceTypeError}</Text>
                        </View>
                    </View>
                    {this.state.selectedDeviceType?
                      <View>
                        <View style={styles.inputfield}>
                            <Text style={styles.inputtext}>Regular Power Outlet</Text>
                            <Text>{this.state.outlet_type}</Text>
                        </View>
                        <View style={styles.inputfield}>
                            <Text style={styles.inputtext}>Effect</Text>
                            <Text>{this.state.effect}</Text>
                        </View>
                        <View style={styles.inputfield}>
                            <Text style={styles.inputtext}>Phases</Text>
                            <Text>{this.state.phases}</Text>
                        </View>
                        <View style={styles.inputfield}>
                            <Text style={styles.inputtext}>Ampere</Text>
                            <Text>{this.state.ampere}</Text>
                        </View>
                        {this.state.accomodation != ''?
                          <View style={styles.inputfield}>
                              <Text style={styles.inputtext}>Accomodation</Text>
                              <Text>{this.state.accomodation}</Text>
                          </View>
                        :
                          null
                        }
                        {this.state.accomodation_charges?
                          <View style={styles.inputfield}>
                              <Text style={styles.inputtext}>Accomodation Charges</Text>
                              <Text>{this.state.accomodation_charges}</Text>
                          </View>
                        :
                          null
                        }
                        <View style={styles.inputfield}>
                            <Text style={styles.inputtext}>Number of Charging Slots</Text>
                            <Text>{this.state.numberofslots}</Text>
                        </View>
                        <View style={styles.inputfield}>
                            <Text style={styles.inputtext}>Price</Text>
                            <Text>{this.state.price}</Text>
                        </View>
                        <View style={styles.inputfield}>
                            <Text style={styles.inputtext}>Description</Text>
                            <Text>{this.state.description}</Text>
                        </View>
                      </View>
                    :
                    null
                  }
                 </View>
                :
                this.state.selectedCharge=='Regular Power Outlet(Long Charge)' ?
                  <View>
                      <View style={styles.inputfield}>
                        <Text style={styles.inputtext}>Device Type</Text>
                        <Picker PickerData={this.state.deviceList}
                         label={'Please select category'}
                         color={'#000000'}
                         selectedValue={this.state.selectedDeviceType}
                         defaultLabel={this.state.selectedDeviceType}
                         getTxt={(val,label)=> this.onClick(val,label,'deviceType')}/>
                        <Text style={styles.error}>{this.state.deviceTypeError}</Text>
                      </View>
                    {this.state.selectedDeviceType?
                      <View>
                          <View style={styles.inputfield}>
                              <Text style={styles.inputtext}>Regular Power Outlet</Text>
                              <Text>{this.state.outlet_type}</Text>
                          </View>
                          {this.state.parking_fee != '' ?
                            <View style={styles.inputfield}>
                                <Text style={styles.inputtext}>Parking Fee</Text>
                                <Text>{this.state.parking_fee}</Text>
                            </View>
                          :
                            null
                          }
                          {this.state.accomodation != ''?
                            <View style={styles.inputfield}>
                                <Text style={styles.inputtext}>Accomodation</Text>
                                <Text>{this.state.accomodation}</Text>
                            </View>
                          :
                            null
                          }
                          {this.state.accomodation_charges != ''?
                            <View style={styles.inputfield}>
                                <Text style={styles.inputtext}>Accomodation Charges</Text>
                                <Text>{this.state.accomodation_charges}</Text>
                            </View>
                          :
                            null
                          }
                          <View style={styles.inputfield}>
                              <Text style={styles.inputtext}>Number of Charging Slots</Text>
                              <Text>{this.state.numberofslots}</Text>
                          </View>
                          <View style={styles.inputfield}>
                              <Text style={styles.inputtext}>Price</Text>
                              <Text>{this.state.price}</Text>
                          </View>
                          <View style={styles.inputfield}>
                              <Text style={styles.inputtext}>Description</Text>
                              <Text>{this.state.description}</Text>
                          </View>
                      </View>
                    :
                    null
                  }
                 </View>
                 :
                 null
                }

                {this.state.cat_id == '2'?
                <View>
                  <View>
                      <View style={styles.inputfield}>
                          <Text style={styles.inputtext}>Device Type</Text>
                          <Picker
                           PickerData={this.state.deviceList}
                           label={'Please select category'}
                           color={'#000000'}
                           getTxt={(val,label)=> this.onClick(val,label,'deviceType')}/>
                      </View>
                      {this.state.deviceTypeError != ''?
                        <Text style={styles.error}>{this.state.deviceTypeError}</Text>
                      :
                        null
                      }
                  </View>
                  {this.state.selectedDeviceType?
                    <View>
                      <View style={styles.inputfield}>
                          <Text style={styles.inputtext}>Rent per day</Text>
                          <Text>{this.state.reantperday}</Text>
                      </View>
                      {this.state.dlnumber == 'yes' ?
                        <View style={styles.inputfield}>
                            <Text style={styles.inputtext}>Drivers Licence Number</Text>
                            <Text>{this.state.dlnumber}</Text>
                        </View>
                      :
                        null
                      }
                      {this.state.insurance == 'yes'?
                        <View style={styles.inputfield}>
                            <Text style={styles.inputtext}>Insurance/Deposit</Text>
                            <Text>{this.state.insurance}</Text>
                        </View>
                      :
                        null
                      }
                      <View style={styles.inputfield}>
                          <Text style={styles.inputtext}>Description</Text>
                          <Text>{this.state.description}</Text>
                      </View>
                    </View>
                  :
                  null
                  }
                </View>
              :
                  null
                }

                {this.state.cat_id == '3'?
                  <View>
                    <View>
                        <View style={styles.inputfield}>
                            <Text style={styles.inputtext}>Device Type</Text>
                            <Picker PickerData={this.state.deviceList}
                             label={'Please select category'}
                             color={'#000000'}
                             getTxt={(val,label)=> this.onClick(val,label,'deviceType')}/>
                        </View>
                        <Text style={styles.error}>{this.state.deviceTypeError}</Text>
                    </View>
                    {this.state.selectedDeviceType?
                      <View>
                        <View style={styles.inputfield}>
                            <Text style={styles.inputtext}>Number of parking lots</Text>
                            <Text>{this.state.parkingLots}</Text>
                        </View>
                        <View style={styles.inputfield}>
                            <Text style={styles.inputtext}>Parking fee per lot</Text>
                            <Text>{this.state.parkingPrice}</Text>
                        </View>
                        <View style={styles.inputfield}>
                            <Text style={styles.inputtext}>Parking fee per day</Text>
                            <Text>{this.state.parkingperday}</Text>
                        </View>
                        {this.state.accomodation != ''?
                          <View style={styles.inputfield}>
                              <Text style={styles.inputtext}>Accomodation</Text>
                              <Text>{this.state.accomodation}</Text>
                          </View>
                        :
                          null
                        }
                        {this.state.accomodation_charges != ''?
                          <View style={styles.inputfield}>
                              <Text style={styles.inputtext}>Accomodation Charges</Text>
                              <Text>{this.state.accomodation_charges}</Text>
                          </View>
                        :
                          null
                        }
                        <View style={styles.inputfield}>
                            <Text style={styles.inputtext}>Description</Text>
                            <Text>{this.state.description}</Text>
                        </View>
                      </View>
                    :
                    null
                    }
                  </View>
                :
                    null
                }

              </View>
              </ScrollView>
            </View>
          }


        </View>
        {!this.state.empty?
          <TouchableOpacity onPress={this.submit_click} style={styles.SectionStyle}>
            <CommonButton label='Book Service' width='100%'/>
          </TouchableOpacity>
        :
          null
        }

     </View>
     <Spinner visible={this.state.isVisible}  />
    </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    paddingTop:  0,
  },

  content: {
      flex: 1,
      height:'70%',
      width:'100%'
  },
  mapcontainer:{
    flex:1,
  },
  formcontainer:{
    flex:1,
    backgroundColor:'#fff',
    padding:10
  },
   innercontainer:{
     width: '100%',
     height: '100%',
     resizeMode:'contain',
   },
   containerin: {
    height:80,
    padding: AppSizes.ResponsiveSize.Padding(2),
    flexDirection: 'row',
    alignItems: 'center',
    width:'100%',
    //backgroundColor:'green',
    marginBottom:10
  },
  texttitle: {
    marginLeft: AppSizes.ResponsiveSize.Padding(4),
    paddingBottom: AppSizes.ResponsiveSize.Padding(2),
    fontSize:AppSizes.ResponsiveSize.Sizes(18),
    fontWeight:'500',
    color:'#ffffff',
    width:'60%'
  },
  textmiles:{
    marginLeft: AppSizes.ResponsiveSize.Padding(4),
    paddingBottom: AppSizes.ResponsiveSize.Padding(2),
    fontSize:AppSizes.ResponsiveSize.Sizes(18),
    fontWeight:'500',
    color:'#ffffff',
    width:'40%'
  },
  text1: {
    marginLeft: AppSizes.ResponsiveSize.Padding(4),
    fontSize:AppSizes.ResponsiveSize.Sizes(12),
    color:'#ffffff',
  },
  photo: {
    flex:1,
    width: undefined,
    height: undefined,
},
map: {
    ...StyleSheet.absoluteFillObject,
},
textInput: {
flex:1,
paddingLeft:AppSizes.ResponsiveSize.Padding(3),
fontSize: AppSizes.ResponsiveSize.Sizes(12),
color:'#000000',
backgroundColor:'#eee',
height:40,
},
inputfield:{
  width:AppSizes.ResponsiveSize.width,

},
inputtext:{marginBottom:10,
  marginTop:AppSizes.ResponsiveSize.Padding(3),
  fontSize:AppSizes.ResponsiveSize.Sizes(14),
  fontWeight:'600',
  marginBottom:AppSizes.ResponsiveSize.Padding(1),
},
error:{
  color:'red',
  fontSize:AppSizes.ResponsiveSize.Sizes(12),
}
});
