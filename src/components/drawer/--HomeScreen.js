import React, { Component } from 'react';
import {AsyncStorage,StyleSheet,Alert,Text,View,Image,TouchableOpacity,Platform,WebView,ImageBackground,ScrollView,Dimensions} from 'react-native';
import { AppStyles, AppSizes, AppColors } from '../../themes/'
import { NavigationActions, StackActions  } from 'react-navigation';
import Spacer from '../common/Spacer'
import Header from '../common/Header'
import Spinner from 'react-native-loading-spinner-overlay';
import * as commonFunctions from '../../utils/CommonFunctions'
import LinearGradient from 'react-native-linear-gradient';
import MapView, { Marker, ProviderPropType } from 'react-native-maps';
import Geocoder from 'react-native-geocoder';
import { pickerData } from '../data/pickerData';

var Constant = require('../../api/ApiRules').Constant;
var WebServices = require('../../api/ApiRules').WebServices;

const bg_image = require('../../themes/Images/bg_image.png')
const all = require('../../themes/Images/station_icon.png')
const car = require('../../themes/Images/car_home.png')
const arrow = require('../../themes/Images/double_arrow.png')
const bike = require('../../themes/Images/bike_home.png')
const electric = require('../../themes/Images/station_home.png')
const rental = require('../../themes/Images/rental_home.png')
const parking = require('../../themes/Images/parking_home.png')
const bed = require('../../themes/Images/bed.png')
const cable = require('../../themes/Images/cable.png')
const gadget = require('../../themes/Images/gadget_home.png')
const cross = require('../../themes/Images/cross_120.png')

var help_audio=require('../../themes/sound/HomeHelp.mp3')


const { width, height } = Dimensions.get('window');
const ASPECT_RATIO = width / height;
const LATITUDE = 37.78825;
const LONGITUDE = -122.4324;
const LATITUDE_DELTA = 0.29;
const LONGITUDE_DELTA = LATITUDE_DELTA * ASPECT_RATIO;
const SPACE = 0.01;
const LAT= 30.7279571
const LANG= 76.8553046

let _that
export default class HomeScreen extends Component {
  constructor (props) {
    super(props)
    this.state = {
      uid:'',
      isVisible: false,
      Modal_Visibility: false,
      stationList:[],
      isMapReady: false,
      region: {
        latitude: LATITUDE,
        longitude: LONGITUDE,
        latitudeDelta: LATITUDE_DELTA,
        longitudeDelta: LONGITUDE_DELTA
      },
      image:all,
      cat:'',
      sub_cat:'',
      cat_id:'',
      subcat_id:'',
      other_cat_id:'',
      tab_el:false,
      tab_rent:false,
      tab_parking:false,
      bike:'0',
      gadget:'0',
      tabSelected:false,
      subCatSelected:false,
      fab:false
    },
    _that = this;
  }

  componentDidMount() {
    AsyncStorage.getItem('UserData').then((UserData) => {
      var data = JSON.parse(UserData);
        uid=data.id
        if(uid != ''){
          _that.setState({
            uid:uid
          })
        }
    })

    try {
    Geocoder.fallbackToGoogle('AIzaSyBX6rKXe6Jsk6ZynShEZiNfDfyhZWgmXsQ');
     navigator.geolocation.getCurrentPosition(
      (position) => {
        var region = {
          lat: position.coords.latitude,
          lng:  position.coords.longitude,
        };
        _that.validationAndApiParameter('ServiceList',uid,region.lat,region.lng)
        this.setState({
          region: {
            latitude: position.coords.latitude,
            longitude: position.coords.longitude,
            latitudeDelta: LATITUDE_DELTA,
            longitudeDelta: LONGITUDE_DELTA,
            accuracy: position.coords.accuracy
          }
        });
        Geocoder.geocodePosition(region).then(res => {
          //console.log(res)
          var address=res[0].formattedAddress
          _that.setState({
            latitude: position.coords.latitude,
            longitude: position.coords.longitude,
            address: address,
          });
        })
        this.watchID = navigator.geolocation.watchPosition((position) => {
          const newRegion = {
            latitude: position.coords.latitude,
            longitude: position.coords.longitude,
            latitudeDelta: LATITUDE_DELTA,
            longitudeDelta: LONGITUDE_DELTA,
            accuracy: position.coords.accuracy
          }
          this.setState({newRegion});
        })
      },
      (error) => console.log('error '+error.message),
      {enableHighAccuracy: false, timeout: 50000, maximumAge: 10000}
    );
  }
  catch(err) {
      console.log(err);
  }
}
  componentWillMount() {
    AsyncStorage.getItem('user_type').then((type) => {
        var type = JSON.parse(type);
        if(type=="seller"){
          _that.props.navigation.navigate('SellerDashboard');
        }
    })
  }

  updateAlert = (visible) => {
        this.setState({Modal_Visibility: !this.state.Modal_Visibility});
  }

  ServicelistBtn(name,id){
    if(this.state.cat_id=='1' && name=='Car'){
      this.setState({subcat_id:id})
      this.setState({sub_cat:name})
      _that.props.navigation.navigate('AvailableServices',{name:name,cat:this.state.cat,sub_cat:'car',cat_id:this.state.cat_id,subcat_id:id,other_cat_id:''});
      this.setState({tab_el:false}),
      this.setState({tab_rent:false}),
      this.setState({tab_parking:false})
      this.setState({tabSelected:false})
      this.setState({cat:''})
      this.setState({cat_id:''})
    }
    else if (this.state.cat_id=='1' && name=='Bike') {
      this.setState({sub_cat:name})
      this.setState({subcat_id:id})
      this.setState({bike:'1'})
      this.setState({gadget:'0'})
      this.setState({subCatSelected:true})
    }
    else if (this.state.cat_id=='1' && name=='Gadgets') {
      this.setState({sub_cat:name})
      this.setState({subcat_id:id})
      this.setState({bike:'0'})
      this.setState({gadget:'1'})
      this.setState({subCatSelected:true})
    }
    else if (this.state.cat_id=='2' && name=='Car') {
      this.setState({sub_cat:name})
      this.setState({subcat_id:id})
      _that.props.navigation.navigate('AvailableServices',{name:name,cat:this.state.cat,sub_cat:'car',cat_id:this.state.cat_id,subcat_id:id,other_cat_id:''});
      this.setState({cat:''})
      this.setState({cat_id:''})
      this.setState({tab_el:false}),
      this.setState({tabSelected:false})
      this.setState({tab_rent:false}),
      this.setState({tab_parking:false})
    }
    else if (this.state.cat_id=='2' && name == 'Bike') {
      this.setState({sub_cat:name})
      this.setState({subcat_id:id})
      this.setState({bike:'1'})
      this.setState({gadget:'0'})
      this.setState({subCatSelected:true})
    }
    else if (this.state.cat_id=='2' && name == 'Gadgets') {
      this.setState({sub_cat:name})
      this.setState({subcat_id:id})
      this.setState({bike:'0'})
      this.setState({gadget:'1'})
      this.setState({subCatSelected:true})
    }
    else if (this.state.cat_id=='3' && name == 'Car') {
      this.setState({sub_cat:name})
      this.setState({subcat_id:id})
      _that.props.navigation.navigate('AvailableServices',{name:name,cat:this.state.cat,sub_cat:'car',cat_id:this.state.cat_id,subcat_id:id,other_cat_id:''});
      this.setState({tab_el:false}),
      this.setState({tab_rent:false}),
      this.setState({tab_parking:false})
      this.setState({cat:''})
      this.setState({cat_id:''})
      this.setState({tabSelected:false})
    }
    else if (this.state.cat_id=='3' && name == 'Bike') {
      this.setState({sub_cat:name})
      this.setState({subcat_id:id})
      this.setState({bike:'1'})
      this.setState({gadget:'0'})
      this.setState({subCatSelected:true})
    }
  }
  handleOnPress(name,id){
    this.setState({tab_el:false}),
    this.setState({tab_rent:false}),
    this.setState({tab_parking:false})
    this.setState({sub_cat:''})
    this.setState({subcat_id:''})
    this.setState({cat:''})
    this.setState({cat_id:''})
    this.setState({tabSelected:false})
    this.setState({bike:'0'})
    this.setState({gadget:'0'})
    _that.props.navigation.navigate('AvailableServices',{name:name,cat:this.state.cat,sub_cat:this.state.sub_cat,cat_id:this.state.cat_id,subcat_id:this.state.subcat_id,other_cat_id:id});
  }

  validationAndApiParameter(apikey,id,lat,lng) {
    if(apikey=='ServiceList'){
      const data = new FormData();
      data.append('uid',id);
      data.append('latitude',lat);
      data.append('longitude',lng);
      //console.log('Data',data);
      _that.setState({isVisible: true});
      this.postToApiCalling('POST',apikey, Constant.URL_allStationList, data);
    }
  }

  postToApiCalling(method, apiKey, apiUrl, data) {
    new Promise(function(resolve, reject) {
      if (method == 'POST') {
        resolve(WebServices.callWebService(apiUrl, data));
      } else {
        resolve(WebServices.callWebService_GET(apiUrl, data));
      }
    }).then((jsonRes) => {
      //console.log(jsonRes);
      _that.setState({ isVisible: false })
      if ((!jsonRes) || (jsonRes.code == 0)) {
          setTimeout(()=>{
            Alert.alert('Login Error',jsonRes.message);
          },200);
      }
      else{
        if (jsonRes.code == 1) {
          _that.apiSuccessfullResponse(apiKey, jsonRes)
        }
      }
    }).catch((error) => {
        console.log("ERROR" + error);
        _that.setState({ isVisible: false })
        setTimeout(()=>{
            Alert.alert("Server issue"+error);
        },200);
    });
  }

  apiSuccessfullResponse(apiKey, jsonRes) {
      if (apiKey == 'ServiceList') {
          stationData=jsonRes.result;
          //console.log('Response',stationData);
          _that.setState({stationList:stationData})
      }
  }
  convert(data){
    var numb = data
    numb = parseFloat(numb).toFixed(2);
    return numb;
  }

    handleHeadingPress(name){
      if(name == 'electric'){
        this.setState({tabSelected:true})
        this.setState({cat:'electric'}),
        this.setState({sub_cat:''}),
        this.setState({bike:'0'}),
        this.setState({gadget:'0'}),
        this.setState({cat_id:'1'}),
        this.setState({tab_el:true}),
        this.setState({tab_rent:false}),
        this.setState({tab_parking:false})
        this.setState({subCatSelected:false})
      }
      else if (name == 'rentals') {
        this.setState({tabSelected:true})
        this.setState({cat:'rentals'}),
        this.setState({sub_cat:''}),
        this.setState({bike:'0'}),
        this.setState({gadget:'0'}),
        this.setState({cat_id:'2'}),
        this.setState({tab_el:false}),
        this.setState({tab_rent:true}),
        this.setState({tab_parking:false})
        this.setState({subCatSelected:false})
      }
      else if (name== 'parking') {
        this.setState({tabSelected:true})
        this.setState({cat:'parkings'}),
        this.setState({sub_cat:''}),
        this.setState({bike:'0'}),
        this.setState({gadget:'0'}),
        this.setState({cat_id:'3'}),
        this.setState({tab_el:false}),
        this.setState({tab_rent:false}),
        this.setState({tab_parking:true})
        this.setState({subCatSelected:false})
      }
    }

  render() {
    var popup_data=<Text>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.</Text>;

    const headerProp = {
      title: 'HOME',
      screens: 'HomeScreen',
      qus_content:popup_data,
      qus_audio:help_audio
        };
    return (

      <View style={styles.container}>
        <Header info={headerProp} navigation={_that.props.navigation} updateAlert={this.updateAlert}/>

        <View style={styles.content}>
          <View style={[this.state.tabSelected?styles.mapcontainer1:styles.mapcontainer]}>
            <MapView style={styles.map}
              region={this.state.region}
              provider='google'
              mapType='standard'
              showsCompass={true}
              followsUserLocation={true}
              showsMyLocationButton={true}
              loadingEnabled={true}
              showsPointsOfInterest
              showsUserLocation={true} >
              {this.state.stationList.map((marker) => (
                <Marker
                  coordinate={{latitude:Number(marker.latitude),longitude:Number(marker.longitude)}}
                  key={marker.id}
                  title={marker.station_name}
                  opacity={0.8}
                  icon={this.state.image}
                />
              ))}
            </MapView>
            <View style={styles.bottombar}>
              {this.state.tabSelected?
                <TouchableOpacity onPress={() => {this.setState({tabSelected:false}),this.setState({tab_el:false}),this.setState({tab_rent:false}),this.setState({tab_parking:false})}} style={styles.fabContainer}>
                  <Image source={cross} style={styles.fab}/>
                </TouchableOpacity>
              :
                null
              }
              <TouchableOpacity style={[this.state.tab_el?styles.activeheading1:styles.headingbtn1]} onPress={()=> this.handleHeadingPress('electric')}>
                <Image source={electric} style={{height:30,width:30}}/>
                <Text style={styles.tab}>Charging</Text>
              </TouchableOpacity>
              <TouchableOpacity style={[this.state.tab_rent?styles.activeheading:styles.headingbtn]}  onPress={()=>this.handleHeadingPress('rentals')}>
                <Image source={rental} style={{height:30,width:30}}/>
                <Text style={styles.tab}>Rentals</Text>
              </TouchableOpacity>
              <TouchableOpacity style={[this.state.tab_parking?styles.activeheading:styles.headingbtn]}  onPress={()=>this.handleHeadingPress('parking')}>
                <Image source={parking} style={{height:30,width:30}}/>
                <Text style={styles.tab}>Parking</Text>
              </TouchableOpacity>
            </View>
          </View>

          <View style={[this.state.tabSelected?styles.formcontainer1:styles.formcontainer]}>
            <ImageBackground source={bg_image} style={styles.innercontainer}>
              <View style={{padding:AppSizes.ResponsiveSize.Padding(3),paddingBottom:10,flexDirection:'row'}}>
                <View style={[this.state.subCatSelected?{width:'40%'}:{width:'100%'}]}>
                 {this.state.cat == 'electric'?
                   <View style={{height:'100%'}}>
                     <TouchableOpacity style={[styles.containerin,styles.shadow1]} onPress={()=>{this.ServicelistBtn('Car','1')}}>
                        <View style={styles.side_icon}>
                          <Image source={car} style={{height:20,width:20}}/>
                        </View>
                       <Text style={styles.texttitle}>Car</Text>
                     </TouchableOpacity>
                     <TouchableOpacity style={[styles.containerin,styles.shadow1]} onPress={()=>{this.ServicelistBtn('Bike','2')}}>
                       <View style={styles.side_icon}>
                         <Image source={bike} style={{height:20,width:20}}/>
                       </View>
                       <Text style={styles.texttitle}>Bike</Text>
                       {this.state.bike == '1'?
                        <Image source={arrow} style={{height:10,width:10}}/>
                       :
                        null
                       }
                     </TouchableOpacity>
                     <TouchableOpacity style={[styles.containerin,styles.shadow1]} onPress={()=>{this.ServicelistBtn('Gadgets','5')}}>
                       <View style={styles.side_icon}>
                         <Image source={gadget} style={{height:20,width:20}}/>
                       </View>
                       <Text style={styles.texttitle}>Gadgets</Text>
                       {this.state.gadget == '1'?
                        <Image source={arrow} style={{height:10,width:10}}/>
                       :
                        null
                       }
                     </TouchableOpacity>
                   </View>
                 :
                   null
                 }
                 {this.state.cat == 'rentals'?
                   <View style={{height:'100%'}}>
                     <TouchableOpacity style={[styles.containerin,styles.shadow1]} onPress={()=>{this.ServicelistBtn('Car','3')}}>
                     <View style={styles.side_icon}>
                       <Image source={car} style={{height:20,width:20}}/>
                     </View>
                       <Text style={styles.texttitle}>Car</Text>
                     </TouchableOpacity>
                     <TouchableOpacity style={[styles.containerin,styles.shadow1]} onPress={()=>{this.ServicelistBtn('Bike','4')}}>
                     <View style={styles.side_icon}>
                       <Image source={bike} style={{height:20,width:20}}/>
                     </View>
                       <Text style={styles.texttitle}>Bike</Text>
                       {this.state.bike == '1'?
                        <Image source={arrow} style={{height:10,width:10}}/>
                       :
                        null
                       }
                     </TouchableOpacity>
                     <TouchableOpacity style={[styles.containerin,styles.shadow1]} onPress={()=>{this.ServicelistBtn('Gadgets','6')}}>
                     <View style={styles.side_icon}>
                       <Image source={gadget} style={{height:20,width:20}}/>
                     </View>
                       <Text style={styles.texttitle}>Gadgets</Text>
                       {this.state.gadget == '1'?
                        <Image source={arrow} style={{height:10,width:10}}/>
                       :
                        null
                       }
                     </TouchableOpacity>
                   </View>
                 :
                   null
                 }
                 {this.state.cat == 'parkings'?
                   <View style={{height:'100%'}}>
                     <TouchableOpacity style={[styles.containerin,styles.shadow1]} onPress={()=>{this.ServicelistBtn('Car','7')}}>
                     <View style={styles.side_icon}>
                       <Image source={car} style={{height:20,width:20}}/>
                     </View>
                       <Text style={styles.texttitle}>Car</Text>
                     </TouchableOpacity>
                     <TouchableOpacity style={[styles.containerin,styles.shadow1]} onPress={()=>{this.ServicelistBtn('Bike','8')}}>
                     <View style={styles.side_icon}>
                       <Image source={bike} style={{height:20,width:20}}/>
                     </View>
                       <Text style={styles.texttitle}>Bike</Text>
                       {this.state.bike == '1'?
                        <Image source={arrow} style={{height:10,width:10,}}/>
                       :
                        null
                       }
                     </TouchableOpacity>
                   </View>
                 :
                   null
                 }
                </View>
                <View style={{width:'60%'}}>
                 {this.state.sub_cat == 'Bike' && this.state.cat=='electric'?
                   <View style={{height:'100%',alignItems:'center',paddingLeft:10,paddingRight:10}}>
                     <TouchableOpacity style={[styles.subCatContainer,styles.shadow1]} onPress={()=>this.handleOnPress('ElBike','5')}>
                       <Text style={styles.texttitle1}>ElBike</Text>
                     </TouchableOpacity>
                     <TouchableOpacity style={[styles.subCatContainer,styles.shadow1]} onPress={()=>this.handleOnPress('Scooters','1')}>
                       <Text style={styles.texttitle1}>Scooters</Text>
                     </TouchableOpacity>
                     <TouchableOpacity style={[styles.subCatContainer,styles.shadow1]} onPress={()=>this.handleOnPress('Skateboards','3')}>
                       <Text style={styles.texttitle1}>Skateboards</Text>
                     </TouchableOpacity>
                     <TouchableOpacity style={[styles.subCatContainer,styles.shadow1]} onPress={()=>this.handleOnPress('3wheelers','6')}>
                       <Text style={styles.texttitle1}>3wheelers</Text>
                     </TouchableOpacity>
                   </View>
                 :
                 this.state.sub_cat == 'Bike' && this.state.cat=='rentals'?
                   <View style={{height:'100%',alignItems:'center',paddingLeft:10,paddingRight:10}}>
                     <TouchableOpacity style={[styles.subCatContainer,styles.shadow1]} onPress={()=>this.handleOnPress('ElBike','7')}>
                       <Text style={styles.texttitle1}>ElBike</Text>
                     </TouchableOpacity>
                     <TouchableOpacity style={[styles.subCatContainer,styles.shadow1]} onPress={()=>this.handleOnPress('Scooters','2')}>
                       <Text style={styles.texttitle1}>Scooters</Text>
                     </TouchableOpacity>
                     <TouchableOpacity style={[styles.subCatContainer,styles.shadow1]} onPress={()=>this.handleOnPress('Skateboards','4')}>
                       <Text style={styles.texttitle1}>Skateboards</Text>
                     </TouchableOpacity>
                     <TouchableOpacity style={[styles.subCatContainer,styles.shadow1]} onPress={()=>this.handleOnPress('3wheelers','13')}>
                       <Text style={styles.texttitle1}>3wheelers</Text>
                     </TouchableOpacity>
                   </View>
                  :
                  this.state.sub_cat == 'Bike' && this.state.cat=='parkings'?
                    <View style={{height:'100%',alignItems:'center',paddingLeft:10,paddingRight:10}}>
                      <TouchableOpacity style={[styles.subCatContainer,styles.shadow1]} onPress={()=>this.handleOnPress('ElBike','16')}>
                        <Text style={styles.texttitle1}>ElBike</Text>
                      </TouchableOpacity>
                      <TouchableOpacity style={[styles.subCatContainer,styles.shadow1]} onPress={()=>this.handleOnPress('Scooters','15')}>
                        <Text style={styles.texttitle1}>Scooters</Text>
                      </TouchableOpacity>
                      <TouchableOpacity style={[styles.subCatContainer,styles.shadow1]} onPress={()=>this.handleOnPress('Skateboards','14')}>
                        <Text style={styles.texttitle1}>Skateboards</Text>
                      </TouchableOpacity>
                      <TouchableOpacity style={[styles.subCatContainer,styles.shadow1]} onPress={()=>this.handleOnPress('3wheelers','17')}>
                        <Text style={styles.texttitle1}>3wheelers</Text>
                      </TouchableOpacity>
                    </View>
                  :
                   null
                 }
                 {this.state.sub_cat == 'Gadgets' && this.state.cat=='electric'?
                   <View style={{height:'100%',alignItems:'center',paddingLeft:10,paddingRight:10}}>
                     <TouchableOpacity style={[styles.subCatContainer,styles.shadow1]} onPress={()=>this.handleOnPress('Laptops','18')}>
                       <Text style={styles.texttitle1}>Laptops</Text>
                     </TouchableOpacity>
                     <TouchableOpacity style={[styles.subCatContainer,styles.shadow1]} onPress={()=>this.handleOnPress('Drone','19')}>
                       <Text style={styles.texttitle1}>Drone</Text>
                     </TouchableOpacity>
                     <TouchableOpacity style={[styles.subCatContainer,styles.shadow1]} onPress={()=>this.handleOnPress('Mobile Phones','20')}>
                       <Text style={styles.texttitle1}>Mobile Phones</Text>
                     </TouchableOpacity>
                     <TouchableOpacity style={[styles.subCatContainer,styles.shadow1]} onPress={()=>this.handleOnPress('PC','21')}>
                       <Text style={styles.texttitle1}>PC</Text>
                     </TouchableOpacity>
                     <TouchableOpacity style={[styles.subCatContainer,styles.shadow1]} onPress={()=>this.handleOnPress('Filling for drones','22')}>
                       <Text style={styles.texttitle1}>Filling for drones</Text>
                     </TouchableOpacity>
                   </View>
                 :
                 this.state.sub_cat == 'Gadgets' && this.state.cat=='rentals'?
                   <View style={{height:'100%',alignItems:'center',paddingLeft:10,paddingRight:10}}>
                     <TouchableOpacity style={[styles.subCatContainer,styles.shadow1]} onPress={()=>this.handleOnPress('Laptops','24')}>
                       <Text style={styles.texttitle1}>Laptops</Text>
                     </TouchableOpacity>
                     <TouchableOpacity style={[styles.subCatContainer,styles.shadow1]} onPress={()=>this.handleOnPress('Drone','27')}>
                       <Text style={styles.texttitle1}>Drone</Text>
                     </TouchableOpacity>
                     <TouchableOpacity style={[styles.subCatContainer,styles.shadow1]} onPress={()=>this.handleOnPress('Mobile Phones','25')}>
                       <Text style={styles.texttitle1}>Mobile Phones</Text>
                     </TouchableOpacity>
                     <TouchableOpacity style={[styles.subCatContainer,styles.shadow1]} onPress={()=>this.handleOnPress('PC','26')}>
                       <Text style={styles.texttitle1}>PC</Text>
                     </TouchableOpacity>
                     <TouchableOpacity style={[styles.subCatContainer,styles.shadow1]} onPress={()=>this.handleOnPress('Filling for drones','23')}>
                       <Text style={styles.texttitle1}>Filling for drones</Text>
                     </TouchableOpacity>
                   </View>
                  :
                   null
                 }
                </View>
              </View>
            </ImageBackground>
          </View>
        </View>
        <Spinner visible={this.state.isVisible}  />
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    paddingTop:  0,
  },
  content: {
      flex: 1,
      height:'70%',
      width:'100%'
  },
  mapcontainer1:{
    flex:1
  },
  mapcontainer:{
    height:'100%'
  },
  formcontainer1:{
    flex:1
  },
   image: {
     flex: 1,
     width: undefined,
     height: undefined,
     resizeMode:'contain'
   },
   innercontainer:{
     width: '100%',
     height: '100%',
     resizeMode:'contain',
   },
   containerin: {
    height:'25%',
    padding: AppSizes.ResponsiveSize.Padding(2),
    flexDirection: 'row',
    alignItems: 'center',
    width:'100%',
    padding:10,
    alignItems:'center',
    backgroundColor:'#261535',
    marginBottom:10
  },
  texttitle: {
    marginLeft: AppSizes.ResponsiveSize.Padding(2),
    fontSize:AppSizes.ResponsiveSize.Sizes(18),
    fontWeight:'500',
    color:'#ffffff',
    width:'60%',
    //backgroundColor:'red',
  },
  texttitle1: {
    marginLeft: AppSizes.ResponsiveSize.Padding(2),
    fontSize:AppSizes.ResponsiveSize.Sizes(18),
    fontWeight:'500',
    color:'#ffffff',
    width:'70%',
    //backgroundColor:'red',
  },
map: {
    ...StyleSheet.absoluteFillObject,
    height:'90%'
},
tab:{
  color:'#fff',
  fontSize:AppSizes.ResponsiveSize.Sizes(20),
  paddingLeft:AppSizes.ResponsiveSize.Padding(4),
  paddingRight:AppSizes.ResponsiveSize.Padding(4),
  //backgroundColor:'green',
  paddingTop:AppSizes.ResponsiveSize.Padding(2),
  paddingBottom:AppSizes.ResponsiveSize.Padding(2),
  textAlign:'center'
},
border:{
  height:2,
  backgroundColor:'#fff'
},
subCatContainer:{
   padding: AppSizes.ResponsiveSize.Padding(2),
   flexDirection: 'row',
   alignItems: 'center',
   width:'100%',
   paddingTop:10,
   paddingBottom:10,
   justifyContent:'center',
   //backgroundColor:'green',
   marginBottom:10
},
headingbtn:{
  width:'30%',
  height:70,
  justifyContent:'center',
  alignItems:'center',
  backgroundColor:'#4a3554',
  paddingTop:AppSizes.ResponsiveSize.Padding(2),
  paddingBottom:AppSizes.ResponsiveSize.Padding(2)
},
activeheading:{
  width:'30%',
  height:70,
  justifyContent:'center',
  alignItems:'center',
  backgroundColor:'#1e0730',
  paddingTop:AppSizes.ResponsiveSize.Padding(2),
  paddingBottom:AppSizes.ResponsiveSize.Padding(2)
},
headingbtn1:{
  height:70,
  justifyContent:'center',
  alignItems:'center',
  width:'40%',
  backgroundColor:'#4a3554',
  paddingTop:AppSizes.ResponsiveSize.Padding(2),
  paddingBottom:AppSizes.ResponsiveSize.Padding(2)
},
activeheading1:{
  height:70,
  justifyContent:'center',
  alignItems:'center',
  width:'40%',
  backgroundColor:'#1e0730',
  paddingTop:AppSizes.ResponsiveSize.Padding(2),
  paddingBottom:AppSizes.ResponsiveSize.Padding(2)
},
shadow:{
  borderRadius: 2,
  borderColor: '#fff',
  justifyContent:'center',
  borderColor: '#fff',
  borderWidth: 0,
  shadowColor: '#fff',
  shadowOffset: { width: 2, height: 1 },
  shadowOpacity: 1,
  shadowRadius: 2,
  elevation: 10,
  zIndex:0,
  backgroundColor:'#4a3554',
},
shadow1:{
  borderRadius: 5,
  borderColor: '#fff',
  justifyContent:'center',
  alignItems:'center',
  borderColor: '#fff',
  borderWidth: 0,
  shadowColor: '#fff',
  shadowOffset: { width: 2, height: 1 },
  shadowOpacity: 1,
  shadowRadius: 2,
  elevation: 10,
  zIndex:0,
  backgroundColor:'#261535',
},
toptab:{
  flexDirection:'row',
  alignItems:'center',
  width:'100%'
},
side_icon:{
  marginRight:AppSizes.ResponsiveSize.Padding(3),
  borderRadius:50,
  backgroundColor:'#4a3554',
  padding:8
},
  bottombar:{
    width:'100%',
    justifyContent:'flex-end',
    flexDirection:'row',
    position:'absolute',
    bottom:0,
    //backgroundColor:'green'
  },
  fab:{
    height:30,
    width:30
  },
  fabContainer:{
    width: 40,
    height: 40,
    alignItems: 'center',
    justifyContent: 'center',
    right: 20,
    bottom: 80,
    backgroundColor: '#4a3554',
    borderRadius: 30,
    elevation: 8,
    position:'absolute'
  }

});
