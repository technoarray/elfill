import React, { Component } from 'react';
import {StyleSheet,Text,View,Image,TouchableOpacity,Platform,Alert,ScrollView,TextInput} from 'react-native';
import { AppStyles, AppSizes, AppColors } from '../../themes/'
import { NavigationActions } from 'react-navigation'
import Spacer from '../common/Spacer'
import * as commonFunctions from '../../utils/CommonFunctions'
import Header from '../common/HeaderWithBack'
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view'
import CommonButton from '../common/CommonButton'
import { Fonts } from '../../utils/Fonts';
var Constant = require('../../api/ApiRules').Constant;

/* Images */


export default class ViewPaymentsScreen extends Component {
  constructor (props) {
      super(props)
      this.state = {
      isVisible: false,
      item:this.props.navigation.state.params.item
    },
    _that = this;
  }

  render() {
    const headerProp = {
      title: 'Payment',
      screens: 'PaymentsScreen',
      type:''
    };
    return (
      <View style={styles.container}>
      <Header info={headerProp} navigation={_that.props.navigation}/>

        <View style={styles.content}>
        <KeyboardAwareScrollView innerRef={() => {return [this.refs.name,this.refs.email, this.refs.password]}} >

          <View style={styles.container2}>

              <View style={styles.SectionStyle}>
                  <View style={styles.inputfield}>
                      <Text style={styles.inputtext}>Order Id</Text>
                      <Text style={styles.smalltext}>#{this.state.item.order_id}</Text>
                  </View>
              </View>
              <View style={styles.SectionStyle}>
                  <View style={styles.inputfield}>
                      <Text style={styles.inputtext}>Station Name</Text>
                      <Text style={styles.smalltext}>{this.state.item.station_name}</Text>
                  </View>
              </View>
              <View style={styles.SectionStyle}>
                  <View style={styles.inputfield}>
                      <Text style={styles.inputtext}>Category </Text>
                      <Text style={styles.smalltext}>{this.state.item.cat_name}</Text>
                  </View>
              </View>
              <View style={styles.SectionStyle}>
                  <View style={styles.inputfield}>
                      <Text style={styles.inputtext}>SubCategory </Text>
                      <Text style={styles.smalltext}>{this.state.item.subcat_name}</Text>
                  </View>
              </View>
              {this.state.item.charge_type?
                <View style={styles.SectionStyle}>
                    <View style={styles.inputfield}>
                        <Text style={styles.inputtext}>Charge Type </Text>
                        <Text style={styles.smalltext}>{this.state.item.charge_type}</Text>
                    </View>
                </View>
              :
                null
              }
              <View style={styles.SectionStyle}>
                  <View style={styles.inputfield}>
                      <Text style={styles.inputtext}>Device Name </Text>
                      <Text style={styles.smalltext}>{this.state.item.device_name}</Text>
                  </View>
              </View>
              {this.state.item.outlet_type?
                <View style={styles.SectionStyle}>
                    <View style={styles.inputfield}>
                        <Text style={styles.inputtext}>Regular Power Outlet</Text>
                        <Text style={styles.smalltext}>{this.state.item.outlet_type}</Text>
                    </View>
                </View>
              :
                null
              }
              {this.state.item.effect?
                <View style={styles.SectionStyle}>
                    <View style={styles.inputfield}>
                        <Text style={styles.inputtext}>Effect</Text>
                        <Text style={styles.smalltext}>{this.state.item.effect}</Text>
                    </View>
                </View>
              :
                null
              }
              {this.state.item.phases?
                <View style={styles.SectionStyle}>
                    <View style={styles.inputfield}>
                        <Text style={styles.inputtext}>Phases</Text>
                        <Text style={styles.smalltext}>{this.state.item.phases}</Text>
                    </View>
                </View>
              :
                null
              }
              {this.state.item.ampere?
                <View style={styles.SectionStyle}>
                    <View style={styles.inputfield}>
                        <Text style={styles.inputtext}>Ampere</Text>
                        <Text style={styles.smalltext}>{this.state.item.ampere}</Text>
                    </View>
                </View>
              :
                null
              }
              {this.state.item.car_reg_no?
                <View style={styles.SectionStyle}>
                    <View style={styles.inputfield}>
                        <Text style={styles.inputtext}>Car Registration Number</Text>
                        <Text style={styles.smalltext}>{this.state.item.car_reg_no}</Text>
                    </View>
                </View>
              :
                null
              }
              {this.state.item.parking_fee?
                <View style={styles.SectionStyle}>
                    <View style={styles.inputfield}>
                        <Text style={styles.inputtext}>Parking Fee</Text>
                        <Text style={styles.smalltext}>{this.state.item.parking_fee}</Text>
                    </View>
                </View>
              :
                null
              }
              {this.state.item.accomodation?
                <View style={styles.SectionStyle}>
                    <View style={styles.inputfield}>
                        <Text style={styles.inputtext}>Accomodation</Text>
                        <Text style={styles.smalltext}>{this.state.item.accomodation}</Text>
                    </View>
                </View>
              :
                null
              }
              {this.state.item.coffee?
                <View style={styles.SectionStyle}>
                    <View style={styles.inputfield}>
                        <Text style={styles.inputtext}>Coffee</Text>
                        <Text style={styles.smalltext}>{this.state.item.coffee}</Text>
                    </View>
                </View>
              :
                null
              }
              {this.state.item.private_home_facility?
                <View style={styles.SectionStyle}>
                    <View style={styles.inputfield}>
                        <Text style={styles.inputtext}>Private Home Facility</Text>
                        <Text style={styles.smalltext}>{this.state.item.private_home_facility}</Text>
                    </View>
                </View>
              :
                null
              }
              {this.state.item.public_facility?
                <View style={styles.SectionStyle}>
                    <View style={styles.inputfield}>
                        <Text style={styles.inputtext}>Public Facility</Text>
                        <Text style={styles.smalltext}>{this.state.item.public_facility}</Text>
                    </View>
                </View>
              :
                null
              }
              {this.state.item.portable_power_bank?
                <View style={styles.SectionStyle}>
                    <View style={styles.inputfield}>
                        <Text style={styles.inputtext}>Poratable Power Bank</Text>
                        <Text style={styles.smalltext}>{this.state.item.portable_power_bank}</Text>
                    </View>
                </View>
              :
                null
              }
              {this.state.item.other_facility?
                <View style={styles.SectionStyle}>
                    <View style={styles.inputfield}>
                        <Text style={styles.inputtext}>Other Facility</Text>
                        <Text style={styles.smalltext}>{this.state.item.other_facility}</Text>
                    </View>
                </View>
              :
                null
              }
              {this.state.item.driver_licence_no?
                <View style={styles.SectionStyle}>
                    <View style={styles.inputfield}>
                        <Text style={styles.inputtext}>Driver Licence Number</Text>
                        <Text style={styles.smalltext}>{this.state.item.driver_licence_no}</Text>
                    </View>
                </View>
              :
                null
              }
              {this.state.item.insurance?
                <View style={styles.SectionStyle}>
                    <View style={styles.inputfield}>
                        <Text style={styles.inputtext}>Insurance/Deposit</Text>
                        <Text style={styles.smalltext}>{this.state.item.insurance}</Text>
                    </View>
                </View>
              :
                null
              }
              <View style={styles.SectionStyle}>
                  <View style={styles.inputfield}>
                      <Text style={styles.inputtext}>Price </Text>
                      <Text style={styles.smalltext}>${this.state.item.price}</Text>
                  </View>
              </View>
              <View style={styles.SectionStyle}>
                  <View style={styles.inputfield}>
                      <Text style={styles.inputtext}>Order Date </Text>
                      <Text style={styles.smalltext}>{this.state.item.order_date}</Text>
                  </View>
              </View>
              <View style={styles.SectionStyle}>
                  <View style={styles.inputfield}>
                      <Text style={styles.inputtext}>Order Time </Text>
                      <Text style={styles.smalltext}>{this.state.item.order_time}</Text>
                  </View>
              </View>

              {/*<View style={styles.SectionStyle}>
                  <View style={styles.inputfield}>
                      <Text style={styles.inputtext}>Status</Text>
                        <View style={styles.highlightbox}>
                          <Text style={[styles.smalltext,styles.highlight]}>{this.state.item.payment_status}</Text>
                        </View>
                  </View>
              </View>*/}
          </View>
        </KeyboardAwareScrollView>
            {/*<View style={styles.container3}>
            <TouchableOpacity onPress={this.submit_click}>
            <View style={[styles.SectionStyle,{marginTop:'5%',borderBottomColor: 'red',justifyContent:'flex-end'}]}>
              <CommonButton label='Edit Profile' width='100%'/>
              </View>
            </TouchableOpacity>
            </View>*/}

        </View>
    </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    paddingTop:  0,
    backgroundColor:'#fff'
  },

  content: {
      flex: 1,
      flexDirection: 'column',
      height:'100%',
      padding:'5%',
      //backgroundColor:'#000'
  },

  container2: {
    height:'100%',
    flexDirection: 'column',
    //backgroundColor:'blue'
  },
  container3: {
    flex: 1,
    height:AppSizes.screen.width/8,
    alignItems: 'center',//replace with flex-end or center
    justifyContent: 'flex-end',
    //backgroundColor:'green'


  },

headerTitle:{
  fontSize:AppSizes.ResponsiveSize.Sizes(25),
  color:'#000000',
  fontWeight:'bold',
  fontFamily:Fonts.RobotoBold,
  paddingBottom:AppSizes.ResponsiveSize.Padding(1),
},
headersubTitle:{
  fontSize:AppSizes.ResponsiveSize.Sizes(14),
  color:'#000000',
  fontWeight:'600',
  fontFamily:Fonts.RobotoBold,
  paddingTop:AppSizes.ResponsiveSize.Padding(2),
},
headerBorder :{
  borderBottomColor: '#000000',
  borderBottomWidth: 3,
  width:'10%',
},
footertext :{
 color :'#000000',
 fontSize:AppSizes.ResponsiveSize.Sizes(12),
 fontWeight:'400',
 fontFamily:Fonts.RobotoRegular,
},
SectionStyle: {
  width:AppSizes.ResponsiveSize.width,

},
smalltext:{
  fontSize:AppSizes.ResponsiveSize.Sizes(13),
  color:'#333',
  fontWeight:'400',
fontFamily:Fonts.RobotoRegular,

},

inputtext:{marginBottom:10,
  marginTop:AppSizes.ResponsiveSize.Padding(3),
  fontSize:AppSizes.ResponsiveSize.Sizes(16),
  fontWeight:'600',
  marginBottom:AppSizes.ResponsiveSize.Padding(1)
},
highlight:{
  backgroundColor:'green',
  padding:AppSizes.ResponsiveSize.Padding(1),
  paddingTop:AppSizes.ResponsiveSize.Padding(.10),
  paddingBottom:AppSizes.ResponsiveSize.Padding(.10),
  borderRadius:15,
  color:'#fff',
  flexDirection:'row'
},
highlightbox:{

  flexDirection:'row',
  flexWrap:'wrap',
},
inputfield:{
  flexDirection: 'row',
  justifyContent: 'space-between',
  alignItems: 'center',
}

});
