import React, { Component } from 'react';
import {AsyncStorage,StyleSheet,Alert,Text,View,PixelRatio,Image,TouchableOpacity,Platform,WebView,ImageBackground,ScrollView,Dimensions} from 'react-native';
import { AppStyles, AppSizes, AppColors } from '../../themes/'
import { NavigationActions, StackActions  } from 'react-navigation';
import Spacer from '../common/Spacer'
import Header from '../common/HeaderWithBack';
import Spinner from 'react-native-loading-spinner-overlay';
import * as commonFunctions from '../../utils/CommonFunctions'
import LinearGradient from 'react-native-linear-gradient';
import ImagePicker from 'react-native-image-picker';
import Picker from 'react-native-q-picker';
import CommonButton from '../common/CommonButton'
import { Fonts } from '../../utils/Fonts';
var Constant = require('../../api/ApiRules').Constant;
var WebServices = require('../../api/ApiRules').WebServices;


const bg_image = require('../../themes/Images/bg_image.png')
const all = require('../../themes/Images/station_icon.png')
const list_car = require('../../themes/Images/list_car.png')

let _that
export default class HomeScreen extends Component {
  constructor (props) {
      super(props)
      this.state = {
        uid:this.props.navigation.state.params.uid,
        station_id:this.props.navigation.state.params.id,
        isVisible: false,
        cat_id:this.props.navigation.state.params.cat_id,
        subcat_id:this.props.navigation.state.params.subcat_id,
        lat:this.props.navigation.state.params.lat,
        lgt:this.props.navigation.state.params.lgt,
        serviceData:[],
        deviceList:[],
        selectedDeviceType:'',
        price:'',
        description:'',
        numberofslots:'',
        outlet_type:'',
        chargedata:[],
        selectedCharge:'',
        private_facility:'',
        public_facility:'',
        power_bank:'',
        other_facility:'',
        effect:'',
        phases:'',
        ampere:'',
        parking_fee:'',
        accomodation:'',
        reg_no:'',
        coffee:'',
        dlnumber:'',
        upload_licence:'',
        insurance:'',
        chargeError:'',
        deviceTypeError:'',
        uploadError:'',
        stationList:[],
        cat:this.props.navigation.state.params.cat,
        sub_cat:this.props.navigation.state.params.sub_cat,
        error: null,
        name:this.props.navigation.state.params.name,
        service_image:'',
        accomodation_charges:'',
        cable_charges:'',
        empty:true
    },
    _that = this;
  }

  componentDidMount() {
    AsyncStorage.getItem('UserData').then((UserData) => {
      var data = JSON.parse(UserData);
        uid=data.id
        if(uid != ''){
          _that.setState({
            uid:uid
          })
        }
    })
    _that.validationAndApiParameter('stationList')
  }

  onSelect(index, value){
    this.setState({status: value})
  }

  submit_click=() =>{
    _that.validationAndApiParameter('buy');
  }

  onClick(val,label,apiKey){
    this.setState({deviceTypeError:''})
    this.setState({selectedDeviceType:''});
    this.setState({selectedDevice:label})
    this.setState({selectedDeviceType: val});
    var type=this.state.stationData
    for (var i = 0; i < type.length; i++) {
      if(type[i].device_id==val){
        this.setState({price:type[i].price})
        this.setState({description:type[i].description})
        this.setState({numberofslots:type[i].no_of_charging_spot})
        this.setState({outlet_type:type[i].outlet_type})
        this.setState({private_facility:type[i].private_home_facility})
        this.setState({public_facility:type[i].public_facility})
        this.setState({power_bank:type[i].portable_power_bank})
        this.setState({other_facility:type[i].other_facility})
        this.setState({effect:type[i].effect})
        this.setState({rentperday:type[i].rentperday})
        this.setState({phases:type[i].phases})
        this.setState({ampere:type[i].ampere})
        this.setState({parking_fee:type[i].parking_fee})
        this.setState({reg_no:type[i].car_reg_no})
        this.setState({accomodation:type[i].accomodation})
        this.setState({coffee:type[i].coffee})
        this.setState({cable_charges:type[i].cable_charges})
        this.setState({dlnumber:type[i].driver_licence_no})
        this.setState({upload_licence:type[i].upload_driver_licence})
        this.setState({insurance:type[i].insurance})
        this.setState({service_id:type[i].id})
        this.setState({accomodation_charges:type[i].accomodation_charges})
      }
    }
  }

  validationAndApiParameter(apikey,id,lat,lng) {
    if(apikey=='stationList'){
      const data = new FormData();
      data.append('station_id',this.state.station_id);
      data.append('cat_id',this.state.cat_id);
      data.append('subcat_id',this.state.subcat_id)

      console.log(data);
      _that.setState({isVisible: true});
      this.postToApiCalling('POST',apikey, Constant.URL_servicesToBuyer, data);
    }
  }

  postToApiCalling(method, apiKey, apiUrl, data) {
    new Promise(function(resolve, reject) {
      if (method == 'POST') {
        resolve(WebServices.callWebService(apiUrl, data));
      } else {
        resolve(WebServices.callWebService_GET(apiUrl, data));
      }
    }).then((jsonRes) => {
      console.log(jsonRes);
      _that.setState({ isVisible: false })
      if ((!jsonRes) || (jsonRes.code == 0)) {
          setTimeout(()=>{
            Alert.alert('Login Error',jsonRes.message);
          },200);
      }
      else if(jsonRes.code == 1){
        this.setState({empty:false})
        _that.apiSuccessfullResponse(apiKey, jsonRes)
      }
      else if (jsonRes.code == 2) {
        this.setState({empty:true})
      }
    }).catch((error) => {
        console.log("ERROR" + error);
        _that.setState({ isVisible: false })
        setTimeout(()=>{
            Alert.alert("Server issue"+error);
        },200);
    });
  }

  apiSuccessfullResponse(apiKey, jsonRes) {
    if (apiKey == 'stationList') {
      stationData=jsonRes.result;
      this.setState({serviceData:stationData})
      console.log(stationData);
      if(stationData.length>0){
        this.setState({empty:false})
      }
    }
  }
  convert(data){
    var numb = data
    numb = parseFloat(numb).toFixed(2);
    return numb;
  }

  render() {
    const headerProp = {
      title: 'Services List',
      screens: 'Serviceslist',
      type:''
    };
    return (

      <View style={styles.container}>
        <Header info={headerProp} navigation={_that.props.navigation} />

       <View style={styles.content}>
        <View style={styles.formcontainer}>
          <View style={styles.innercontainer}>
          {this.state.cat_id=='5'?
            <View style={{width:'100%',backgroundColor:'#3995f7',flexDirection:'row',padding:10,borderTopWidth:1,borderTopColor:'#fff'}}>

              <Text style={{color:'#fff',width:'50%',paddingLeft:20}}>Description</Text>
              <Text style={{color:'#fff',width:'30%',textAlign:'center'}}>Price </Text>
            </View>
          :
          this.state.cat_id=='3'?
            <View style={{width:'100%',backgroundColor:'#3995f7',flexDirection:'row',padding:10,borderTopWidth:1,borderTopColor:'#fff'}}>

              <Text style={{color:'#fff',width:'50%',paddingLeft:20}}>About Service</Text>
              <Text style={{color:'#fff',width:'30%',textAlign:'center'}}>Price </Text>
            </View>
            :
            <View style={{width:'100%',backgroundColor:'#3995f7',flexDirection:'row',padding:10,borderTopWidth:1,borderTopColor:'#fff'}}>

              <Text style={{color:'#fff',width:'50%',paddingLeft:20}}>About Service</Text>
              <Text style={{color:'#fff',width:'30%',textAlign:'center'}}>Price </Text>
            </View>
          }
          {this.state.empty?
            <View style={{alignItems:'center',justifyContent:'center',height:'100%'}}>
             <Text style={{color:'#3995f7',fontSize:25}}>No service found </Text>
            </View>
          :
            this.state.cat_id=='1'?
              <View style={styles.main}>
                <ScrollView showsVerticalScrollIndicator={false}>
                 {this.state.serviceData.map((data) =>
                   <TouchableOpacity activeOpacity={.6} key={data.id} style={[styles.containerin,styles.shadow1]} onPress={()=>this.props.navigation.navigate('PaymentMethod',{data:data,station_id:this.state.station_id})}>
                     <View style={{width:'100%',marginBottom:15,marginTop:8,flexDirection:'row',paddingLeft:'2%',paddingRight:'2%'}}>
                       <View style={{width:AppSizes.screen.width/5,height:AppSizes.screen.height/11,marginBottom:15,marginTop:8}}>
                         {data.service_image != '' ?
                           <Image source={{uri: Constant.image_path+data.service_image}} style={styles.photo} />
                           :
                           <Image source={list_car} style={styles.photo} />
                         }

                       </View>
                       <View style={{width:'50%'}}>
                         {data.charge_type.length>0?<Text style={styles.texttitle}>{data.charge_type}{data.effect.length>0?<Text style={styles.text1}>({data.effect},</Text>:null}{data.ampere.length>0?<Text style={styles.text1}>{data.ampere})</Text>:null}</Text>:null}
                         {data.outlet_type.length>0?<Text style={styles.texttitle}>{data.outlet_type}</Text>:null}
                         <Text></Text>
                         <Text style={styles.text1} ellipsizeMode={'tail'} numberOfLines={2}>{data.description}</Text>
                       </View>
                       <View style={{width:'30%'}}>
                         <Text style={styles.text2}>{data.price}</Text>
                         {data.charge_type=='Regular Power Outlet(Long Charge)'?
                           <View>
                            <Text style={styles.text2}>Parking price</Text>
                             <Text style={styles.text2}>{data.parking_fee}</Text>
                           </View>
                           :
                           null
                         }
                       </View>
                     </View>
                   </TouchableOpacity>
                 )}
                </ScrollView>
              </View>
            :
              this.state.cat_id=='2'?
              <View style={styles.main}>
                <ScrollView showsVerticalScrollIndicator={false}>
                  {this.state.serviceData.map((data)=>
                    <TouchableOpacity activeOpacity={.6} key={data.id} style={[styles.containerin,styles.shadow1]} onPress={()=>this.props.navigation.navigate('PaymentMethod',{data:data,station_id:this.state.station_id})}>
                      <View style={{width:'100%',marginBottom:15,marginTop:8,flexDirection:'row',paddingLeft:'2%',paddingRight:'2%'}}>
                      <View style={{width:AppSizes.screen.width/6,height:AppSizes.screen.height/11,marginBottom:15,marginTop:8}}>
                        {data.service_image != '' ?
                          <Image source={{uri: Constant.image_path+data.service_image}} style={styles.photo} />
                          :
                          <Image source={list_car} style={styles.photo} />
                        }

                      </View>
                        <View style={{width:'50%'}}>
                          <Text style={styles.text1}>{data.description}</Text>
                        </View>
                        <View style={{width:'30%'}}>
                          <Text style={styles.textmiles}>Per day</Text>
                          <Text style={styles.textmiles}>{data.rentperday}</Text>
                          <Text style={styles.textmiles}>Deposit</Text>
                          <Text style={styles.textmiles}>{data.insurance}</Text>
                        </View>
                      </View>
                    </TouchableOpacity>
                  )}
                </ScrollView>
              </View>
              :
              this.state.cat_id=='3'?
              <View style={styles.main}>
                <ScrollView showsVerticalScrollIndicator={false}>
                  {this.state.serviceData.map((data)=>
                    <TouchableOpacity activeOpacity={.6} key={data.id} style={[styles.containerin,styles.shadow1]} onPress={()=>this.props.navigation.navigate('PaymentMethod',{data:data,station_id:this.state.station_id})}>
                      <View style={{width:'100%',marginBottom:15,marginTop:8,flexDirection:'row',paddingLeft:'2%',paddingRight:'2%'}}>
                      <View style={{width:AppSizes.screen.width/6,height:AppSizes.screen.height/11,marginBottom:15,marginTop:8}}>
                        {data.service_image != '' ?
                          <Image source={{uri: Constant.image_path+data.service_image}} style={styles.photo} />
                          :
                          <Image source={list_car} style={styles.photo} />
                        }

                      </View>
                        <View style={{width:'50%'}}>
                          <Text style={styles.text1} ellipsizeMode={'tail'} numberOfLines={2}>{data.description}</Text>
                        </View>
                        <View style={{width:'30%'}}>
                          <Text style={styles.text2}>Per lot</Text>
                          <Text style={styles.text2}>{data.parkingPrice}</Text>
                          <Text style={styles.text2}>Per day</Text>
                          <Text style={styles.text2}>{data.parkingperday}</Text>
                        </View>
                      </View>
                    </TouchableOpacity>
                  )}
                </ScrollView>
              </View>
              :
              this.state.cat_id=='5'?
              <View style={styles.main}>
                <ScrollView showsVerticalScrollIndicator={false}>
                  {this.state.serviceData.map((data)=>
                    <TouchableOpacity activeOpacity={.6} key={data.id} style={[styles.containerin,styles.shadow1]} onPress={()=>this.props.navigation.navigate('PaymentMethod',{data:data,station_id:this.state.station_id})}>
                      <View style={{width:'100%',marginBottom:15,marginTop:8,flexDirection:'row',paddingLeft:'2%',paddingRight:'2%'}}>
                      <View style={{width:AppSizes.screen.width/6,height:AppSizes.screen.height/11,marginBottom:15,marginTop:8}}>
                        {data.service_image != '' ?
                          <Image source={{uri: Constant.image_path+data.service_image}} style={styles.photo} />
                          :
                          <Image source={list_car} style={styles.photo} />
                        }

                      </View>

                        <View style={{width:'50%'}}>
                          <Text style={styles.text1} ellipsizeMode={'tail'} numberOfLines={2}>{data.description}</Text>
                        </View>
                        <View style={{width:'30%'}}>
                          <Text style={styles.text1}>{data.cable_charges}</Text>
                        </View>
                      </View>
                    </TouchableOpacity>
                  )}
                </ScrollView>
              </View>
              :
              null
          }
          </View>
        </View>
       </View>
     <Spinner visible={this.state.isVisible}  />
    </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    paddingTop:  0,
  },

  content: {
      flex: 1,
      height:'70%',
      width:'100%'
  },
  mapcontainer:{
    flex:1,
  },
  container2:{
    width:'100%',
    //backgroundColor:'green',
    paddingLeft:10,
    paddingRight:10,
    paddingTop:10,
    paddingBottom:30
  },
  formcontainer:{
    flex:1,
    backgroundColor:'#fff',
  },
   innercontainer:{
     width: '100%',
     height: '100%',
     resizeMode:'contain',
     alignItems:'center',
   },
    texttitle: {
      marginLeft: AppSizes.ResponsiveSize.Padding(4),
      fontSize:AppSizes.ResponsiveSize.Sizes(16),
      color:'#000',
      width:'90%',
      fontWeight:'500',
      fontFamily:Fonts.RobotoBold,
      //fontFamily: "Proxima Nova Semibold",
    },
    textmiles:{
      marginLeft: AppSizes.ResponsiveSize.Padding(4),

      fontSize:AppSizes.ResponsiveSize.Sizes(17),
      fontWeight:'500',
      color:'#3995f7',
      width:'100%',
      textAlign:'center',
      fontFamily:Fonts.RobotoBold,
    },
    text1: {
      marginLeft: AppSizes.ResponsiveSize.Padding(4),
      fontSize:AppSizes.ResponsiveSize.Sizes(14),
      lineHeight:22,
      color:'#747474',
      fontFamily:Fonts.RobotoRegular,
    },
    text2:{
      marginLeft: AppSizes.ResponsiveSize.Padding(4),
      fontSize:AppSizes.ResponsiveSize.Sizes(14),
      lineHeight:22,
      color:'#747474',
      textAlign:'center',
      fontFamily:Fonts.RobotoRegular,
    },
    photo: {
      flex:1,
      width: undefined,
      height: undefined,

  },

  photo1: {
    flex:1,
  },
  map: {
      ...StyleSheet.absoluteFillObject,
  },
  textInput: {
    flex:1,
    paddingLeft:AppSizes.ResponsiveSize.Padding(3),
    fontSize: AppSizes.ResponsiveSize.Sizes(12),
    color:'#fff',
    backgroundColor:'#eee',
    height:40,
    fontFamily:Fonts.RobotoRegular,
  },
  inputfield:{
    width:AppSizes.ResponsiveSize.width,

  },
  inputtext:{marginBottom:10,
    marginTop:AppSizes.ResponsiveSize.Padding(3),
    fontSize:AppSizes.ResponsiveSize.Sizes(18),
    fontWeight:'600',
    marginBottom:AppSizes.ResponsiveSize.Padding(1),
    color:'#fff',
    fontFamily:Fonts.RobotoBold,
  },
  error:{
    color:'red',
    fontSize:AppSizes.ResponsiveSize.Sizes(12),
    fontFamily:Fonts.RobotoRegular,
  },
  inputdrop:{
    borderWidth:1,
    borderColor:'#fff'
  },
  retext:{
    color:'#fff',
    fontFamily:Fonts.RobotoRegular,
  },
  avatarContainer: {
   justifyContent: 'center',
   backgroundColor:'transparent',
   alignItems:'center',
  },
  avatar: {
   borderColor: '#9B9B9B',
   borderWidth: 1 / PixelRatio.get(),
   borderRadius: 10,
   width:300,
   height:50,
   alignItems:'center',
   justifyContent:'center',
   backgroundColor:'#fff'
  },
  avatar1: {
   borderColor: '#9B9B9B',
   borderWidth: 1 / PixelRatio.get(),
   borderRadius: 75,
   width:120,
   height:120,
  },
  containerin: {
   paddingTop: AppSizes.ResponsiveSize.Padding(2),
   paddingBottom: AppSizes.ResponsiveSize.Padding(3),
   flexDirection: 'row',
   alignItems: 'center',
   width:'100%',
   borderBottomColor:'#999',
   borderBottomWidth:1,
   marginBottom:10,
   marginLeft:5,
   borderRadius:8,
   paddingLeft:AppSizes.ResponsiveSize.Padding(2),
   paddingRight:AppSizes.ResponsiveSize.Padding(2)
 },

main:{
  width:'100%',
  alignItems:'center',
  justifyContent:'center',
  marginTop:10
}
});
