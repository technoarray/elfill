import React, { Component } from 'react';
import {AsyncStorage,StyleSheet,Text,View,Image,PixelRatio,TouchableOpacity,Platform,Modal,Dimensions,Alert,ScrollView,TextInput} from 'react-native';
import { AppStyles, AppSizes, AppColors } from '../../themes/'
import { NavigationActions } from 'react-navigation'
import Spacer from '../common/Spacer'
import Spinner from 'react-native-loading-spinner-overlay';
import * as commonFunctions from '../../utils/CommonFunctions'
import Header from '../common/HeaderWithBack'
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view'
import CommonButton from '../common/CommonButton'
import Geocoder from 'react-native-geocoder';
import {RadioGroup, RadioButton} from 'react-native-flexi-radio-button'
import ImagePicker from 'react-native-image-picker';
import MapView, { Marker, ProviderPropType } from 'react-native-maps';
import { Fonts } from '../../utils/Fonts';
var Constant = require('../../api/ApiRules').Constant;
var WebServices = require('../../api/ApiRules').WebServices;

/* Images */
const bg_image = require('../../themes/Images/bg_image.png')
const left_arrow = require('../../themes/Images/left-arrow.png')
const profile_icon = require('../../themes/Images/profile.png')
const email_icon = require('../../themes/Images/email.png')
const password_icon = require('../../themes/Images/password.png')
const device_icon = require('../../themes/Images/car.png')
const modal_icon = require('../../themes/Images/modal.png')
const mobile_icon = require('../../themes/Images/mobile.png')

const { width, height } = Dimensions.get('window');

const ASPECT_RATIO = width / height;
const LATITUDE = 37.78825;
const LONGITUDE = -122.4324;
const LATITUDE_DELTA = 0.0922;
const LONGITUDE_DELTA = LATITUDE_DELTA * ASPECT_RATIO;
let id = 0;
var count=0;

let _that

export default class AddStationScreen extends Component {
  constructor (props) {
    super(props)
    this.state = {
      isVisible: false,
      item:[],
      uid:'',
      station_name:'',
      station_id:'',
      location:'',
      state:'',
      city:'',
      country:'',
      zipcode:'',
      description:'',
      stationerr:'',
      locationerr:'',
      stateerr:'',
      countryerr:'',
      ziperr:'',
      cityerr:'',
      descriptionerr:'',
      status:1,
      avatarSource:null,
      avatarerr:'',
      addressStatus:'1',
      latitude:Number,
      longitude:Number,
      region: {
        latitude: LATITUDE,
        longitude: LONGITUDE,
        latitudeDelta: LATITUDE_DELTA,
        longitudeDelta: LONGITUDE_DELTA,
      },
      markers: [],
      Modal_Visibility:false,
      form:false,
      count:false
    },
    _that = this;
  }

  componentWillMount(){
    AsyncStorage.getItem('UserData').then((UserData) => {
      var data = JSON.parse(UserData);
        uid=data.id
        if(uid != ''){
          _that.setState({
            uid:uid,
          })
        }
    })
  }

  getLocation(){
    this.setState({isVisible:true})
    try {
    Geocoder.fallbackToGoogle('AIzaSyBX6rKXe6Jsk6ZynShEZiNfDfyhZWgmXsQ');
     navigator.geolocation.getCurrentPosition(
      (position) => {
        var region = {
          lat: position.coords.latitude,
          lng:  position.coords.longitude,
        };
        console.log(region);
        Geocoder.geocodePosition(region).then(res => {
          var address=res[0].formattedAddress
          _that.setState({
            latitude: position.coords.latitude,
            longitude: position.coords.longitude,
            location: address,
            city:res[0].subAdminArea,
            state:res[0].adminArea,
            country:res[0].country,
            zipcode:res[0].postalCode
          });
          this.setState({isVisible:false})
        })
      },
        (error) => console.log('error '+error.message),
        {enableHighAccuracy: false, timeout: 50000, maximumAge: 10000}
      );
    }
    catch(err) {
        console.log(err);
    }
  }

  getAddress(){
    //this.setState({isVisible:true})
    if(this.state.count){
      var region={
        lat:Number(this.state.latitude.toFixed(7)),
        lng:Number(this.state.longitude.toFixed(7)),
      };
      console.log(region);
      Geocoder.fallbackToGoogle('AIzaSyBX6rKXe6Jsk6ZynShEZiNfDfyhZWgmXsQ');
      Geocoder.geocodePosition(region).then(res => {
        console.log(res);
        var address=res[0].formattedAddress
        _that.setState({
          location: address,
          city:res[0].subAdminArea,
          state:res[0].adminArea,
          country:res[0].country,
          zipcode:res[0].postalCode
        });
      })
      this.setState({form:true})
    }
    else{
      alert('Please select a location')
    }
  }

  onAddressSelect(index,value){
    this.setState({addressStatus:value})
    if(value=='0'){
      _that.getLocation()
      this.setState({markers:[]})
    }
    if(value=='1'){
      this.setState({location:''})
      this.setState({city:''})
      this.setState({state:''})
      this.setState({country:''})
      this.setState({zipcode:''})
      this.setState({markers:[]})
    }
    if(value=='2'){
      this.setState({Modal_Visibility:true})
    }
  }

  onSelect(index, value){
    this.setState({status: value})
  }

  selectPhotoTapped() {
    const options = {
      quality: 1.0,
      maxWidth: 500,
      maxHeight: 500,
      storageOptions: {
        skipBackup: true,
      },
    };
      ImagePicker.showImagePicker(options, (response) => {
      //console.log('Response = ', response);

      if (response.didCancel) {
        console.log('User cancelled image picker');
      } else if (response.error) {
        console.log('ImagePicker Error: ', response.error);
      } else if (response.customButton) {
        console.log('User tapped custom button: ', response.customButton);
      } else {
        const source = { uri: response.uri };

        this.setState({
          avatarSource: source,
          avatarerr:''
        });
      }
    });
   }

   onMapPress(e) {
     count=count+1;
     if(count==1){
       this.setState({count:true})
       this.setState({latitude:e.nativeEvent.coordinate.latitude})
       this.setState({longitude:e.nativeEvent.coordinate.longitude})
       //console.log(e.nativeEvent.coordinate.latitude);
       this.setState({
         markers: [
           ...this.state.markers,
           {
             coordinate: e.nativeEvent.coordinate,
             key: id++,
             color: '#3995f7',
           },
         ],
       });
     }
  }

  submit_click(){
    _that.validationAndApiParameter()
  }

  scroll_to_top=()=>{
    _that.scrollV.scrollTo({x: 0, y: 0, animated: false})
  }

  validationAndApiParameter() {
        const { method,avatarSource, uid,station_name, location, state,city, country,zipcode,description,station_id,status,isVisible,latitude,longitude } = this.state

        var error=0;

        if (station_name.length <= 0) {
          _that.scroll_to_top();
          _that.setState({stationerr:'Please enter service station name'})
          error=1;
        }
        else{
            _that.setState({stationerr:''})
        }
        if (location.length <= 0) {
          _that.setState({locationerr:'Please enter location'})
          error=1;
        }
        else{
            _that.setState({locationerr:''})
        }
        if (state.length <= 0) {
          _that.setState({stateerr:'Please enter state'})
          error=1;
        }
        else{
            _that.setState({stateerr:''})
        }
        if (country.length <= 0) {
          _that.setState({countryerr:'Please enter country'})
          error=1;
        }
        else{
            _that.setState({countryerr:''})
        }
        if (zipcode.length <= 0) {
          _that.setState({ziperr:'Please enter zipcode'})
          error=1;
        }
        else{
            _that.setState({ziperr:''})
        }
        if (city.length <= 0) {
          _that.setState({cityerr:'Please enter city'})
          error=1;
        }
        else{
            _that.setState({cityerr:''})
        }
        if (description.length <= 0) {
          _that.setState({descriptionerr:'Please enter description'})
          error=1;
        }
        else{
            _that.setState({descriptionerr:''})
        }
        if (avatarSource===null) {
          _that.setState({avatarerr:'Please select photo'})
          error=1;
        }
        else{
            _that.setState({avatarerr:''})
        }
        if(error==0){
          if(this.state.addressStatus=='1'){
            var address=location+','+city+','+state+','+country+' '+zipcode;
            Geocoder.fallbackToGoogle(Constant.Google_apiKey);
            Geocoder.geocodeAddress(address).then(res => {
              var position=res[0].position
              console.log(position);
              var latitude=position.lat;
              var longitude=position.lng;
              //this.setState({latitude:position.lat});
            //  this.setState({longitude:position.lng});
              _that.api_call(latitude,longitude)
            })
          }
          else{
            const { latitude,longitude } = this.state
            _that.api_call(latitude,longitude)
          }
        }
      }

      api_call(latitude,longitude){
        const { method,avatarSource, uid,station_name, location, state,city, country,zipcode,description,station_id,status,isVisible } = this.state

        const data = new FormData();
        data.append('uid', uid);
        data.append('station_name', station_name);
        data.append('location', location);
        data.append('state', state);
        data.append('city', city);
        data.append('country', country);
        data.append('zip', zipcode);
        data.append('description', description);
        data.append('latitude', latitude);
        data.append('longitude', longitude);
        data.append('station_status', status);
        data.append('station_image', {
          uri:  avatarSource.uri,
          type: 'image/jpeg', // or photo.type
          name: 'stationImage.jpg'
        });
        console.log(data);

        _that.setState({isVisible: true});
        this.postToApiCalling('POST', 'station_add', Constant.URL_serviceStationAdd, data);
      }

      postToApiCalling(method, apiKey, apiUrl, data) {

         new Promise(function(resolve, reject) {
              if (method == 'POST') {
                  resolve(WebServices.callWebService(apiUrl, data));
              } else {
                  resolve(WebServices.callWebService_GET(apiUrl, data));
              }
          }).then((jsonRes) => {
            console.log(jsonRes);
            _that.setState({ isVisible: false })

              if ((!jsonRes) || (jsonRes.code == 0)) {

              setTimeout(()=>{
                  Alert.alert('Login Error',jsonRes.message);
              },200);

              } else {
                  if (jsonRes.code == 1) {
                      _that.apiSuccessfullResponse(apiKey, jsonRes)
                  }
              }
          }).catch((error) => {
              console.log("ERROR" + error);
              _that.setState({ isVisible: false })

              setTimeout(()=>{
                  Alert.alert("Server issue");
              },200);
          });
      }

      apiSuccessfullResponse(apiKey, jsonRes) {
          if (apiKey == 'station_add') {
            stationlist=jsonRes.result
            _that.props.navigation.navigate('SellerDashboard',{stationlist:stationlist});
          }
      }

  render() {
    const headerProp = {
      title: 'Add Service Station',
      screens: 'Stationlistseller',
      type:''
    };
    return (
      <View style={styles.container}>
      <Header info={headerProp} navigation={_that.props.navigation}/>

        <View style={styles.content}>
            <KeyboardAwareScrollView innerRef={() => {return [this.refs.name,this.refs.email, this.refs.password]}} >
            <ScrollView ref={(c) => {this.scrollV = c}}>
            <View style={styles.container2}>

            <TouchableOpacity onPress={this.selectPhotoTapped.bind(this)} style={{alignItems:'center'}}>
            <View style={[styles.avatar,  styles.avatarContainer,{ marginBottom: 20 },]}>
              {this.state.avatarSource === null ? (
                <Text>Select a Photo</Text>
              ) : (
                <Image style={styles.avatar} source={this.state.avatarSource} />
              )}
            </View>
            </TouchableOpacity>
            { !(this.state.avatarerr) ? null :
              <Text style={styles.error}>{this.state.avatarerr}</Text>
            }
              <View style={styles.SectionStyle}>
                  <View style={styles.inputfield}>
                      <Text style={styles.inputtext}>Service Station Name</Text>
                      <TextInput
                        autoCorrect={false}
                        underlineColorAndroid="transparent"
                        returnKeyType={ "next"}
                        value={this.state.station_name}
                        selectionColor={"#000000"}
                        autoFocus={ false}
                        placeholder="Service Station Name"
                        placeholderTextColor="#808080"
                        style={styles.textInput}
                        ref="station_name"
                        keyboardType={ 'email-address'}
                        onFocus={ () => this.setState({stationerr:''}) }
                        onChangeText={station_name=> this.setState({station_name})}
                      />
                  </View>
                  { !(this.state.stationerr) ? null :
                    <Text style={styles.error}>{this.state.stationerr}</Text>
                  }
              </View>
              <View style={styles.SectionStyle}>
                  <View style={styles.inputfield}>
                      <Text style={styles.inputtext}>Address</Text>
                      <RadioGroup
                        color='#000'
                        activeColor='#3995f7'
                        selectedIndex={1}
                        style={styles.SectionRadioGroup}
                        onSelect = {(index, value) => this.onAddressSelect(index, value)}
                      >
                        <RadioButton value={'0'}>
                          <Text style={{color:'#000000',fontSize:AppSizes.ResponsiveSize.Sizes(12)}}>Auto-Locate</Text>
                        </RadioButton>
                        <RadioButton value={'1'}>
                          <Text style={{color:'#000000',fontSize:AppSizes.ResponsiveSize.Sizes(12)}}>Add Manually</Text>
                        </RadioButton>
                        <RadioButton value={'2'}>
                          <Text style={{color:'#000000',fontSize:AppSizes.ResponsiveSize.Sizes(12)}}>Locate on map</Text>
                        </RadioButton>
                      </RadioGroup>

                  </View>
              </View>
              {this.state.addressStatus=='1'?
                <View>
                  <View style={styles.SectionStyle}>
                    <View style={styles.inputfield}>
                      <Text style={styles.inputtext}>Location</Text>
                      <TextInput
                        autoCapitalize={'none'}
                        autoCorrect={false}
                        value={this.state.location}
                        underlineColorAndroid="transparent"
                        returnKeyType={ "next"}
                        selectionColor={"#000000"}
                        autoFocus={ false}
                        placeholder="Address"
                        placeholderTextColor="#808080"
                        style={styles.textInput}
                        ref="location"
                        keyboardType={ 'email-address'}
                        onFocus={ () => this.setState({locationerr:''}) }
                        onChangeText={location=> this.setState({location})}
                      />
                    </View>
                    { !(this.state.locationerr) ? null :
                      <Text style={styles.error}>{this.state.locationerr}</Text>
                    }
                  </View>
                  <View style={styles.SectionStyle}>
                      <View style={styles.inputfield}>
                          <Text style={styles.inputtext}>City</Text>
                          <TextInput
                            autoCapitalize={'none'}
                            autoCorrect={false}
                            value={this.state.city}
                            underlineColorAndroid="transparent"
                            returnKeyType={ "next"}
                            selectionColor={"#000000"}
                            autoFocus={ false}
                            placeholder="City"
                            placeholderTextColor="#808080"
                            style={styles.textInput}
                            ref="city"
                            keyboardType={ 'email-address'}
                            onFocus={ () => this.setState({cityerr:''}) }
                            onChangeText={city=> this.setState({city})}
                          />
                      </View>
                      { !(this.state.cityerr) ? null :
                        <Text style={styles.error}>{this.state.cityerr}</Text>
                      }
                  </View>
                  <View style={styles.SectionStyle}>
                      <View style={styles.inputfield}>
                          <Text style={styles.inputtext}>State</Text>
                          <TextInput
                            autoCapitalize={'none'}
                            autoCorrect={false}
                            value={this.state.state}
                            underlineColorAndroid="transparent"
                            returnKeyType={ "next"}
                            selectionColor={"#000000"}
                            autoFocus={ false}
                            placeholder="State"
                            placeholderTextColor="#808080"
                            style={styles.textInput}
                            ref="state"
                            keyboardType={ 'email-address'}
                            onFocus={ () => this.setState({stateerr:''}) }
                            onChangeText={state=> this.setState({state})}
                          />
                      </View>
                      { !(this.state.stateerr) ? null :
                        <Text style={styles.error}>{this.state.stateerr}</Text>
                      }
                  </View>
                  <View style={styles.SectionStyle}>
                      <View style={styles.inputfield}>
                          <Text style={styles.inputtext}>Country</Text>
                          <TextInput
                            autoCapitalize={'none'}
                            autoCorrect={false}
                            underlineColorAndroid="transparent"
                            value={this.state.country}
                            returnKeyType={ "next"}
                            selectionColor={"#000000"}
                            autoFocus={ false}
                            placeholder="Country"
                            placeholderTextColor="#808080"
                            style={styles.textInput}
                            ref="country"
                            keyboardType={ 'email-address'}
                            onFocus={ () => this.setState({countryerr:''}) }
                            onChangeText={country=> this.setState({country})}
                          />
                      </View>
                      { !(this.state.countryerr) ? null :
                        <Text style={styles.error}>{this.state.countryerr}</Text>
                      }
                  </View>
                  <View style={styles.SectionStyle}>
                      <View style={styles.inputfield}>
                          <Text style={styles.inputtext}>Zip Code</Text>
                          <TextInput
                            autoCapitalize={'none'}
                            autoCorrect={false}
                            value={this.state.zipcode}
                            underlineColorAndroid="transparent"
                            returnKeyType={ "next"}
                            selectionColor={"#000000"}
                            autoFocus={ false}
                            placeholder="Zip Code"
                            placeholderTextColor="#808080"
                            style={styles.textInput}
                            ref="zipcode"
                            keyboardType={ 'email-address'}
                            onFocus={ () => this.setState({ziperr:''}) }
                            onChangeText={zipcode=> this.setState({zipcode})}
                          />
                      </View>
                      { !(this.state.ziperr) ? null :
                        <Text style={styles.error}>{this.state.ziperr}</Text>
                      }
                  </View>
                </View>
              :
                null
              }

              {this.state.addressStatus=='0'?
                <View>
                  <View style={styles.SectionStyle}>
                    <View style={styles.inputfield}>
                      <Text style={styles.inputtext}>Location</Text>
                      <Text>{this.state.location}</Text>
                    </View>
                  </View>
                  <View style={styles.SectionStyle}>
                      <View style={styles.inputfield}>
                          <Text style={styles.inputtext}>City</Text>
                          <Text>{this.state.city}</Text>
                      </View>
                  </View>
                  <View style={styles.SectionStyle}>
                      <View style={styles.inputfield}>
                          <Text style={styles.inputtext}>State</Text>
                          <Text>{this.state.state}</Text>
                      </View>
                      { !(this.state.stateerr) ? null :
                        <Text style={styles.error}>{this.state.stateerr}</Text>
                      }
                  </View>
                  <View style={styles.SectionStyle}>
                      <View style={styles.inputfield}>
                          <Text style={styles.inputtext}>Country</Text>
                          <Text>{this.state.country}</Text>
                      </View>
                      { !(this.state.countryerr) ? null :
                        <Text style={styles.error}>{this.state.countryerr}</Text>
                      }
                  </View>
                  <View style={styles.SectionStyle}>
                      <View style={styles.inputfield}>
                          <Text style={styles.inputtext}>Zip Code</Text>
                          <Text>{this.state.zipcode}</Text>
                      </View>
                      { !(this.state.ziperr) ? null :
                        <Text style={styles.error}>{this.state.ziperr}</Text>
                      }
                  </View>
                  <View style={styles.SectionStyle}>
                      <View style={styles.inputfield}>
                          <Text style={styles.inputtext}>Latitude</Text>
                          <Text>{this.state.latitude}</Text>
                      </View>
                  </View>
                  <View style={styles.SectionStyle}>
                      <View style={styles.inputfield}>
                          <Text style={styles.inputtext}>Longitude</Text>
                          <Text>{this.state.longitude}</Text>
                      </View>
                  </View>
                </View>
              :
                null
              }

              {this.state.addressStatus=='2'?
                <Modal
                  visible={this.state.Modal_Visibility}
                  transparent={true}
                  onRequestClose={()=> this.setState({Modal_Visibility:false})}
                  animationType={"fade"}>
                  <View style={{flex:1,padding:10}}>
                    <MapView style={styles.map}
                      provider='google'
                      mapType='standard'
                      showsCompass={true}
                      showsPointsOfInterest
                      showsUserLocation={true}
                      onPress={(e) => this.onMapPress(e)}>
                      {this.state.markers.map(marker => (
                        <Marker
                          key={marker.key}
                          coordinate={marker.coordinate}
                          pinColor={marker.color}
                          onDragStart={(e) => console.log('onDragStart', e.nativeEvent.coordinate)}
                          onDragEnd={(e) => {this.setState({latitude:e.nativeEvent.coordinate.latitude}),this.setState({longitude:e.nativeEvent.coordinate.longitude})}}
                          draggable
                        />
                      ))}
                    </MapView>
                    <View style={styles.okBtn}>
                    <TouchableOpacity onPress={() => {this.setState({Modal_Visibility:false}),this.getAddress()}} style={styles.SectionStyle}>
                      <CommonButton label='Confirm' width='100%'/>
                    </TouchableOpacity>
                    </View>
                  </View>
                </Modal>
              :
                null
              }

              {this.state.form?
                <View>
                  <View style={styles.SectionStyle}>
                    <View style={styles.inputfield}>
                      <Text style={styles.inputtext}>Location</Text>
                      <Text>{this.state.location}</Text>
                    </View>
                  </View>
                  <View style={styles.SectionStyle}>
                      <View style={styles.inputfield}>
                          <Text style={styles.inputtext}>City</Text>
                          <Text>{this.state.city}</Text>
                      </View>
                  </View>
                  <View style={styles.SectionStyle}>
                      <View style={styles.inputfield}>
                          <Text style={styles.inputtext}>State</Text>
                          <Text>{this.state.state}</Text>
                      </View>
                      { !(this.state.stateerr) ? null :
                        <Text style={styles.error}>{this.state.stateerr}</Text>
                      }
                  </View>
                  <View style={styles.SectionStyle}>
                      <View style={styles.inputfield}>
                          <Text style={styles.inputtext}>Country</Text>
                          <Text>{this.state.country}</Text>
                      </View>
                      { !(this.state.countryerr) ? null :
                        <Text style={styles.error}>{this.state.countryerr}</Text>
                      }
                  </View>
                  <View style={styles.SectionStyle}>
                      <View style={styles.inputfield}>
                          <Text style={styles.inputtext}>Zip Code</Text>
                          <Text>{this.state.zipcode}</Text>
                      </View>
                      { !(this.state.ziperr) ? null :
                        <Text style={styles.error}>{this.state.ziperr}</Text>
                      }
                  </View>
                  <View style={styles.SectionStyle}>
                      <View style={styles.inputfield}>
                          <Text style={styles.inputtext}>Latitude</Text>
                          <Text>{this.state.latitude}</Text>
                      </View>
                  </View>
                  <View style={styles.SectionStyle}>
                      <View style={styles.inputfield}>
                          <Text style={styles.inputtext}>Longitude</Text>
                          <Text>{this.state.longitude}</Text>
                      </View>
                  </View>
                </View>
              :
                null
              }

              <View style={styles.SectionStyle}>
                  <View style={styles.inputfield}>
                      <Text style={styles.inputtext}>Description</Text>
                      <TextInput
                        autoCapitalize={'none'}
                        autoCorrect={false}
                        value={this.state.description}
                        multiline={true}
                        numberOfLines={4}
                        underlineColorAndroid="transparent"
                        returnKeyType={ "next"}
                        selectionColor={"#000000"}
                        autoFocus={ false}
                        placeholder="Description"
                        placeholderTextColor="#808080"
                        style={[styles.textInput,{height:80}]}
                        ref="description"
                        keyboardType={ 'email-address'}
                        onFocus={ () => this.setState({descriptionerr:''}) }
                        onChangeText={description=> this.setState({description})}
                      />
                  </View>
                  { !(this.state.descriptionerr) ? null :
                    <Text style={styles.error}>{this.state.descriptionerr}</Text>
                  }
              </View>

              <View style={styles.SectionStyle}>
                <View style={styles.inputfield}>
                  <Text style={styles.inputtext}>Status</Text>
                    <RadioGroup
                      color='#000000'
                      selectedIndex={1}
                      style={styles.SectionRadioGroup}
                      onSelect = {(index, value) => this.onSelect(index, value)}
                    >
                      <RadioButton value={'0'}>
                        <Text style={{color:'#000000'}}>InActive</Text>
                      </RadioButton>

                      <RadioButton value={'1'}>
                        <Text style={{color:'#000000'}}>Active</Text>
                      </RadioButton>
                    </RadioGroup>
                  </View>
              </View>

              </View>
              </ScrollView>
            </KeyboardAwareScrollView>

            <TouchableOpacity onPress={this.submit_click} style={[styles.SectionStyle1,{marginTop:AppSizes.ResponsiveSize.Padding(3)}]}>
              <CommonButton label='Save' width='100%'/>
            </TouchableOpacity>

        </View>
          <Spinner visible={this.state.isVisible}  />
    </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    paddingTop:  0,
  },

  content: {
      flex: 1,
      flexDirection: 'column',
      height:'70%',
      backgroundColor:'#ffffff'
  },
  container1: {
    flex: 3,

    height:AppSizes.screen.width/4,
    paddingLeft:AppSizes.ResponsiveSize.Padding(5),
    //backgroundColor:'red'
  },

  container2: {
    flex: 10,
    height:'70%',
    paddingLeft:'4%',
    paddingRight:'4%',
    paddingTop:'4%',
    flexDirection: 'column',

    //backgroundColor:'blue'
  },
  container3: {
    flex: 3,
    height:AppSizes.screen.width/8,
    alignItems: 'center',//replace with flex-end or center
    justifyContent: 'flex-end',
 },

 avatarContainer: {
     borderColor: '#9B9B9B',
     borderWidth: 1 / PixelRatio.get(),
     justifyContent: 'center',
     alignItems: 'center',
   },
   avatar: {
     borderRadius: 75,
     width: 120,
     height: 120,
 },

headerTitle:{
  fontSize:AppSizes.ResponsiveSize.Sizes(25),
  color:'#000000',
  fontWeight:'bold',
  fontFamily:Fonts.RobotoBold,
  paddingBottom:AppSizes.ResponsiveSize.Padding(1),
},
headersubTitle:{
  fontSize:AppSizes.ResponsiveSize.Sizes(14),
  color:'#000000',
  fontWeight:'600',
  fontFamily:Fonts.RobotoBold,
  paddingTop:AppSizes.ResponsiveSize.Padding(2),
},
headerBorder :{
  borderBottomColor: '#000000',
  borderBottomWidth: 3,
  width:'10%',
},
footertext :{
 color :'#000000',
 fontSize:AppSizes.ResponsiveSize.Sizes(12),
 fontWeight:'400',
 fontFamily:Fonts.RobotoRegular,
},
SectionStyle: {
  width:AppSizes.ResponsiveSize.width
},

ImageStyle: {
  height: AppSizes.screen.width/21,
  width: AppSizes.screen.width/21,
  resizeMode : 'contain',
},
textInput: {
flex:1,
paddingLeft:AppSizes.ResponsiveSize.Padding(3),
fontSize: AppSizes.ResponsiveSize.Sizes(12),
color:'#000000',
backgroundColor:'#eee',
height:40,
fontFamily:Fonts.RobotoRegular,
},
inputfield:{
  width:AppSizes.ResponsiveSize.width
},
inputtext:{marginBottom:10,
  marginTop:AppSizes.ResponsiveSize.Padding(3),
  fontSize:AppSizes.ResponsiveSize.Sizes(14),
  fontWeight:'600',
  fontFamily:Fonts.RobotoBold,
  marginBottom:AppSizes.ResponsiveSize.Padding(1)
},
error:{
  color:'red',
  fontSize:AppSizes.ResponsiveSize.Sizes(12),
  textAlign:'center',
  fontFamily:Fonts.RobotoRegular,
},
SectionRadioGroup:{
  flexDirection:'row',
},
map: {
    ...StyleSheet.absoluteFillObject,
  },
  okBtn:{
    position:'absolute',
    bottom:0,
    width:'110%',
    //backgroundColor:'red',
    left:0,
    right:0
  },
  SectionStyle1: {
    width:'100%'
  },
});
