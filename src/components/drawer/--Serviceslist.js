import React, { Component } from 'react';
import {StyleSheet,Text,View,Image,TouchableOpacity,Platform,Alert,ScrollView,TextInput} from 'react-native';
import { AppStyles, AppSizes, AppColors } from '../../themes/'
import { NavigationActions } from 'react-navigation'
import Spacer from '../common/Spacer'
import * as commonFunctions from '../../utils/CommonFunctions'
import Header from '../common/Header'
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view'
import CommonButton from '../common/CommonButton'
import LinearGradient from 'react-native-linear-gradient';

var Constant = require('../../api/ApiRules').Constant;

/* Images */
const bg_image = require('../../themes/Images/bg_image.png')
var left= require( '../../themes/Images/left-arrow.png')

export default class AccountScreen extends Component {
  constructor (props) {
      super(props)
      this.state = {
      isVisible: false,
      Modal_Visibility: false
    },
    _that = this;
  }

  updateAlert = () => {
        this.setState({Modal_Visibility: !this.state.Modal_Visibility});
  }

  website_link() {
    Alert.alert(
        'eThera',
        'You will be redirected to your default browser, to visit our Website',
        [
          {text: 'Cancel', onPress: () => console.log('Cancel Button Pressed'), style: 'cancel'},
          {text: 'OK', onPress: () => Linking.openURL(Constant.SITE_URL)},

        ]

      )
  }

  render() {
    const headerProp = {
      title: 'Serviceslist',
      screens: 'Serviceslist',
    };
    return (
      <View style={styles.container}>
      <Header info={headerProp} navigation={_that.props.navigation}/>

        <View style={styles.content}>
            <View style={styles.container1}>


                      <LinearGradient colors={['#4c669f', '#3b5998', '#192f6a']} style={[styles.linearGradient,styles.servicebox]}>
                            <Text style={styles.title}>Service one</Text>
                            <Text style={styles.smalltext}>Panchkula,Haryana</Text>
                            <View style={styles.sideicon}>
                                <Image source={left} style={[styles.ImageStyle,styles.sidearrow]} />
                            </View>
                        </LinearGradient>

                        <LinearGradient colors={['#4c669f', '#3b5998', '#192f6a']} style={[styles.linearGradient,styles.servicebox]}>
                              <Text style={styles.title}>Service one</Text>
                              <Text style={styles.smalltext}>Panchkula,Haryana</Text>
                              <View style={styles.sideicon}>
                                  <Image source={left} style={[styles.ImageStyle,styles.sidearrow]} />
                              </View>
                          </LinearGradient>
                          <LinearGradient colors={['#4c669f', '#3b5998', '#192f6a']} style={[styles.linearGradient,styles.servicebox]}>
                                <Text style={styles.title}>Service one</Text>
                                <Text style={styles.smalltext}>Panchkula,Haryana</Text>
                                <View style={styles.sideicon}>
                                    <Image source={left} style={[styles.ImageStyle,styles.sidearrow]} />
                                </View>
                            </LinearGradient>
                            <LinearGradient colors={['#4c669f', '#3b5998', '#192f6a']} style={[styles.linearGradient,styles.servicebox]}>
                                  <Text style={styles.title}>Service one</Text>
                                  <Text style={styles.smalltext}>Panchkula,Haryana</Text>
                                  <View style={styles.sideicon}>
                                      <Image source={left} style={[styles.ImageStyle,styles.sidearrow]} />
                                  </View>
                              </LinearGradient>
                              <LinearGradient colors={['#4c669f', '#3b5998', '#192f6a']} style={[styles.linearGradient,styles.servicebox]}>
                                    <Text style={styles.title}>Service one</Text>
                                    <Text style={styles.smalltext}>Panchkula,Haryana</Text>
                                    <View style={styles.sideicon}>
                                        <Image source={left} style={[styles.ImageStyle,styles.sidearrow]} />
                                    </View>
                                </LinearGradient>

            </View>


        </View>
    </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    paddingTop:  0,
  },

  content: {
      flex: 1,
      padding:'5%',
      backgroundColor:'#ffffff'
  },
  container1: {
    flex: 1,
    justifyContent: 'space-around',
    flexDirection: 'column',
  },
  servicebox:{
    backgroundColor:'red',
    padding:AppSizes.ResponsiveSize.Padding(4),
    paddingBottom:AppSizes.ResponsiveSize.Padding(6),
    position:'relative',
  },
  sideicon:{
    position:'absolute',
    right:10,
    top:0,
    bottom:0,


  },
  ImageStyle: {
    height: AppSizes.screen.width/15,
    width: AppSizes.screen.width/15,
    resizeMode : 'contain',
  },
  title:{
    fontSize:AppSizes.ResponsiveSize.Sizes(20),
    color:'#fff',
    fontWeight:'700',
  },
  smalltext:{
    fontSize:AppSizes.ResponsiveSize.Sizes(10),
    color:'#fff',
    fontWeight:'400'
  },
  linearGradient: {
    flex: 1,
    paddingLeft: 15,
    paddingRight: 15,
    borderRadius: 5
  },
  sidearrow:{
    marginTop:AppSizes.ResponsiveSize.Padding(105),

  }



});
