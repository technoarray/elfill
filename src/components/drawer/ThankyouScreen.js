import React, { Component } from 'react';
import {StyleSheet,Text,View,Image,TouchableOpacity,ImageBackground,Platform,Alert,ScrollView} from 'react-native';
import { NavigationActions } from 'react-navigation'
import Header from '../common/HeaderWithoutBack';
import CommonButton from '../common/CommonButton'
import { AppStyles, AppSizes, AppColors } from '../../themes/'
import { Fonts } from '../../utils/Fonts';
var Constant = require('../../api/ApiRules').Constant;
var WebServices = require('../../api/ApiRules').WebServices;

const bg_image = require('../../themes/Images/bg_image.png')
const smile = require('../../themes/Images/smile.png')


let _that
export default class ThankyouScreen extends Component {
  constructor (props) {
      super(props)
      this.state = {
        isVisible:false,
        //station_id:this.props.navigation.state.params.station_id,
        name:'',
        mobile:'',
        station_name:'',
        address:'',
        country:'',
        zipcode:'',

      },
      _that=this;
  }

  componentDidMount(){
  //  _that.validationAndApiParameter('seller_details')
  }

  validationAndApiParameter(apikey){
    if(apikey == 'seller_details'){
      const data = new FormData();
      data.append('station_id',this.state.station_id);

      _that.setState({isVisible: true});
      this.postToApiCalling('POST',apikey, Constant.URL_sellerDetails, data);
    }
  }

  postToApiCalling(method, apiKey, apiUrl, data) {
    new Promise(function(resolve, reject) {
      if (method == 'POST') {
        resolve(WebServices.callWebService(apiUrl, data));
      } else {
        resolve(WebServices.callWebService_GET(apiUrl, data));
      }
    }).then((jsonRes) => {
      console.log(jsonRes);
      _that.setState({ isVisible: false })
      if ((!jsonRes) || (jsonRes.code == 0)) {
          setTimeout(()=>{
            Alert.alert('Login Error',jsonRes.message);
          },200);
      }
      else {
        _that.apiSuccessfullResponse(jsonRes)
      }
    }).catch((error) => {
        console.log("ERROR" + error);
        _that.setState({ isVisible: false })
        setTimeout(()=>{
            Alert.alert("Server issue"+error);
        },200);
    });
  }

  apiSuccessfullResponse(jsonRes){
    stationData=jsonRes.result;
    console.log(stationData)

    this.setState({name:stationData.name})
    this.setState({mobile:stationData.telephone})
    this.setState({address:stationData.address})
    this.setState({zipcode:stationData.zip_code})
    this.setState({country:stationData.country})
    this.setState({station_name:stationData.station_name})
  }

  submit_click=()=>{
    this.props.navigation.navigate('HomeScreen')
  }

  render() {
    const headerProp = {
      title: 'Thankyou',
      screens: 'ThankyouScreen',
        type:''
    };

    return (
      <View style={{flex:1}}>
      <View style={styles.container}>
        <View style={{alignItems:'center',width:'100%',paddingTop:AppSizes.ResponsiveSize.Padding(3)}}>
          <View style={styles.main}>
            <View style={styles.imgbox}>
                <Text style={styles.thantext}>Thank You</Text>
            </View>
            <View style={styles.detail}>
                <View style={[styles.boxtitle,styles.shadow]}>
                    <Text style={styles.texttitle}>Your Booking Details</Text>
                </View>
                <View style={styles.list}>
                      <Text style={styles.text}>Order Number:-0124521dsf12</Text>
                      <Text style={styles.text}>Station Name:-Electric Filling station</Text>
                      <Text style={styles.text}>Total amount:-$130</Text>
                      <Text style={styles.text}>Amount Paid:-$12</Text>
                      <Text style={styles.text}>Amount Payable to seller:-$118</Text>
                </View>
            </View>
            <View style={styles.detail}>
                <View style={[styles.boxtitle,styles.shadow]}>
                    <Text style={styles.texttitle}>Seller Details</Text>
                </View>
                <View style={styles.list}>
                      <Text style={styles.text}>Name:-Neha</Text>
                      <Text style={styles.text}>Address:-sco-4,swastik vihar</Text>
                      <Text style={styles.text}>Mobile Number:-8053857544</Text>
                      <Text style={styles.text}>State:-Panchkula</Text>
                      <Text style={styles.text}>Country:-India</Text>
                </View>
            </View>
          </View>
        </View>
        <View style={styles.container3}>
        <TouchableOpacity onPress={this.submit_click}>
          <View style={[styles.SectionStyle,{justifyContent:'flex-end'}]}>
            <CommonButton label='Go back to home page' width='100%'/>
          </View>
        </TouchableOpacity>
        </View>
      </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    paddingTop: 10,
    //backgroundColor:'red',
    alignItems:'center'
  },
  innercontainer:{
    width: '100%',
    height: '100%',
    resizeMode:'contain',
  },
  main:{
    width:'100%',
    alignItems:'center',
    height:'92.5%'
    //backgroundColor:'red'
  },
  imgbox:{
    width:'100%',
    alignItems:'center',
    marginTop:'5%',
    marginBottom:AppSizes.ResponsiveSize.Padding(4)
  },

  detail:{
    width:'100%',
    alignItems:'center',
    //backgroundColor:'green',
    paddingBottom:AppSizes.ResponsiveSize.Padding(4)
  },
  thantext:{
    fontSize:AppSizes.ResponsiveSize.Sizes(32),
    color:'#000',
    fontWeight:'700',
    width:'100%',
    textAlign:'center',
    marginBottom:AppSizes.ResponsiveSize.Padding(3),
    fontFamily:Fonts.RobotoBlack,
  },
  booktext:{
    fontSize:AppSizes.ResponsiveSize.Sizes(20),
    fontWeight:'600',
    color:'#000',
    marginBottom:AppSizes.ResponsiveSize.Padding(2),
    fontFamily:Fonts.RobotoBold,
  },

  shadow:{
   //borderWidth:1,
   borderRadius: 0,
   borderColor: '#000',
   justifyContent:'center',
   //backgroundColor:'#fff',
   borderColor: '#ddd',
   //borderBottomWidth: 1,
   shadowColor: '#999',
   shadowOffset: { width: 0, height: 2 },
   shadowOpacity: 0.8,
   shadowRadius: 2,
   elevation: 0,
   //backgroundColor:'red',
  },
  boxtitle:{
    width:'100%',
    height:50,
    alignItems:'center',
    backgroundColor:'#452959',
    marginBottom:AppSizes.ResponsiveSize.Padding(2)
  },
  texttitle:{
    marginRight:AppSizes.ResponsiveSize.Padding(1),
    fontSize:AppSizes.ResponsiveSize.Sizes(15),
    fontWeight:'300',
    color:'#fff',
    fontFamily:Fonts.RobotoRegular,
  },
  text:{
    fontSize:AppSizes.ResponsiveSize.Sizes(14),
    color:'#333',
    marginBottom:5,
    fontFamily:Fonts.RobotoRegular,
  },
  list:{
    alignItems:'center',
    padding:AppSizes.ResponsiveSize.Padding(3)
  },
  container3: {
    position:'absolute',
    bottom:0,
    width:'100%',
    alignItems: 'center',
    justifyContent: 'flex-end',

    //backgroundColor:'red'
  },
});
