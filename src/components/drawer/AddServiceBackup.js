import React, { Component } from 'react';
import {AsyncStorage,StyleSheet,Text,View,Image,PixelRatio,TouchableOpacity,Platform,Alert,ScrollView,TextInput} from 'react-native';
import { AppStyles, AppSizes, AppColors } from '../../themes/'
import { NavigationActions } from 'react-navigation'
import Spinner from 'react-native-loading-spinner-overlay';
import * as commonFunctions from '../../utils/CommonFunctions'
import Header from '../common/HeaderWithBack'
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view'
import CommonButton from '../common/CommonButton'
import ImagePicker from 'react-native-image-picker';
import Picker from 'react-native-q-picker';
import {RadioGroup, RadioButton} from 'react-native-flexi-radio-button';
import { pickerData } from '../data/pickerData';
import CheckBox from 'react-native-check-box';
import { Fonts } from '../../utils/Fonts';
var Constant = require('../../api/ApiRules').Constant;
var WebServices = require('../../api/ApiRules').WebServices;

/* Images */
const bg_image = require('../../themes/Images/bg_image.png')
const left_arrow = require('../../themes/Images/left-arrow.png')
const arrow_right = require('../../themes/Images/right_arrow.png')
const arrow_down = require('../../themes/Images/down_arrow.png')
const profile_icon = require('../../themes/Images/profile.png')
const email_icon = require('../../themes/Images/email.png')
const password_icon = require('../../themes/Images/password.png')
const device_icon = require('../../themes/Images/car.png')
const modal_icon = require('../../themes/Images/modal.png')
const mobile_icon = require('../../themes/Images/mobile.png')

var numberchargeitems = [];

for( var i = 1; i <=20; i++){
  var numbers = {
      name: i+"",
      id: i
  };
  numberchargeitems.push(numbers);
}

let _that
export default class AddserviceScreen extends Component {
  constructor (props) {
      super(props)
      this.state = {
      isVisible: false,
      uid:'',
      selectedType:'',
      empty:false,
      type_id:[],
      status:'1',
      station_id:'',
      otherCategory:[],
      diviceType:[],
      bike_outlet:pickerData.bike_outlet,
      chargedata:pickerData.chargedata,
      accomodation:pickerData.accomodation,
      coffee:pickerData.coffee,
      car_outlet:pickerData.car_outlet,
      effect:pickerData.effect,
      phases:pickerData.phase,
      ampere:pickerData.ampere,
      selectedSlots:'',
      cat_id:'',
      subcat_id:'',
      selectedOtherCategory:'',
      selectedChargeType:'',
      selectedDeviceType:'',
      selectedoutlet:'',
      selectedparkingFee:'',
      selectedaccomodation:'',
      selectedeffect:'',
      selectedphase:'',
      selectedampere:'',
      selecteddriver_licence_no:'',
      selectedupload_licence:'',
      selectedinsurance:'',
      numberchargeitems:numberchargeitems,
      selectedprice:'',
      selecteddescription:'',
      avatarSource:null,
      avatarerr:'',
      car:false,
      bike:false,
      gadget:false,
      stationError:'',
      slotsError:'',
      categoryError:'',
      subCatError:'',
      chargeError:'',
      deviceTypeError:'',
      outletError:'',
      priceError:'',
      otherCatError:'',
      descriptionError:'',
      parkingLots:'',
      parkingPrice:'',
      accomodation_charges:'',
      parkingperday:'',
      reantperday:'',
      selectedOtherCategory:'',
      car_charge:false,
      car_park:false,
      car_bed:false,
      car_cable:false,
      car_rent:false,
      bike_charge:false,
      bike_park:false,
      bike_bed:false,
      bike_cable:false,
      bike_rent:false,
      gadget_charge:false,
      gadget_bed:false,
      gadget_cable:false,
      gadget_rent:false,
    },
    _that = this;
  }

  componentWillMount(){
    AsyncStorage.getItem('UserData').then((UserData) => {
      var data = JSON.parse(UserData);
        uid=data.id
        if(uid != ''){
          _that.setState({
            uid:uid,
          })
        }
    })
  }

  onSelect(index, value){
    console.log(value);
    this.setState({status: value})
  }

  selectPhotoTapped() {
    const options = {
      quality: 1.0,
      maxWidth: 500,
      maxHeight: 500,
      storageOptions: {
        skipBackup: true,
      },
    };
      ImagePicker.showImagePicker(options, (response) => {
      console.log('Response = ', response);

      if (response.didCancel) {
        console.log('User cancelled image picker');
      } else if (response.error) {
        console.log('ImagePicker Error: ', response.error);
      } else if (response.customButton) {
        console.log('User tapped custom button: ', response.customButton);
      } else {
        const source = { uri: response.uri };

        this.setState({
          avatarSource: source,
          avatarerr:''
        });
      }
    });
   }

  submit_click=() =>{
    const { selectedSlots,selectedOtherCategory,cat_id,subcat_id,station_id,selectedType,othercat_id,reantperday,selectedDeviceType,selectedChargeType,selectedoutlet,selectedparkingFee,accomodation_charges,selectedaccomodation,selecteddescription,selectedstatus,selectedeffect,selectedphase,selectedampere,selecteddriver_licence_no,selectedupload_licence,selectedinsurance,selectedprice,avatarSource,status,parkingLots,parkingPrice,parkingperday} = this.state
    var error=0;
      if(this.state.cat_id=='1'){
        if(this.state.subcat_id=='1'){
          if(selectedChargeType==''){
            console.log('error');
            this.setState({chargeError:'Field is required'})
            error=1;
          }
        }
        if(this.state.subcat_id==''){
          this.setState({subCatError:'Field is required'})
          error=1;
        }
        if(selectedprice==''){
          console.log('error');
          this.setState({priceError:'Field is required'})
          error=1;
        }
        if(selectedoutlet==''){
          console.log('error');
          this.setState({outletError:'Field is required'})
          error=1;
        }
        if (selectedSlots == '') {
          console.log('error');
          this.setState({slotsError:'Field is required'})
          error=1;
        }
        if(cat_id==''){
          console.log('error');
          this.setState({categoryError:'Field is required'})
          error=1;
        }
        if(this.state.subcat_id!='Elcar'){
          if(selectedOtherCategory==''){
            console.log('error');
            this.setState({otherCatError:'Field is required'})
            error=1;
          }
        }
        if(selectedDeviceType==''){
          console.log('error');
          this.setState({deviceTypeError:'Field is required'})
          error=1;
        }
      }
      else if(this.state.cat_id=='3'){
        if(cat_id==''){
          console.log('error');
          this.setState({categoryError:'Field is required'})
          error=1;
        }
        if(subcat_id==''){
          console.log('error');
          this.setState({subCatError:'Field is required'})
          error=1;
        }
      }
      else if(this.state.cat_id=='2'){
        if(cat_id==''){
          console.log('error');
          this.setState({categoryError:'Field is required'})
          error=1;
        }
        if(subcat_id==''){
          console.log('error');
          this.setState({subCatError:'Field is required'})
          error=1;
        }
        if(selectedoutlet==''){
          console.log('error');
          this.setState({outletError:'Field is required'})
          error=1;
        }
      }
      if(selecteddescription==''){
        console.log('error');
        this.setState({descriptionError:'Field is required'})
        error=1;
      }
      if (avatarSource===null) {
        console.log('error');
        _that.setState({avatarerr:'Please select photo'})
        error=1;
      }
        if(error==0){
         const data = new FormData();
         data.append('uid', this.state.uid);
         data.append('station_id',station_id);
         data.append('type_id',selectedType)
         data.append('cat_id',cat_id);
         data.append('subcat_id',subcat_id);
         data.append('other_cat_id',othercat_id)
         data.append('device_id',selectedDeviceType);
         data.append('charge_type',selectedChargeType);
         data.append('outlet_type',selectedoutlet);
         data.append('parking_fee',selectedparkingFee);
         data.append('accomodation',selectedaccomodation);
         data.append('accomodation_charges',accomodation_charges)
         data.append('effect',selectedeffect);
         data.append('phases',selectedphase);
         data.append('ampere',selectedampere);
         data.append('driver_licence_no',selecteddriver_licence_no);
         data.append('upload_driver_licence',selectedupload_licence);
         data.append('insurance',selectedinsurance);
         data.append('reantperday',reantperday)
         data.append('parkingLots',parkingLots);
         data.append('parkingPrice',parkingPrice);
         data.append('parkingperday',parkingperday);
         data.append('price',selectedprice);
         data.append('description', selecteddescription);
         data.append('service_image', {
           uri:  avatarSource.uri,
           type: 'image/jpeg',
           name: 'serviceImage.jpg'
         });
         data.append('no_of_charging_spot',selectedSlots);
         data.append('service_status', status);

         console.log('Data:',data);

         _that.setState({isVisible: true});
         this.postToApiCalling('POST', 'service_add', Constant.URL_serviceAdd, data);
        }
  }

  onClick(val,key){
    this.setState({selectedDeviceType:''})
    this.setState({selectedDeviceId:''})
    this.setState({diviceType:[]})
    if(key == 'car_charge'){
      if(this.state.diviceType==''){
        _that.validationAndApiParameter(val,key)
      }
    }
    else if (key=='car_rent') {
      if(this.state.diviceType==''){
        _that.validationAndApiParameter(val,key)
      }
    }
    else if (key=='bikeotherCategory'){
      if(this.state.otherCategory==''){
        _that.validationAndApiParameter(val,key)
      }
    }
    else if (key=='otherCategory') {
      //console.log('hi');
      _that.validationAndApiParameter(val,key)
    }
  }

  validationAndApiParameter(val,apikey) {
    if (apikey== 'car_charge') {
      const data = new FormData();
      data.append('subcat_id',1);
      data.append('other_catid',0)

      _that.setState({isVisible: true});
      _that.postToApiCalling('POST',apikey, Constant.URL_devicesList,data);
    }
    else if (apikey== 'car_rent') {
      const data = new FormData();
      data.append('subcat_id',3);
      data.append('other_catid',0)

      _that.setState({isVisible: true});
      _that.postToApiCalling('POST',apikey, Constant.URL_devicesList,data);
    }
    else if (apikey== 'bikeotherCategory') {
      const data = new FormData();
      data.append('subcat_id',val);
      //console.log(data);
      _that.setState({isVisible: true});
      _that.postToApiCalling('POST',apikey, Constant.URL_otherCategoryList,data);
    }
    else if (apikey=='otherCategory') {
      const data =new FormData();
      data.append('subcat_id',this.state.subcat_id)
      data.append('other_catid',val)
      //console.log(data);
      _that.setState({isVisible:true})
      _that.postToApiCalling('POST',apikey, Constant.URL_devicesList,data);
    }
  }

    postToApiCalling(method, apiKey, apiUrl, data) {
      //console.log(data);
      new Promise(function(resolve, reject) {
        if (method == 'POST') {
          resolve(WebServices.callWebService(apiUrl, data));
        } else {
          resolve(WebServices.callWebService_GET(apiUrl, data));
        }
      }).then((jsonRes) => {
        console.log(jsonRes);
        _that.setState({ isVisible: false })
        if ((!jsonRes) || (jsonRes.code == 0)) {
            setTimeout(()=>{
              Alert.alert('Login Error',jsonRes.message);
            },200);
        }
        else{
          if(jsonRes.code == 1){
            _that.apiSuccessfullResponse(apiKey, jsonRes)
          }
          else if (jsonRes.code == 2) {
            this.setState({sublength:false})
          }
        }
      }).catch((error) => {
          console.log("ERROR" + error);
          _that.setState({ isVisible: false })
          setTimeout(()=>{
              Alert.alert("Server issue"+error);
          },200);
      });
  }

  apiSuccessfullResponse(apiKey, jsonRes) {
      if (apiKey == 'car_rent') {
        deviceData=jsonRes.result;
        console.log(deviceData);

        deviceData.map(ddata=> this.state.diviceType.push({name:ddata.name,id:parseInt(ddata.id)}));
      }
      if (apiKey == 'car_charge') {
       deviceData=jsonRes.result;
       console.log(deviceData);

       deviceData.map(ddata=> this.state.diviceType.push({name:ddata.name,id:parseInt(ddata.id)}));
     }
      if(apiKey=='gadgetsdeviceList'){
        servicelist=jsonRes.result
        console.log(deviceData);

        deviceData.map(ddata=> this.state.diviceType.push({name:ddata.name,id:parseInt(ddata.id)}));
      }
      if(apiKey=='bikeotherCategory'){
        otherData=jsonRes.result;

        otherData.map(data => {this.state.otherCategory.push({name:data.name,id:parseInt(data.id)});this.setState({sublength:true})});
      }
      if(apiKey=='otherCategory'){
        otherData=jsonRes.result;

        otherData.map(data => {this.state.diviceType.push({name:data.name,id:parseInt(data.id)});this.setState({sublength:true})});
      }
  }

  handleOnPress(key){
    if(key=='El-Car'){
      this.setState({car:!this.state.car})
      this.setState({diviceType:[]})
      this.setState({car_charge:false})
      this.setState({car_park:false})
      this.setState({car_bed:false})
      this.setState({car_cable:false})
      this.setState({car_rent:false})
    }
    else if (key=='El-Bike') {
      this.setState({bike:!this.state.bike})
      this.setState({diviceType:[]})
      this.setState({bike_charge:false})
      this.setState({bike_park:false})
      this.setState({bike_bed:false})
      this.setState({bike_cable:false})
      this.setState({bike_rent:false})
    }
    else if (key=='Gadgets') {
      this.setState({gadget:!this.state.gadget})
      this.setState({diviceType:[]})
      this.setState({gadget_charge:false})
      this.setState({gadget_cable:false})
      this.setState({gadget_bed:false})
      this.setState({gadget_rent:false})
    }
  }

  handlePress(key){
    if(key=='bike_charge'){
      this.setState({cat_id:1})
      this.setState({bike_charge:!this.state.bike_charge}),
      this.setState({otherCategory:[]}),
      this.setState({subcat_id:2}),
      this.onClick(2,'bikeotherCategory')
    }
    else if (key=='bike_park') {
      this.setState({cat_id:3})
      this.setState({bike_park:!this.state.bike_park}),
      this.setState({otherCategory:[]}),
      this.setState({subcat_id:8}),
      this.onClick(8,'bikeotherCategory')
    }
    else if (key=='bike_rent') {
      this.setState({cat_id:2})
      this.setState({bike_rent:!this.state.bike_rent}),
      this.setState({otherCategory:[]}),
      this.setState({subcat_id:4}),
      this.onClick(4,'bikeotherCategory')
    }
    else if (key=='gadget_charge') {
      this.setState({cat_id:1})
      this.setState({gadget_charge:!this.state.gadget_charge}),
      this.setState({otherCategory:[]}),
      this.setState({subcat_id:5}),
      this.onClick(5,'bikeotherCategory')
    }
    else if (key=='gadget_rent') {
      this.setState({cat_id:2})
      this.setState({gadget_rent:!this.state.gadget_rent}),
      this.setState({otherCategory:[]}),
      this.setState({subcat_id:6}),
      this.onClick(6,'bikeotherCategory')
    }
  }

  render() {
    const headerProp = {
      title: 'Add Service',
      screens: 'Serviceslistseller',
      type:''
    };

    return (
      <View style={styles.container}>
        <Header info={headerProp} navigation={_that.props.navigation}/>

        {!this.state.empty?
          <View style={styles.content}>
            <KeyboardAwareScrollView innerRef={() => {return [this.refs.name,this.refs.email, this.refs.password]}} >
              <ScrollView>
                <View style={styles.container2}>

                <View style={{alignItems:'center'}}>
                  <TouchableOpacity onPress={this.selectPhotoTapped.bind(this)} style={{height:120,width:120}}>
                  <View style={[styles.avatar,  styles.avatarContainer,{ marginBottom: 20 },]}>
                    {this.state.avatarSource === null ? (
                      <Text>Select a Photo</Text>
                    ) : (
                      <Image style={styles.avatar} source={this.state.avatarSource} />
                    )}
                  </View>
                  </TouchableOpacity>
                </View>
                { !(this.state.avatarerr) ? null :
                  <Text style={styles.error}>{this.state.avatarerr}</Text>
                }

                  <View style={[styles.SectionStyle,{marginTop:20,marginBottom:20}]}>
                      <TouchableOpacity onPress={()=>this.handleOnPress('El-Car')} style={styles.textHead}>
                          <Text style={styles.inputtext}>El-Car</Text>
                          {this.state.car?
                           <Image source={arrow_down} style={{height:15,width:15,}}/>
                          :
                           <Image source={arrow_right} style={{height:15,width:15,}}/>
                          }
                      </TouchableOpacity>
                  </View>

                  {this.state.car ?
                    <View style={{padding:10}}>
                      <View style={[styles.SectionStyle,{marginBottom:10}]}>
                          <TouchableOpacity style={styles.textSubHead} onPress={()=>{this.setState({car_charge:!this.state.car_charge}),this.setState({cat_id:1}),this.onClick(1,'car_charge')}}>
                              <Text style={styles.inputtext}>Charging</Text>
                              {this.state.car_charge?
                               <Image source={arrow_down} style={{height:15,width:15,}}/>
                              :
                               <Image source={arrow_right} style={{height:15,width:15,}}/>
                              }
                          </TouchableOpacity>
                      </View>
                      {this.state.car_charge?
                        <View>
                          <View style={styles.SectionStyle}>
                              <View style={styles.inputfield}>
                                  <Text style={styles.inputtext}>Number of Charging Spot</Text>
                                  <Picker PickerData={this.state.numberchargeitems}
                                   label={'Charging Spot'}
                                   color={'#000000'}
                                   getTxt={(val,label)=>{this.setState({selectedSlots: label});this.setState({slotsError:''})}}/>
                              </View>
                              <Text style={styles.error}>{this.state.slotsError}</Text>
                          </View>
                          <View style={styles.SectionStyle}>
                              <View style={styles.inputfield}>
                                  <Text style={styles.inputtext}>Charge</Text>
                                  <Picker PickerData={this.state.chargedata}
                                   label={'Charge'}
                                   color={'#000000'}
                                   getTxt={(val,label)=> this.setState({selectedChargeType:label})}/>
                              </View>
                          </View>
                          {this.state.selectedChargeType =='Fast charge' ?
                            <View>
                              <View style={styles.SectionStyle}>
                                  <View style={styles.inputfield}>
                                      <Text style={styles.inputtext}>Effect(KW)</Text>
                                      <Picker PickerData={this.state.effect}
                                       label={'Effect(KW)'}
                                       color={'#000000'}
                                       selectedValue={this.state.selectedeffect}
                                       defaultLabel={this.state.selectedeffect}
                                       getTxt={(val,label)=>this.setState({selectedeffect: label})}/>
                                  </View>
                              </View>
                              <View style={styles.SectionStyle}>
                                  <View style={styles.inputfield}>
                                      <Text style={styles.inputtext}>Phases</Text>
                                      <Picker PickerData={this.state.phases}
                                       label={'Phases'}
                                       color={'#000000'}
                                       selectedValue={this.state.selectedphase}
                                       defaultLabel={this.state.selectedphase}
                                       getTxt={(val,label)=>this.setState({selectedphase: label})}/>
                                  </View>
                              </View>
                              <View style={styles.SectionStyle}>
                                  <View style={styles.inputfield}>
                                      <Text style={styles.inputtext}>Ampere</Text>
                                      <Picker PickerData={this.state.ampere}
                                       label={'Ampere'}
                                       color={'#000000'}
                                       selectedValue={this.state.selectedampere}
                                       defaultLabel={this.state.selectedampere}
                                       getTxt={(val,label)=>this.setState({selectedampere: label})}/>
                                  </View>
                              </View>
                            </View>
                          :
                            this.state.selectedChargeType =='Regular Power Outlet(Long Charge)' ?
                              <View>
                                <View style={styles.SectionStyle}>
                                    <View style={styles.inputfield}>
                                        <Text style={styles.inputtext}>Parking Fee</Text>
                                        <TextInput
                                          autoCorrect={false}
                                          underlineColorAndroid="transparent"
                                          selectionColor={"#000000"}
                                          autoFocus={ false}
                                          ref="selectedparkingFee"
                                          placeholder="Parking Fee"
                                          placeholderTextColor="#808080"
                                          style={styles.textInput}
                                          keyboardType={ 'number-pad'}
                                          onChangeText={selectedparkingFee => this.setState({selectedparkingFee})}
                                        />
                                    </View>
                                </View>
                              </View>
                            :
                            null
                          }
                          {this.state.diviceType?
                            <View style={styles.SectionStyle}>
                                <View style={styles.inputfield}>
                                    <Text style={styles.inputtext}>Device Type</Text>
                                    <Picker PickerData={this.state.diviceType}
                                     label={'Device Type'}
                                     color={'#000000'}
                                     selectedValue={this.state.selectedDeviceType}
                                     defaultLabel={this.state.selectedDeviceType}
                                     getTxt={(val,label)=>{this.setState({selectedDeviceType: label});this.setState({selectedDeviceId: val});console.log(val);;this.setState({deviceTypeError:''})}}/>
                                </View>
                                <Text style={styles.error}>{this.state.deviceTypeError}</Text>
                            </View>
                            :
                            null
                          }
                          <View style={styles.SectionStyle}>
                              <View style={styles.inputfield}>
                                  <Text style={styles.inputtext}>Regular Power Outlet</Text>
                                  <Picker PickerData={this.state.car_outlet}
                                   label={'Regular Power Outlet'}
                                   color={'#000000'}
                                   getTxt={(val,label)=>{this.setState({selectedoutlet: label});this.setState({outletError:''})}}/>
                              </View>
                          </View>
                          <Text style={styles.error}>{this.state.outletError}</Text>
                        </View>
                      :
                        null
                      }

                      <View style={[styles.SectionStyle,{marginBottom:10}]}>
                        <TouchableOpacity style={styles.textSubHead} onPress={()=>{this.setState({car_park:!this.state.car_park}),this.setState({cat_id:3})}}>
                            <Text style={styles.inputtext}>Parking</Text>
                            {this.state.car_park?
                             <Image source={arrow_down} style={{height:15,width:15,}}/>
                            :
                             <Image source={arrow_right} style={{height:15,width:15,}}/>
                            }
                        </TouchableOpacity>
                      </View>

                      {this.state.car_park?
                        <View>
                          <View style={styles.SectionStyle}>
                              <View style={styles.inputfield}>
                                  <Text style={styles.inputtext}>Number of Parking Lots</Text>
                                  <Picker PickerData={this.state.numberchargeitems}
                                   label={'Parking Lots'}
                                   color={'#000000'}
                                   getTxt={(val,label)=>{this.setState({parkingLots: label})}}/>
                              </View>
                          </View>
                          <View style={styles.SectionStyle}>
                              <View style={styles.inputfield}>
                                  <Text style={styles.inputtext}>Parking fee per lot</Text>
                                  <TextInput
                                    autoCorrect={false}
                                    underlineColorAndroid="transparent"
                                    selectionColor={"#000000"}
                                    autoFocus={ false}
                                    ref="parkingPrice"
                                    placeholder="Enter parking fee per lot"
                                    placeholderTextColor="#808080"
                                    style={styles.textInput}
                                    keyboardType={ 'number-pad'}
                                    onChangeText={parkingPrice => this.setState({parkingPrice})}
                                  />
                              </View>
                          </View>
                          <View style={styles.SectionStyle}>
                              <View style={styles.inputfield}>
                                  <Text style={styles.inputtext}>Parking fee per day</Text>
                                  <TextInput
                                    autoCorrect={false}
                                    underlineColorAndroid="transparent"
                                    selectionColor={"#000000"}
                                    autoFocus={ false}
                                    ref="parkingperday"
                                    placeholder="Enter parking fee per day"
                                    placeholderTextColor="#808080"
                                    style={styles.textInput}
                                    keyboardType={ 'number-pad'}
                                    onChangeText={parkingperday => this.setState({parkingperday})}
                                  />
                              </View>
                          </View>
                        </View>
                      :
                        null
                      }
                      <View style={[styles.SectionStyle,{marginBottom:10}]}>
                        <TouchableOpacity style={styles.textSubHead} onPress={()=>this.setState({car_bed:!this.state.car_bed})}>
                            <Text style={styles.inputtext}>Bed</Text>
                            {this.state.car_bed?
                             <Image source={arrow_down} style={{height:15,width:15,}}/>
                            :
                             <Image source={arrow_right} style={{height:15,width:15,}}/>
                            }
                        </TouchableOpacity>
                      </View>
                      {this.state.car_bed?
                        <View>
                          <View style={styles.SectionStyle}>
                              <View style={styles.inputfield}>
                                  <Text style={styles.inputtext}>Accomodation</Text>
                                  <Picker PickerData={this.state.accomodation}
                                   label={'Please select type'}
                                   color={'#000000'}
                                   selectedValue={this.state.selectedaccomodation}
                                   defaultLabel={this.state.selectedaccomodation}
                                   getTxt={(val,label) => this.setState({selectedaccomodation:label})}/>
                              </View>
                          </View>
                          <View style={styles.SectionStyle}>
                              <View style={styles.inputfield}>
                                  <Text style={styles.inputtext}>Accomodation charges</Text>
                                  <TextInput
                                    autoCorrect={false}
                                    underlineColorAndroid="transparent"
                                    selectionColor={"#000000"}
                                    autoFocus={ false}
                                    ref="accomodation_charges"
                                    placeholder="Accomodation charges"
                                    placeholderTextColor="#808080"
                                    style={styles.textInput}
                                    keyboardType={ 'number-pad'}
                                    onChangeText={accomodation_charges => this.setState({accomodation_charges})}
                                  />
                              </View>
                          </View>
                        </View>
                      :
                        null
                      }
                      <View style={[styles.SectionStyle,{marginBottom:10}]}>
                        <TouchableOpacity style={styles.textSubHead} onPress={()=>this.setState({car_cable:!this.state.car_cable})}>
                            <Text style={styles.inputtext}>Cable</Text>
                            {this.state.car_cable?
                             <Image source={arrow_down} style={{height:15,width:15,}}/>
                            :
                             <Image source={arrow_right} style={{height:15,width:15,}}/>
                            }
                        </TouchableOpacity>
                      </View>
                      {this.state.car_cable?
                        <View style={styles.SectionStyle}>
                          <View style={styles.inputfield}>
                            <Text style={styles.inputtext}>Cable Charges</Text>
                            <TextInput
                              autoCapitalize={'none'}
                              autoCorrect={false}
                              value={this.state.price}
                              underlineColorAndroid="transparent"
                              returnKeyType={ "next"}
                              selectionColor={"#000000"}
                              autoFocus={ false}
                              placeholder="Price"
                              placeholderTextColor="#808080"
                              style={styles.textInput}
                              ref="selectedprice"
                              keyboardType={ 'number-pad'}
                              onFocus={ () => this.setState({priceError:''}) }
                              onChangeText={selectedprice=> this.setState({selectedprice})}
                            />
                          </View>
                        </View>
                      :
                        null
                      }
                      <View style={[styles.SectionStyle,{marginBottom:10}]}>
                        <TouchableOpacity style={styles.textSubHead} onPress={()=>{this.setState({car_rent:!this.state.car_rent}),this.setState({cat_id:2}),this.onClick(3,'car_rent')}}>
                            <Text style={styles.inputtext}>Rent</Text>
                            {this.state.car_rent?
                             <Image source={arrow_down} style={{height:15,width:15,}}/>
                            :
                             <Image source={arrow_right} style={{height:15,width:15,}}/>
                            }
                        </TouchableOpacity>
                      </View>
                      {this.state.car_rent?
                        <View>
                          {this.state.diviceType.length>0?
                            <View style={styles.SectionStyle}>
                                <View style={styles.inputfield}>
                                    <Text style={styles.inputtext}>Device Type</Text>
                                    <Picker PickerData={this.state.diviceType}
                                     label={'Device Type'}
                                     color={'#000000'}
                                     selectedValue={this.state.selectedDeviceType}
                                     defaultLabel={this.state.selectedDeviceType}
                                     getTxt={(val,label)=>{this.setState({selectedDeviceType: label});this.setState({selectedDeviceId: val});this.setState({deviceTypeError:''})}}/>
                                </View>
                                <Text style={styles.error}>{this.state.deviceTypeError}</Text>
                            </View>
                            :
                            null
                          }
                          <View style={styles.SectionStyle}>
                              <View style={styles.inputfield}>
                                  <Text style={styles.inputtext}>Rent per day</Text>
                                  <TextInput
                                    autoCorrect={false}
                                    underlineColorAndroid="transparent"
                                    selectionColor={"#000000"}
                                    autoFocus={ false}
                                    ref="reantperday"
                                    placeholder="Enter parking fee per lot"
                                    placeholderTextColor="#808080"
                                    style={styles.textInput}
                                    keyboardType={ 'number-pad'}
                                    onChangeText={reantperday => this.setState({reantperday})}
                                  />
                              </View>
                          </View>
                          <View style={styles.SectionStyle}>
                              <View style={styles.inputfield}>
                                  <Text style={styles.inputtext}>Drivers Licence Number</Text>
                                  <Picker PickerData={this.state.coffee}
                                   label={'Please select '}
                                   color={'#000000'}
                                   getTxt={(val,label) => this.setState({selecteddriver_licence_no:label})}/>
                              </View>
                          </View>
                          <View style={styles.SectionStyle}>
                              <View style={styles.inputfield}>
                                  <Text style={styles.inputtext}>Upload copy of Drivers Licence</Text>
                                  <Picker PickerData={this.state.coffee}
                                   label={'Please select '}
                                   color={'#000000'}
                                   getTxt={(val,label) => this.setState({selectedupload_licence:label})}/>
                              </View>
                          </View>
                          <View style={styles.SectionStyle}>
                              <View style={styles.inputfield}>
                                  <Text style={styles.inputtext}>Insurance/Deposit</Text>
                                  <TextInput
                                    autoCorrect={false}
                                    underlineColorAndroid="transparent"
                                    selectionColor={"#000000"}
                                    autoFocus={ false}
                                    ref="selectedinsurance"
                                    placeholder="Amount"
                                    placeholderTextColor="#808080"
                                    style={styles.textInput}
                                    keyboardType={ 'number-pad'}
                                    onChangeText={selectedinsurance => this.setState({selectedinsurance})}
                                  />

                              </View>
                          </View>
                        </View>
                      :
                        null
                      }
                    </View>
                  :
                    null
                  }

                  <View style={[styles.SectionStyle,{marginBottom:20}]}>
                      <TouchableOpacity onPress={()=>this.handleOnPress('El-Bike')} style={styles.textHead}>
                          <Text style={styles.inputtext}>El-Bike</Text>
                          {this.state.bike?
                           <Image source={arrow_down} style={{height:15,width:15,}}/>
                          :
                           <Image source={arrow_right} style={{height:15,width:15,}}/>
                          }
                      </TouchableOpacity>
                  </View>

                  {this.state.bike ?
                    <View style={{padding:10}}>
                      <View style={[styles.SectionStyle,{marginBottom:10}]}>
                          <TouchableOpacity style={styles.textSubHead} onPress={()=>this.handlePress('bike_charge')}>
                              <Text style={styles.inputtext}>Charging</Text>
                              {this.state.bike_charge?
                               <Image source={arrow_down} style={{height:15,width:15,}}/>
                              :
                               <Image source={arrow_right} style={{height:15,width:15,}}/>
                              }
                          </TouchableOpacity>
                      </View>
                      {this.state.bike_charge?
                        <View>
                          {this.state.otherCategory?
                            <View style={styles.SectionStyle}>
                                <View style={styles.inputfield}>
                                    <Text style={styles.inputtext}>Inner Sub Category</Text>
                                    <Picker PickerData={this.state.otherCategory}
                                     label={'Select Inner Category'}
                                     color={'#000000'}
                                     selectedValue={this.state.selectedOtherCategory}
                                     defaultLabel={this.state.selectedOtherCategory}
                                     getTxt={(val,label)=> {this.onClick(val,'otherCategory'),this.setState({selectedOtherCategory:label})}}/>
                                </View>
                            </View>
                          :
                            null
                          }
                          <View style={styles.SectionStyle}>
                              <View style={styles.inputfield}>
                                  <Text style={styles.inputtext}>Number of Charging Spot</Text>
                                  <Picker PickerData={this.state.numberchargeitems}
                                   label={'Charging Spot'}
                                   color={'#000000'}
                                   getTxt={(val,label)=>{this.setState({selectedSlots: label});this.setState({slotsError:''})}}/>
                              </View>
                          </View>
                          <Text style={styles.error}>{this.state.slotsError}</Text>
                          {this.state.diviceType.length>0?
                            <View style={styles.SectionStyle}>
                                <View style={styles.inputfield}>
                                    <Text style={styles.inputtext}>Device Type</Text>
                                    <Picker PickerData={this.state.diviceType}
                                     label={'Please select category'}
                                     color={'#000000'}
                                     getTxt={(val,label)=>{this.setState({selectedDeviceType: val});this.setState({deviceTypeError:''})}}/>
                                </View>
                                <Text style={styles.error}>{this.state.deviceTypeError}</Text>
                            </View>
                          :
                            null
                          }
                          <View style={styles.SectionStyle}>
                              <View style={styles.inputfield}>
                                  <Text style={styles.inputtext}>Regular Power Outlet</Text>
                                  <Picker PickerData={this.state.bike_outlet}
                                   label={'Select option'}
                                   color={'#000000'}
                                   getTxt={(val,label)=>{this.setState({selectedoutlet: label});this.setState({outletError:''})}}/>
                              </View>
                          </View>
                          <Text style={styles.error}>{this.state.outletError}</Text>
                        </View>
                      :
                        null
                      }
                      <View style={[styles.SectionStyle,{marginBottom:10}]}>
                        <TouchableOpacity style={styles.textSubHead} onPress={()=>this.handlePress('bike_park')}>
                            <Text style={styles.inputtext}>Parking</Text>
                            {this.state.bike_park?
                             <Image source={arrow_down} style={{height:15,width:15,}}/>
                            :
                             <Image source={arrow_right} style={{height:15,width:15,}}/>
                            }
                        </TouchableOpacity>
                      </View>
                      {this.state.bike_park?
                        <View>
                          {this.state.otherCategory?
                            <View style={styles.SectionStyle}>
                                <View style={styles.inputfield}>
                                    <Text style={styles.inputtext}>Inner Sub Category</Text>
                                    <Picker PickerData={this.state.otherCategory}
                                     label={'Select Inner Category'}
                                     color={'#000000'}
                                     selectedValue={this.state.selectedOtherCategory}
                                     defaultLabel={this.state.selectedOtherCategory}
                                     getTxt={(val,label)=> {this.onClick(val,'otherCategory')}}/>
                                </View>
                            </View>
                          :
                            null
                          }
                          <View style={styles.SectionStyle}>
                              <View style={styles.inputfield}>
                                  <Text style={styles.inputtext}>Number of Parking Lots</Text>
                                  <Picker PickerData={this.state.numberchargeitems}
                                   label={'Parking Lots'}
                                   color={'#000000'}
                                   getTxt={(val,label)=>{this.setState({parkingLots: label})}}/>
                              </View>
                          </View>
                          <View style={styles.SectionStyle}>
                              <View style={styles.inputfield}>
                                  <Text style={styles.inputtext}>Parking fee per lot</Text>
                                  <TextInput
                                    autoCorrect={false}
                                    underlineColorAndroid="transparent"
                                    selectionColor={"#000000"}
                                    autoFocus={ false}
                                    ref="parkingPrice"
                                    placeholder="Enter parking fee per lot"
                                    placeholderTextColor="#808080"
                                    style={styles.textInput}
                                    keyboardType={ 'number-pad'}
                                    onChangeText={parkingPrice => this.setState({parkingPrice})}
                                  />
                              </View>
                          </View>
                          <View style={styles.SectionStyle}>
                              <View style={styles.inputfield}>
                                  <Text style={styles.inputtext}>Parking fee per day</Text>
                                  <TextInput
                                    autoCorrect={false}
                                    underlineColorAndroid="transparent"
                                    selectionColor={"#000000"}
                                    autoFocus={ false}
                                    ref="parkingperday"
                                    placeholder="Enter parking fee per day"
                                    placeholderTextColor="#808080"
                                    style={styles.textInput}
                                    keyboardType={ 'number-pad'}
                                    onChangeText={parkingperday => this.setState({parkingperday})}
                                  />
                              </View>
                          </View>
                        </View>
                      :
                        null
                      }
                      <View style={[styles.SectionStyle,{marginBottom:10}]}>
                        <TouchableOpacity style={styles.textSubHead} onPress={()=>this.setState({bike_bed:!this.state.bike_bed})}>
                            <Text style={styles.inputtext}>Bed</Text>
                            {this.state.bike_bed?
                             <Image source={arrow_down} style={{height:15,width:15,}}/>
                            :
                             <Image source={arrow_right} style={{height:15,width:15,}}/>
                            }
                        </TouchableOpacity>
                      </View>
                      {this.state.bike_bed?
                        <View>
                          <View style={styles.SectionStyle}>
                              <View style={styles.inputfield}>
                                  <Text style={styles.inputtext}>Accomodation</Text>
                                  <Picker PickerData={this.state.accomodation}
                                   label={'Please select type'}
                                   color={'#000000'}
                                   selectedValue={this.state.selectedaccomodation}
                                   defaultLabel={this.state.selectedaccomodation}
                                   getTxt={(val,label) => this.setState({selectedaccomodation:label})}/>
                              </View>
                          </View>
                          <View style={styles.SectionStyle}>
                              <View style={styles.inputfield}>
                                  <Text style={styles.inputtext}>Accomodation charges</Text>
                                  <TextInput
                                    autoCorrect={false}
                                    underlineColorAndroid="transparent"
                                    selectionColor={"#000000"}
                                    autoFocus={ false}
                                    ref="accomodation_charges"
                                    placeholder="Accomodation charges"
                                    placeholderTextColor="#808080"
                                    style={styles.textInput}
                                    keyboardType={ 'number-pad'}
                                    onChangeText={accomodation_charges => this.setState({accomodation_charges})}
                                  />
                              </View>
                          </View>
                        </View>
                      :
                        null
                      }
                      <View style={[styles.SectionStyle,{marginBottom:10}]}>
                        <TouchableOpacity style={styles.textSubHead} onPress={()=>this.setState({bike_cable:!this.state.bike_cable})}>
                            <Text style={styles.inputtext}>Cable</Text>
                            {this.state.bike_cable?
                             <Image source={arrow_down} style={{height:15,width:15,}}/>
                            :
                             <Image source={arrow_right} style={{height:15,width:15,}}/>
                            }
                        </TouchableOpacity>
                      </View>
                      {this.state.bike_cable?
                        <View style={styles.SectionStyle}>
                          <View style={styles.inputfield}>
                            <Text style={styles.inputtext}>Cable Charges</Text>
                            <TextInput
                              autoCapitalize={'none'}
                              autoCorrect={false}
                              value={this.state.price}
                              underlineColorAndroid="transparent"
                              returnKeyType={ "next"}
                              selectionColor={"#000000"}
                              autoFocus={ false}
                              placeholder="Price"
                              placeholderTextColor="#808080"
                              style={styles.textInput}
                              ref="selectedprice"
                              keyboardType={ 'number-pad'}
                              onFocus={ () => this.setState({priceError:''}) }
                              onChangeText={selectedprice=> this.setState({selectedprice})}
                            />
                          </View>
                        </View>
                      :
                        null
                      }
                      <View style={[styles.SectionStyle,{marginBottom:10}]}>
                        <TouchableOpacity style={styles.textSubHead} onPress={()=>this.handlePress('bike_rent')}>
                            <Text style={styles.inputtext}>Rent</Text>
                            {this.state.bike_rent?
                             <Image source={arrow_down} style={{height:15,width:15,}}/>
                            :
                             <Image source={arrow_right} style={{height:15,width:15,}}/>
                            }
                        </TouchableOpacity>
                      </View>
                      {this.state.bike_rent?
                        <View>
                          {this.state.otherCategory?
                            <View style={styles.SectionStyle}>
                                <View style={styles.inputfield}>
                                    <Text style={styles.inputtext}>Inner Sub Category</Text>
                                    <Picker PickerData={this.state.otherCategory}
                                     label={'Select Inner Category'}
                                     color={'#000000'}
                                     selectedValue={this.state.selectedOtherCategory}
                                     defaultLabel={this.state.selectedOtherCategory}
                                     getTxt={(val,label)=> {this.onClick(val,'otherCategory'),this.setState({selectedOtherCategory:label})}}/>
                                </View>
                            </View>
                          :
                            null
                          }
                          {this.state.diviceType.length>0?
                            <View style={styles.SectionStyle}>
                                <View style={styles.inputfield}>
                                    <Text style={styles.inputtext}>Device Type</Text>
                                    <Picker PickerData={this.state.diviceType}
                                     label={'Please select category'}
                                     color={'#000000'}
                                     getTxt={(val,label)=>{this.setState({selectedDeviceType: val});this.setState({deviceTypeError:''})}}/>
                                </View>
                                <Text style={styles.error}>{this.state.deviceTypeError}</Text>
                            </View>
                          :
                            null
                          }
                          <View style={styles.SectionStyle}>
                              <View style={styles.inputfield}>
                                  <Text style={styles.inputtext}>Rent per day</Text>
                                  <TextInput
                                    autoCorrect={false}
                                    underlineColorAndroid="transparent"
                                    selectionColor={"#000000"}
                                    autoFocus={ false}
                                    ref="reantperday"
                                    placeholder="Enter parking fee per lot"
                                    placeholderTextColor="#808080"
                                    style={styles.textInput}
                                    keyboardType={ 'number-pad'}
                                    onChangeText={reantperday => this.setState({reantperday})}
                                  />
                              </View>
                          </View>
                          <View style={styles.SectionStyle}>
                              <View style={styles.inputfield}>
                                  <Text style={styles.inputtext}>Drivers Licence Number</Text>
                                  <Picker PickerData={this.state.coffee}
                                   label={'Please select '}
                                   color={'#000000'}
                                   getTxt={(val,label) => this.setState({selecteddriver_licence_no:label})}/>
                              </View>
                          </View>
                          <View style={styles.SectionStyle}>
                              <View style={styles.inputfield}>
                                  <Text style={styles.inputtext}>Upload copy of Drivers Licence</Text>
                                  <Picker PickerData={this.state.coffee}
                                   label={'Please select '}
                                   color={'#000000'}
                                   getTxt={(val,label) => this.setState({selectedupload_licence:label})}/>
                              </View>
                          </View>
                          <View style={styles.SectionStyle}>
                              <View style={styles.inputfield}>
                                  <Text style={styles.inputtext}>Insurance/Deposit</Text>
                                  <TextInput
                                    autoCorrect={false}
                                    underlineColorAndroid="transparent"
                                    selectionColor={"#000000"}
                                    autoFocus={ false}
                                    ref="selectedinsurance"
                                    placeholder="Amount"
                                    placeholderTextColor="#808080"
                                    style={styles.textInput}
                                    keyboardType={ 'number-pad'}
                                    onChangeText={selectedinsurance => this.setState({selectedinsurance})}
                                  />
                              </View>
                          </View>
                        </View>
                      :
                        null
                      }
                    </View>
                  :
                    null
                  }

                  <View style={[styles.SectionStyle,{marginBottom:20}]}>
                      <TouchableOpacity onPress={()=>this.handleOnPress('Gadgets')} style={styles.textHead}>
                          <Text style={styles.inputtext}>Gadgets</Text>
                          {this.state.gadget?
                           <Image source={arrow_down} style={{height:15,width:15,}}/>
                          :
                           <Image source={arrow_right} style={{height:15,width:15,}}/>
                          }
                      </TouchableOpacity>
                  </View>

                  {this.state.gadget ?
                    <View style={{padding:10}}>
                      <View style={[styles.SectionStyle,{marginBottom:10}]}>
                          <TouchableOpacity style={styles.textSubHead} onPress={()=>this.handlePress('gadget_charge')}>
                              <Text style={styles.inputtext}>Charging</Text>
                              {this.state.gadget_charge?
                               <Image source={arrow_down} style={{height:15,width:15,}}/>
                              :
                               <Image source={arrow_right} style={{height:15,width:15,}}/>
                              }
                          </TouchableOpacity>
                      </View>
                      {this.state.gadget_charge?
                        <View>
                          {this.state.otherCategory?
                            <View style={styles.SectionStyle}>
                                <View style={styles.inputfield}>
                                    <Text style={styles.inputtext}>Inner Sub Category</Text>
                                    <Picker PickerData={this.state.otherCategory}
                                     label={'Select Inner Category'}
                                     color={'#000000'}
                                     selectedValue={this.state.selectedOtherCategory}
                                     defaultLabel={this.state.selectedOtherCategory}
                                     getTxt={(val,label)=> {this.onClick(val,'otherCategory'),this.setState({selectedOtherCategory:label})}}/>
                                </View>
                            </View>
                          :
                            null
                          }
                          {this.state.diviceType.length>0?
                            <View style={styles.SectionStyle}>
                                <View style={styles.inputfield}>
                                    <Text style={styles.inputtext}>Device Type</Text>
                                    <Picker PickerData={this.state.diviceType}
                                     label={'Please select category'}
                                     color={'#000000'}
                                     getTxt={(val,label)=>{this.setState({selectedDeviceType: val});this.setState({deviceTypeError:''})}}/>
                                </View>
                                <Text style={styles.error}>{this.state.deviceTypeError}</Text>
                            </View>
                          :
                            null
                          }
                          <View style={styles.SectionStyle}>
                              <View style={styles.inputfield}>
                                  <Text style={styles.inputtext}>Number of Charging Spot</Text>
                                  <Picker PickerData={this.state.numberchargeitems}
                                   label={'Charging Spot'}
                                   color={'#000000'}
                                   getTxt={(val,label)=>{this.setState({selectedSlots: label});this.setState({slotsError:''})}}/>
                              </View>
                          </View>
                          <Text style={styles.error}>{this.state.slotsError}</Text>

                          <View style={styles.SectionStyle}>
                              <View style={styles.inputfield}>
                                  <Text style={styles.inputtext}>Regular Power Outlet</Text>
                                  <Picker PickerData={this.state.bike_outlet}
                                   label={'Select option'}
                                   color={'#000000'}
                                   getTxt={(val,label)=>{this.setState({selectedoutlet: label});this.setState({outletError:''})}}/>
                              </View>
                          </View>
                          <Text style={styles.error}>{this.state.outletError}</Text>
                        </View>
                      :
                        null
                      }
                      <View style={[styles.SectionStyle,{marginBottom:10}]}>
                        <TouchableOpacity style={styles.textSubHead} onPress={()=>this.setState({gadget_bed:!this.state.gadget_bed})}>
                            <Text style={styles.inputtext}>Bed</Text>
                            {this.state.gadget_bed?
                             <Image source={arrow_down} style={{height:15,width:15,}}/>
                            :
                             <Image source={arrow_right} style={{height:15,width:15,}}/>
                            }
                        </TouchableOpacity>
                      </View>
                      {this.state.gadget_bed?
                        <View>
                          <View style={styles.SectionStyle}>
                              <View style={styles.inputfield}>
                                  <Text style={styles.inputtext}>Accomodation</Text>
                                  <Picker PickerData={this.state.accomodation}
                                   label={'Please select type'}
                                   color={'#000000'}
                                   selectedValue={this.state.selectedaccomodation}
                                   defaultLabel={this.state.selectedaccomodation}
                                   getTxt={(val,label) => this.setState({selectedaccomodation:label})}/>
                              </View>
                          </View>
                          <View style={styles.SectionStyle}>
                              <View style={styles.inputfield}>
                                  <Text style={styles.inputtext}>Accomodation charges</Text>
                                  <TextInput
                                    autoCorrect={false}
                                    underlineColorAndroid="transparent"
                                    selectionColor={"#000000"}
                                    autoFocus={ false}
                                    ref="accomodation_charges"
                                    placeholder="Accomodation charges"
                                    placeholderTextColor="#808080"
                                    style={styles.textInput}
                                    keyboardType={ 'number-pad'}
                                    onChangeText={accomodation_charges => this.setState({accomodation_charges})}
                                  />
                              </View>
                          </View>
                        </View>
                      :
                        null
                      }
                      <View style={[styles.SectionStyle,{marginBottom:10}]}>
                        <TouchableOpacity style={styles.textSubHead} onPress={()=>this.setState({gadget_cable:!this.state.gadget_cable})}>
                            <Text style={styles.inputtext}>Cable</Text>
                            {this.state.gadget_cable?
                             <Image source={arrow_down} style={{height:15,width:15,}}/>
                            :
                             <Image source={arrow_right} style={{height:15,width:15,}}/>
                            }
                        </TouchableOpacity>
                      </View>
                      {this.state.gadget_cable?
                        <View style={styles.SectionStyle}>
                          <View style={styles.inputfield}>
                            <Text style={styles.inputtext}>Cable Charges</Text>
                            <TextInput
                              autoCapitalize={'none'}
                              autoCorrect={false}
                              value={this.state.price}
                              underlineColorAndroid="transparent"
                              returnKeyType={ "next"}
                              selectionColor={"#000000"}
                              autoFocus={ false}
                              placeholder="Price"
                              placeholderTextColor="#808080"
                              style={styles.textInput}
                              ref="selectedprice"
                              keyboardType={ 'number-pad'}
                              onFocus={ () => this.setState({priceError:''}) }
                              onChangeText={selectedprice=> this.setState({selectedprice})}
                            />
                          </View>
                        </View>
                      :
                        null
                      }
                      <View style={[styles.SectionStyle,{marginBottom:10}]}>
                        <TouchableOpacity style={styles.textSubHead} onPress={()=>this.handlePress('gadget_rent')}>
                            <Text style={styles.inputtext}>Rent</Text>
                            {this.state.gadget_rent?
                             <Image source={arrow_down} style={{height:15,width:15,}}/>
                            :
                             <Image source={arrow_right} style={{height:15,width:15,}}/>
                            }
                        </TouchableOpacity>
                      </View>
                      {this.state.gadget_rent?
                        <View>
                          {this.state.otherCategory?
                            <View style={styles.SectionStyle}>
                                <View style={styles.inputfield}>
                                    <Text style={styles.inputtext}>Inner Sub Category</Text>
                                    <Picker PickerData={this.state.otherCategory}
                                     label={'Select Inner Category'}
                                     color={'#000000'}
                                     selectedValue={this.state.selectedOtherCategory}
                                     defaultLabel={this.state.selectedOtherCategory}
                                     getTxt={(val,label)=> {this.onClick(val,'otherCategory'),this.setState({selectedOtherCategory:label})}}/>
                                </View>
                            </View>
                          :
                            null
                          }
                          {this.state.diviceType.length>0?
                            <View style={styles.SectionStyle}>
                                <View style={styles.inputfield}>
                                    <Text style={styles.inputtext}>Device Type</Text>
                                    <Picker PickerData={this.state.diviceType}
                                     label={'Please select category'}
                                     color={'#000000'}
                                     getTxt={(val,label)=>{this.setState({selectedDeviceType: val});this.setState({deviceTypeError:''})}}/>
                                </View>
                                <Text style={styles.error}>{this.state.deviceTypeError}</Text>
                            </View>
                          :
                            null
                          }
                          <View style={styles.SectionStyle}>
                              <View style={styles.inputfield}>
                                  <Text style={styles.inputtext}>Rent per day</Text>
                                  <TextInput
                                    autoCorrect={false}
                                    underlineColorAndroid="transparent"
                                    selectionColor={"#000000"}
                                    autoFocus={ false}
                                    ref="reantperday"
                                    placeholder="Enter parking fee per lot"
                                    placeholderTextColor="#808080"
                                    style={styles.textInput}
                                    keyboardType={ 'number-pad'}
                                    onChangeText={reantperday => this.setState({reantperday})}
                                  />
                              </View>
                          </View>
                          <View style={styles.SectionStyle}>
                              <View style={styles.inputfield}>
                                  <Text style={styles.inputtext}>Drivers Licence Number</Text>
                                  <Picker PickerData={this.state.coffee}
                                   label={'Please select '}
                                   color={'#000000'}
                                   getTxt={(val,label) => this.setState({selecteddriver_licence_no:label})}/>
                              </View>
                          </View>
                          <View style={styles.SectionStyle}>
                              <View style={styles.inputfield}>
                                  <Text style={styles.inputtext}>Upload copy of Drivers Licence</Text>
                                  <Picker PickerData={this.state.coffee}
                                   label={'Please select '}
                                   color={'#000000'}
                                   getTxt={(val,label) => this.setState({selectedupload_licence:label})}/>
                              </View>
                          </View>
                          <View style={styles.SectionStyle}>
                              <View style={styles.inputfield}>
                                  <Text style={styles.inputtext}>Insurance/Deposit</Text>
                                  <TextInput
                                    autoCorrect={false}
                                    underlineColorAndroid="transparent"
                                    selectionColor={"#000000"}
                                    autoFocus={ false}
                                    ref="selectedinsurance"
                                    placeholder="Amount"
                                    placeholderTextColor="#808080"
                                    style={styles.textInput}
                                    keyboardType={ 'number-pad'}
                                    onChangeText={selectedinsurance => this.setState({selectedinsurance})}
                                  />
                              </View>
                          </View>
                        </View>
                      :
                        null
                      }
                    </View>
                  :
                    null
                  }

                  {/*<View style={styles.SectionStyle}>
                      <View style={styles.inputfield}>
                          <Text style={styles.inputtext}>Description</Text>
                          <TextInput
                            autoCapitalize={'none'}
                            autoCorrect={false}
                            value={this.state.description}
                            multiline={true}
                            numberOfLines={4}
                            underlineColorAndroid="transparent"
                            returnKeyType={ "next"}
                            selectionColor={"#000000"}
                            autoFocus={ false}
                            placeholder="Description"
                            placeholderTextColor="#808080"
                            style={[styles.textInput,{height:80}]}
                            ref="selecteddescription"
                            keyboardType={ 'default'}
                            onFocus={ () => this.setState({descriptionError:''}) }
                            onChangeText={selecteddescription=> this.setState({selecteddescription})}
                          />
                      </View>
                      <Text style={styles.error}>{this.state.descriptionError}</Text>
                  </View>*/}

                  {/*<View style={styles.SectionStyle}>
                    <View style={styles.inputfield}>
                      <Text style={styles.inputtext}>Status</Text>
                        <RadioGroup
                          color='#000000'
                          selectedIndex={1}
                          style={styles.SectionRadioGroup}
                          onSelect = {(index, value) => this.onSelect(index, value)}
                        >
                          <RadioButton value={'0'}>
                            <Text style={{color:'#000000'}}>InActive</Text>
                          </RadioButton>

                          <RadioButton value={'1'}>
                            <Text style={{color:'#000000'}}>Active</Text>
                          </RadioButton>
                        </RadioGroup>
                      </View>
                  </View>*/}
                </View>
              </ScrollView>
            </KeyboardAwareScrollView>

            <TouchableOpacity onPress={this.submit_click} style={[styles.SectionStyle,{marginTop:AppSizes.ResponsiveSize.Padding(3)}]}>
              <CommonButton label='Save' width='100%'/>
            </TouchableOpacity>
          </View>
        :
          <View style={{alignItems:'center',width:'100%'}}>
            <Text style={{fontSize:AppSizes.ResponsiveSize.Sizes(20)}}>Please add a station first</Text>
          </View>
        }

        <Spinner visible={this.state.isVisible}  />
      </View>
    );
  }
}


const styles = StyleSheet.create({
  container: {
    flex: 1,
    paddingTop:  0,
  },
  content: {
      flex: 1,
      flexDirection: 'column',
      height:'70%',
      backgroundColor:'#ffffff'
  },
  container1: {
    flex: 3,
    height:AppSizes.screen.width/4,
    paddingLeft:AppSizes.ResponsiveSize.Padding(5),
    //backgroundColor:'red'
  },
  container2: {
    flex: 10,
    height:'70%',
    paddingLeft:'4%',
    paddingRight:'4%',
    paddingTop:'4%',
    flexDirection: 'column',
    //backgroundColor:'blue'
  },
  container3: {
    flex: 3,
    height:AppSizes.screen.width/8,
    alignItems: 'center',//replace with flex-end or center
    justifyContent: 'flex-end',
  },
  avatarContainer: {
     borderColor: '#9B9B9B',
     borderWidth: 1 / PixelRatio.get(),
     justifyContent: 'center',
     alignItems: 'center',
  },
  avatar: {
   borderRadius: 75,
   width: 120,
   height: 120,
  },
  headerTitle:{
   fontSize:AppSizes.ResponsiveSize.Sizes(25),
   color:'#000000',
   fontWeight:'bold',
   fontFamily:Fonts.RobotoBold,
   paddingBottom:AppSizes.ResponsiveSize.Padding(1),
  },
  headersubTitle:{
   fontSize:AppSizes.ResponsiveSize.Sizes(14),
   color:'#000000',
   fontWeight:'600',
   fontFamily:Fonts.RobotoBold,
   paddingTop:AppSizes.ResponsiveSize.Padding(2),
  },
  headerBorder :{
   borderBottomColor: '#000000',
   borderBottomWidth: 3,
   width:'10%',
  },
  footertext :{
   color :'#000000',
   fontSize:AppSizes.ResponsiveSize.Sizes(12),
   fontWeight:'400',
   fontFamily:Fonts.RobotoRegular,
  },
  SectionStyle: {
   width:AppSizes.ResponsiveSize.width,
  },
  ImageStyle: {
   height: AppSizes.screen.width/21,
   width: AppSizes.screen.width/21,
   resizeMode : 'contain',
  },
  textInput: {
   flex:1,
   paddingLeft:AppSizes.ResponsiveSize.Padding(3),
   fontSize: AppSizes.ResponsiveSize.Sizes(12),
   color:'black',
   backgroundColor:'#eee',
   height:40,
  },
  inputfield:{
   width:AppSizes.ResponsiveSize.width,
  },
  inputtext:{marginBottom:10,
   fontSize:AppSizes.ResponsiveSize.Sizes(14),
   fontWeight:'600',
   fontFamily:Fonts.RobotoBold,
  },
  error:{
   color:'red',
   fontFamily:Fonts.RobotoRegular,
   fontSize:AppSizes.ResponsiveSize.Sizes(12),
  },
  SectionRadioGroup:{
    flexDirection:'row',
  },
  facilityView:{
    alignItems:'center',
    margin:AppSizes.ResponsiveSize.Padding(2)
  },
  facilityText:{
    fontSize:25,
    color:'#000',
    fontFamily:Fonts.RobotoRegular,
  },
  checkboxSection :{
    marginBottom: AppSizes.ResponsiveSize.Padding(2),
    height:AppSizes.screen.width/15,
    width:AppSizes.screen.width/3
    //backgroundColor:'red',
  },
  SectionRadioView: {
    borderBottomWidth: 1,
    borderBottomColor: '#ffffff',
    borderRadius: 5 ,
    width:AppSizes.screen.width*90/100,
    marginLeft: AppSizes.ResponsiveSize.Padding(5),
    marginRight: AppSizes.ResponsiveSize.Padding(5),
    marginBottom: (Platform.OS === 'ios') ? AppSizes.ResponsiveSize.Padding(1) :AppSizes.ResponsiveSize.Padding(2),

  },
  textHead:{
    width:'100%',
    flexDirection:'row',
    justifyContent:'space-between',
    backgroundColor:'#b7b7b7',
    padding:10,
  },
  textSubHead:{
    width:'100%',
    flexDirection:'row',
    justifyContent:'space-between',
  }
});
