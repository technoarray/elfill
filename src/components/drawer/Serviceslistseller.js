import React, { Component } from 'react';
import {AsyncStorage,StyleSheet,Text,View,Image,TouchableOpacity,Platform,Alert,ScrollView,TextInput} from 'react-native';
import { AppStyles, AppSizes, AppColors } from '../../themes/'
import { NavigationActions } from 'react-navigation'
import Spacer from '../common/Spacer'
import * as commonFunctions from '../../utils/CommonFunctions'
import Header from '../common/HeaderWithBack'
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view'
import CommonButton from '../common/CommonButton'
import LinearGradient from 'react-native-linear-gradient';
import { Fonts } from '../../utils/Fonts';
import Spinner from 'react-native-loading-spinner-overlay';

var Constant = require('../../api/ApiRules').Constant;
var WebServices = require('../../api/ApiRules').WebServices;
/* Images */
const bg_image = require('../../themes/Images/bg_image.png')
var del= require( '../../themes/Images/delete.png')
var edit= require( '../../themes/Images/edit.png')
var no_image= require( '../../themes/Images/no_station.png')


let _that
export default class Serviceslistseller extends Component {
  constructor (props) {
      super(props)
      this.state = {
      isVisible: false,
      uid:'',
      dataSource: [],
    },
    _that = this;
  }

  componentWillMount(){
    AsyncStorage.getItem('UserData').then((UserData) => {
      var data = JSON.parse(UserData);
        uid=data.id
        if(uid != ''){
          _that.setState({uid:uid})
          _that.validationAndApiParameter(uid,'serviceslist')
        }
    })
  }

  validationAndApiParameter(id,apiKey) {
    const { uid,isVisible } = this.state
       if(apiKey=='servicedelete'){
       const data = new FormData();
       data.append('id', id);
       data.append('uid', uid);
        //console.log(data);
       _that.setState({isVisible: true});

       _that.postToApiCalling('POST', apiKey, Constant.URL_serviceDelete, data);
     }
     else if(apiKey=='serviceslist'){
       const data = new FormData();
       data.append('uid', uid);
        console.log(data);
       _that.setState({isVisible: true});

      _that.postToApiCalling('POST', apiKey, Constant.URL_service, data);
     }
    }

    postToApiCalling(method, apiKey, apiUrl, data) {

       new Promise(function(resolve, reject) {
            if (method == 'POST') {
                resolve(WebServices.callWebService(apiUrl, data));
            } else {
                resolve(WebServices.callWebService_GET(apiUrl, data));
            }
        }).then((jsonRes) => {
          _that.setState({ isVisible: false })

            if ((!jsonRes) || (jsonRes.code == 0)) {

            setTimeout(()=>{
                Alert.alert('Error',jsonRes.message);
            },200);

            } else {
                _that.apiSuccessfullResponse(apiKey, jsonRes)
            }
        }).catch((error) => {
            console.log("ERROR" + error);
            _that.setState({ isVisible: false })

            setTimeout(()=>{
                Alert.alert("Server issue");
            },200);
        });
    }

    apiSuccessfullResponse(apiKey, jsonRes) {
        if (apiKey == 'servicedelete') {
            console.log(jsonRes)
            servicelist=jsonRes.result
            _that.setState({
                //dataSource: ds.cloneWithRows(jsonRes.ResponsePacket),
               dataSource: servicelist,
             });
        }
        else if(apiKey =='serviceslist'){
          console.log(jsonRes)
          servicelist=jsonRes.result
          _that.setState({
             dataSource: servicelist
           });
          //_that.props.navigation.navigate('Serviceslistseller',{servicelist:servicelist});
        }
    }

  viewBtn=(item)=>{
     _that.props.navigation.navigate('ViewServiceScreen',{item:item});
  }
  editBtn=(item)=>{
    console.log(item);
     _that.props.navigation.navigate('Editservices',{item:item});
  }

  deleteBtn=(item)=>{
    console.log(item);
    var id=item.id
    Alert.alert(
      'Delete Service',
      'Are you sure you want to delete service!',
      [
        {text: 'Cancel', onPress: () => console.log('Cancel Pressed'), style: 'cancel'},
        {text: 'OK', onPress: () => _that.validationAndApiParameter(id,'servicedelete')},
      ],
      { cancelable: false }
    )

  }

  render() {
    const headerProp = {
      title: 'Services',
      screens: 'SellerDashboard',
      type:''
    };
    return (
      <View style={styles.container}>
      <Header info={headerProp} navigation={_that.props.navigation}/>

        <View style={styles.content}>
        {this.state.dataSource && this.state.dataSource.length > 0 ?
        <ScrollView>
            <View style={styles.container1}>
            {
              this.state.dataSource.map((item, index) => (
                <LinearGradient colors={['#fff','#fff','#fff','#fff']} key={item.id} style={[styles.linearGradient,styles.servicebox,styles.shadow]}>
                  <TouchableOpacity style={styles.serviceimg} activeOpacity={.6} onPress={() => this.viewBtn(item)}>
                    {item.service_image != '' ?
                      <Image source={{uri: Constant.image_path+item.service_image}} style={styles.Image} />
                      :
                      <Image source={no_image} style={styles.Image} />
                    }
                    </TouchableOpacity>
                    <TouchableOpacity activeOpacity={.6} onPress={() => this.viewBtn(item)} style={styles.titleblk}>
                      <Text style={styles.title}>{item.cat_name}</Text>
                      <View style={{flexDirection:'row'}}>
                        <Text style={styles.smalltext}>{item.subcat_name} , </Text>
                        <Text style={styles.smalltext}> {item.station_name}</Text>
                      </View>
                    </TouchableOpacity>
                    <View style={styles.sideicon}>
                        <TouchableOpacity activeOpacity={.6} style={styles.icon} onPress={() => this.editBtn(item)} >
                              <Image source={edit} style={[styles.ImageStyle,styles.sidearrow]} />
                        </TouchableOpacity>
                        <TouchableOpacity activeOpacity={.6} style={styles.icon} onPress={() => this.deleteBtn(item)} >
                          <View style={styles.icon}>
                                <Image source={del} style={[styles.ImageStyle,styles.sidearrow]} />
                          </View>
                      </TouchableOpacity>
                    </View>
                  </LinearGradient>
              ))
           }
          </View>

  </ScrollView>
  :
  <Text style={{textAlign:'center'}}>No Record Found</Text>
 }

        </View>
        <Spinner visible={this.state.isVisible}  />

    </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    paddingTop:  0,
  },

  content: {
      flex: 1,
      paddingLeft:'4%',
      paddingRight:'4%',
      paddingTop:'4%',
      backgroundColor:'#fff'
  },
  container1: {
    flex: 1,
    //backgroundColor:'red',
    flexDirection: 'column',
  },
  servicebox:{
    flexDirection:'row',
    padding:AppSizes.ResponsiveSize.Sizes(15),
    paddingBottom:AppSizes.ResponsiveSize.Sizes(20),
    borderRadius: 5,
    marginBottom:AppSizes.ResponsiveSize.Padding(2),
    paddingRight:AppSizes.ResponsiveSize.Padding(0)
  },
  titleblk:{
    width:'50%',
    height:'100%',
    paddingLeft:13
  },
  sideicon:{
      width:'30%',
      alignItems: 'center',
      flexDirection:'row',
      justifyContent: 'center',
  },
  ImageStyle: {
    height: AppSizes.screen.width/19,
    width: AppSizes.screen.width/19,
    resizeMode : 'contain',
  },
  title:{
    fontSize:AppSizes.ResponsiveSize.Sizes(20),
    paddingBottom: AppSizes.ResponsiveSize.Sizes(5),
    color:'#452959',
    fontWeight:'700',
    fontFamily:Fonts.RobotoBold,
  },
  smalltext:{
    fontSize:AppSizes.ResponsiveSize.Sizes(10),
    color:'#000',
    fontWeight:'400',
    fontFamily:Fonts.RobotoRegular,
  },
    icon:{
      padding:AppSizes.ResponsiveSize.Padding(5),
    },
    /*containerend:{
      position:'absolute',
      right:0,
      bottom:8
    },*/

    sideiconmain:{
        width:'10%',
        alignItems: 'center',
        paddingTop:AppSizes.ResponsiveSize.Padding(2),

        //backgroundColor:'gray'
    },
    serviceimg:{
    width:55,
    height:50,
    borderRadius:60,
    overflow:'hidden'
    }, Image: {
    height:'100%',
    width:'100%',
    resizeMode : 'cover',
    },
    shadow:{
     borderWidth:1,
     borderRadius: 2,
     borderColor: '#fff',
     justifyContent:'center',
     backgroundColor:'#fff',
     borderColor: '#ddd',
     borderBottomWidth: 1,
     shadowColor: '#999',
     shadowOffset: { width: 0, height: 2 },
     shadowOpacity: 0.8,
     shadowRadius: 2,
     elevation: 0,
     //backgroundColor:'red',

    },
});
