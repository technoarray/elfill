import React, { Component } from 'react';
import {StyleSheet,Text,View,Image,TouchableOpacity,Platform,ScrollView,Alert,ImageBackground,StatusBar,TextInput,Keyboard,AsyncStorage} from 'react-native';
import { AppStyles, AppSizes, AppColors } from '../../themes/'
import { NavigationActions } from 'react-navigation'
import Spacer from '../common/Spacer'
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view'
import Header from '../common/HeaderWithBack'
import CommonButton from '../common/CommonButton'
import * as commonFunctions from '../../utils/CommonFunctions'
import {RadioGroup, RadioButton} from 'react-native-flexi-radio-button'
import Picker from 'react-native-q-picker';
import CheckBox from 'react-native-check-box'
import Spinner from 'react-native-loading-spinner-overlay';
import { Fonts } from '../../utils/Fonts';
var Constant = require('../../api/ApiRules').Constant;
var WebServices = require('../../api/ApiRules').WebServices;

const bg_image = require('../../themes/Images/bg_image.png')
const left_arrow = require('../../themes/Images/left-arrow.png')
const profile_icon = require('../../themes/Images/profile.png')
const email_icon = require('../../themes/Images/email.png')
const password_icon = require('../../themes/Images/password.png')
const device_icon = require('../../themes/Images/car.png')
const modal_icon = require('../../themes/Images/modal.png')
const mobile_icon = require('../../themes/Images/mobile.png')


let _that
let seller_services=[]

export default class EditProfile extends Component {
  constructor (props) {
    super(props)
    this.state = {
    user_name:'',
    email:'',
    password:'',
    mobile_number:'',
    user_type:'',
    radioindex:'',
    show_buyer_section:false,
    show_seller_section:false,
    deviceitems:[
      {name:'Car',id:1},
      {name:'Scooter',id:2},
      {name:'Drone',id:3},
    ],
    device_type: '',
    modalitems:[
      {name:'Modal1',id:1},
      {name:'Modal2',id:2},
      {name:'Modal3',id:3},
    ],
    modal_type: '',
    scooter:false,
    car:false,
    parking_lots:false,
    gadgets:false,
    isChecked:true,
    isVisible: false,
    nameerr :'',
    emailerr :'',
    pwderr :'',
    typeerr :'',
  },
  _that = this;
}

componentWillMount(){
  AsyncStorage.getItem('UserData').then((UserData) => {
    var data = JSON.parse(UserData);
      uid=data.id
      services=data.seller_services;
      console.log(data);
      var res_services = services.split(",");
      if (res_services.indexOf("car") > -1) {
          this.setState({car:true})
      }
      if (res_services.indexOf("drone") > -1) {
          this.setState({drone:true})
      }
      if (res_services.indexOf("scooter") > -1) {
          this.setState({scooter:true})
      }
      if(uid != ''){
        if(data.user_type=='buyer'){
          this.setState({radioindex:0})
          this.onSelect(0, 'buyer')
        }
        else if(data.user_type=='seller'){
          this.setState({radioindex:1})
          this.onSelect(1, 'seller')
        }
        else{
          this.setState({radioindex:2})
          this.onSelect(2, 'both')
        }
        _that.setState({
          uid:uid,
          user_name:data.name,
          email:data.email,
          password:data.password,
          mobile_number:data.telephone,
          user_type:data.user_type,
          device_type:data.buyer_device_type,
          modal_type:data.buyer_device_modal
        })
      }
  })
}

submit_click(){
  //_that.props.navigation.navigate('VerifyOtpScreen');
  _that.validationAndApiParameter()
}

validationAndApiParameter() {
      const { method, uid, user_name, password, mobile_number,user_type,device_type,modal_type,scooter,car,drone,isVisible } = this.state
      var error=0;
      if ((user_name.length <= 0)) {
        _that.setState({nameerr:'Please enter your name'})
        error=1;
          //Alert.alert('','Please enter User name!');
      }
      else{
        _that.setState({nameerr:''})
      }

      if ((password.indexOf(' ') >= 0 || password.length <= 0)) {
        _that.setState({pwderr:'Please enter password'})
        error=1;
      }
      else{
          _that.setState({pwderr:''})
      }

      if ((user_type.length <= 0)) {
        _that.setState({typeerr:'Please enter type'})
        error=1;
      }
      else{
        _that.setState({typeerr:''})
      }
       //else if ((mobile_number.indexOf(' ') >= 0 || mobile_number.length <= 0)) {
        //  Alert.alert('','Please enter Mobile number!');
    //  }
      if(error==0) {
        Keyboard.dismiss()
        console.log(car + scooter + drone)
          if(car==true){
            seller_services.push('Car')
          }
          if(scooter==true){
            seller_services.push('Scooter')
          }
          if(parking_lots==true){
            seller_services.push('Parking Lots')
          }
          if(gadgets==true){
            seller_services.push('Gadgets')
          }
          var json_seller_services = JSON.stringify(seller_services);
        //  console.log(json_seller_services);
          const data = new FormData();
          data.append('uid', uid);
          data.append('name', user_name);
          data.append('password', password);
          data.append('telephone', mobile_number);
          data.append('user_type', user_type);
          data.append('buyer_device_type', device_type);
          data.append('buyer_device_modal', modal_type);
          data.append('seller_services', json_seller_services);
        //  console.log(data);
          _that.setState({   isVisible: true });
        this.postToApiCalling('POST', 'updateProfile', Constant.URL_updateProfile, data);
        }

}

postToApiCalling(method, apiKey, apiUrl, data) {

   new Promise(function(resolve, reject) {
        if (method == 'POST') {
            resolve(WebServices.callWebService(apiUrl, data));
        } else {
            resolve(WebServices.callWebService_GET(apiUrl, data));
        }
    }).then((jsonRes) => {
      _that.setState({ isVisible: false })
      console.log(jsonRes)

        if ((!jsonRes) || (jsonRes.code == 0)) {
        //_that.setState({ isVisible: false })
        setTimeout(()=>{
            Alert.alert(jsonRes.message);
        },200);

        } else {
            if (jsonRes.code == 1) {
              jdata=jsonRes.data;
              seller_services=[]
              //AsyncStorage.setItem("loggedIn", JSON.stringify(true)).done();
              AsyncStorage.setItem("user_type", JSON.stringify(jdata.user_type));
              AsyncStorage.setItem('UserData', JSON.stringify(jsonRes.data));
              setTimeout(()=>{
                  Alert.alert("Profile Updated Successfully");
              },200);
            }
        }
    }).catch((error) => {
        console.log("ERROR" + error);
        _that.setState({ isVisible: false })

        setTimeout(()=>{
            Alert.alert("Server issue");
        },200);
    });
}


onSelect(index, value){
  this.setState({typeerr:''})
  this.setState({user_type: value})
  if(value=='buyer'){
    seller_services=[]
      this.setState({
        show_buyer_section: true,
        show_seller_section: false,
        scooter:false,
        car:false,
        parking_lots:false,
        gadets:false
      })

  }
  else if(value=='seller'){
    this.setState({
      show_buyer_section: false,
      show_seller_section: true,
    })
  }
  else{
    this.setState({
      show_buyer_section: true,
      show_seller_section: true,
    })
  }
}

render() {
  const headerProp = {
    title: 'Edit Profile',
    screens: 'EditProfile',
    type:''
  };
  return (
    <View style={styles.container}>
      <Header info={headerProp} navigation={_that.props.navigation}/>
      <View style={styles.content}>
        <KeyboardAwareScrollView innerRef={() => {return [this.refs.name,this.refs.email, this.refs.password]}} >
        <View style={styles.container2}>
          <View style={styles.SectionStyle}>
              <View style={styles.inputfield}>
                  <Text style={styles.inputtext}>Name</Text>
                  <TextInput
                    autoCapitalize={'none'}
                    autoCorrect={false}
                    underlineColorAndroid="transparent"
                    returnKeyType={ "next"}
                    selectionColor={"#000000"}
                    autoFocus={ false}
                    placeholder="Name"
                    placeholderTextColor="#808080"
                    style={styles.textInput}
                    ref="user_name"
                    value={this.state.user_name}
                    keyboardType={ 'email-address'}
                    onFocus={ () => this.setState({nameerr:''}) }
                    onChangeText={user_name=> this.setState({user_name})}
                  />
              </View>
              { !(this.state.nameerr) ? null :
                <Text style={styles.error}>{this.state.nameerr}</Text>
              }
          </View>

          <View style={styles.SectionStyle}>
              <View style={styles.inputfield}>
                  <Text style={styles.inputtext}>Email</Text>
                  <TextInput
                    editable={false}
                    autoCapitalize={'none'}
                    autoCorrect={false}
                    underlineColorAndroid="transparent"
                    returnKeyType={ "next"}
                    selectionColor={"#000000"}
                    autoFocus={ false}
                    placeholder="Email"
                    placeholderTextColor="#808080"
                    style={styles.textInput}
                    value={this.state.email}
                    ref="email"
                    />
              </View>
          </View>
          <View style={styles.SectionStyle}>
              <View style={styles.inputfield}>
                  <Text style={styles.inputtext}>Password</Text>
                  <TextInput
                    autoCapitalize={'none'}
                    autoCorrect={false}
                    underlineColorAndroid="transparent"
                    returnKeyType={ "next"}
                    selectionColor={"#000000"}
                    autoFocus={ false}
                    placeholder="*******"
                    placeholderTextColor="#808080"
                    style={styles.textInput}
                    ref="password"
                    secureTextEntry={true}
                    value={this.state.password}
                    onFocus={ () => this.setState({pwderr:''}) }
                    onChangeText={password=> this.setState({password})}
                  />
              </View>
              { !(this.state.pwderr) ? null :
                <Text style={styles.error}>{this.state.pwderr}</Text>
              }
          </View>

          <View style={styles.SectionStyle}>
              <View style={styles.inputfield}>
                  <Text style={styles.inputtext}>Telephone</Text>
                  <TextInput
                    autoCapitalize={'none'}
                    autoCorrect={false}
                    underlineColorAndroid="transparent"
                    returnKeyType={ "next"}
                    selectionColor={"#000000"}
                    autoFocus={ false}
                    placeholder="Telephone"
                    placeholderTextColor="#808080"
                    style={styles.textInput}
                    ref="mobile"
                    value={this.state.mobile_number}
                    keyboardType={ 'number-pad'}
                    onChangeText={mobile_number=> this.setState({mobile_number})}
                  />
              </View>
          </View>
          <View style={styles.SectionStyle}>
              <View style={styles.inputfield}>
                  <Text style={styles.inputtext}>User Type</Text>
                  <View style={styles.SectionRadioView}>
                      <RadioGroup
                        color='#000000'
                        style={styles.SectionRadioGroup}
                        selectedIndex={this.state.radioindex}
                        onSelect = {(index, value) => this.onSelect(index, value)}
                      >
                        <RadioButton value={'buyer'}>
                          <Text style={{color:'#000000'}}>Buyer</Text>
                        </RadioButton>

                        <RadioButton value={'seller'}>
                          <Text style={{color:'#000000'}}>Seller</Text>
                        </RadioButton>

                        <RadioButton value={'both'}>
                          <Text style={{color:'#000000'}}>Both</Text>
                        </RadioButton>
                      </RadioGroup>

                  </View>
              </View>
              { !(this.state.typeerr) ? null :
                <Text style={styles.error}>{this.state.typeerr}</Text>
              }
          </View>
          { (!this.state.show_buyer_section) ? null :
            <View style={{width:AppSizes.screen.width}}>
              <View style={[styles.SectionRadioView,{width:'90%'}]}>
              { this.state.device_type != "" ?
                  <Picker PickerData={this.state.deviceitems}
                   label={'Select Device'}
                   color={'#000000'}
                   defaultLabel={this.state.device_type}
                   selectedValue={this.state.device_type}
                   getTxt={(val,label)=>this.setState({device_type: label})}/>
                   : null }
              </View>
            </View>
          }

          { (!this.state.show_seller_section) ? null :
            <View style={{width:AppSizes.screen.width,paddingBottom:20}}>
              <View style={[styles.SectionRadioView,{width:'90%'}]}>
                <CheckBox
                  style={styles.checkboxSection}
                  checkBoxColor="#000000"
                  onClick={()=>{this.setState({scooter:!this.state.scooter})}}
                  isChecked={this.state.scooter}
                  rightText={"El-scooter"}
                  rightTextStyle={{color:"#000000"}}
                />
                <CheckBox
                  style={styles.checkboxSection}
                  checkBoxColor="#000000"
                  onClick={()=>{this.setState({car:!this.state.car})}}
                  isChecked={this.state.car}
                  rightText={"El-car"}
                  rightTextStyle={{color:"#000000"}}
                />
                <CheckBox
                  style={styles.checkboxSection}
                  checkBoxColor="#000000"
                  onClick={()=>{this.setState({parking_lots:!this.state.parking_lots})}}
                  isChecked={this.state.parking_lots}
                  rightText={"Parking Lots"}
                  rightTextStyle={{color:"#000000"}}
                />
                <CheckBox
                  style={styles.checkboxSection}
                  checkBoxColor="#000000"
                  onClick={()=>{this.setState({gadgets:!this.state.gadgets})}}
                  isChecked={this.state.gadgets}
                  rightText={"Gadgets"}
                  rightTextStyle={{color:"#000000"}}
                />
              </View>
            </View>
          }
        </View>
        </KeyboardAwareScrollView>
        <View style={styles.container3}>
          <TouchableOpacity onPress={this.submit_click}>
            <View style={[styles.SectionStyle,{marginTop:'5%',borderBottomColor: 'red',justifyContent:'flex-end'}]}>
              <CommonButton label='Save' width='100%'/>
            </View>
          </TouchableOpacity>
        </View>
      </View>
      <Spinner visible={this.state.isVisible}  />
    </View>
  );
}
}

const styles = StyleSheet.create({
container: {
  flex: 1,
  paddingTop:  0,
},

content: {
    flex: 1,
    flexDirection: 'column',
    height:'70%',
    padding:'5%',
    backgroundColor:'#ffffff'
},
container1: {
  flex: 3,
  height:AppSizes.screen.width/4,
  paddingLeft:AppSizes.ResponsiveSize.Padding(5),
  //backgroundColor:'red'
},

container2: {
  flex: 10,
  height:'70%',
  flexDirection: 'column',

  //backgroundColor:'blue'
},
container3: {
  flex: 3,
  height:AppSizes.screen.width/8,
  alignItems: 'center',//replace with flex-end or center
  justifyContent: 'flex-end',



},

headerTitle:{
fontSize:AppSizes.ResponsiveSize.Sizes(25),
color:'#000000',
fontWeight:'bold',
paddingBottom:AppSizes.ResponsiveSize.Padding(1),
fontFamily:Fonts.RobotoRegular,
},
headersubTitle:{
fontSize:AppSizes.ResponsiveSize.Sizes(14),
color:'#000000',
fontWeight:'600',
paddingTop:AppSizes.ResponsiveSize.Padding(2),
fontFamily:Fonts.RobotoRegular,
},
headerBorder :{
borderBottomColor: '#000000',
borderBottomWidth: 3,
width:'10%',
},
footertext :{
color :'#000000',
fontSize:AppSizes.ResponsiveSize.Sizes(12),
fontWeight:'400',
fontFamily:Fonts.RobotoRegular,
},
SectionStyle: {
width:AppSizes.ResponsiveSize.width,

},

ImageStyle: {
height: AppSizes.screen.width/21,
width: AppSizes.screen.width/21,
resizeMode : 'contain',
},
textInput: {
flex:1,
paddingLeft:AppSizes.ResponsiveSize.Padding(3),
fontSize: AppSizes.ResponsiveSize.Sizes(12),
color:'#000000',
backgroundColor:'#eee',
height:40,
fontFamily:Fonts.RobotoRegular,
},
inputfield:{
width:AppSizes.ResponsiveSize.width,

},
inputtext:{marginBottom:10,
marginTop:AppSizes.ResponsiveSize.Padding(3),
fontSize:AppSizes.ResponsiveSize.Sizes(14),
fontWeight:'600',
marginBottom:AppSizes.ResponsiveSize.Padding(1),
fontFamily:Fonts.RobotoBold,
},
SectionRadioView: {
  borderBottomWidth: 1,
  borderBottomColor: '#000000',
  borderRadius: 5 ,
  //paddingBottom:AppSizes.ResponsiveSize.Padding(1)
},
SectionRadioGroup:{
  flexDirection:'row',
},
checkboxSection :{
  marginBottom: AppSizes.ResponsiveSize.Padding(2),
  height:AppSizes.screen.width/15,
  //backgroundColor:'red',
},
error:{
  color:'red',
  textAlign:'center',
  fontFamily:Fonts.RobotoRegular,
}
});
