import React, { Component } from 'react';
import {StyleSheet,Text,View,Image,TouchableOpacity,ImageBackground,Platform,Alert,ScrollView} from 'react-native';
import { NavigationActions } from 'react-navigation'
import Header from '../common/Header'
import CommonButton from '../common/CommonButton'
import { AppStyles, AppSizes, AppColors } from '../../themes/'
import MapView, {ProviderPropType} from 'react-native-maps';
import Geocoder from 'react-native-geocoder';
import PolyLine from '@mapbox/polyline';
import { Fonts } from '../../utils/Fonts';
var Constant = require('../../api/ApiRules').Constant;
var WebServices = require('../../api/ApiRules').WebServices;

const bg_image = require('../../themes/Images/bg_image.png')
const smile = require('../../themes/Images/smile.png')

const LATITUDE = 37.78825;
const LONGITUDE = -122.4324;
const LATITUDE_DELTA = 0.29;
const LONGITUDE_DELTA = LATITUDE_DELTA * ASPECT_RATIO;
const SPACE = 0.01;
const LAT= 30.7279571
const LANG= 76.8553046

let _that
export default class ThankyouScreen extends Component {
  constructor (props) {
      super(props)
      this.state = {
        isVisible:false,
        //station_id:this.props.navigation.state.params.station_id,
        name:'',
        mobile:'',
        station_name:'',
        address:'',
        country:'',
        zipcode:'',
        isMapReady: false,
        region: {
          latitude: LATITUDE,
          longitude: LONGITUDE,
          latitudeDelta: LATITUDE_DELTA,
          longitudeDelta: LONGITUDE_DELTA
        },
      },
      this.mergeLot = this.mergeLot.bind(this);
      _that=this;
  }

  componentDidMount(){
    try {
    Geocoder.fallbackToGoogle('AIzaSyBX6rKXe6Jsk6ZynShEZiNfDfyhZWgmXsQ');
     navigator.geolocation.getCurrentPosition(
      (position) => {
        var region = {
          lat: position.coords.latitude,
          lng:  position.coords.longitude,
        };
        //_that.validationAndApiParameter('ServiceList',uid,region.lat,region.lng)
        this.setState({
          region: {
            latitude: position.coords.latitude,
            longitude: position.coords.longitude,
            latitudeDelta: LATITUDE_DELTA,
            longitudeDelta: LONGITUDE_DELTA,
            accuracy: position.coords.accuracy
          }
        });
        this.setState({
           latitude: position.coords.latitude,
           longitude: position.coords.longitude,
           error: null,
         });
        this.mergeLot();
        Geocoder.geocodePosition(region).then(res => {
          //console.log(res)
          var address=res[0].formattedAddress
          _that.setState({
            latitude: position.coords.latitude,
            longitude: position.coords.longitude,
            address: address,
          });
        })
        this.watchID = navigator.geolocation.watchPosition((position) => {
          const newRegion = {
            latitude: position.coords.latitude,
            longitude: position.coords.longitude,
            latitudeDelta: LATITUDE_DELTA,
            longitudeDelta: LONGITUDE_DELTA,
            accuracy: position.coords.accuracy
          }
          this.setState({newRegion});
        })
      },
      (error) => console.log('error '+error.message),
      {enableHighAccuracy: false, timeout: 50000, maximumAge: 10000}
    );
    }
    catch(err) {
        console.log(err);
    }
  //  _that.validationAndApiParameter('seller_details')
  }

  async getDirections(startLoc, destinationLoc) {
           try {
               let resp = await fetch(`https://maps.googleapis.com/maps/api/directions/json?origin=${ startLoc }&destination=${ destinationLoc }&key=AIzaSyBX6rKXe6Jsk6ZynShEZiNfDfyhZWgmXsQ`)
               console.log(resp);
               let respJson = await resp.json();
               let points = PolyLine.decode(respJson.routes[0].overview_polyline.points);
               let coords = points.map((point, index) => {
                   return  {
                       latitude : point[0],
                       longitude : point[1]
                   }
               })
               this.setState({coords: coords})
               this.setState({x: "true"})
               return coords
           } catch(error) {
             console.log('error',error)
               this.setState({x: "error"})
               return error
           }
       }

       mergeLot(){

         if (this.state.latitude != null && this.state.longitude!=null)
          {
            let concatLot = this.state.latitude +","+this.state.longitude
            let destination=this.state.lat +","+this.state.lgt
            console.log(concatLot);
            console.log(destination);
            this.setState({
              concat: concatLot
            }, () => {
              this.getDirections(concatLot,destination);
            });
          }
        }

  validationAndApiParameter(apikey){
    if(apikey == 'seller_details'){
      const data = new FormData();
      data.append('station_id',this.state.station_id);

      _that.setState({isVisible: true});
      this.postToApiCalling('POST',apikey, Constant.URL_sellerDetails, data);
    }
  }

  postToApiCalling(method, apiKey, apiUrl, data) {
    new Promise(function(resolve, reject) {
      if (method == 'POST') {
        resolve(WebServices.callWebService(apiUrl, data));
      } else {
        resolve(WebServices.callWebService_GET(apiUrl, data));
      }
    }).then((jsonRes) => {
      console.log(jsonRes);
      _that.setState({ isVisible: false })
      if ((!jsonRes) || (jsonRes.code == 0)) {
          setTimeout(()=>{
            Alert.alert('Login Error',jsonRes.message);
          },200);
      }
      else {
        _that.apiSuccessfullResponse(jsonRes)
      }
    }).catch((error) => {
        console.log("ERROR" + error);
        _that.setState({ isVisible: false })
        setTimeout(()=>{
            Alert.alert("Server issue"+error);
        },200);
    });
  }

  apiSuccessfullResponse(jsonRes){
    stationData=jsonRes.result;
    console.log(stationData)

    this.setState({name:stationData.name})
    this.setState({mobile:stationData.telephone})
    this.setState({address:stationData.address})
    this.setState({zipcode:stationData.zip_code})
    this.setState({country:stationData.country})
    this.setState({station_name:stationData.station_name})
  }

  submit_click=()=>{
    this.props.navigation.navigate('HomeScreen')
  }

  render() {
    const headerProp = {
      title: 'Thankyou',
      screens: 'ThankyouScreen',
        type:''
    };

    return (
      <View style={{flex:1}}>
      <Header info={headerProp} navigation={_that.props.navigation}/>
      <View style={styles.container}>
        <View style={{alignItems:'center',width:'100%',}}>
          <View style={styles.main}>
            <View style={styles.imgbox}>
            <MapView style={styles.map}
                      region={this.state.region}
                      provider='google'
                      mapType='standard'
                      showsCompass={true}
                      showsPointsOfInterest >
                      {!!this.state.latitude && !!this.state.longitude &&
                        <MapView.Marker
                         pinColor={'#3995f7'}
                         coordinate={{"latitude":this.state.latitude,"longitude":this.state.longitude}}
                         title={"Your Location"}
                         showsUserLocation={true}
                       />}

                       {!!this.state.cordLatitude && !!this.state.cordLongitude &&
                         <MapView.Marker
                          pinColor={'#3995f7'}
                          coordinate={{"latitude":this.state.cordLatitude,"longitude":this.state.cordLongitude}}
                          title={this.state.name}
                          showsUserLocation={true}
                        />}

                       {!!this.state.latitude && !!this.state.longitude && this.state.x == 'true' &&
                       <MapView.Polyline
                            coordinates={this.state.coords}
                            strokeWidth={3}
                            lineCap={'round'}
                            lineJoin={'round'}
                            strokeColor="#3995f7"/>
                        }

                        {!!this.state.latitude && !!this.state.longitude && this.state.x == 'error' &&
                        <MapView.Polyline
                          coordinates={[
                              {latitude: this.state.latitude, longitude: this.state.longitude},
                              {latitude: this.state.cordLatitude, longitude: this.state.cordLongitude},
                          ]}
                          lineCap={'round'}
                          lineJoin={'round'}
                          strokeWidth={3}
                          strokeColor="#3995f7"/>
                         }
              </MapView>
            </View>
            <ScrollView style={styles.scrollbox}>
                  <View style={styles.detail}>
                      <View style={[styles.boxtitle,styles.shadow]}>
                          <Text style={styles.texttitle}>Your Booking Details</Text>
                      </View>
                      <View style={styles.list}>
                            <Text style={styles.text}>Order Number:-0124521dsf12</Text>
                            <Text style={styles.text}>Station Name:-Electric Filling station</Text>
                            <Text style={styles.text}>Total amount:-$130</Text>
                            <Text style={styles.text}>Amount Paid:-$12</Text>
                            <Text style={styles.text}>Amount Payable to seller:-$118</Text>
                      </View>
                  </View>
                  <View style={styles.detail}>
                      <View style={[styles.boxtitle,styles.shadow]}>
                          <Text style={styles.texttitle}>Seller Details</Text>
                      </View>
                      <View style={styles.list}>
                            <Text style={styles.text}>Name:-Neha</Text>
                            <Text style={styles.text}>Address:-sco-4,swastik vihar</Text>
                            <Text style={styles.text}>Mobile Number:-8053857544</Text>
                            <Text style={styles.text}>State:-Panchkula</Text>
                            <Text style={styles.text}>Country:-India</Text>
                      </View>
                  </View>
            </ScrollView>
          </View>
        </View>
        <View style={styles.container3}>
        <TouchableOpacity onPress={this.submit_click}>
          <View style={[styles.SectionStyle,{justifyContent:'flex-end'}]}>
            <CommonButton label='Go back to home page' width='100%'/>
          </View>
        </TouchableOpacity>
        </View>
      </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,

    //backgroundColor:'red',
    alignItems:'center'
  },
  innercontainer:{
    width: '100%',
    height: '100%',
    resizeMode:'contain',
  },
  main:{
    width:'100%',
    alignItems:'center',
    //height:'92%',

    //backgroundColor:'green'
  },
  imgbox:{
    width:'100%',
    alignItems:'center',
    marginTop:'5%',
    marginBottom:AppSizes.ResponsiveSize.Padding(4)
  },

  detail:{
    width:'100%',
    alignItems:'center',
    //backgroundColor:'green',
    paddingBottom:AppSizes.ResponsiveSize.Padding(4)
  },
  thantext:{
    fontSize:AppSizes.ResponsiveSize.Sizes(32),
    color:'#000',
    fontWeight:'700',
    width:'100%',
    textAlign:'center',
    marginBottom:AppSizes.ResponsiveSize.Padding(3),
    fontFamily:Fonts.RobotoBlack,
  },
  booktext:{
    fontSize:AppSizes.ResponsiveSize.Sizes(20),
    fontWeight:'600',
    color:'#000',
    marginBottom:AppSizes.ResponsiveSize.Padding(2),
    fontFamily:Fonts.RobotoBold,
  },

  shadow:{
   //borderWidth:1,
   borderRadius: 0,
   borderColor: '#000',
   justifyContent:'center',
   //backgroundColor:'#fff',
   borderColor: '#ddd',
   //borderBottomWidth: 1,
   shadowColor: '#999',
   shadowOffset: { width: 0, height: 2 },
   shadowOpacity: 0.8,
   shadowRadius: 2,
   elevation: 0,
   //backgroundColor:'red',
  },
  boxtitle:{
    width:'100%',
    height:50,
    alignItems:'center',
    backgroundColor:'#5b545c',
    marginBottom:AppSizes.ResponsiveSize.Padding(2)
  },
  texttitle:{
    marginRight:AppSizes.ResponsiveSize.Padding(1),
    fontSize:AppSizes.ResponsiveSize.Sizes(15),
    fontWeight:'300',
    color:'#fff',
    fontFamily:Fonts.RobotoRegular,
  },
  text:{
    fontSize:AppSizes.ResponsiveSize.Sizes(14),
    color:'#333',
    marginBottom:5,
    fontFamily:Fonts.RobotoRegular,
  },
  list:{
    alignItems:'center',
    padding:AppSizes.ResponsiveSize.Padding(3)
  },
  container3: {
    alignItems: 'center',
    justifyContent: 'flex-end',
    backgroundColor:'red',

  },
  scrollbox:{
    height:AppSizes.screen.height/2.1,
    width:'100%',
    //backgroundColor:'red'
  },
  imgbox:{
    height:AppSizes.screen.height/3,
    width:'100%'
  }
});
