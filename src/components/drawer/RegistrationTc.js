import React, { Component } from 'react';
import {AsyncStorage,StyleSheet,Text,View,Image,TouchableOpacity,Platform,Alert,Modal,ScrollView} from 'react-native';
import { AppStyles, AppSizes, AppColors } from '../../themes/'
import { NavigationActions } from 'react-navigation'
import Spacer from '../common/Spacer'
import * as commonFunctions from '../../utils/CommonFunctions'
import Header from '../common/HeaderWithBack'
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view'
import CommonButton from '../common/CommonButton'
import LinearGradient from 'react-native-linear-gradient';
import DatePicker from 'react-native-datepicker'
import {RadioGroup, RadioButton} from 'react-native-flexi-radio-button'
import Picker from 'react-native-q-picker';
import DateTimePicker from 'react-native-modal-datetime-picker';
import Spinner from 'react-native-loading-spinner-overlay';
import CheckBox from 'react-native-check-box';
import { Fonts } from '../../utils/Fonts';
var Constant = require('../../api/ApiRules').Constant;
var WebServices = require('../../api/ApiRules').WebServices;

/* Images */
const paypal = require('../../themes/Images/paypal.png')
var stripe= require( '../../themes/Images/stripe.png')
const clock= require('../../themes/Images/clock.png')
const calendar=require('../../themes/Images/calendar.png')

let _that
export default class PaymentMethod extends Component {
  static title = 'Custom Card'
  constructor (props) {
      super(props)
      this.state = {
        tc:false,
      },
      _that = this;
  }

  submit_click(){
    AsyncStorage.setItem('onScreen','AcceptedTc')
    this.props.navigation.navigate('VerifyOtpScreen');
  }

  render() {
    const headerProp = {
      title: 'Review & Pay',
      screens: 'SideMenu',
        type:''
    };

    const { loading, token, error, params, errorParams } = this.state

    return (
      <View style={styles.container}>
        <View style={styles.content}>
          <View style={{flex:1,padding:15,backgroundColor:'#fff'}}>
            <Text style={{fontFamily:'400',color:'#000',fontSize:AppSizes.ResponsiveSize.Sizes(25),textAlign:'center',marginBottom:20,marginTop:10}}>Terms & Conditions</Text>
            <ScrollView>
              <Text style={styles.tccontent}>
                Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industrys standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.
               </Text>
            </ScrollView>
            <CheckBox
              style={styles.checkboxSection}
              checkBoxColor={"#000000"}
              onClick={()=>{this.setState({tc:!this.state.tc})}}
              isChecked={this.state.tc}
              rightText={"I ACCEPT TERMS AND CONDITIONS"}
              rightTextStyle={{color:"#000000"}}
            />
            <View style={styles.okBtn}>
            {this.state.tc?
              <TouchableOpacity onPress={() =>this.submit_click()} style={styles.SectionStyle}>
                <CommonButton label='Confirm' width='100%'/>
              </TouchableOpacity>
            :
              null
            }
            </View>
          </View>
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    paddingTop:  0,
    backgroundColor:'#fff'
  },

  content: {
      flex: 1,
      padding:'5%',
      paddingBottom:0,
      //backgroundColor:'#000'
  },
  container1: {
    flex: 1,
    //backgroundColor:'red',
    flexDirection: 'column',
  },
  servicebox:{
    flexDirection:'column',
    padding:AppSizes.ResponsiveSize.Sizes(15),
    borderRadius: 5,
    marginBottom:AppSizes.ResponsiveSize.Padding(2),
    //backgroundColor:'red',
  },

    shadow:{
     borderWidth:1,
     borderRadius: 2,
     borderColor: '#fff',
     justifyContent:'center',
     backgroundColor:'#fff',
     borderColor: '#ddd',
     borderBottomWidth: 1,
     shadowColor: '#999',
     shadowOffset: { width: 0, height: 2 },
     shadowOpacity: 0.8,
     shadowRadius: 2,
     elevation: 0,
     //backgroundColor:'red',

    },
  contentContainer:{
    height:AppSizes.screen.height/.7
  },
  title:{
    fontSize:AppSizes.ResponsiveSize.Sizes(18),
    paddingBottom: AppSizes.ResponsiveSize.Sizes(5),
    color:'#333',
    fontWeight:'700',
    fontFamily:Fonts.RobotoBold,
  },
  border:{
    borderWidth:1.5,
    borderColor:'#333',
    width:30,
  },
  smalltitle:{
    fontSize:AppSizes.ResponsiveSize.Sizes(15),
    paddingBottom: AppSizes.ResponsiveSize.Sizes(3),
    color:'#333',
    fontWeight:'700',
    marginTop:AppSizes.ResponsiveSize.Padding(2),
    fontFamily:Fonts.RobotoBold,
  },
  description:{
    fontSize:AppSizes.ResponsiveSize.Sizes(11),
    paddingBottom: AppSizes.ResponsiveSize.Sizes(3),
    color:'#9b9b9b',
    fontWeight:'500',
    fontFamily:Fonts.RobotoRegular,
  },
  pikbox:{
    marginTop:AppSizes.ResponsiveSize.Padding(2)
  },
  valueText: {
    fontSize: 18,
    marginBottom: 50,
  },
  container3: {
    height:AppSizes.screen.width/5,
    alignItems: 'center',
    justifyContent: 'flex-end',

    //backgroundColor:'red'
  },
  ImageStyle:{
    height:AppSizes.screen.height/10,
    width:AppSizes.screen.width/2,
    marginLeft:AppSizes.ResponsiveSize.Padding(2)
  },
  methodbox:{
    marginTop:10,
    backgroundColor:'#eee',
    alignItems:'center',
    borderWidth: 1,
    borderColor: '#000',
    borderRadius: 5 ,
  },
  SectionRadioView: {
    borderBottomWidth:1,
    borderBottomColor:'#ffffff',
    borderRadius:5,
    width:'100%',
    marginBottom: (Platform.OS === 'ios') ? AppSizes.ResponsiveSize.Padding(1) :AppSizes.ResponsiveSize.Padding(2)
  },
    timePicker:{
      flexDirection:'row',
      alignItems:'center',
      justifyContent:'center',
      backgroundColor:'#eee',
      height:40,
      borderWidth:1,
      borderColor:'#bdc3c7',
      marginTop:AppSizes.ResponsiveSize.Padding(2),
      marginBottom:AppSizes.ResponsiveSize.Padding(2),
      position:'relative'
    },
    inputtext:{marginBottom:10,
      marginTop:AppSizes.ResponsiveSize.Padding(3),
      fontSize:AppSizes.ResponsiveSize.Sizes(14),
      fontWeight:'600',
      marginBottom:AppSizes.ResponsiveSize.Padding(1),
      fontFamily:Fonts.RobotoRegular,
    },
    smaions:{
      height:'50%',
      width:'7.5%',
      position:'absolute',
      right:10
    },
  field: {
    width: 300,
    color: '#449aeb',
    borderColor: '#000000',
    borderWidth: 1,
    borderRadius: 5,
    backgroundColor: '#FFFFFF',
    overflow: 'hidden',
  },
  checkboxSection :{
    marginBottom: AppSizes.ResponsiveSize.Padding(2),
    height:AppSizes.screen.width/15,
  },
  tccontent:{
    color:AppColors.contentColor,
    textAlign:'center',
    fontSize:AppSizes.ResponsiveSize.Sizes(13),
    lineHeight: AppSizes.ResponsiveSize.Sizes(13 * 1.70),
    fontWeight:'300',
    fontFamily:Fonts.RobotoRegular,
 },
 textcontent:{
   color:AppColors.contentColor,
   fontSize:AppSizes.ResponsiveSize.Sizes(13),
   lineHeight: AppSizes.ResponsiveSize.Sizes(13 * 1.70),
   fontWeight:'300',
   fontFamily:Fonts.RobotoRegular,
 }
});
