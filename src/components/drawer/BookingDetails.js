import React, { Component } from 'react';
import {StyleSheet,Text,View,Image,TouchableOpacity,Platform,Alert,ScrollView,TextInput,Dimensions} from 'react-native';
import { AppStyles, AppSizes, AppColors } from '../../themes/'
import { NavigationActions } from 'react-navigation'
import Spacer from '../common/Spacer'
import * as commonFunctions from '../../utils/CommonFunctions'
import Header from '../common/HeaderWithBack'
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view'
import CommonButton from '../common/CommonButton'
import MapView, {ProviderPropType} from 'react-native-maps';
import Geocoder from 'react-native-geocoder';
import PolyLine from '@mapbox/polyline';
import call from 'react-native-phone-call';
import email from 'react-native-email';
import { Fonts } from '../../utils/Fonts';

const phone = require('../../themes/Images/phone.png')
const email_icon=require('../../themes/Images/email_icon.png')

const { width, height } = Dimensions.get('window');
const ASPECT_RATIO = width / height;
const LATITUDE = 37.78825;
const LONGITUDE = -122.4324;
const LATITUDE_DELTA = 0.29;
const LONGITUDE_DELTA = LATITUDE_DELTA * ASPECT_RATIO;
const SPACE = 0.01;
const LAT= 30.7279571
const LANG= 76.8553046

var Constant = require('../../api/ApiRules').Constant;
var WebServices = require('../../api/ApiRules').WebServices;

export default class BookingDetails extends Component {
  constructor (props) {
      super(props)
      this.state = {
        isVisible: false,
        item:[],

        isMapReady: false,
        region: {
          latitude: LATITUDE,
          longitude: LONGITUDE,
          latitudeDelta: LATITUDE_DELTA,
          longitudeDelta: LONGITUDE_DELTA
        },
        concat: null,
        latitude: null,
        longitude: null,
        error: null,
        concat: null,
        coords:[],
        x: 'false',

    },
    _that = this;
  }

  componentWillMount(){
    var item=this.props.navigation.state.params.item
      _that.setState({
        item:item,
        lat:Number(item.latitude),
        lgt:Number(item.longitude),
        cordLatitude:Number(item.latitude),
        cordLongitude:Number(item.longitude),
      })

    try {
    Geocoder.fallbackToGoogle('AIzaSyBX6rKXe6Jsk6ZynShEZiNfDfyhZWgmXsQ');
     navigator.geolocation.getCurrentPosition(
      (position) => {
        var region = {
          lat: position.coords.latitude,
          lng:  position.coords.longitude,
        };
        //_that.validationAndApiParameter('ServiceList',uid,region.lat,region.lng)
        this.setState({
          region: {
            latitude: position.coords.latitude,
            longitude: position.coords.longitude,
            latitudeDelta: LATITUDE_DELTA,
            longitudeDelta: LONGITUDE_DELTA,
            accuracy: position.coords.accuracy
          }
        });
        this.setState({
           latitude: position.coords.latitude,
           longitude: position.coords.longitude,
           error: null,
         });
        this.mergeLot();
        Geocoder.geocodePosition(region).then(res => {
          //console.log(res)
          var address=res[0].formattedAddress
          _that.setState({
            latitude: position.coords.latitude,
            longitude: position.coords.longitude,
            address: address,
          });
        })
        this.watchID = navigator.geolocation.watchPosition((position) => {
          const newRegion = {
            latitude: position.coords.latitude,
            longitude: position.coords.longitude,
            latitudeDelta: LATITUDE_DELTA,
            longitudeDelta: LONGITUDE_DELTA,
            accuracy: position.coords.accuracy
          }
          this.setState({newRegion});
        })
      },
      (error) => console.log('error '+error.message),
      {enableHighAccuracy: false, timeout: 50000, maximumAge: 10000}
    );
    }
    catch(err) {
        console.log(err);
    }
  }

  async getDirections(startLoc, destinationLoc) {
   try {
       let resp = await fetch(`https://maps.googleapis.com/maps/api/directions/json?origin=${ startLoc }&destination=${ destinationLoc }&key=AIzaSyBX6rKXe6Jsk6ZynShEZiNfDfyhZWgmXsQ`)
       console.log(resp);
       let respJson = await resp.json();
       let points = PolyLine.decode(respJson.routes[0].overview_polyline.points);
       let coords = points.map((point, index) => {
           return  {
               latitude : point[0],
               longitude : point[1]
           }
       })
       this.setState({coords: coords})
       this.setState({x: "true"})
       return coords
     }
     catch(error) {
       console.log('error',error)
       this.setState({x: "error"})
       return error
     }
  }
  mergeLot(){
   if (this.state.latitude != null && this.state.longitude!=null)
    {
      let concatLot = this.state.latitude +","+this.state.longitude
      let destination=this.state.lat +","+this.state.lgt
      console.log(concatLot);
      console.log(destination);
      this.setState({
        concat: concatLot
      }, () => {
        this.getDirections(concatLot,destination);
      });
    }
  }

  Call(){
    const args = {
      number: this.state.item.telephone,
      prompt: false,
    };
    call(args).catch(console.error);
  }

  render() {
    const headerProp = {
      title: 'Booking Details',
      screens: 'BookingDetails',
      type:''
    };
    return (
      <View style={styles.container}>
      <Header info={headerProp} navigation={_that.props.navigation}/>

        <View style={styles.content}>
            <View style={styles.mapcontainer}>
              <MapView style={styles.map}
                        region={this.state.region}
                        provider='google'
                        mapType='standard'
                        showsCompass={true}
                        showsPointsOfInterest >
                        {!!this.state.latitude && !!this.state.longitude &&
                          <MapView.Marker
                           pinColor={'#3995f7'}
                           coordinate={{"latitude":this.state.latitude,"longitude":this.state.longitude}}
                           title={"Your Location"}
                           showsUserLocation={true}
                         />}

                         {!!this.state.cordLatitude && !!this.state.cordLongitude &&
                           <MapView.Marker
                            pinColor={'#3995f7'}
                            coordinate={{"latitude":this.state.cordLatitude,"longitude":this.state.cordLongitude}}
                            title={this.state.name}
                            showsUserLocation={true}
                          />}

                         {!!this.state.latitude && !!this.state.longitude && this.state.x == 'true' &&
                         <MapView.Polyline
                              coordinates={this.state.coords}
                              strokeWidth={3}
                              lineCap={'round'}
                              lineJoin={'round'}
                              strokeColor="#3995f7"/>
                          }

                          {!!this.state.latitude && !!this.state.longitude && this.state.x == 'error' &&
                          <MapView.Polyline
                            coordinates={[
                                {latitude: this.state.latitude, longitude: this.state.longitude},
                                {latitude: this.state.cordLatitude, longitude: this.state.cordLongitude},
                            ]}
                            lineCap={'round'}
                            lineJoin={'round'}
                            strokeWidth={3}
                            strokeColor="#3995f7"/>
                           }
                </MapView>
            </View>

            <View style={styles.container2}>
                <View style={styles.SectionStyle}>
                    <View style={styles.inputfield}>
                        <Text style={styles.inputtext}>Name</Text>
                        <Text style={styles.smalltext}>{this.state.item.name}</Text>
                    </View>
                </View>
                <View style={styles.SectionStyle}>
                    <View style={styles.inputfield}>
                        <Text style={styles.inputtext}>Station Name</Text>
                        <Text style={styles.smalltext}>{this.state.item.station_name}</Text>
                    </View>
                </View>

                {this.state.item.address ?
                <View style={styles.SectionStyle}>
                    <View style={styles.inputfield}>
                        <Text style={styles.inputtext}>Address </Text>
                        <Text style={styles.smalltext}>{this.state.item.address}</Text>
                    </View>
                </View>
                : null }

                {this.state.item.telephone ?
                <View style={styles.SectionStyle}>
                    <View style={styles.inputfield}>
                        <Text style={styles.inputtext}>Contact Number </Text>
                        <Text style={styles.smalltext}>{this.state.item.telephone}</Text>
                    </View>
                </View>
                : null }

                {this.state.item.zip_code ?
                <View style={styles.SectionStyle}>
                    <View style={styles.inputfield}>
                        <Text style={styles.inputtext}>Zip Code</Text>
                        <Text style={styles.smalltext}>{this.state.item.zip_code}</Text>
                    </View>
                </View>
              : null }

              {this.state.item.country ?
                <View style={styles.SectionStyle}>
                    <View style={styles.inputfield}>
                        <Text style={styles.inputtext}>Country</Text>
                        <Text style={styles.smalltext}>{this.state.item.country}</Text>
                    </View>
                </View>
              : null }

            </View>
            <View style={styles.containerbtn}>
              <TouchableOpacity style={[styles.ebtn,styles.borderRight]} onPress={()=> this.Call()}>
                <Image style={styles.bicon} source={phone}/>
                <Text style={styles.textbtn}>Call</Text>
              </TouchableOpacity>
              <TouchableOpacity style={styles.ebtn} onPress={this.handleEmail}>
                <Image style={styles.bicon1} source={email_icon}/>
                <Text style={styles.textbtn}>Mail</Text>
              </TouchableOpacity>
            </View>
        </View>
      </View>
    );
  }

  handleEmail=()=>{
    const to = this.state.item.email
    email(to,{
      subject:'ElFill'
    }).catch(console.error)
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    paddingTop:  0,
    backgroundColor:'#fff'
  },
  content: {
      flex: 1,
      flexDirection: 'column',
      height:'100%',
      //backgroundColor:'red'
  },
  container2: {
    flex:1,
    flexDirection: 'column',
    padding:AppSizes.ResponsiveSize.Padding(3)
  },
  container3: {
    flex: 1,
    height:AppSizes.screen.width/8,
    alignItems: 'center',
    justifyContent: 'flex-end'
  },
  headerTitle:{
    fontSize:AppSizes.ResponsiveSize.Sizes(25),
    color:'#000000',
    fontWeight:'bold',
    fontFamily:Fonts.RobotoBold,
    paddingBottom:AppSizes.ResponsiveSize.Padding(1),
  },
  headersubTitle:{
    fontSize:AppSizes.ResponsiveSize.Sizes(14),
    color:'#000000',
    fontFamily:Fonts.RobotoBold,
    fontWeight:'600',
    paddingTop:AppSizes.ResponsiveSize.Padding(2),
  },
  headerBorder :{
    borderBottomColor: '#000000',
    borderBottomWidth: 3,
    width:'10%',
  },
  footertext :{
   color :'#000000',
   fontSize:AppSizes.ResponsiveSize.Sizes(12),
   fontWeight:'400',
   fontFamily:Fonts.RobotoRegular,
  },
  SectionStyle: {
    width:AppSizes.ResponsiveSize.width,

  },
  smalltext:{
    width:'50%',
    fontSize:AppSizes.ResponsiveSize.Sizes(13),
    color:'#333',
    fontWeight:'400',
    fontFamily:Fonts.RobotoRegular,
  },

  inputtext:{marginBottom:10,
    width:'50%',
    marginTop:AppSizes.ResponsiveSize.Padding(3),
    fontSize:AppSizes.ResponsiveSize.Sizes(16),
    fontWeight:'600',
    fontFamily:Fonts.RobotoBold,
    marginBottom:AppSizes.ResponsiveSize.Padding(1)
  },
  highlight:{
    backgroundColor:'green',
    padding:AppSizes.ResponsiveSize.Padding(1),
    paddingTop:AppSizes.ResponsiveSize.Padding(.10),
    paddingBottom:AppSizes.ResponsiveSize.Padding(.10),
    borderRadius:15,
    color:'#fff',
    flexDirection:'row'
  },
  highlightbox:{

    flexDirection:'row',
    flexWrap:'wrap',
  },
  inputfield:{
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
  },
  booktext:{
    fontSize:AppSizes.ResponsiveSize.Sizes(20),
    fontWeight:'600',
    color:'#000',
    fontFamily:Fonts.RobotoBold,
    marginBottom:AppSizes.ResponsiveSize.Padding(2)
  },
  bookbox:{
    width:'100%',
    borderColor:'#fff',
    alignItems:'center',
  },
  mapcontainer:{
    flex:1,
  },
  map: {
    height:'100%',
    width:'100%'
  },
  textbtn:{
    color:'#ffffff',
    fontSize:AppSizes.ResponsiveSize.Sizes(16),
    fontWeight:'500',
    fontFamily:Fonts.RobotoBold,
    textAlign:'center'
  },
  containerbtn:{
    flexDirection:'row',
    justifyContent:'space-between'
  },
  ebtn:{
    backgroundColor:'#3995f7',
    justifyContent: 'center',
    height:AppSizes.screen.width/8,
    borderWidth:1,
    borderColor:'#3995f7',
    flexDirection:'row',
    alignItems: 'center',
    justifyContent:'center',
    width:'50%'
  },
  borderRight:{
    borderWidth:0,
    borderRightWidth:1,
    borderColor:'#fff',
  },
  bicon:{
    width:AppSizes.screen.width/12,
    height:AppSizes.screen.height/26,
    marginRight:AppSizes.ResponsiveSize.Padding(1)
  },

  bicon1:{
    width:AppSizes.screen.width/12,
    height:AppSizes.screen.height/26,
    marginRight:AppSizes.ResponsiveSize.Padding(1)
  }

});
