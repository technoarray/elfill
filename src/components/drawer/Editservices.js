import React, { Component } from 'react';
import {AsyncStorage,StyleSheet,Text,View,Image,PixelRatio,TouchableOpacity,Platform,Alert,ScrollView,TextInput} from 'react-native';
import { AppStyles, AppSizes, AppColors } from '../../themes/'
import { NavigationActions } from 'react-navigation'
import Spinner from 'react-native-loading-spinner-overlay';
import ImagePicker from 'react-native-image-picker';
import Picker from 'react-native-q-picker';
import {RadioGroup, RadioButton} from 'react-native-flexi-radio-button';
import * as commonFunctions from '../../utils/CommonFunctions'
import Header from '../common/HeaderWithBack'
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view'
import CommonButton from '../common/CommonButton'
import CheckBox from 'react-native-check-box';
import { pickerData } from '../data/pickerData';
import { Fonts } from '../../utils/Fonts';
var Constant = require('../../api/ApiRules').Constant;
var WebServices = require('../../api/ApiRules').WebServices;

/* Images */
const bg_image = require('../../themes/Images/bg_image.png')
const left_arrow = require('../../themes/Images/left-arrow.png')
const profile_icon = require('../../themes/Images/profile.png')
const email_icon = require('../../themes/Images/email.png')
const password_icon = require('../../themes/Images/password.png')
const device_icon = require('../../themes/Images/car.png')
const modal_icon = require('../../themes/Images/modal.png')
const mobile_icon = require('../../themes/Images/mobile.png')

var numberchargeitems = [];

for( var i = 1; i <=20; i++){
  var numbers = {
      name: i+"",
      id: i
  };
  numberchargeitems.push(numbers);
}

let _that
export default class Editservices extends Component {
  constructor (props) {
      super(props)
      this.state = {
      isVisible: false,
      id:'',
      uid:'',
      status:'',
      stationitems:[],
      categoryitems:[],
      subcategoryitems:[],
      diviceType:[],
      bike_outlet:pickerData.bike_outlet,
      chargedata:pickerData.chargedata,
      accomodation:pickerData.accomodation,
      coffee:pickerData.coffee,
      car_outlet:pickerData.car_outlet,
      effect:pickerData.effect,
      phase:pickerData.phase,
      ampere:pickerData.ampere,
      selectedStation:'',
      selectedStationID:'',
      selectedSlots:'',
      selectedCategory:'',
      selectedCategoryID:'',
      selectedSubCategory:'',
      selectedSubCategoryID:'',
      selectedChargeType:'',
      selectedDeviceType:'',
      selectedDeviceID:'',
      selectedoutlet:'',
      selectedparkingFee:'',
      selectedaccomodation:'',
      selectedeffect:'',
      selectedphases:'',
      selectedampere:'',
      selecteddriver_licence_no:'',
      selectedupload_licence:'',
      selectedinsurance:'',
      numberchargeitems:numberchargeitems,
      selectedprice:'',
      selecteddescription:'',
      avatarSource:null,
      imageSource:null,
      avatarerr:'',
      stationError:'',
      slotsErros:'',
      categoryError:'',
      subCatError:'',
      chargeError:'',
      deviceTypeError:'',
      outletError:'',
      priceError:'',
      descriptionError:'',
    },
    _that = this;
  }
  componentWillMount(){
    AsyncStorage.getItem('UserData').then((UserData) => {
      var data = JSON.parse(UserData);
        uid=data.id
        if(uid != ''){
          _that.setState({
            uid:uid,
          })

        }
    })

    var item=this.props.navigation.state.params.item
    console.log(item);
    _that.validationAndApiParameter('stationList',item.uid)

    _that.validationAndApiParameter('subcategoryList',item.cat_id)
    _that.validationAndApiParameter('deviceList',item.subcat_id)
    _that.setState({
      id:item.id,
      imageSource:item.service_image,
      selectedDeviceID:item.device_id,
      selectedCategoryID:item.cat_id,
      selectedSubCategoryID:item.subcat_id,
      selectedOtherCategoryID:item.other_cat_id,
      selectedstation:item.station_name,
      selectedSlots:item.no_of_charging_spot,
      selectedCategory:item.cat_name,
      selectedSubCategory:item.subcat_name,
      selectedDeviceType:item.brand_name,
      selectedChargeType:item.charge_type,
      selectedoutlet:item.outlet_type,
      selectedparkingFee:item.parking_fee,
      selectedaccomodation:item.accomodation,
      selectedeffect:item.effect,
      selectedphases:item.phases,
      selectedampere:item.ampere,
      selecteddriver_licence_no:item.driver_licence_no,
      selectedupload_licence:item.upload_driver_licence,
      selectedinsurance:item.insurance,
      selectedprice:item.price,
      selecteddescription:item.description,
      status:item.status,
    })
  }

  onSelect(index, value){
    this.setState({status: value})
  }

  selectPhotoTapped() {
    const options = {
      quality: 1.0,
      maxWidth: 500,
      maxHeight: 500,
      storageOptions: {
        skipBackup: true,
      },
    };
      ImagePicker.showImagePicker(options, (response) => {
      console.log('Response = ', response);

      if (response.didCancel) {
        console.log('User cancelled image picker');
      } else if (response.error) {
        console.log('ImagePicker Error: ', response.error);
      } else if (response.customButton) {
        console.log('User tapped custom button: ', response.customButton);
      } else {
        const source = { uri: response.uri };

        this.setState({
          avatarSource: source,
          avatarerr:''
        });
      }
    });
   }

   onClick(val,label,key){
     if(key == 'category'){
       this.setState({selectedType:''})
       this.setState({length:false})
       this.setState({cat_id:val})
       this.setState({selectedChargeType:''})
       this.setState({selectedSubCategory:''})
       _that.validationAndApiParameter('subcategoryList',val);
     }
     else if (key == 'otherCategory') {
       var type=this.state.type_id
       //console.log(val);
       for (var i = 0; i < type.length; i++) {
         if(type[i].name==label){
           this.setState({selectedType:type[i].id})
           console.log(type[i].id);
         }
       }
       this.setState({selectedOtherCategory:label});
       this.setState({other_catid:val})
       this.setState({subCatError:''});
       this.setState({diviceType:[]});
       this.setState({selectedeffect:''})
       this.setState({selectedphase:''})
       this.setState({selectedampere:''})
       this.setState({selectedparkingFee:''})
       _that.validationAndApiParameter('otherdeviceList',val);
     }
     else if(key == 'subCat'){
       this.setState({selectedSubCategory:label});
       if(label=='Elcar'){
         _that.validationAndApiParameter('cardeviceList',val);
       }
       else{
         console.log('bye');
         _that.validationAndApiParameter('otherCategory',val);
       }
       this.setState({subcat_id:val})
       this.setState({othercat_id:val})
     }
     else if(key == 'charge'){
       this.setState({selectedChargeType: label});
       this.setState({chargeError:''})
     }
   }

   submit_click=() =>{
     const { selectedSlots,selectedOtherCategory,cat_id,subcat_id,station_id,selectedStation,selectedType,othercat_id,selectedCategory,reantperday,selectedSubCategory,selectedDeviceType,selectedChargeType,selectedoutlet,selectedparkingFee,accomodation_charges,selectedaccomodation,selecteddescription,selectedstatus,selectedeffect,selectedphase,selectedampere,selecteddriver_licence_no,selectedupload_licence,selectedinsurance,selectedprice,avatarSource,status,parkingLots,parkingPrice,parkingperday} = this.state
     var error=0;
       if(this.state.cat_id=='1'){
         if(selectedChargeType==''){
           console.log('error');
           this.setState({chargeError:'Field is required'})
           error=1;
         }
         if(selectedprice==''){
           console.log('error');
           this.setState({priceError:'Field is required'})
           error=1;
         }
         if(selectedoutlet==''){
           console.log('error');
           this.setState({outletError:'Field is required'})
           error=1;
         }
         if (selectedStation == '') {
           console.log('error');
           this.setState({stationError:'Field is required'});
           error=1;
         }
         if (selectedSlots == '') {
           console.log('error');
           this.setState({slotsError:'Field is required'})
           error=1;
         }
         if(selectedCategory==''){
           console.log('error');
           this.setState({categoryError:'Field is required'})
           error=1;
         }
         if(this.state.selectedSubCategory!='Elcar'){
           if(selectedOtherCategory==''){
             console.log('error');
             this.setState({otherCatError:'Field is required'})
             error=1;
           }
         }
         if(selectedDeviceType==''){
           console.log('error');
           this.setState({deviceTypeError:'Field is required'})
           error=1;
         }
       }
       else if(this.state.cat_id=='3'){
         if (selectedStation == '') {
           console.log('error');
           this.setState({stationError:'Field is required'});
           error=1;
         }
         if(selectedCategory==''){
           console.log('error');
           this.setState({categoryError:'Field is required'})
           error=1;
         }
         if(selectedSubCategory==''){
           console.log('error');
           this.setState({subCatError:'Field is required'})
           error=1;
         }
         if(selectedOtherCategory==''){
           console.log('error');
           this.setState({otherCatError:'Field is required'})
           error=1;
         }
       }
       else if(this.state.cat_id=='2'){
         if (selectedStation == '') {
           console.log('error');
           this.setState({stationError:'Field is required'});
           error=1;
         }
         if(selectedCategory==''){
           console.log('error');
           this.setState({categoryError:'Field is required'})
           error=1;
         }
         if(selectedSubCategory==''){
           console.log('error');
           this.setState({subCatError:'Field is required'})
           error=1;
         }
         if(selectedOtherCategory==''){
           console.log('error');
           this.setState({otherCatError:'Field is required'})
           error=1;
         }
         if(selectedoutlet==''){
           console.log('error');
           this.setState({outletError:'Field is required'})
           error=1;
         }
       }
       if(selecteddescription==''){
         console.log('error');
         this.setState({descriptionError:'Field is required'})
         error=1;
       }
       if (avatarSource===null) {
         console.log('error');
         _that.setState({avatarerr:'Please select photo'})
         error=1;
       }
         if(error==0){
           const data = new FormData();
          data.append('uid', this.state.uid);
          data.append('station_id',station_id);
          data.append('type_id',selectedType)
          data.append('cat_id',cat_id);
          data.append('subcat_id',subcat_id);
          data.append('other_cat_id',othercat_id)
          data.append('device_id',selectedDeviceType);
          data.append('charge_type',selectedChargeType);
          data.append('outlet_type',selectedoutlet);
          data.append('parking_fee',selectedparkingFee);
          data.append('accomodation',selectedaccomodation);
          data.append('accomodation_charges',accomodation_charges)
          data.append('effect',selectedeffect);
          data.append('phases',selectedphase);
          data.append('ampere',selectedampere);
          data.append('driver_licence_no',selecteddriver_licence_no);
          data.append('upload_driver_licence',selectedupload_licence);
          data.append('insurance',selectedinsurance);
          data.append('reantperday',reantperday)
          data.append('parkingLots',parkingLots);
          data.append('parkingPrice',parkingPrice);
          data.append('parkingperday',parkingperday);
          data.append('price',selectedprice);
          data.append('description', selecteddescription);
          data.append('service_image', {
            uri:  avatarSource.uri,
            type: 'image/jpeg',
            name: 'serviceImage.jpg'
          });
          data.append('no_of_charging_spot',selectedSlots);
          data.append('service_status', status);

          console.log('Data:',data);

          _that.setState({isVisible: true});
          this.postToApiCalling('POST', 'service_add', Constant.URL_serviceAdd, data);
         }
   }

   validationAndApiParameter(apikey,id) {
     if(apikey=='stationList'){
       const data = new FormData();
       data.append('uid',id);

       _that.setState({isVisible: true});
       this.postToApiCalling('POST',apikey, Constant.URL_stationList, data);
     }
     else if(apikey=='subcategoryList'){
       const data = new FormData();
       data.append('cat_id',id);

       _that.setState({isVisible: true});
       this.postToApiCalling('POST',apikey, Constant.URL_subCategoryList, data);
     }
     else if (apikey== 'cardeviceList') {
       const data = new FormData();
       data.append('subcat_id',id);
       data.append('other_catid',0)
       //console.log(data);
       _that.setState({isVisible: true});
       _that.postToApiCalling('POST',apikey, Constant.URL_devicesList,data);
     }
     else if (apikey== 'otherdeviceList') {
       const data = new FormData();
       data.append('subcat_id',this.state.subcat_id);
       data.append('other_catid',id)

       _that.setState({isVisible: true});
       _that.postToApiCalling('POST',apikey, Constant.URL_devicesList,data);
     }
     else if (apikey== 'otherCategory') {
       const data = new FormData();
       data.append('subcat_id',id);

       _that.setState({isVisible: true});
       _that.postToApiCalling('POST',apikey, Constant.URL_otherCategoryList,data);
     }
   }

     postToApiCalling(method, apiKey, apiUrl, data) {
       new Promise(function(resolve, reject) {
         if (method == 'POST') {
           resolve(WebServices.callWebService(apiUrl, data));
         } else {
           resolve(WebServices.callWebService_GET(apiUrl, data));
         }
       }).then((jsonRes) => {
         console.log(jsonRes);
         _that.setState({ isVisible: false })
         if ((!jsonRes) || (jsonRes.code == 0)) {
             setTimeout(()=>{
               Alert.alert('Login Error',jsonRes.message);
             },200);
         }
         else{
           if(jsonRes.code == 1){
             _that.apiSuccessfullResponse(apiKey, jsonRes)
           }
           else if (jsonRes.code == 2) {
             this.setState({sublength:false})
           }
         }
       }).catch((error) => {
           console.log("ERROR" + error);
           _that.setState({ isVisible: false })
           setTimeout(()=>{
               Alert.alert("Server issue"+error);
           },200);
       });
   }

   apiSuccessfullResponse(apiKey, jsonRes) {
       if (apiKey == 'stationList') {
         stationData=jsonRes.station_list;
         categoryData=jsonRes.category_list;
         if(stationData.length=='0'){
           this.setState({empty:true})
         }
         else{
           stationData.map(sdata=> this.state.stationitems.push({name:sdata.label,id:parseInt(sdata.value)}));
         }
         categoryData.map(cdata=> this.state.categoryitems.push({name:cdata.name,id:parseInt(cdata.id)}));
       }
       if(apiKey=='subcategoryList'){
         this.setState({subcategoryitems:[]})
         this.setState({type_id:[]})
         subCatData=jsonRes.result;
         if(subCatData != ''){
           subCatData.map(data => {this.state.subcategoryitems.push({name:data.name,id:parseInt(data.id)});this.setState({length:true})});
           subCatData.map(data => this.state.type_id.push({name:data.name,id:parseInt(data.type_id)}));
         }
       }
        if (apiKey == 'cardeviceList') {
         deviceData=jsonRes.result;
         console.log(deviceData);

         deviceData.map(ddata=> this.state.diviceType.push({name:ddata.name,id:parseInt(ddata.id)}));
       }
       if (apiKey == 'otherdeviceList') {
        deviceData=jsonRes.result;
        console.log(deviceData);

        deviceData.map(ddata=> this.state.diviceType.push({name:ddata.name,id:parseInt(ddata.id)}));
      }
       if(apiKey=='service_add'){
         servicelist=jsonRes.result
         _that.props.navigation.navigate('Serviceslistseller',{servicelist:servicelist});
       }
       if(apiKey=='otherCategory'){
         otherData=jsonRes.result;

         otherData.map(data => {this.state.otherCategory.push({name:data.name,id:parseInt(data.id)});this.setState({sublength:true})});
       }
   }

  onClick(val,label){
    if(label == 'category'){
      this.setState({selectedCategory:label});
      this.setState({selectedCategoryID:val});
      this.setState({categoryError:''});
      _that.validationAndApiParameter('subcategoryList',val);
    }
    else if (label == 'subCat') {
      this.setState({selectedSubCategory:label});
      this.setState({selectedSubCategoryID:val});
      this.setState({subCatError:''});
      if(this.state.selectedCategory=='Electric filling' || this.state.selectedSubCategory=='Laptop')
        _that.validationAndApiParameter('deviceList',val);
    }
  }

  validationAndApiParameter(apikey,id) {
    if(apikey=='stationList'){
      const data = new FormData();
      data.append('uid',id);
      console.log(data);

      _that.setState({isVisible: true});
      this.postToApiCalling('POST',apikey, Constant.URL_stationList, data);
    }
    else if(apikey=='subcategoryList'){
      const data = new FormData();
      data.append('cat_id',id);
      console.log(data);

      _that.setState({isVisible: true});
      this.postToApiCalling('POST',apikey, Constant.URL_subCategoryList, data);
    }
    else if (apikey== 'deviceList') {
      const data = new FormData();
      data.append('subcat_id',id);
      console.log(data);

      _that.setState({isVisible: true});
      _that.postToApiCalling('POST',apikey, Constant.URL_devicesList,data);
    }
  }

  postToApiCalling(method, apiKey, apiUrl, data) {
    new Promise(function(resolve, reject) {
      if (method == 'POST') {
        resolve(WebServices.callWebService(apiUrl, data));
      } else {
        resolve(WebServices.callWebService_GET(apiUrl, data));
      }
    }).then((jsonRes) => {
      _that.setState({ isVisible: false })
      if ((!jsonRes) || (jsonRes.code == 0)) {
          setTimeout(()=>{
            console.log(jsonRes);
            Alert.alert('Login Error',jsonRes.message);
          },200);
      }
      else{
        if (jsonRes.code == 1) {
          _that.apiSuccessfullResponse(apiKey, jsonRes)
        }
      }
    }).catch((error) => {
        console.log("ERROR" + error);
        _that.setState({ isVisible: false })
        setTimeout(()=>{
            Alert.alert("Server issue"+error);
        },200);
    });
  }

  apiSuccessfullResponse(apiKey, jsonRes) {
    if (apiKey == 'stationList') {
        stationData=jsonRes.station_list;
        categoryData=jsonRes.category_list;

        stationData.map(sdata=> this.state.stationitems.push({name:sdata.label,id:parseInt(sdata.value)}));
        categoryData.map(cdata=> this.state.categoryitems.push({name:cdata.name,id:parseInt(cdata.id)}));
    }
    else if(apiKey=='subcategoryList'){
      this.setState({subcategoryitems:[]});
      subCatData=jsonRes.result;
      subCatData.map(data => this.state.subcategoryitems.push({name:data.name,id:parseInt(data.id)}));
    }
    else if (apiKey == 'deviceList') {
      this.setState({deviceType:[]})
      deviceData=jsonRes.result;
      deviceData.map(ddata=> this.state.diviceType.push({name:ddata.name,id:parseInt(ddata.id)}));
    }
    else if(apiKey=='service_add'){
      servicelist=jsonRes.result
      _that.props.navigation.navigate('Serviceslistseller',{servicelist:servicelist});
    }
  }

  render() {
    const headerProp = {
      title: 'Edit Service',
      screens: 'Serviceslistseller',
      type:''
    };

    var image='';
    if(this.state.avatarSource !== null){
      image=this.state.avatarSource;
    }
    else if(this.state.imageSource !== null){
      image=Constant.image_path+this.state.imageSource;
    }
    return (
      <View style={styles.container}>
        <Header info={headerProp} navigation={_that.props.navigation}/>
        <View style={styles.content}>
          <KeyboardAwareScrollView innerRef={() => {return [this.refs.name,this.refs.email, this.refs.password]}} >
            <ScrollView>
              <View style={styles.container2}>

              <View style={{alignItems:'center'}}>
                <TouchableOpacity onPress={this.selectPhotoTapped.bind(this)} style={{height:120,width:120}}>
                <View style={[styles.avatar,  styles.avatarContainer,{ marginBottom: 20 },]}>
                  {this.state.avatarSource === null ? (
                    <Text>Select a Photo</Text>
                  ) : (
                    <Image style={styles.avatar} source={this.state.avatarSource} />
                  )}
                </View>
                </TouchableOpacity>
              </View>
              { !(this.state.avatarerr) ? null :
                <Text style={styles.error}>{this.state.avatarerr}</Text>
              }

              <View style={styles.SectionStyle}>
                  <View style={styles.inputfield}>
                      <Text style={styles.inputtext}>Service Station</Text>
                      <Picker PickerData={this.state.stationitems}
                       label={'Select service station'}
                       color={'#000000'}
                       selectedValue={this.state.selectedstation}
                       defaultLabel={this.state.selectedstation}
                       getTxt={(val,label)=>{this.setState({selectedStation: label});this.setState({selectedStationID:val}); this.setState({stationError:''})}}/>
                  </View>
              </View>
              <Text style={styles.error}>{this.state.stationError}</Text>

                <View style={styles.SectionStyle}>
                    <View style={styles.inputfield}>
                        <Text style={styles.inputtext}>Category</Text>
                        <Picker PickerData={this.state.categoryitems}
                         label={'Category'}
                         color={'#000000'}
                         selectedValue={this.state.selectedCategory}
                         defaultLabel={this.state.selectedCategory}
                         getTxt={(val,label)=> {this.onClick(val,'category')}}/>
                    </View>
                </View>
                <Text style={styles.error}>{this.state.categoryError}</Text>

                {this.state.subcategoryitems ?
                  <View style={styles.SectionStyle}>
                      <View style={styles.inputfield}>
                          <Text style={styles.inputtext}>Subcategory</Text>
                          <Picker PickerData={this.state.subcategoryitems}
                           label={'Select a subcategory'}
                           color={'#000000'}
                           selectedValue={this.state.selectedSubCategory}
                           defaultLabel={this.state.selectedSubCategory}
                           getTxt={(val,label)=> {this.onClick(val,'subCat')}}/>
                      </View>
                  </View>
                  :
                  <View style={styles.SectionStyle}>
                      <View style={styles.inputfield}>
                          <Text style={styles.inputtext}>Subcategory</Text>
                          <Text style={{color:'#bdc3c7',fontSize:15}}>Select a subcategory</Text>
                      </View>
                  </View>
                }
                <Text style={styles.error}>{this.state.subCatError}</Text>

                {this.state.selectedCategoryID == '1' && this.state.selectedSubCategoryID=='1'?
                <View>
                  <View style={styles.SectionStyle}>
                      <View style={styles.inputfield}>
                          <Text style={styles.inputtext}>Number of Charging Spot</Text>
                          <Picker PickerData={this.state.numberchargeitems}
                           label={'Charging Spot'}
                           color={'#000000'}
                           selectedValue={this.state.selectedSlots}
                           defaultLabel={this.state.selectedSlots}
                           getTxt={(val,label)=>{this.setState({selectedSlots: label});this.setState({slotsErros:''})}}/>
                      </View>
                  </View>
                  <Text style={styles.error}>{this.state.slotsErros}</Text>
                  <View style={styles.SectionStyle}>
                      <View style={styles.inputfield}>
                          <Text style={styles.inputtext}>Charge</Text>
                          <Picker PickerData={this.state.chargedata}
                           label={'Charge'}
                           color={'#000000'}
                           selectedValue={this.state.selectedChargeType}
                           defaultLabel={this.state.selectedChargeType}
                           getTxt={(val,label)=>{this.setState({selectedChargeType: label});this.setState({chargeError:''})}}/>
                      </View>
                  </View>
                  <View style={styles.SectionStyle}>
                      <View style={styles.inputfield}>
                          <Text style={styles.inputtext}>Device Type</Text>
                          <Picker PickerData={this.state.diviceType}
                           label={'Device Type'}
                           color={'#000000'}
                           selectedValue={this.state.selectedDeviceType}
                           defaultLabel={this.state.selectedDeviceType}
                           getTxt={(val,label)=>{this.setState({selectedDeviceType: label});this.setState({selectedDeviceID:val});this.setState({deviceTypeError:''})}}/>
                      </View>
                  </View>
                  <Text style={styles.error}>{this.state.deviceTypeError}</Text>
                  <View style={styles.SectionStyle}>
                      <View style={styles.inputfield}>
                          <Text style={styles.inputtext}>Regular Power Outlet</Text>
                          <Picker PickerData={this.state.car_outlet}
                           label={'Regular Power Outlet'}
                           color={'#000000'}
                           selectedValue={this.state.selectedoutlet}
                           defaultLabel={this.state.selectedoutlet}
                           getTxt={(val,label)=>{this.setState({selectedoutlet: label});this.setState({outletError:''})}}/>
                      </View>
                  </View>
                  <Text style={styles.error}>{this.state.outletError}</Text>

                </View>
                :
                  this.state.selectedCategoryID == '1' && this.state.selectedSubCategoryID=='2'?
                    <View>
                      <View style={styles.SectionStyle}>
                          <View style={styles.inputfield}>
                              <Text style={styles.inputtext}>Number of Charging Spot</Text>
                              <Picker PickerData={this.state.numberchargeitems}
                               label={'Charging Spot'}
                               color={'#000000'}
                               selectedValue={this.state.selectedSlots}
                               defaultLabel={this.state.selectedSlots}
                               getTxt={(val,label)=>{this.setState({selectedSlots: label});this.setState({slotsErros:''})}}/>
                          </View>
                      </View>
                      <Text style={styles.error}>{this.state.slotsErros}</Text>
                        <View style={styles.SectionStyle}>
                            <View style={styles.inputfield}>
                                <Text style={styles.inputtext}>Device Type</Text>
                                <Picker PickerData={this.state.diviceType}
                                 label={'Please select category'}
                                 color={'#000000'}
                                 selectedValue={this.state.selectedDeviceType}
                                 defaultLabel={this.state.selectedDeviceType}
                                 getTxt={(val,label)=>{this.setState({selectedDeviceType: label});this.setState({selectedDeviceID:val});this.setState({deviceTypeError:''})}}/>
                            </View>
                        </View>
                        <Text style={styles.error}>{this.state.deviceTypeError}</Text>
                        <View style={styles.SectionStyle}>
                            <View style={styles.inputfield}>
                                <Text style={styles.inputtext}>Regular Power Outlet</Text>
                                <Picker PickerData={this.state.bike_outlet}
                                 label={'Select option'}
                                 color={'#000000'}
                                 selectedValue={this.state.selectedoutlet}
                                 defaultLabel={this.state.selectedoutlet}
                                 getTxt={(val,label)=>{this.setState({selectedoutlet: label});this.setState({outletError:''})}}/>
                            </View>
                        </View>
                        <Text style={styles.error}>{this.state.outletError}</Text>
                    </View>
                  :
                    this.state.selectedCategoryID == '1' && this.state.selectedSubCategoryID=='5'?
                      <View>
                          <View style={styles.SectionStyle}>
                              <View style={styles.inputfield}>
                                  <Text style={styles.inputtext}>Number of Charging Spot</Text>
                                  <Picker PickerData={this.state.numberchargeitems}
                                   label={'Charging Spot'}
                                   color={'#000000'}
                                   selectedValue={this.state.selectedSlots}
                                   defaultLabel={this.state.selectedSlots}
                                   getTxt={(val,label)=>{this.setState({selectedSlots: label});this.setState({slotsErros:''})}}/>
                              </View>
                          </View>
                          <Text style={styles.error}>{this.state.slotsErros}</Text>
                          <View style={styles.SectionStyle}>
                              <View style={styles.inputfield}>
                                  <Text style={styles.inputtext}>Device Type</Text>
                                  <Picker PickerData={this.state.diviceType}
                                   label={'Please select category'}
                                   color={'#000000'}
                                   selectedValue={this.state.selectedDeviceType}
                                   defaultLabel={this.state.selectedDeviceType}
                                   getTxt={(val,label)=>{this.setState({selectedDeviceType: val});this.setState({deviceTypeError:''})}}/>
                              </View>
                          </View>
                          <Text style={styles.error}>{this.state.deviceTypeError}</Text>
                          <View style={styles.SectionStyle}>
                              <View style={styles.inputfield}>
                                  <Text style={styles.inputtext}>Regular Power Outlet</Text>
                                  <Picker PickerData={this.state.bike_outlet}
                                   label={'Select option'}
                                   color={'#000000'}
                                   selectedValue={this.state.selectedoutlet}
                                   defaultLabel={this.state.selectedoutlet}
                                   getTxt={(val,label)=>{this.setState({selectedoutlet: label});this.setState({outletError:''})}}/>
                              </View>
                          </View>
                          <Text style={styles.error}>{this.state.outletError}</Text>
                      </View>
                    :
                      null
                }

                {this.state.selectedCategoryID == '2'?
                  <View>
                    <View style={styles.SectionStyle}>
                        <View style={styles.inputfield}>
                            <Text style={styles.inputtext}>Device Type</Text>
                            <Picker PickerData={this.state.diviceType}
                             label={'Device Type'}
                             color={'#000000'}
                             selectedValue={this.state.selectedDeviceType}
                             defaultLabel={this.state.selectedDeviceType}
                             getTxt={(val,label)=>{this.setState({selectedDeviceType: val});this.setState({deviceTypeError:''})}}/>
                        </View>
                    </View>
                    <Text style={styles.error}>{this.state.deviceTypeError}</Text>
                    <View style={styles.SectionStyle}>
                        <View style={styles.inputfield}>
                            <Text style={styles.inputtext}>Rent per day</Text>
                            <TextInput
                              autoCorrect={false}
                              underlineColorAndroid="transparent"
                              selectionColor={"#000000"}
                              autoFocus={ false}
                              ref="reantperday"
                              placeholder="Enter parking fee per lot"
                              placeholderTextColor="#808080"
                              style={styles.textInput}
                              keyboardType={ 'number-pad'}
                              onChangeText={reantperday => this.setState({reantperday})}
                            />
                        </View>
                    </View>
                  </View>
                :
                  null
                }

                {this.state.selectedCategoryId == '3'?
                  <View>
                    <View style={styles.SectionStyle}>
                        <View style={styles.inputfield}>
                            <Text style={styles.inputtext}>Number of Parking Lots</Text>
                            <Picker PickerData={this.state.numberchargeitems}
                             label={'Parking Lots'}
                             color={'#000000'}
                             selectedValue={this.state.selectedSlots}
                             defaultLabel={this.state.selectedSlots}
                             getTxt={(val,label)=>{this.setState({parkingLots: label})}}/>
                        </View>
                    </View>
                    <View style={styles.SectionStyle}>
                        <View style={styles.inputfield}>
                            <Text style={styles.inputtext}>Parking fee per lot</Text>
                            <TextInput
                              autoCorrect={false}
                              underlineColorAndroid="transparent"
                              selectionColor={"#000000"}
                              autoFocus={ false}
                              ref="parkingPrice"
                              placeholder="Enter parking fee per lot"
                              placeholderTextColor="#808080"
                              style={styles.textInput}
                              keyboardType={ 'number-pad'}
                              onChangeText={parkingPrice => this.setState({parkingPrice})}
                            />
                        </View>
                    </View>
                    <View style={styles.SectionStyle}>
                        <View style={styles.inputfield}>
                            <Text style={styles.inputtext}>Parking fee per day</Text>
                            <TextInput
                              autoCorrect={false}
                              underlineColorAndroid="transparent"
                              selectionColor={"#000000"}
                              autoFocus={ false}
                              ref="parkingperday"
                              placeholder="Enter parking fee per day"
                              placeholderTextColor="#808080"
                              style={styles.textInput}
                              keyboardType={ 'number-pad'}
                              onChangeText={parkingperday => this.setState({parkingperday})}
                            />
                        </View>
                    </View>
                  </View>
                :
                  null
                }

                {this.state.selectedChargeType =='Fast charge' ?
                  <View>
                    <View style={styles.SectionStyle}>
                        <View style={styles.inputfield}>
                            <Text style={styles.inputtext}>Effect(KW)</Text>
                            <Picker PickerData={this.state.effect}
                             label={'Effect(KW)'}
                             color={'#000000'}
                             selectedValue={this.state.selectedeffect}
                             defaultLabel={this.state.selectedeffect}
                             getTxt={(val,label)=>this.setState({selectedeffect: label})}/>
                        </View>
                    </View>
                    <View style={styles.SectionStyle}>
                        <View style={styles.inputfield}>
                            <Text style={styles.inputtext}>Phases</Text>
                            <Picker PickerData={this.state.phases}
                             label={'Phases'}
                             color={'#000000'}
                             selectedValue={this.state.selectedphases}
                             defaultLabel={this.state.selectedphases}
                             getTxt={(val,label)=>this.setState({selectedphase: label})}/>
                        </View>
                    </View>
                    <View style={styles.SectionStyle}>
                        <View style={styles.inputfield}>
                            <Text style={styles.inputtext}>Ampere</Text>
                            <Picker PickerData={this.state.ampere}
                             label={'Ampere'}
                             color={'#000000'}
                             selectedValue={this.state.selectedampere}
                             defaultLabel={this.state.selectedampere}
                             getTxt={(val,label)=>this.setState({selectedampere: label})}/>
                        </View>
                    </View>
                  </View>
                :
                  this.state.selectedChargeType =='Regular Power Outlet(Long Charge)' ?
                    <View>
                      <View style={styles.SectionStyle}>
                          <View style={styles.inputfield}>
                              <Text style={styles.inputtext}>Parking Fee</Text>
                              <TextInput
                                autoCorrect={false}
                                value={this.state.selectedparkingFee}
                                underlineColorAndroid="transparent"
                                selectionColor={"#000000"}
                                autoFocus={ false}
                                ref="selectedparkingFee"
                                placeholder="Parking Fee"
                                placeholderTextColor="#808080"
                                style={styles.textInput}
                                keyboardType={ 'number-pad'}
                                onChangeText={selectedparkingFee => this.setState({selectedparkingFee})}
                              />
                          </View>
                      </View>
                    </View>
                  :
                  null
                }

                {this.state.selectedCategory=='Rentals'?
                  <View>
                    <View style={styles.SectionStyle}>
                        <View style={styles.inputfield}>
                            <Text style={styles.inputtext}>Drivers Licence Number</Text>
                            <Picker PickerData={this.state.coffee}
                             label={'Please select '}
                             color={'#000000'}
                             selectedValue={this.state.selecteddriver_licence_no}
                             defaultLabel={this.state.selecteddriver_licence_no}
                             getTxt={(val,label) => this.setState({selecteddriver_licence_no:label})}/>
                        </View>
                    </View>
                    <View style={styles.SectionStyle}>
                        <View style={styles.inputfield}>
                            <Text style={styles.inputtext}>Upload copy of Drivers Licence</Text>
                            <Picker PickerData={this.state.coffee}
                             label={'Please select '}
                             selectedValue={this.state.selectedupload_licence}
                             defaultLabel={this.state.selectedupload_licence}
                             color={'#000000'}
                             getTxt={(val,label) => this.setState({selectedupload_licence:label})}/>
                        </View>
                    </View>
                    <View style={styles.SectionStyle}>
                        <View style={styles.inputfield}>
                            <Text style={styles.inputtext}>Insurance/Deposit</Text>
                            <Picker PickerData={this.state.coffee}
                             label={'Please select'}
                             color={'#000000'}
                             selectedValue={this.state.selectedinsurance}
                             defaultLabel={this.state.selectedinsurance}
                             getTxt={(val,label) => this.setState({selectedinsurance:label})}/>
                        </View>
                    </View>
                  </View>
                :
                  this.state.selectedCategory =='Electric filling' || this.state.selectedCategory=='Parking lots'?
                    <View>
                      <View style={styles.SectionStyle}>
                          <View style={styles.inputfield}>
                              <Text style={styles.inputtext}>Accomodation</Text>
                              <Picker PickerData={this.state.accomodation}
                               label={'Please select type'}
                               color={'#000000'}
                               selectedValue={this.state.selectedaccomodation}
                               defaultLabel={this.state.selectedaccomodation}
                               getTxt={(val,label) => this.setState({selectedaccomodation:label})}/>
                          </View>
                      </View>
                      <View style={styles.SectionStyle}>
                          <View style={styles.inputfield}>
                              <Text style={styles.inputtext}>Accomodation charges</Text>
                              <TextInput
                                autoCorrect={false}
                                underlineColorAndroid="transparent"
                                selectionColor={"#000000"}
                                autoFocus={ false}
                                ref="accomodation_charges"
                                placeholder="Accomodation charges"
                                placeholderTextColor="#808080"
                                style={styles.textInput}
                                keyboardType={ 'number-pad'}
                                onChangeText={accomodation_charges => this.setState({accomodation_charges})}
                              />
                          </View>
                      </View>
                    </View>
                  :
                   null
                }

                {this.state.selectedCategoryID=='1'?
                  <View style={styles.SectionStyle}>
                    <View style={styles.inputfield}>
                      <Text style={styles.inputtext}>Price</Text>
                      <TextInput
                        autoCapitalize={'none'}
                        autoCorrect={false}
                        value={this.state.selectedprice}
                        underlineColorAndroid="transparent"
                        returnKeyType={ "next"}
                        selectionColor={"#000000"}
                        autoFocus={ false}
                        placeholder="Price"
                        placeholderTextColor="#808080"
                        style={styles.textInput}
                        ref="selectedprice"
                        keyboardType={ 'number-pad'}
                        onFocus={ () => this.setState({priceError:''}) }
                        onChangeText={selectedprice=> this.setState({selectedprice})}
                      />
                    </View>
                    <Text style={styles.error}>{this.state.priceError}</Text>
                  </View>
                :
                  null
                }

                <View style={styles.SectionStyle}>
                    <View style={styles.inputfield}>
                        <Text style={styles.inputtext}>Description</Text>
                        <TextInput
                          autoCapitalize={'none'}
                          autoCorrect={false}
                          value={this.state.selecteddescription}
                          multiline={true}
                          numberOfLines={4}
                          underlineColorAndroid="transparent"
                          returnKeyType={ "next"}
                          selectionColor={"#000000"}
                          autoFocus={ false}
                          placeholder="Description"
                          placeholderTextColor="#808080"
                          style={[styles.textInput,{height:80}]}
                          ref="selecteddescription"
                          keyboardType={ 'default'}
                          onFocus={ () => this.setState({descriptionError:''}) }
                          onChangeText={selecteddescription=> this.setState({selecteddescription})}
                        />
                    </View>
                    <Text style={styles.error}>{this.state.descriptionError}</Text>
                </View>

                <View style={styles.SectionStyle}>
                  <View style={styles.inputfield}>
                    <Text style={styles.inputtext}>Status</Text>
                      <RadioGroup
                        color='#000000'
                        selectedIndex={1}
                        style={styles.SectionRadioGroup}
                        onSelect = {(index, value) => this.onSelect(index, value)}
                      >
                        <RadioButton value={'0'}>
                          <Text style={{color:'#000000'}}>InActive</Text>
                        </RadioButton>

                        <RadioButton value={'1'}>
                          <Text style={{color:'#000000'}}>Active</Text>
                        </RadioButton>
                      </RadioGroup>
                    </View>
                </View>
              </View>
            </ScrollView>
          </KeyboardAwareScrollView>

          <TouchableOpacity onPress={this.submit_click} style={[styles.SectionStyle,{marginTop:AppSizes.ResponsiveSize.Padding(3)}]}>
            <CommonButton label='Save' width='100%'/>
          </TouchableOpacity>
        </View>
        <Spinner visible={this.state.isVisible}  />
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    paddingTop:  0,
  },
  content: {
      flex: 1,
      flexDirection: 'column',
      height:'70%',
      padding:'5%',
      backgroundColor:'#ffffff'
  },
  container2: {
    flex: 10,
    height:'70%',
    flexDirection: 'column',
  },
  container3: {
    flex: 3,
    height:AppSizes.screen.width/8,
    alignItems: 'center',
    justifyContent: 'flex-end',
  },
SectionStyle: {
  width:AppSizes.ResponsiveSize.width,
},
textInput: {
flex:1,
paddingLeft:AppSizes.ResponsiveSize.Padding(3),
fontSize: AppSizes.ResponsiveSize.Sizes(12),
color:'#000000',
backgroundColor:'#eee',
height:40,
fontFamily:Fonts.RobotoRegular,
},
inputfield:{
  width:AppSizes.ResponsiveSize.width,
},
inputtext:{marginBottom:10,
  marginTop:AppSizes.ResponsiveSize.Padding(3),
  fontSize:AppSizes.ResponsiveSize.Sizes(14),
  fontWeight:'600',
  fontFamily:Fonts.RobotoBold,
  marginBottom:10
},
SectionRadioGroup:{
  flexDirection:'row',
},
avatarContainer: {
   borderColor: '#9B9B9B',
   borderWidth: 1 / PixelRatio.get(),
   justifyContent: 'center',
   alignItems: 'center',
},
avatar: {
   borderRadius: 75,
   width: 120,
   height: 120,
}
});
