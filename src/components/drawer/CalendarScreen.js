import React, { Component } from 'react';
import {AsyncStorage,View,StyleSheet,TouchableOpacity,Text,ScrollView,Alert} from 'react-native';
import { Calendar, CalendarList, Agenda } from 'react-native-calendars';
import { AppStyles, AppSizes, AppColors } from '../../themes/'
import { NavigationActions } from 'react-navigation'
import Spinner from 'react-native-loading-spinner-overlay';
import Header from '../common/Header'
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view'
import LinearGradient from 'react-native-linear-gradient';
import { Fonts } from '../../utils/Fonts';
var Constant = require('../../api/ApiRules').Constant;
var WebServices = require('../../api/ApiRules').WebServices;

let _that
export default class Calendars extends Component {

  constructor(props){
    super(props)
    this.state = {
      dates:[],
      date:'',
      markedDates:null,
      pressed:false,
      dataSource:[],
      uid:''
    },
    _that=this
  }

  componentDidMount(){
    AsyncStorage.getItem('UserData').then((UserData) => {
    var data = JSON.parse(UserData);
      uid=data.id
      if(uid != ''){
        _that.setState({uid:uid})
        _that.validationAndApiParameter('calenderlist',uid)
      }
    })
  }

  anotherFunc(nextDay){
    var obj = nextDay.reduce((c, v) => Object.assign(c, {[v]: {selected: true,marked: true}}), {});
    this.setState({ marked : obj});
  }

  handleDatePress(date){
    this.setState({date:date.dateString});
    this.setState({pressed:true})
    var date=date.dateString;
    _that.validationAndApiParameter('calenderDetails',date);
  }

  handleOnPress(){
    _that.props.navigation.navigate('CalendarDetail')
  }

  validationAndApiParameter(apiKey,date) {
    const { uid,isVisible } = this.state
       if(apiKey=='calenderDetails'){
         const data = new FormData();
         data.append('uid', uid);
         data.append('date',date);
          console.log(data);
         _that.setState({isVisible: true});

        _that.postToApiCalling('POST', apiKey, Constant.URL_calenderDetails, data);
      }
      else if(apiKey=='calenderlist'){
        const data = new FormData();
        data.append('uid', date);
         console.log(data);
        _that.setState({isVisible: true});

       _that.postToApiCalling('POST', apiKey, Constant.URL_calender, data);
      }
    }

    postToApiCalling(method, apiKey, apiUrl, data) {
       new Promise(function(resolve, reject) {
            if (method == 'POST') {
                resolve(WebServices.callWebService(apiUrl, data));
            } else {
                resolve(WebServices.callWebService_GET(apiUrl, data));
            }
        }).then((jsonRes) => {
          _that.setState({ isVisible: false })
            if ((!jsonRes) || (jsonRes.code == 0)) {
            setTimeout(()=>{
                Alert.alert('Error',jsonRes.message);
            },200);
            } else {
                _that.apiSuccessfullResponse(apiKey, jsonRes)
            }
        }).catch((error) => {
            console.log("ERROR" + error);
            _that.setState({ isVisible: false })

            // setTimeout(()=>{
            //     Alert.alert("Server issue");
            // },200);
        });
    }

    apiSuccessfullResponse(apiKey, jsonRes) {
        if (apiKey == 'calenderDetails') {
          console.log(jsonRes)
          if(jsonRes.code==1){
            details=jsonRes.result
            console.log(details)
            _that.setState({
               dataSource: details,
             });
          }
        }
        else if(apiKey =='calenderlist'){
          console.log(jsonRes.result);
          if(jsonRes.code==1){
            this.setState({dates:jsonRes.result})
            selectedDates=jsonRes.result
            this.anotherFunc(selectedDates)
          }
          else{
            selectedDates=[];
            this.anotherFunc(selectedDates)
          }
          //_that.props.navigation.navigate('Calendar',{selectedDates:selectedDates});
        }
    }

    viewBtn=(item)=>{
       _that.props.navigation.navigate('CalendarDetail',{item:item});
    }

  render(){
    const headerProp = {
      title: 'Calendar',
      screens: 'CalendarScreen',
        type:''
    };

    //console.log(this.state.datasource);
    return(
      <View style={styles.container}>
      <Header info={headerProp} navigation={_that.props.navigation}/>
        <Calendar
          onDayPress={(day) => {this.handleDatePress(day)}}
          monthFormat={'MM/yyyy'}
          hideExtraDays={true}
          disableMonthChange={true}
          markedDates={this.state.marked}
          theme={{
            backgroundColor: '#ffffff',
            calendarBackground: '#ffffff',
            textSectionTitleColor: '#3995f7',
            selectedDayBackgroundColor: '#3995f7',
            selectedDayTextColor: '#ffffff',
            todayTextColor: '#3995f7',
            dayTextColor: '#2d4150',
            textDisabledColor: '#d9e1e8',
            dotColor: '#00adf5',
            selectedDotColor: '#ffffff',
            arrowColor: '#3995f7',
            monthTextColor: '#3995f7',
            textDayFontFamily: 'monospace',
            textMonthFontFamily: 'monospace',
            textDayHeaderFontFamily: 'monospace',
            textMonthFontWeight: 'bold',
            textDayFontSize: 16,
            textMonthFontSize: 16,
            textDayHeaderFontSize: 16
          }}
          style={{
            borderWidth: 2,
            borderColor: '#3995f7',
          }}
        />
        {this.state.pressed?
          <View style={styles.content}>
          {this.state.dataSource && this.state.dataSource.length > 0 ?
            <ScrollView>
            {
              this.state.dataSource.map((item, index) => (
                <TouchableOpacity style={styles.container1} onPress={() => this.viewBtn(item)} activeOpacity={.6}>
                  <LinearGradient colors={['#fff','#fff','#fff','#fff']} style={[styles.linearGradient,styles.servicebox,styles.shadow]}>
                    <View  style={styles.titleblk}>
                      <View style={styles.titlesec}>
                        <Text style={styles.title}>{item.name}</Text>
                        <Text style={styles.price}>{item.order_time}</Text>
                      </View>
                        <View style={styles.titlesec1}>
                            <Text style={styles.stationname}>{item.station_name}</Text>
                            <Text style={styles.smalltext}>Category : {item.cat_name}</Text>
                            <Text style={styles.smalltext}>Sub Category : {item.subcat_name}</Text>
                            <Text style={styles.smalltext}>Price : ${item.price}</Text>
                        </View>
                    </View>
                  </LinearGradient>
                </TouchableOpacity>
              ))
            }

               </ScrollView>
               :
            <Text style={{textAlign:'center'}}>No Record Found</Text>
           }
          </View>
        :
          null
        }
  <Spinner visible={this.state.isVisible}  />
      </View>
    )
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    paddingTop:  0,
  },
  titleblk:{
    width:'100%',
    height:'100%',
    paddingLeft:13
  },

  content: {
    flex: 1,
    paddingLeft:'2%',
    paddingRight:'2%',
    paddingTop:'2%',
    backgroundColor:'#fff',
    //backgroundColor:'red'
  },

  servicebox:{
    flexDirection:'row',
    padding:AppSizes.ResponsiveSize.Sizes(15),
    borderRadius: 5,
    marginBottom:AppSizes.ResponsiveSize.Padding(2),
  },
  container1: {
    height:AppSizes.screen.height/5,
    flexDirection: 'column',
  //  backgroundColor:'gray'
  },
  servicebox:{
    flexDirection:'row',
    padding:AppSizes.ResponsiveSize.Sizes(15),
    borderRadius: 5,
    marginBottom:AppSizes.ResponsiveSize.Padding(2),
  },
titlesec:{
  flexDirection:'row'
},
price:{
  width:'30%',

  textAlign:'center',
  color:'#3995f7',
  fontSize:AppSizes.ResponsiveSize.Sizes(17),
  fontWeight:'600',
  marginTop:2,
  fontFamily:Fonts.RobotoBold,
},
stationname:{
  fontSize:AppSizes.ResponsiveSize.Sizes(13),
  color:'#000',
  fontWeight:'400',
  fontFamily:Fonts.RobotoRegular,
},
shadow:{
 borderWidth:1,
 borderRadius: 2,
 borderColor: '#999',
 justifyContent:'center',
 backgroundColor:'#fff',
 borderColor: '#ddd',
 borderBottomWidth: 1,
 shadowColor: '#999',
 shadowOffset: { width: 0, height: 2 },
 shadowOpacity: 0.8,
 shadowRadius: 2,
 elevation: 0,
 //backgroundColor:'red',

},
title:{
  fontSize:AppSizes.ResponsiveSize.Sizes(20),
  paddingBottom: AppSizes.ResponsiveSize.Sizes(2),
  color:'#3995f7',
  fontWeight:'800',
  width:'70%',
  fontFamily:Fonts.RobotoBlack,
},
smalltext:{
  fontSize:AppSizes.ResponsiveSize.Sizes(10),
  color:'#000',
  fontWeight:'400',
  fontFamily:Fonts.RobotoRegular,
},
})
