import React, { Component } from 'react';
import {StyleSheet,Text,View,Image,TouchableOpacity,Platform,Alert,ScrollView} from 'react-native';
import { AppStyles, AppSizes, AppColors } from '../../themes/'
import { NavigationActions } from 'react-navigation'
import Spacer from '../common/Spacer'
import * as commonFunctions from '../../utils/CommonFunctions'
import Header from '../common/HeaderWithBack'
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view'
import CommonButton from '../common/CommonButton'
import LinearGradient from 'react-native-linear-gradient';
import DatePicker from 'react-native-datepicker'
import {RadioGroup, RadioButton} from 'react-native-flexi-radio-button'
import Picker from 'react-native-q-picker';
import DateTimePicker from 'react-native-modal-datetime-picker';
import Spinner from 'react-native-loading-spinner-overlay';
import RNPaypal from 'react-native-paypal-lib';

var Constant = require('../../api/ApiRules').Constant;
var WebServices = require('../../api/ApiRules').WebServices;

/* Images */
const paypal = require('../../themes/Images/paypal.png')
var stripe= require( '../../themes/Images/stripe.png')
const clock= require('../../themes/Images/clock.png')
const calendar=require('../../themes/Images/calendar.png')

let _that
export default class PaymentMethod extends Component {
  static title = 'Custom Card'
  constructor (props) {
      super(props)
      this.state = {
        status:'',
        date:"Feb-02-2019",
        time:"11:30 AM",
        timestamp:'',
        isVisible: false,
        Modal_Visibility: false,
        show_buyer_section:false,
        show_seller_section:false,
        isDatePickerVisible:false,
        isTimePickerVisible:false,
        uid:this.props.navigation.state.params.data[0],
        seller_id:this.props.navigation.state.params.data[1],
        station_id:this.props.navigation.state.params.data[2],
        type_id:this.props.navigation.state.params.data[3],
        cat_id:this.props.navigation.state.params.data[4],
        subcat_id:this.props.navigation.state.params.data[5],
        selectedDeviceType:this.props.navigation.state.params.data[6],
        selectedCharge:this.props.navigation.state.params.data[7],
        outlet_type:this.props.navigation.state.params.data[8],
        parking_fee:this.props.navigation.state.params.data[9],
        reg_no:this.props.navigation.state.params.data[10],
        accomodation:this.props.navigation.state.params.data[11],
        coffee:this.props.navigation.state.params.data[12],
        effect:this.props.navigation.state.params.data[13],
        phases:this.props.navigation.state.params.data[14],
        ampere:this.props.navigation.state.params.data[15],
        private_facility:this.props.navigation.state.params.data[16],
        public_facility:this.props.navigation.state.params.data[17],
        power_bank:this.props.navigation.state.params.data[18],
        other_facility:this.props.navigation.state.params.data[19],
        dlnumber:this.props.navigation.state.params.data[20],
        upload_licence:this.props.navigation.state.params.data[21],
        insurance:this.props.navigation.state.params.data[22],
        price:this.props.navigation.state.params.data[23],
        description:this.props.navigation.state.params.data[24],
        numberofslots:this.props.navigation.state.params.data[25],
        selectedDevice:this.props.navigation.state.params.data[26],
        service_id:this.props.navigation.state.params.data[27],
      },
      _that = this;
  }

  componentDidMount(){
    if(this.state.type_id == '2' && this.state.cat_id != '2'){
      this.setState({selectedCharge:''})
      this.setState({effect:''})
      this.setState({phases:''})
      this.setState({ampere:''})
      this.setState({parking_fee:''})
      this.setState({accomodation:''})
      this.setState({reg_no:''})
      this.setState({coffee:''})
      this.setState({private_facility:''})
      this.setState({public_facility:''})
      this.setState({power_bank:''})
      this.setState({other_facility:''})
      this.setState({dlnumber:''})
      this.setState({upload_licence:''})
      this.setState({insurance:''})
    }
    else if (this.state.type_id=='1' && this.state.cat_id != '2') {
      this.setState({private_facility:''})
      this.setState({public_facility:''})
      this.setState({power_bank:''})
      this.setState({other_facility:''})
      this.setState({dlnumber:''})
      this.setState({upload_licence:''})
      this.setState({insurance:''})
      if(this.state.selectedCharge=='fast_charge'){
        this.setState({parking_fee:''})
        this.setState({accomodation:''})
        this.setState({reg_no:''})
        this.setState({coffee:''})
      }
      else if (this.state.selectedCharge=='regular_power') {
        this.setState({effect:''})
        this.setState({phases:''})
        this.setState({ampere:''})
      }
    }
    else if (this.state.type_id=='3' && this.state.cat_id != '2') {
      this.setState({selectedCharge:''})
      this.setState({effect:''})
      this.setState({phases:''})
      this.setState({ampere:''})
      this.setState({parking_fee:''})
      this.setState({accomodation:''})
      this.setState({reg_no:''})
      this.setState({coffee:''})
      this.setState({dlnumber:''})
      this.setState({upload_licence:''})
      this.setState({insurance:''})
    }
    else if (this.state.cat_id == '2') {
      if(this.state.type_id == '2'){
        this.setState({selectedCharge:''})
        this.setState({effect:''})
        this.setState({phases:''})
        this.setState({ampere:''})
        this.setState({parking_fee:''})
        this.setState({accomodation:''})
        this.setState({reg_no:''})
        this.setState({coffee:''})
        this.setState({private_facility:''})
        this.setState({public_facility:''})
        this.setState({power_bank:''})
        this.setState({other_facility:''})
      }
      else if (this.state.type_id=='1') {
        this.setState({private_facility:''})
        this.setState({public_facility:''})
        this.setState({power_bank:''})
        this.setState({other_facility:''})
      }
      else if (this.state.type_id=='3') {
        this.setState({selectedCharge:''})
        this.setState({effect:''})
        this.setState({phases:''})
        this.setState({ampere:''})
        this.setState({parking_fee:''})
        this.setState({accomodation:''})
        this.setState({reg_no:''})
        this.setState({coffee:''})
      }
    }
  }

  onSelect(index, value){
    console.log(this.state.seller_id);
    _that.setState({status: value})
    //console.log(_that.state.status);
  }

  submit_click(){
    // if(_that.state.status=='paypal'){
    //   RNPaypal.paymentRequest({
    //     clientId: 'ARyVQCytJyQ-zBEPzoja-74Qf41BlSoRaAEpgc_V1pB7kci_nWBh_WiWQnB_QHunZfEDojKeinF2XKVi',
    //     environment: RNPaypal.ENVIRONMENT.NO_NETWORK,
    //     intent: RNPaypal.INTENT.SALE,
    //     price:1,
    //     currency: 'USD',
    //     description: `Android testing`,
    //     acceptCreditCards: false
    //   }).then(response => {
    //       console.log(response);
    //     }).catch(err => {
    //       console.log(err.message)
    //     })
    // }
    _that.validationAndApiParameter('buyService')
  }

  validationAndApiParameter(apikey) {
    var error=0;
    if(this.state.status == ''){
      error=1;
      Alert.alert("Please Select a Payment Method");
    }
    if(error==0){
      if(apikey=='buyService'){
        const data = new FormData();
        data.append('uid',this.state.uid);//0
        data.append('seller_id',this.state.seller_id);//1
        data.append('station_id',this.state.station_id);//2
        data.append('type_id',this.state.type_id);//3
        data.append('cat_id',this.state.cat_id);//4
        data.append('subcat_id',this.state.subcat_id);//5
        data.append('device_id',this.state.selectedDeviceType);//6
        data.append('charge_type',this.state.selectedCharge);//7
        data.append('outlet_type',this.state.outlet_type);//8
        data.append('parking_fee',this.state.parking_fee);//9
        data.append('car_reg_no',this.state.reg_no);//10
        data.append('accomodation',this.state.accomodation);//11
        data.append('coffee',this.state.coffee);//12
        data.append('effect',this.state.effect);//13
        data.append('phases',this.state.phases);//14
        data.append('ampere',this.state.ampere);//15
        data.append('private_home_facility',this.state.private_facility);//16
        data.append('public_facility',this.state.public_facility);//17
        data.append('portable_power_bank',this.state.power_bank);//18
        data.append('other_facility',this.state.other_facility);//19
        data.append('driver_licence_no',this.state.dlnumber);//20
        data.append('upload_driver_licence',this.state.upload_licence);//21
        data.append('insurance',this.state.insurance);//22
        data.append('price',this.state.price);//23
        data.append('order_date',this.state.date);
        data.append('order_time',this.state.time);
        data.append('service_id',this.state.service_id)
        //console.log(data);

        _that.setState({isVisible: true});
        this.postToApiCalling('POST',apikey, Constant.URL_buyService, data);
      }
    }
  }

  postToApiCalling(method, apiKey, apiUrl, data) {
    new Promise(function(resolve, reject) {
      if (method == 'POST') {
        resolve(WebServices.callWebService(apiUrl, data));
      } else {
        resolve(WebServices.callWebService_GET(apiUrl, data));
      }
    }).then((jsonRes) => {
      console.log(jsonRes);
      _that.setState({ isVisible: false })
      if ((!jsonRes) || (jsonRes.code == 0)) {
          setTimeout(()=>{
            Alert.alert('Login Error',jsonRes.message);
          },200);
      }
      else{
        if (jsonRes.code == 1) {
          _that.apiSuccessfullResponse(apiKey, jsonRes)
        }
      }
    }).catch((error) => {
        console.log("ERROR" + error);
        _that.setState({ isVisible: false })
        setTimeout(()=>{
            Alert.alert("Server issue"+error);
        },200);
    });
  }

  apiSuccessfullResponse(apiKey, jsonRes) {
    if (apiKey == 'buyService') {
      stationData=jsonRes.result;
      console.log(stationData)
      _that.props.navigation.navigate('Thankyou',{station_id:this.state.station_id})
    }
  }

  servicesBtn=()=>{
     _that.props.navigation.navigate('Serviceslistseller');
  }
  stationBtn=()=>{
     _that.props.navigation.navigate('Stationlistseller');
  }

  _showDatePicker = () => this.setState({ isDatePickerVisible: true });

  _hideDatePicker = () => this.setState({ isDatePickerVisible: false });

  _handleDatePicked = date => {
    var str= date.toString();
    var re= str.split(" ");
    var val =[]
    val.push(re[1]);
    val.push(re[2])
    val.push(re[3])
    var str2= val.join("-")
    this.setState({date:str2})
    this._hideDatePicker();
  };

  _showTimePicker = () => this.setState({ isTimePickerVisible: true });

  _hideTimePicker = () => this.setState({ isTimePickerVisible: false });

  _handleTimePicked = (time) => {
    console.log(time);
    var str= time.toString();
    var re= str.split(" ");
    var str2=re[4]
    var str3=str2.slice(0,-3)
    var t=parseInt(str3)
    if(t>=12){
      this.setState({timestamp:'PM'})
    }
    else{
      this.setState({timestamp:'AM'})
    }
    this.setState({time:str3})
    this._hideTimePicker();
  };

  render() {
    const headerProp = {
      title: 'Dashboard',
      screens: 'SideMenu',
        type:''
    };

    const { loading, token, error, params, errorParams } = this.state

    return (
      <View style={styles.container}>
      <Header info={headerProp} navigation={_that.props.navigation}/>

            <View style={styles.content}>
                <ScrollView contentContainerStyle={styles.contentContainer} showsVerticalScrollIndicator={false}>
                    <View style={styles.container1}>


                      <View  style={[styles.servicebox,styles.shadow]}>
                      <View style={{flexDirection:'row'}}>
                        <View style={{width:'80%'}}>
                            <Text style={styles.title}>Service</Text>
                        </View>
                        <TouchableOpacity onPress={() => _that.props.navigation.goBack(null)} style={{width:'20%',justifyContent:'center',alignItems:'center',backgroundColor:'#3995f7'}}>
                          <Text style={{color:'#fff'}}>Edit</Text>
                        </TouchableOpacity>
                      </View>
                      <View style={styles.border} />
                      {this.state.type_id=='1'?

                          <View style={styles.SectionStyle}>
                              <View style={styles.inputfield}>
                                  <Text style={styles.inputtext}>Charge</Text>
                                  <Text>{this.state.selectedCharge}</Text>
                              </View>
                          </View>

                      :
                        null
                      }

                      {this.state.selectedCharge=='fast_charge' ?
                        <View>
                          <View style={styles.SectionStyle}>
                              <View style={styles.inputfield}>
                                  <Text style={styles.inputtext}>Device Type</Text>
                                  <Text>{this.state.selectedDevice}</Text>
                              </View>
                          </View>
                          {this.state.selectedDeviceType?
                            <View style={styles.SectionStyle}>
                                <View style={styles.inputfield}>
                                    <Text style={styles.inputtext}>Regular Power Outlet</Text>
                                    <Text>{this.state.outlet_type}</Text>
                                </View>
                                <View style={styles.inputfield}>
                                    <Text style={styles.inputtext}>Effect</Text>
                                    <Text>{this.state.effect}</Text>
                                </View>
                                <View style={styles.inputfield}>
                                    <Text style={styles.inputtext}>Phases</Text>
                                    <Text>{this.state.phases}</Text>
                                </View>
                                <View style={styles.inputfield}>
                                    <Text style={styles.inputtext}>Ampere</Text>
                                    <Text>{this.state.ampere}</Text>
                                </View>
                                <View style={styles.inputfield}>
                                    <Text style={styles.inputtext}>Number of Charging Slots</Text>
                                    <Text>{this.state.numberofslots}</Text>
                                </View>
                                <View style={styles.inputfield}>
                                    <Text style={styles.inputtext}>Price</Text>
                                    <Text>{this.state.price}</Text>
                                </View>
                                <View style={styles.inputfield}>
                                    <Text style={styles.inputtext}>Description</Text>
                                    <Text>{this.state.description}</Text>
                                </View>
                            </View>
                          :
                          null
                        }
                       </View>
                      :
                      this.state.selectedCharge=='regular_power' ?
                        <View>
                          <View style={styles.SectionStyle}>
                              <View style={styles.inputfield}>
                                  <Text style={styles.inputtext}>Device Type</Text>
                                  <Text>{this.state.selectedDevice}</Text>
                              </View>
                          </View>
                          {this.state.selectedDeviceType?
                            <View style={styles.SectionStyle}>
                                <View style={styles.inputfield}>
                                    <Text style={styles.inputtext}>Regular Power Outlet</Text>
                                    <Text>{this.state.outlet_type}</Text>
                                </View>
                                {this.state.parking_fee != '' ?
                                  <View style={styles.inputfield}>
                                      <Text style={styles.inputtext}>Parking Fee</Text>
                                      <Text>{this.state.parking_fee}</Text>
                                  </View>
                                :
                                  null
                                }
                                {this.state.reg_no == 'yes' ?
                                  <View style={styles.inputfield}>
                                      <Text style={styles.inputtext}>Car Registration Number</Text>
                                      <Text>{this.state.reg_no}</Text>
                                  </View>
                                :
                                  null
                                }
                                {this.state.accomodation != ''?
                                  <View style={styles.inputfield}>
                                      <Text style={styles.inputtext}>Accomodation</Text>
                                      <Text>{this.state.accomodation}</Text>
                                  </View>
                                :
                                  null
                                }
                                {this.state.coffee == 'yes' ?
                                  <View style={styles.inputfield}>
                                      <Text style={styles.inputtext}>Coffee</Text>
                                      <Text>{this.state.coffee}</Text>
                                  </View>
                                :
                                  null
                                }
                                <View style={styles.inputfield}>
                                    <Text style={styles.inputtext}>Number of Charging Slots</Text>
                                    <Text>{this.state.numberofslots}</Text>
                                </View>
                                <View style={styles.inputfield}>
                                    <Text style={styles.inputtext}>Price</Text>
                                    <Text>{this.state.price}</Text>
                                </View>
                                <View style={styles.inputfield}>
                                    <Text style={styles.inputtext}>Description</Text>
                                    <Text>{this.state.description}</Text>
                                </View>
                            </View>
                          :
                          null
                        }
                       </View>
                       :
                       null
                      }

                      {this.state.type_id == '2'?
                      <View>
                        <View style={styles.SectionStyle}>
                            <View style={styles.inputfield}>
                                <Text style={styles.inputtext}>Device Type</Text>
                                <Text>{this.state.selectedDevice}</Text>
                            </View>
                        </View>
                        {this.state.selectedDeviceType?
                          <View style={styles.SectionStyle}>
                            <View style={styles.inputfield}>
                                <Text style={styles.inputtext}>Outlet Type</Text>
                                <Text>{this.state.outlet_type}</Text>
                            </View>
                            <View style={styles.inputfield}>
                                <Text style={styles.inputtext}>Number of Charging Slots</Text>
                                <Text>{this.state.numberofslots}</Text>
                            </View>
                            <View style={styles.inputfield}>
                                <Text style={styles.inputtext}>Price</Text>
                                <Text>{this.state.price}</Text>
                            </View>
                            <View style={styles.inputfield}>
                                <Text style={styles.inputtext}>Description</Text>
                                <Text>{this.state.description}</Text>
                            </View>
                          </View>
                        :
                        null
                        }
                      </View>
                    :
                        null
                      }

                      {this.state.type_id == '3'?
                        <View>
                          <View style={styles.SectionStyle}>
                              <View style={styles.inputfield}>
                                  <Text style={styles.inputtext}>Device Type</Text>
                                  <Text>{this.state.selectedDevice}</Text>
                              </View>
                          </View>
                          {this.state.selectedDeviceType?
                            <View style={styles.SectionStyle}>
                              <View style={styles.inputfield}>
                                  <Text style={styles.inputtext}>Outlet Type</Text>
                                  <Text>{this.state.outlet_type}</Text>
                              </View>
                              <View style={styles.inputfield}>
                                  <Text style={styles.inputtext}>Number of Charging Slots</Text>
                                  <Text>{this.state.numberofslots}</Text>
                              </View>
                              {this.state.private_facility == 'yes'?
                                <View style={styles.inputfield}>
                                    <Text style={styles.inputtext}>Private Home Facility</Text>
                                    <Text>{this.state.private_facility}</Text>
                                </View>
                              :
                                null
                              }
                              {this.state.public_facility == 'yes' ?
                                <View style={styles.inputfield}>
                                    <Text style={styles.inputtext}>Public Facility</Text>
                                    <Text>{this.state.public_facility}</Text>
                                </View>
                              :
                                null
                              }
                              {this.state.power_bank == 'yes'?
                                <View style={styles.inputfield}>
                                    <Text style={styles.inputtext}>Portable Power Bank</Text>
                                    <Text>{this.state.power_bank}</Text>
                                </View>
                              :
                                null
                              }
                              {this.state.other_facility!= '' ?
                                <View style={styles.inputfield}>
                                    <Text style={styles.inputtext}>Other Facility</Text>
                                    <Text>{this.state.other_facility}</Text>
                                </View>
                              :
                                null
                              }

                              <View style={styles.inputfield}>
                                  <Text style={styles.inputtext}>Price</Text>
                                  <Text>{this.state.price}</Text>
                              </View>
                              <View style={styles.inputfield}>
                                  <Text style={styles.inputtext}>Description</Text>
                                  <Text>{this.state.description}</Text>
                              </View>
                            </View>
                          :
                          null
                          }
                        </View>
                      :
                          null
                      }

                      {this.state.cat_id == '2'?
                        <View>
                          {this.state.dlnumber == 'yes' ?
                            <View style={styles.inputfield}>
                                <Text style={styles.inputtext}>Drivers Licence Number</Text>
                                <Text>{this.state.dlnumber}</Text>
                            </View>
                          :
                            null
                          }
                          {this.state.insurance == 'yes'?
                            <View style={styles.inputfield}>
                                <Text style={styles.inputtext}>Insurance/Deposit</Text>
                                <Text>{this.state.insurance}</Text>
                            </View>
                          :
                            null
                          }
                        </View>
                      :
                        null
                      }
                  </View>
                        <View style={[styles.servicebox,styles.shadow]}>
                          <Text style={styles.title}>
                              Pick Date
                          </Text>
                        <View style={styles.border} />
                        <TouchableOpacity style={styles.timePicker} onPress={this._showDatePicker}>
                          <Text>{this.state.date}</Text>
                          <Image style={styles.smaions} source={calendar}/>
                        </TouchableOpacity>
                          <DateTimePicker
                            isVisible={this.state.isDatePickerVisible}
                            onConfirm={this._handleDatePicked}
                            onCancel={this._hideDatePicker}
                          />
                            <Text style={styles.title}>Pick Time</Text>
                          <View style={styles.border} />
                                <TouchableOpacity style={styles.timePicker} onPress={this._showTimePicker}>
                                  <Text>{this.state.time} {this.state.timestamp}</Text>
                                  <Image style={styles.smaions} source={clock}/>
                                </TouchableOpacity>
                                <DateTimePicker
                                  mode={'time'}
                                  isVisible={this.state.isTimePickerVisible}
                                  onConfirm={this._handleTimePicked}
                                  onCancel={this._hideTimePicker}
                                  is24Hour={false}
                                />
                              </View>
                              <TouchableOpacity activeOpacity={.6}  style={[styles.servicebox,styles.shadow]}>
                                <Text style={styles.title}>
                                  Payment Method
                                </Text>
                                <View style={styles.border} />

                                <View style={styles.SectionRadioView}>
                                <RadioGroup
                                 color='#000'
                                 onSelect = {(index, value) => this.onSelect(index, value)}
                                >
                                  <RadioButton style={styles.methodbox} value={'paypal'}>
                                    <Image source={paypal} style={styles.ImageStyle} />
                                  </RadioButton>

                                  <RadioButton style={styles.methodbox} value={'stripe'}>
                                    <Image source={stripe} style={styles.ImageStyle} />
                                  </RadioButton>
                                </RadioGroup>

                            </View>
                        </TouchableOpacity>
                    </View>
                  </ScrollView>
                  <View style={styles.container3}>
                  <TouchableOpacity onPress={this.submit_click}>
                    <View style={[styles.SectionStyle,{marginTop:'3%',borderBottomColor: 'red',justifyContent:'flex-end'}]}>
                      <CommonButton label='Submit' width='100%'/>
                    </View>
                  </TouchableOpacity>
                  </View>
              </View>
              <Spinner visible={this.state.isVisible}  />
        </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    paddingTop:  0,
    backgroundColor:'#fff'
  },

  content: {
      flex: 1,
      padding:'5%',
      paddingBottom:0,
      //backgroundColor:'#000'
  },
  container1: {
    flex: 1,
    //backgroundColor:'red',
    flexDirection: 'column',
  },
  servicebox:{
    flexDirection:'column',
    padding:AppSizes.ResponsiveSize.Sizes(15),
    borderRadius: 5,
    marginBottom:AppSizes.ResponsiveSize.Padding(2),
    //backgroundColor:'red',
  },

    shadow:{
     borderWidth:1,
     borderRadius: 2,
     borderColor: '#fff',
     justifyContent:'center',
     backgroundColor:'#fff',
     borderColor: '#ddd',
     borderBottomWidth: 1,
     shadowColor: '#999',
     shadowOffset: { width: 0, height: 2 },
     shadowOpacity: 0.8,
     shadowRadius: 2,
     elevation: 0,
     //backgroundColor:'red',

    },
  contentContainer:{
    height:AppSizes.screen.height/.7
  },
  title:{
    fontSize:AppSizes.ResponsiveSize.Sizes(18),
    paddingBottom: AppSizes.ResponsiveSize.Sizes(5),
    color:'#333',
    fontWeight:'700'
  },
  border:{
    borderWidth:1.5,
    borderColor:'#333',
    width:30,
  },
  smalltitle:{
    fontSize:AppSizes.ResponsiveSize.Sizes(15),
    paddingBottom: AppSizes.ResponsiveSize.Sizes(3),
    color:'#333',
    fontWeight:'700',
    marginTop:AppSizes.ResponsiveSize.Padding(2)
  },
  description:{
    fontSize:AppSizes.ResponsiveSize.Sizes(11),
    paddingBottom: AppSizes.ResponsiveSize.Sizes(3),
    color:'#9b9b9b',
    fontWeight:'500',
  },
  pikbox:{
    marginTop:AppSizes.ResponsiveSize.Padding(2)
  },
  valueText: {
    fontSize: 18,
    marginBottom: 50,
  },
  container3: {
    height:AppSizes.screen.width/6,
    alignItems: 'center',
    justifyContent: 'flex-end',
    paddingBottom:AppSizes.ResponsiveSize.Padding(5),
    //backgroundColor:'red'
  },
  ImageStyle:{
    height:AppSizes.screen.height/10,
    width:AppSizes.screen.width/2,
    marginLeft:AppSizes.ResponsiveSize.Padding(2)
  },
  methodbox:{
    marginTop:10,
    backgroundColor:'#eee',
    alignItems:'center',
    borderWidth: 1,
    borderColor: '#000',
    borderRadius: 5 ,
  },
  SectionRadioView: {
    borderBottomWidth:1,
    borderBottomColor:'#ffffff',
    borderRadius:5,
    width:'100%',
    marginBottom: (Platform.OS === 'ios') ? AppSizes.ResponsiveSize.Padding(1) :AppSizes.ResponsiveSize.Padding(2)
  },
    timePicker:{
      flexDirection:'row',
      alignItems:'center',
      justifyContent:'center',
      backgroundColor:'#eee',
      height:40,
      borderWidth:1,
      borderColor:'#bdc3c7',
      marginTop:AppSizes.ResponsiveSize.Padding(2),
      marginBottom:AppSizes.ResponsiveSize.Padding(2),
      position:'relative'
    },
    inputtext:{marginBottom:10,
      marginTop:AppSizes.ResponsiveSize.Padding(3),
      fontSize:AppSizes.ResponsiveSize.Sizes(14),
      fontWeight:'600',
      marginBottom:AppSizes.ResponsiveSize.Padding(1)
    },
    smaions:{
      height:'50%',
      width:'7.5%',
      position:'absolute',
      right:10
    },
  field: {
    width: 300,
    color: '#449aeb',
    borderColor: '#000000',
    borderWidth: 1,
    borderRadius: 5,
    backgroundColor: '#FFFFFF',
    overflow: 'hidden',
  }
});
