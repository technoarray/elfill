import PropTypes from 'prop-types';
import React, {Component} from 'react';
import {AsyncStorage,StyleSheet,ScrollView, Text, View,TouchableOpacity, Image,Linking,Alert,ImageBackground} from 'react-native';
import {NavigationActions,StackActions} from 'react-navigation';
import { AppStyles, AppSizes, AppColors } from '../../themes/'
import * as commonFunctions from '../../utils/CommonFunctions'
import LinearGradient from 'react-native-linear-gradient';
import Spinner from 'react-native-loading-spinner-overlay';
import { Fonts } from '../../utils/Fonts';
var Constant = require('../../api/ApiRules').Constant;
var WebServices = require('../../api/ApiRules').WebServices;

var close_icon = require( '../../themes/Images/cross_512.png')
var home = require( '../../themes/Images/home.png')
var become =require('../../themes/Images/become.png')
var settings=require('../../themes/Images/settings.png')
var about=require('../../themes/Images/about.png')
var privacy=require('../../themes/Images/privacy.png')
var tc=require('../../themes/Images/tc.png')
var calendars=require('../../themes/Images/calender_sidebar.png')
var logout=require('../../themes/Images/logout.png')
var my_booking=require('../../themes/Images/my_booking.png')
var payments=require('../../themes/Images/payment_sidebar.png')
var services=require('../../themes/Images/charge_station.png')

let _that
class SideMenu extends Component {
  constructor(props) {
      super(props)
      this.state = {
        currentScreen: 'HomeScreen',
        user_type:'seller',
        uid:''
      };
      _that = this;
  }
  componentWillMount() {
    AsyncStorage.getItem('loggedIn').then((value) => {
        var loggedIn = JSON.parse(value);
        if (loggedIn) {
            AsyncStorage.getItem('UserData').then((UserData) => {
              var data = JSON.parse(UserData);
            //  console.log(data)
                _that.setState({
                  user_type: data.user_type,
                  uid: data.id
                });
            })
        }
    })
}

validationAndApiParameter() {
    const { method, uid, user_type, isVisible } = this.state
    const data = new FormData();
    data.append('uid', uid);
    data.append('user_type', user_type);
     console.log(data);
    _that.setState({isVisible: true});
    _that.postToApiCalling('POST', 'change_usertype', Constant.URL_Changetype, data);
  }

  postToApiCalling(method, apiKey, apiUrl, data) {

     new Promise(function(resolve, reject) {
          if (method == 'POST') {
              resolve(WebServices.callWebService(apiUrl, data));
          } else {
              resolve(WebServices.callWebService_GET(apiUrl, data));
          }
      }).then((jsonRes) => {
        _that.setState({ isVisible: false })
          if ((!jsonRes) || (jsonRes.code == 0)) {

          setTimeout(()=>{
              Alert.alert('Error',jsonRes.message);
          },200);

          } else {
              _that.apiSuccessfullResponse(apiKey, jsonRes)
          }
      }).catch((error) => {
          console.log("ERROR" + error);
          _that.setState({ isVisible: false })

          setTimeout(()=>{
              Alert.alert("Server issue");
          },200);
      });
  }


  apiSuccessfullResponse(apiKey, jsonRes) {
      if (apiKey == 'change_usertype') {
          jdata=jsonRes.data;
          console.log(jdata);
          AsyncStorage.setItem('UserData', JSON.stringify(jdata));
          AsyncStorage.setItem("user_type", JSON.stringify(jdata.user_type));
              const resetAction = StackActions.reset({
                    index: 0,
                    actions: [NavigationActions.navigate({ routeName: 'StartScreen' })],
                });
            _that.props.navigation.dispatch(resetAction);

      }
  }


  navigateToScreen = (route) => () => {
    if(route=="UserTypeScreen"){
        _that.validationAndApiParameter()
    }
    else{
      this.setState({currentScreen: route});
      const navigateAction = NavigationActions.navigate({
        routeName: route,
      });
      _that.props.navigation.dispatch(navigateAction);
    }
  }

  menu_close() {
    _that.props.navigation.closeDrawer();
  }



  render () {
//console.log(this.state.user_type)
    return (
      <View style={styles.container}>
        <TouchableOpacity onPress={this.menu_close}>
          <View style={{ marginTop:'15%', marginRight:'5%',alignSelf: 'flex-end',}}>
            <Image source={close_icon} style={{resizeMode: 'contain', width: 30, height: 30, }} />
          </View>
        </TouchableOpacity>

        <View style={{ margin:'2%',height:'5%'}}>
        {/*<Text style={[styles.navtitle,{textTransform:'uppercase',}]}>Hello, {this.state.user_email}</Text>*/}
        </View>

        <View style={styles.navSectionStyle}>
          {/*<View style={[styles.navBorderStyle,{borderBottomColor:'#76cadd',borderTopWidth:2, borderTopColor:'#76cadd'}]} >*/}
          {this.state.user_type=='buyer'?
            <View>
              <TouchableOpacity onPress={this.navigateToScreen('HomeScreen')} style={[styles.navBorderStyle,(this.state.currentScreen === 'HomeScreen' ? styles.navactive : {} )]}>
                  <View style={styles.sideicon}>
                      <Image source={home} style={styles.ImageStyle} />
                  </View>
                  <View style={styles.sidetext}>
                    <Text style={[styles.navItemStyle,(this.state.currentScreen === 'HomeScreen' ? styles.textactive : {} )]}>
                      Home
                      </Text>
                  </View>
                </TouchableOpacity>
            </View>
          :
            <View>
              <TouchableOpacity onPress={this.navigateToScreen('SellerDashboard')} style={[styles.navBorderStyle,(this.state.currentScreen === 'SellerDashboard' ? styles.navactive : {} )]}>
                <View style={styles.sideicon}>
                    <Image source={home} style={styles.ImageStyle} />
                </View>
                <View style={styles.sidetext}>
                  <Text style={[styles.navItemStyle,(this.state.currentScreen === 'SellerDashboard'  ? styles.textactive : {} )]}>
                    Home
                  </Text>
                </View>
              </TouchableOpacity>
            </View>
          }

          <View>
            <TouchableOpacity onPress={this.navigateToScreen('UserTypeScreen')} style={[styles.navBorderStyle,(this.state.currentScreen === 'UserTypeScreen' ? styles.navactive : {} )]}>
                <View style={styles.sideicon}>
                    <Image source={become} style={styles.ImageStyle} />
                </View>
                <View style={styles.sidetext}>
                  <Text style={[styles.navItemStyle,(this.state.currentScreen === 'UserTypeScreen' ? styles.textactive : {} )]} >
                    Become A {this.state.user_type=='buyer'?'Seller':'Buyer'}
                    </Text>
                </View>
              </TouchableOpacity>
          </View>

          {this.state.user_type=='buyer'?
            <View>
              <TouchableOpacity onPress={this.navigateToScreen('MyBookings')} style={[styles.navBorderStyle,(this.state.currentScreen === 'MyBookings' ? styles.navactive : {} )]}>
                  <View style={styles.sideicon}>
                      <Image source={my_booking} style={styles.ImageStyle} />
                  </View>
                  <View style={styles.sidetext}>
                    <Text style={[styles.navItemStyle,(this.state.currentScreen === 'MyBookings' ? styles.textactive : {} )]} >
                      My Bookings
                      </Text>
                  </View>
              </TouchableOpacity>
            </View>
          :
            <View>
              <TouchableOpacity onPress={this.navigateToScreen('PaymentsScreen')} style={[styles.navBorderStyle,(this.state.currentScreen === 'PaymentsScreen' ? styles.navactive : {} )]}>
                  <View style={styles.sideicon}>
                      <Image source={payments} style={styles.ImageStyle} />
                  </View>
                  <View style={styles.sidetext}>
                    <Text style={[styles.navItemStyle,(this.state.currentScreen === 'PaymentsScreen' ? styles.textactive : {} )]} >
                      Payments
                    </Text>
                  </View>
              </TouchableOpacity>
              <TouchableOpacity onPress={this.navigateToScreen('CalendarScreen')} style={[styles.navBorderStyle,(this.state.currentScreen === 'CalendarScreen' ? styles.navactive : {} )]}>
                  <View style={styles.sideicon}>
                      <Image source={calendars} style={styles.ImageStyle} />
                  </View>
                  <View style={styles.sidetext}>
                    <Text style={[styles.navItemStyle,(this.state.currentScreen === 'CalendarScreen' ? styles.textactive : {} )]} >
                      Calendar
                    </Text>
                  </View>
              </TouchableOpacity>
              <TouchableOpacity onPress={this.navigateToScreen('Serviceslistseller')} style={[styles.navBorderStyle,(this.state.currentScreen === 'Serviceslistseller' ? styles.navactive : {} )]}>
                  <View style={styles.sideicon}>
                      <Image source={services} style={styles.ImageStyle} />
                  </View>
                  <View style={styles.sidetext}>
                    <Text style={[styles.navItemStyle,(this.state.currentScreen === 'Serviceslistseller' ? styles.textactive : {} )]} >
                      Services
                    </Text>
                  </View>
              </TouchableOpacity>
            </View>
          }

          <View>
            <TouchableOpacity onPress={this.navigateToScreen('SettingScreen')} style={[styles.navBorderStyle,(this.state.currentScreen === 'SettingScreen' ? styles.navactive : {} )]}>
                <View style={styles.sideicon}>
                    <Image source={settings} style={styles.ImageStyle} />
                </View>
                <View style={styles.sidetext}>
                  <Text style={[styles.navItemStyle,(this.state.currentScreen === 'SettingScreen' ? styles.textactive : {} )]} >
                    Settings
                    </Text>
                </View>
            </TouchableOpacity>
          </View>

        <View>
          <TouchableOpacity onPress={this.navigateToScreen('AboutScreen')} style={[styles.navBorderStyle,(this.state.currentScreen === 'AboutScreen' ? styles.navactive : {} )]}>
            <View style={styles.sideicon}>
                <Image source={about} style={styles.ImageStyle} />
            </View>
            <View style={styles.sidetext}>
              <Text style={[styles.navItemStyle,(this.state.currentScreen === 'AboutScreen' ? styles.textactive : {} )]}>
                About Us
              </Text>
            </View>
          </TouchableOpacity>
        </View>

        <View>
          <TouchableOpacity onPress={this.navigateToScreen('PrivacyPolicyScreen')} style={[styles.navBorderStyle,(this.state.currentScreen === 'PrivacyPolicyScreen' ? styles.navactive : {} )]}>
            <View style={styles.sideicon}>
                <Image source={privacy} style={styles.ImageStyle} />
            </View>
            <View style={styles.sidetext}>
                <Text style={[styles.navItemStyle,(this.state.currentScreen === 'PrivacyPolicyScreen' ? styles.textactive : {} )]}>
                  Privacy Policy
                  </Text>
              </View>
          </TouchableOpacity>
        </View>

        <View>
          <TouchableOpacity onPress={this.navigateToScreen('TermConditionScreen')} style={[styles.navBorderStyle,(this.state.currentScreen === 'TermConditionScreen' ? styles.navactive : {} )]}>
            <View style={styles.sideicon}>
                <Image source={tc} style={styles.ImageStyle} />
            </View>
            <View style={styles.sidetext}>
                <Text style={[styles.navItemStyle,(this.state.currentScreen === 'TermConditionScreen' ? styles.textactive : {} )]}>
                  Terms & Condition
                  </Text>
              </View>
          </TouchableOpacity>
        </View>

        <View>
          <TouchableOpacity onPress={this.navigateToScreen('LogoutScreen')} style={[styles.navBorderStyle,(this.state.currentScreen === 'LogoutScreen' ? styles.navactive : {} )]}>
              <View style={styles.sideicon}>
                  <Image source={logout} style={styles.ImageStyle} />
              </View>
              <View style={styles.sidetext}>
                  <Text style={[styles.navItemStyle,(this.state.currentScreen === 'LogoutScreen' ? styles.textactive : {} )]} >
                  Logout
                  </Text>
              </View>
          </TouchableOpacity>
        </View>

      </View>
      <Spinner visible={this.state.isVisible}  />
            {/* </ImageBackground>
          </View>*/ }
      </View>
    );
  }
}

SideMenu.propTypes = {
  navigation: PropTypes.object
};


export default SideMenu;

const styles = StyleSheet.create({
  container: {
      flex:1,
    },
    imgBackground: {
        width: '100%',
        height: '100%',
        flex: 1
    },
    navactive:{
      backgroundColor:'#3995f7',

    },
    textactive:{
      color:'#ffffff',
      fontWeight:'bold',
      fontFamily:Fonts.RobotoBold,
    },
    navtitle:{
      textAlign:'center',
      fontSize:commonFunctions.getAdjustedFontSize(20,AppSizes.screen.width),
      fontWeight:'bold',
        fontFamily:Fonts.RobotoBold,
      color:'#000'
    },
    navItemStyle: {
      //marginBottom:10,
      fontSize:commonFunctions.getAdjustedFontSize(15,AppSizes.screen.width),
      //textTransform:'uppercase',
      color:'#000'
    },
    navBorderStyle: {
       padding:'4%',
       flexDirection:'row'
    },
    navSectionStyle: {
        marginTop:'3%',
    },
    sectionHeadingStyle: {
      paddingVertical: 10,
      paddingHorizontal: 5
    },

    sideicon:{
      width:'17%',
    },
    sidetext:{
      width:'80%',
      textAlign:'left',
      paddingTop:'2%'
    },
    ImageStyle: {
      height: AppSizes.screen.width/15,
      width: AppSizes.screen.width/15,
      resizeMode : 'contain',
    },


});
