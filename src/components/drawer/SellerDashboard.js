import React, { Component } from 'react';
import {AsyncStorage,StyleSheet,Text,View,Image,TouchableOpacity,Platform,Alert,ScrollView,TextInput} from 'react-native';
import { AppStyles, AppSizes, AppColors } from '../../themes/'
import { NavigationActions } from 'react-navigation'
import Spacer from '../common/Spacer'
import * as commonFunctions from '../../utils/CommonFunctions'
import Header from '../common/HeaderWithMenuandAdd'
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view'
import CommonButton from '../common/CommonButton'
import LinearGradient from 'react-native-linear-gradient';
import Spinner from 'react-native-loading-spinner-overlay';
import { Fonts } from '../../utils/Fonts';

var Constant = require('../../api/ApiRules').Constant;
var WebServices = require('../../api/ApiRules').WebServices;

/* Images */
const bg_image = require('../../themes/Images/bg_image.png')
var left= require( '../../themes/Images/right.png')
var del= require( '../../themes/Images/deletewhite.png')
var edit= require( '../../themes/Images/editwhite.png')
var no_image= require( '../../themes/Images/no_station.png')
var add=require('../../themes/Images/add.png')

let _that
export default class Stationlistseller extends Component {
  constructor (props) {
      super(props)
      this.state = {
      isVisible: false,
      uid:'',
      dataSource: [],
    },
    _that = this;
  }

 componentDidMount(){
   AsyncStorage.getItem('UserData').then((UserData) => {
     var data = JSON.parse(UserData);
       uid=data.id
       if(uid != ''){
         _that.setState({uid:uid})
         _that.validationAndApiParameter(uid,'stationlist')
       }
   })
 }

 componentWillMount() {
    this.subs = this.props.navigation.addListener("didFocus", () =>
    AsyncStorage.getItem('UserData').then((UserData) => {
      var data = JSON.parse(UserData);
        uid=data.id
        if(uid != ''){
          _that.setState({uid:uid})
          _that.validationAndApiParameter(uid,'stationlist')
        }
    })
    );
  }

 validationAndApiParameter(id,apiKey) {
   const { uid,isVisible } = this.state
      if(apiKey=='stationdelete'){
      const data = new FormData();
      data.append('id', id);
      data.append('uid', uid);
       //console.log(data);
      _that.setState({isVisible: true});

      _that.postToApiCalling('POST', apiKey, Constant.URL_serviceStationDelete, data);
    }
    else if(apiKey=='stationlist'){
      const data = new FormData();
      data.append('uid', id);
      console.log(data);

      _that.setState({isVisible: true});
     _that.postToApiCalling('POST', apiKey, Constant.URL_serviceStation, data);
   }
  }

   postToApiCalling(method, apiKey, apiUrl, data) {

      new Promise(function(resolve, reject) {
           if (method == 'POST') {
               resolve(WebServices.callWebService(apiUrl, data));
           } else {
               resolve(WebServices.callWebService_GET(apiUrl, data));
           }
       }).then((jsonRes) => {
         console.log(jsonRes);
         _that.setState({ isVisible: false })

           if ((!jsonRes) || (jsonRes.code == 0)) {

           setTimeout(()=>{
               Alert.alert('Error',jsonRes.message);
           },200);

           } else {
               _that.apiSuccessfullResponse(apiKey, jsonRes)
           }
       }).catch((error) => {
           console.log("ERROR" + error);
           _that.setState({ isVisible: false })

           setTimeout(()=>{
               Alert.alert("Server issue");
           },200);
       });
   }

   apiSuccessfullResponse(apiKey, jsonRes) {
       if (apiKey == 'stationdelete') {
           console.log('delete',jsonRes)
           stationlist=jsonRes.result
           _that.setState({
               //dataSource: ds.cloneWithRows(jsonRes.ResponsePacket),
              dataSource: stationlist,
            });
       }
       else if (apiKey == 'stationlist') {
         console.log('list',jsonRes)
         stationlist=jsonRes.result
         _that.setState({
            dataSource: stationlist,
          });
       }
   }

  viewBtn=(item)=>{
     _that.props.navigation.navigate('ViewStationScreen',{item:item});
  }
  addBtn=(item)=>{
     _that.props.navigation.navigate('AddserviceScreen',{item:item});
  }
  editBtn=(item)=>{
     _that.props.navigation.navigate('EditStationScreen',{item:item});
  }

  deleteBtn=(item)=>{
    console.log(item);
    var id=item.id
    Alert.alert(
      'Delete Service Station',
      'Are you sure you want to delete service station!',
      [
        {text: 'Cancel', onPress: () => console.log('Cancel Pressed'), style: 'cancel'},
        {text: 'OK', onPress: () => _that.validationAndApiParameter(id,'stationdelete')},
      ],
      { cancelable: false }
    )

  }


  render() {
    const headerProp = {
      title: 'Station lists',
      screens: 'Stationlistseller',
      type:'AddStationScreen'
    };

    return (
      <View style={styles.container}>
        <Header info={headerProp} navigation={_that.props.navigation}/>
        <ScrollView>
        <View style={styles.content}>
        {this.state.dataSource && this.state.dataSource.length > 0 ?

            <View style={styles.container1}>
              {this.state.dataSource.map((item, index) => (
                  <View  key={item.id} style={[styles.servicebox,styles.shadow]}>
                    <TouchableOpacity style={styles.stationimg} activeOpacity={.6} onPress={() => this.viewBtn(item)}>
                      {item.service_station_image != '' ?
                        <Image source={{uri: Constant.image_path+item.service_station_image}} style={styles.Image} />
                        :
                        <Image source={no_image} style={styles.Image} />
                      }
                    </TouchableOpacity>
                    <LinearGradient colors={['#3995f7','#3995f7','#3995f7','#3995f7']} style={{position:'absolute',width:'100%',flexDirection:'row',backgroundColor:'#00469a',bottom:0,paddingBottom:10,paddingTop:10}}>
                        <TouchableOpacity activeOpacity={.6} onPress={() => this.viewBtn(item)} style={styles.titleblk}>
                          <Text style={styles.title}>{item.station_name}</Text>
                          <Text style={styles.smalltext}>{item.state},{item.country}</Text>
                        </TouchableOpacity>
                        <View style={styles.sideicon}>
                          <TouchableOpacity activeOpacity={.6} style={styles.icon} onPress={() => this.addBtn(item)} >
                            <Image source={add} style={[styles.ImageStyle,styles.sidearrow]} />
                          </TouchableOpacity>
                          <TouchableOpacity activeOpacity={.6} style={styles.icon} onPress={() => this.editBtn(item)} >
                            <Image source={edit} style={[styles.ImageStyle,styles.sidearrow]} />
                          </TouchableOpacity>
                          <TouchableOpacity activeOpacity={.6} style={styles.icon} onPress={() => this.deleteBtn(item)} >
                            <View style={styles.icon}>
                              <Image source={del} style={[styles.ImageStyle,styles.sidearrow]} />
                            </View>
                          </TouchableOpacity>
                        </View>
                    </LinearGradient>
                  </View>
                ))
              }
            </View>

        :
          <Text style={{textAlign:'center'}}>No Record Found</Text>
        }
        </View>
        <Spinner visible={this.state.isVisible}  />
          </ScrollView>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    paddingTop:  0,
  },

  content: {
      flex: 1,
      paddingLeft:'4%',
      paddingRight:'4%',
      paddingTop:'4%',
      backgroundColor:'#fff'
  },
  container1: {
    flex: 1,
    //backgroundColor:'red',
    flexDirection: 'column',
  },
  servicebox:{
    flexDirection:'column',
    padding:AppSizes.ResponsiveSize.Sizes(0),
    paddingBottom:AppSizes.ResponsiveSize.Sizes(20),

    marginBottom:15,

    position:'relative'
  },
  titleblk:{
    width:'70%',
    height:'100%',
    paddingLeft:13
  },
  sideicon:{
      width:'30%',
      alignItems: 'center',
      flexDirection:'row',
      justifyContent: 'center',
      alignSelf:'stretch'
  },
  ImageStyle: {
    height: AppSizes.screen.width/19,
    width: AppSizes.screen.width/19,
    resizeMode : 'contain',
  },
  title:{
    fontSize:AppSizes.ResponsiveSize.Sizes(20),
    paddingBottom: AppSizes.ResponsiveSize.Sizes(5),
    color:'#fff',
    fontWeight:'700',
    fontFamily:Fonts.RobotoBold
  },
  smalltext:{
    fontSize:AppSizes.ResponsiveSize.Sizes(10),
    color:'#fff',
    fontWeight:'400',
    fontFamily:Fonts.RobotoRegular
  },
    icon:{
      padding:AppSizes.ResponsiveSize.Padding(5),
    },
    /*containerend:{
      position:'absolute',
      right:0,
      bottom:8
    },*/

    sideiconmain:{
        width:'10%',
        alignItems: 'center',
        paddingTop:AppSizes.ResponsiveSize.Padding(2),

        //backgroundColor:'gray'
    },
    stationimg:{
    width:'100%',
    height:180,

    overflow:'hidden',
    borderWidth:0,
    },

    Image: {
    height:'100%',
    width:'100%',
    resizeMode : 'cover',
    },


    shadow:{
    borderWidth:1,
    borderRadius: 8,
    borderColor: '#fff',
    justifyContent:'center',
    backgroundColor:'#fff',
    borderColor: '#ddd',
    borderBottomWidth: 0,
    shadowColor: '#999',
    shadowOffset: { width: 0, height: 2 },
    shadowOpacity: 0.8,
    shadowRadius: 2,
    elevation: 2,
    zIndex:0,
    overflow:'hidden'
    //backgroundColor:'red',
 },
});
