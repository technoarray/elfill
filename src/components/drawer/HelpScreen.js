import React, { Component } from 'react';
import {StyleSheet,Text,View,Image,TouchableOpacity,Platform,Alert,ScrollView,TextInput} from 'react-native';
import { AppStyles, AppSizes, AppColors } from '../../themes/'
import { NavigationActions } from 'react-navigation'
import Spacer from '../common/Spacer'
import * as commonFunctions from '../../utils/CommonFunctions'
import Header from '../common/Header'
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view'
import CommonButton from '../common/CommonButton'
import LinearGradient from 'react-native-linear-gradient';
import { Fonts } from '../../utils/Fonts';
var Constant = require('../../api/ApiRules').Constant;

/* Images */
const bg_image = require('../../themes/Images/bg_image.png')
var left= require( '../../themes/Images/right.png')
var payment= require( '../../themes/Images/payments.png')
var service= require( '../../themes/Images/services.png')
var servicestation= require( '../../themes/Images/service-station.png')

export default class AccountScreen extends Component {
  constructor (props) {
      super(props)
      this.state = {
      isVisible: false,
      Modal_Visibility: false
    },
    _that = this;
  }

  updateAlert = () => {
        this.setState({Modal_Visibility: !this.state.Modal_Visibility});
  }

  payBtn=()=>{
     _that.props.navigation.navigate('PaymentsScreen');
  }
  servicesBtn=()=>{
     _that.props.navigation.navigate('Serviceslistseller');
  }
  stationBtn=()=>{
     _that.props.navigation.navigate('Stationlistseller');
  }

  website_link() {
    Alert.alert(
        'eThera',
        'You will be redirected to your default browser, to visit our Website',
        [
          {text: 'Cancel', onPress: () => console.log('Cancel Button Pressed'), style: 'cancel'},
          {text: 'OK', onPress: () => Linking.openURL(Constant.SITE_URL)},

        ]

      )
  }

  render() {
    const headerProp = {
      title: 'Dashboard',
      screens: 'SideMenu',
    };
    return (
      <View style={styles.container}>
      <Header info={headerProp} navigation={_that.props.navigation}/>

            <View style={styles.content}>
                <ScrollView contentContainerStyle={styles.contentContainer}>
                    <View style={styles.container1}>
                      <TouchableOpacity activeOpacity={.6} onPress={this.payBtn} style={[styles.servicebox,styles.shadow]}>

                            <View style={styles.sideicon}>
                                <Image source={payment} style={styles.ImageStyle} />
                            </View>
                            <View style={styles.titleblk} >
                              <Text style={styles.title}>PAYMENTS</Text>
                              <Text style={styles.smalltext}>More</Text>
                            </View>

                          <LinearGradient colors={['#3995f7','#3995f7','#3995f7','#3995f7']}  style={[styles.linearGradient,styles.numberbox]}>
                                <Text style={styles.numberboxtext}>3</Text>
                            </LinearGradient>
                        </TouchableOpacity>
                        <TouchableOpacity activeOpacity={.6} onPress={this.servicesBtn} style={[styles.servicebox,styles.shadow]}>

                            <View style={styles.sideicon}>
                                <Image source={service} style={styles.ImageStyle} />
                            </View>
                            <View style={styles.titleblk}>
                              <Text style={styles.title}>SERVICES</Text>
                              <Text style={styles.smalltext}>More</Text>
                            </View>

                            <LinearGradient colors={['#3995f7','#3995f7','#3995f7','#3995f7']}  style={[styles.linearGradient,styles.numberbox]}>
                                <Text style={styles.numberboxtext}>3</Text>
                            </LinearGradient>
                          </TouchableOpacity>
                          <TouchableOpacity activeOpacity={.6} onPress={this.stationBtn} style={[styles.servicebox,styles.shadow]}>
                            <View style={styles.sideicon}>
                                <Image source={servicestation} style={styles.ImageStyle} />
                            </View>
                            <View style={styles.titleblk}>
                              <Text style={styles.title}>SERVICE STATION</Text>
                              <Text style={styles.smalltext}>More</Text>
                            </View>

                            <LinearGradient colors={['#3995f7','#3995f7','#3995f7','#3995f7']}  style={[styles.linearGradient,styles.numberbox]}>
                                <Text style={styles.numberboxtext}>3</Text>
                            </LinearGradient>
                        </TouchableOpacity>
                    </View>
                  </ScrollView>
              </View>
        </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    paddingTop:  0,
  },

  content: {
      flex: 1,
      padding:'5%',
      paddingBottom:0,
      //backgroundColor:'#000'
  },
  container1: {
    flex: 1,

    flexDirection: 'column',
  },
  servicebox:{
    flexDirection:'row',
    padding:AppSizes.ResponsiveSize.Sizes(15),
    borderRadius: 5,
    marginBottom:AppSizes.ResponsiveSize.Padding(2),
    backgroundColor:'red'
  },
  titleblk:{
    width:'65%',

    justifyContent:'center',
    //backgroundColor:'red'
  },
  sideicon:{
      width:'35%',
      alignItems: 'center',
      paddingTop:AppSizes.ResponsiveSize.Padding(2),
      //backgroundColor:'red'
  },
  ImageStyle: {
    height: AppSizes.screen.width/5,
    width: AppSizes.screen.width/5,
    resizeMode : 'contain',
  },
  numberbox:{
      height:18,
      width:18,
      borderRadius:30,
      textAlign:'center',
      alignItems:'center',
      position:'absolute',
      right:20,
      top:20,
      paddingTop:AppSizes.ResponsiveSize.Padding(1)
  },
  numberboxtext:{
    color:'#fff',
    fontFamily:Fonts.RobotoRegular,
    fontSize:AppSizes.ResponsiveSize.Sizes(10),
  },
  title:{
    fontSize:AppSizes.ResponsiveSize.Sizes(15),
    paddingBottom: AppSizes.ResponsiveSize.Sizes(5),
    color:'#4a2e59',
    fontWeight:'700',
    textTransform:'uppercase',
    fontFamily:Fonts.RobotoBlack,

  },
  smalltext:{
    fontSize:AppSizes.ResponsiveSize.Sizes(10),
    color:'#575459',
    fontWeight:'600',
    position:'absolute',
    bottom:0,
    right:0,
    fontFamily:Fonts.RobotoBold,
  },
  contentContainer: {
      paddingVertical: 0,
      height:'100%',
    //backgroundColor:'green'
    },
    shadow:{
     borderWidth:1,
     borderRadius: 2,
     borderColor: '#fff',
     justifyContent:'center',
     backgroundColor:'#fff',
     borderColor: '#ddd',
     borderBottomWidth: 1,
     shadowColor: '#999',
     shadowOffset: { width: 0, height: 2 },
     shadowOpacity: 0.8,
     shadowRadius: 2,
     elevation: 0,
     //backgroundColor:'red',

    },

});
