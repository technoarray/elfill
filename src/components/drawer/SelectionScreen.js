import React, { Component } from 'react';
import {StyleSheet,Text,View,Image,TouchableOpacity,Platform,Alert,Modal,ScrollView} from 'react-native';
import { AppStyles, AppSizes, AppColors } from '../../themes/'
import { NavigationActions } from 'react-navigation'
import Spacer from '../common/Spacer'
import * as commonFunctions from '../../utils/CommonFunctions'
import Header from '../common/HeaderWithBack'
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view'
import CommonButton from '../common/CommonButton'
import LinearGradient from 'react-native-linear-gradient';
import DatePicker from 'react-native-datepicker'
import {RadioGroup, RadioButton} from 'react-native-flexi-radio-button'
import Picker from 'react-native-q-picker';
import DateTimePicker from 'react-native-modal-datetime-picker';
import Spinner from 'react-native-loading-spinner-overlay';
import CheckBox from 'react-native-check-box';
import { Fonts } from '../../utils/Fonts';
var Constant = require('../../api/ApiRules').Constant;
var WebServices = require('../../api/ApiRules').WebServices;

/* Images */
const clock= require('../../themes/Images/clock.png')
const calendar=require('../../themes/Images/calendar.png')

let _that
export default class PaymentMethod extends Component {
  static title = 'Custom Card'
  constructor (props) {
      super(props)
      this.state = {
        paytc:false,
        pay:false,
        tc:false,
        submit:false,
        status:'',
        Modal_Visibility:false,
        date:"Select Date",
        time:"Select Time",
        timestamp:'',
        isVisible: false,
        Modal_Visibility: false,
        show_buyer_section:false,
        show_seller_section:false,
        isDatePickerVisible:false,
        isTimePickerVisible:false,
      },
      _that = this;
  }

  componentDidMount(){
    var date=new Date()
    console.log('Date',date);
    this.setState({default:date})
  }

  onSelect(index, value){
    _that.setState({status: value})
  }

  submit_click(){
    _that.props.navigation.navigate('PaymentsScreen',{data:data})
  }

  validationAndApiParameter(apikey) {
    var error=0;
    if(this.state.date == ''){
      error=1;
      Alert.alert("Please Select a Payment Method");
    }
    if(error==0){
      if(apikey=='buyService'){
        _that.setState({isVisible: true});
        this.postToApiCalling('POST',apikey, Constant.URL_buyService, data);
      }
    }
  }

  postToApiCalling(method, apiKey, apiUrl, data) {
    new Promise(function(resolve, reject) {
      if (method == 'POST') {
        resolve(WebServices.callWebService(apiUrl, data));
      } else {
        resolve(WebServices.callWebService_GET(apiUrl, data));
      }
    }).then((jsonRes) => {
      console.log(jsonRes);
      _that.setState({ isVisible: false })
      if ((!jsonRes) || (jsonRes.code == 0)) {
          setTimeout(()=>{
            Alert.alert('Login Error',jsonRes.message);
          },200);
      }
      else{
        if (jsonRes.code == 1) {
          _that.apiSuccessfullResponse(apiKey, jsonRes)
        }
      }
    }).catch((error) => {
        console.log("ERROR" + error);
        _that.setState({ isVisible: false })
        setTimeout(()=>{
            Alert.alert("Server issue"+error);
        },200);
    });
  }

  apiSuccessfullResponse(apiKey, jsonRes) {
    if (apiKey == 'buyService') {
      stationData=jsonRes.result;
      console.log(stationData)
      _that.props.navigation.navigate('Thankyou',{station_id:this.state.station_id})
    }
  }

  servicesBtn=()=>{
     _that.props.navigation.navigate('Serviceslistseller');
  }
  stationBtn=()=>{
     _that.props.navigation.navigate('Stationlistseller');
  }

  _showDatePicker = () => this.setState({ isDatePickerVisible: true });

  _hideDatePicker = () => this.setState({ isDatePickerVisible: false });

  _handleDatePicked = date => {
    console.log(date);
    this.setState({default:date})
    var str= date.toString();
    var re= str.split(" ");
    console.log(re);
    var val =[]
    val.push(re[2]);
    val.push(re[1])
    val.push(re[3])
    var str2= val.join("-")
    this.setState({date:str2})
    this._hideDatePicker();
  };

  _showTimePicker = () => this.setState({ isTimePickerVisible: true });

  _hideTimePicker = () => this.setState({ isTimePickerVisible: false });

  _handleTimePicked = (time) => {
    console.log(time);
    var str= time.toString();
    var re= str.split(" ");
    var str2=re[4]
    var str3=str2.slice(0,-3)
    var t=parseInt(str3)
    if(t>=12){
      var time=str3.concat(' PM')
      this.setState({time:time})
      console.log(time);
    }
    else{
      var time=str3.concat(' AM')
      this.setState({time:time})
      console.log(time);
    }

    this._hideTimePicker();
  };

  render() {
    const headerProp = {
      title: 'Select Date and Time',
      screens: 'SideMenu',
        type:''
    };

    const { loading, token, error, params, errorParams } = this.state

    return (
      <View style={styles.container}>
        <Header info={headerProp} navigation={_that.props.navigation}/>

        <View style={styles.content}>
          <View style={[styles.servicebox,styles.shadow]}>
            <Text style={styles.title}>
                Pick Date
            </Text>
          <View style={styles.border} />
          <TouchableOpacity style={styles.timePicker} onPress={this._showDatePicker}>
            <Text>{this.state.date}</Text>
            <Image style={styles.smaions} source={calendar}/>
          </TouchableOpacity>
            <DateTimePicker
              date={this.state.default}
              isVisible={this.state.isDatePickerVisible}
              onConfirm={this._handleDatePicked}
              onCancel={this._hideDatePicker}
            />
          </View>
          <View style={[styles.servicebox,styles.shadow]}>
            <Text style={styles.title}>Pick Time</Text>
            <View style={styles.border} />
                  <TouchableOpacity style={styles.timePicker} onPress={this._showTimePicker}>
                    <Text>{this.state.time}</Text>
                    <Image style={styles.smaions} source={clock}/>
                  </TouchableOpacity>
                  <DateTimePicker
                    mode={'time'}
                    isVisible={this.state.isTimePickerVisible}
                    onConfirm={this._handleTimePicked}
                    onCancel={this._hideTimePicker}
                    is24Hour={false}
                  />
          </View>
        </View>
        <View style={styles.container3}>
          <TouchableOpacity onPress={this.submit_click}>
            <View style={[styles.SectionStyle,{justifyContent:'flex-end'}]}>
              <CommonButton label='Next' width='100%'/>
            </View>
          </TouchableOpacity>
        </View>
        <Spinner visible={this.state.isVisible}  />
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    paddingTop:  0,
    backgroundColor:'#fff'
  },
  content: {
      flex: 1,
      padding:'5%',
      paddingBottom:0,
      //backgroundColor:'green'
  },
  servicebox:{
    padding:AppSizes.ResponsiveSize.Sizes(15),
    borderRadius: 5,
    marginBottom:AppSizes.ResponsiveSize.Padding(6),
    //backgroundColor:'red',
  },
    shadow:{
     borderWidth:1,
     borderRadius: 2,
     borderColor: '#fff',
     justifyContent:'center',
     backgroundColor:'#fff',
     borderColor: '#ddd',
     borderBottomWidth: 1,
     shadowColor: '#999',
     shadowOffset: { width: 0, height: 2 },
     shadowOpacity: 0.8,
     shadowRadius: 2,
     elevation: 2,
     //backgroundColor:'red',
   },
  title:{
    fontSize:AppSizes.ResponsiveSize.Sizes(18),
    paddingBottom: AppSizes.ResponsiveSize.Sizes(5),
    color:'#333',
    fontWeight:'700',
    fontFamily:Fonts.RobotoRegular,
    //backgroundColor:'red'
  },
  border:{
    borderWidth:1.5,
    borderColor:'#333',
    width:30,
  },
  container3: {
    height:AppSizes.screen.width/5,
    alignItems: 'center',
    justifyContent: 'flex-end',
    //backgroundColor:'red'
  },
  timePicker:{
    flexDirection:'row',
    alignItems:'center',
    justifyContent:'center',
    backgroundColor:'#eee',
    height:40,
    borderWidth:1,
    borderColor:'#bdc3c7',
    marginTop:AppSizes.ResponsiveSize.Padding(5),
  },
  smaions:{
    height:'50%',
    width:'7.5%',
    position:'absolute',
    right:10
  },
});
