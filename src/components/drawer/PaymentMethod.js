import React, { Component } from 'react';
import {StyleSheet,Text,View,Image,TouchableOpacity,AsyncStorage,Platform,Alert,Modal,ScrollView} from 'react-native';
import { AppStyles, AppSizes, AppColors } from '../../themes/'
import { NavigationActions } from 'react-navigation'
import Spacer from '../common/Spacer'
import * as commonFunctions from '../../utils/CommonFunctions'
import Header from '../common/HeaderWithBack'
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view'
import CommonButton from '../common/CommonButton'
import LinearGradient from 'react-native-linear-gradient';
import DatePicker from 'react-native-datepicker'
import {RadioGroup, RadioButton} from 'react-native-flexi-radio-button'
import Picker from 'react-native-q-picker';
import DateTimePicker from 'react-native-modal-datetime-picker';
import Spinner from 'react-native-loading-spinner-overlay';
import CheckBox from 'react-native-check-box';
import { Fonts } from '../../utils/Fonts';
var Constant = require('../../api/ApiRules').Constant;
var WebServices = require('../../api/ApiRules').WebServices;

/* Images */
const paypal = require('../../themes/Images/paypal.png')
var stripe= require( '../../themes/Images/stripe.png')
const clock= require('../../themes/Images/clock.png')
const calendar=require('../../themes/Images/calendar.png')

let _that
export default class PaymentMethod extends Component {
  static title = 'Custom Card'
  constructor (props) {
      super(props)
      this.state = {
        paytc:false,
        pay:false,
        title:'Select & Pay',
        tc:false,
        submit:false,
        status:'',
        Modal_Visibility:false,
        date:"Select Date",
        time:"Select Time",
        timestamp:'',
        isVisible: false,
        Modal_Visibility: false,
        show_buyer_section:false,
        show_seller_section:false,
        isDatePickerVisible:false,
        isTimePickerVisible:false,
        uid:'',
        accomodation:this.props.navigation.state.params.data.accomodation,
        accomodation_charges:this.props.navigation.state.params.data.accomodation_charges,
        ampere:this.props.navigation.state.params.data.ampere,
        cable_charges:this.props.navigation.state.params.data.cable_charges,
        cat_id:this.props.navigation.state.params.data.cat_id,
        charge_type:this.props.navigation.state.params.data.charge_type,
        description:this.props.navigation.state.params.data.description,
        selectedDeviceType:this.props.navigation.state.params.data.device_id,
        selectedDeviceName:this.props.navigation.state.params.data.device_name,
        dlnumber:this.props.navigation.state.params.data.driver_licence_no,
        effect:this.props.navigation.state.params.data.effect,
        insurance:this.props.navigation.state.params.data.insurance,
        numberofslots:this.props.navigation.state.params.data.no_of_charging_spot,
        other_catid:this.props.navigation.state.params.data.other_cat_id,
        outlet_type:this.props.navigation.state.params.data.outlet_type,
        parkingLots:this.props.navigation.state.params.data.parkingLots,
        seller_id:this.props.navigation.state.params.data.uid,
        station_id:this.props.navigation.state.params.data.station_id,
        subcat_id:this.props.navigation.state.params.data.subcat_id,
        parking_fee:this.props.navigation.state.params.data.parking_fee,
        phases:this.props.navigation.state.params.data.phases,
        upload_licence:this.props.navigation.state.params.data.upload_driver_licence,
        reantperday:this.props.navigation.state.params.data.reantperday,
        parkingPrice:this.props.navigation.state.params.data.parkingPrice,
        parkingperday:this.props.navigation.state.params.data.parkingperday,
        price:this.props.navigation.state.params.data.price,
        service_id:this.props.navigation.state.params.data.id,
      },
      _that = this;
  }
  componentDidMount(){
    console.log('cat_id',this.props.navigation.state.params.data.cat_id);
    AsyncStorage.getItem('UserData').then((UserData) => {
      var data = JSON.parse(UserData);
        uid=data.id
        if(uid != ''){
          _that.setState({
            uid:uid,
          })
        }
    })
  }

  onSelect(index, value){
    _that.setState({status: value})
  }

  submit_click(){
    _that.setState({pay:true})
  }

  validationAndApiParameter(apikey) {
    var error=0;
    if(this.state.status == ''){
      error=1;
      Alert.alert("Please Select a Payment Method");
    }
    if(error==0){
      if(apikey=='buyService'){
        const data = new FormData();
        data.append('uid',this.state.uid);//0
        data.append('seller_id',this.state.seller_id);//1
        data.append('station_id',this.state.station_id);//2
        data.append('service_id',this.state.service_id);//3
        data.append('cat_id',this.state.cat_id);//4
        data.append('subcat_id',this.state.subcat_id);//5
        data.append('device_id',this.state.selectedDeviceType);//6
        data.append('charge_type',this.state.selectedCharge);//7
        data.append('outlet_type',this.state.outlet_type);//8
        data.append('parking_fee',this.state.parking_fee);//9
        data.append('accomodation',this.state.accomodation);//10
        data.append('accomodation_charges',this.state.accomodation_charges);//11
        data.append('effect',this.state.effect);//12
        data.append('phases',this.state.phases);//13
        data.append('ampere',this.state.ampere);//14
        data.append('driver_licence_no',this.state.dlnumber);//15
        data.append('upload_driver_licence',this.state.upload_licence);//16
        data.append('insurance',this.state.insurance);//17
        data.append('reantperday',this.state.reantperday);//18
        data.append('parkingLots',this.state.parkingLots);//19
        data.append('parkingPrice',this.state.parkingPrice);//20
        data.append('parkingperday',this.state.parkingperday);//21
        data.append('price',this.state.price);//23
        data.append('order_date',this.state.date);
        data.append('order_time',this.state.time);
        console.log(data);

        _that.setState({isVisible: true});
        this.postToApiCalling('POST',apikey, Constant.URL_buyService, data);
      }
    }

  }

  postToApiCalling(method, apiKey, apiUrl, data) {
    new Promise(function(resolve, reject) {
      if (method == 'POST') {
        resolve(WebServices.callWebService(apiUrl, data));
      } else {
        resolve(WebServices.callWebService_GET(apiUrl, data));
      }
    }).then((jsonRes) => {
      console.log(jsonRes);
      _that.setState({ isVisible: false })
      if ((!jsonRes) || (jsonRes.code == 0)) {
          setTimeout(()=>{
            Alert.alert('Login Error',jsonRes.message);
          },200);
      }
      else{
        if (jsonRes.code == 1) {
          this.setState({pay:false})
          _that.apiSuccessfullResponse(apiKey, jsonRes)
        }
      }
    }).catch((error) => {
        console.log("ERROR" + error);
        _that.setState({ isVisible: false })
        setTimeout(()=>{
            Alert.alert("Server issue"+error);
        },200);
    });
  }

  apiSuccessfullResponse(apiKey, jsonRes) {
    if (apiKey == 'buyService') {
      stationData=jsonRes.result;
      console.log(stationData)
      _that.props.navigation.navigate('Thankyou',{station_id:this.state.station_id})
    }
  }

  servicesBtn=()=>{
     _that.props.navigation.navigate('Serviceslistseller');
  }
  stationBtn=()=>{
     _that.props.navigation.navigate('Stationlistseller');
  }

  _showDatePicker = () => this.setState({ isDatePickerVisible: true });

  _hideDatePicker = () => this.setState({ isDatePickerVisible: false });

  _handleDatePicked = date => {
    var str= date.toString();
    var re= str.split(" ");
    var val =[]
    val.push(re[1]);
    val.push(re[2])
    val.push(re[3])
    var str2= val.join("-")
    this.setState({date:str2})
    this._hideDatePicker();
  };

  _showTimePicker = () => this.setState({ isTimePickerVisible: true });

  _hideTimePicker = () => this.setState({ isTimePickerVisible: false });

  _handleTimePicked = (time) => {
    console.log(time);
    var str= time.toString();
    var re= str.split(" ");
    var str2=re[4]
    var str3=str2.slice(0,-3)
    var t=parseInt(str3)
    if(t>=12){
      var time=str3.concat(' PM')
      this.setState({time:time})
      console.log(time);
    }
    else{
      var time=str3.concat(' AM')
      this.setState({time:time})
      console.log(time);
    }

    this._hideTimePicker();
  };

  render() {
    const headerProp = {
      title: this.state.title,
      screens: 'PaymentMethod',
        type:''
    };

    const { loading, token, error, params, errorParams } = this.state

    return (
      <View style={styles.container}>
        <Header info={headerProp} navigation={_that.props.navigation}/>
        <ScrollView contentContainerStyle={styles.contentContainer}>
        <View style={styles.content}>
            <View style={[styles.servicebox1,styles.shadow1]}>
                <View style={{flexDirection:'row'}}>

                  <View style={{width:'100%'}}>
                      <Text style={styles.title}>Service</Text>
                  </View>
                  {/*<TouchableOpacity onPress={() => _that.props.navigation.goBack(null)} style={{width:'20%',justifyContent:'center',alignItems:'center',backgroundColor:'#3995f7'}}>
                    <Text style={{color:'#fff'}}>Edit</Text>
                  </TouchableOpacity>*/}
                </View>
                <View style={styles.border} />

                {this.state.cat_id=='1'?
                    <View style={styles.SectionStyle}>
                        {this.state.charge_type.length>0?
                          <View style={styles.inputfield}>
                            <Text style={styles.inputtext}>Charge</Text>
                            <Text>{this.state.charge_type}</Text>
                          </View>
                        :
                          null
                        }
                        {this.state.parking_fee.length>0?
                          <View style={styles.inputfield}>
                            <Text style={styles.inputtext}>Parking Fee</Text>
                            <Text>{this.state.parking_fee}</Text>
                          </View>
                        :
                          null
                        }
                        {this.state.effect.length>0?
                          <View style={styles.inputfield}>
                            <Text style={styles.inputtext}>Effect</Text>
                            <Text>{this.state.effect}</Text>
                          </View>
                        :
                          null
                        }
                        {this.state.phases.length>0?
                          <View style={styles.inputfield}>
                            <Text style={styles.inputtext}>Phases</Text>
                            <Text>{this.state.phases}</Text>
                          </View>
                        :
                          null
                        }
                        {this.state.ampere.length>0?
                          <View style={styles.inputfield}>
                            <Text style={styles.inputtext}>Ampere</Text>
                            <Text>{this.state.ampere}</Text>
                          </View>
                        :
                          null
                        }
                        {this.state.selectedDeviceName.length>0?
                          <View style={styles.inputfield}>
                              <Text style={styles.inputtext}>Device Type</Text>
                              <Text>{this.state.selectedDeviceName}</Text>
                          </View>
                        :
                          null
                        }
                        <View style={styles.inputfield}>
                            <Text style={styles.inputtext}>Description</Text>
                            <Text>{this.state.description}</Text>
                        </View>
                    </View>
                :
                  null
                }

                {this.state.cat_id == '2'?
                <View>
                  {this.state.selectedDeviceName.length>0?
                    <View style={styles.SectionStyle}>
                      <View style={styles.inputfield}>
                          <Text style={styles.inputtext}>Device Type</Text>
                          <Text>{this.state.selectedDeviceName}</Text>
                      </View>
                    </View>
                  :
                    null
                  }
                  {this.state.description.length>0?
                    <View style={styles.SectionStyle}>
                      <View style={styles.inputfield}>
                          <Text style={styles.inputtext}>Description</Text>
                          <Text>{this.state.description}</Text>
                      </View>
                    </View>
                  :
                    null
                  }
                </View>
                :
                  null
                }

                {this.state.cat_id == '3'?
                  <View>
                    {this.state.selectedDeviceName?
                      <View style={styles.SectionStyle}>
                        <View style={styles.inputfield}>
                            <Text style={styles.inputtext}>Device Name</Text>
                            <Text>{this.state.selectedDeviceName}</Text>
                        </View>
                      </View>
                    :
                      null
                    }
                    {this.state.description.length>0?
                      <View style={styles.SectionStyle}>
                        <View style={styles.inputfield}>
                            <Text style={styles.inputtext}>Description</Text>
                            <Text>{this.state.description}</Text>
                        </View>
                      </View>
                    :
                      null
                    }
                  </View>
                :
                    null
                }

                {this.state.cat_id == '5'?
                  <View>
                    {this.state.cable_charges.length>0?
                      <View style={styles.inputfield}>
                          <Text style={styles.inputtext}>Cable Charges</Text>
                          <Text>{this.state.cable_charges}</Text>
                      </View>
                    :
                      null
                    }
                    {this.state.description.length>0?
                      <View style={styles.inputfield}>
                          <Text style={styles.inputtext}>Description</Text>
                          <Text>{this.state.description}</Text>
                      </View>
                    :
                      null
                    }
                  </View>
                :
                  null
                }

                {this.state.cat_id == '6'?
                  <View>
                    {this.state.accomodation.length>0?
                      <View style={styles.inputfield}>
                          <Text style={styles.inputtext}>Accomodation</Text>
                          <Text>{this.state.accomodation}</Text>
                      </View>
                    :
                      null
                    }
                    {this.state.accomodation_charges.length>0?
                      <View style={styles.inputfield}>
                          <Text style={styles.inputtext}>Accomodation Charges</Text>
                          <Text>{this.state.accomodation_charges}</Text>
                      </View>
                    :
                      null
                    }
                    {this.state.description.length>0?
                      <View style={styles.inputfield}>
                          <Text style={styles.inputtext}>Description</Text>
                          <Text>{this.state.description}</Text>
                      </View>
                    :
                      null
                    }
                  </View>
                :
                  null
                }
              </View>


            </View>

            <View style={[styles.servicebox,styles.shadow]}>
              <Text style={styles.title}>
                  Pick Date
              </Text>
              <View style={styles.border} />
              <TouchableOpacity style={styles.timePicker} onPress={this._showDatePicker}>
                <Text>{this.state.date}</Text>
                <Image style={styles.smaions} source={calendar}/>
              </TouchableOpacity>
              <DateTimePicker
                isVisible={this.state.isDatePickerVisible}
                onConfirm={this._handleDatePicked}
                onCancel={this._hideDatePicker}
              />
              <Text style={styles.title}>Pick Time</Text>
              <View style={styles.border} />
              <TouchableOpacity style={styles.timePicker} onPress={this._showTimePicker}>
                <Text>{this.state.time}</Text>
                <Image style={styles.smaions} source={clock}/>
              </TouchableOpacity>
              <DateTimePicker
                mode={'time'}

                isVisible={this.state.isTimePickerVisible}
                onConfirm={this._handleTimePicked}
                onCancel={this._hideTimePicker}
                is24Hour={false}
              />
            </View>
            <View style={[styles.servicebox,styles.shadow]}>
              <Text style={styles.title}>
                Payment Method
              </Text>
              <View style={styles.border} />

              <View style={styles.SectionRadioView}>
                <RadioGroup
                 color='#000'
                 onSelect = {(index, value) => this.onSelect(index, value)}
                >
                  <RadioButton style={styles.methodbox} value={'paypal'}>
                    <Image source={paypal} style={styles.ImageStyle} />
                  </RadioButton>

                  <RadioButton style={styles.methodbox} value={'stripe'}>
                    <Image source={stripe} style={styles.ImageStyle} />
                  </RadioButton>
                </RadioGroup>
              </View>
            </View>

        </ScrollView>

        {this.state.pay?
          <Modal
            visible={this.state.pay}
            onRequestClose={()=> this.setState({pay:false})}
            animationType={"fade"}>
            <View style={{flex:1}}>
              <View style={{flex:1,padding:15,backgroundColor:'#fff'}}>
                <Text style={{fontFamily:'400',color:'#000',fontSize:AppSizes.ResponsiveSize.Sizes(25),textAlign:'center',marginTop:'10%'}}>Terms & Conditions</Text>
                <View style={{height:2,width:'10%',backgroundColor:'#000',marginBottom:'5%',alignSelf:'center',marginTop:'2%'}} />
                <ScrollView style={{width:'100%'}}>
                  <View style={{flexDirection:'row',width:'100%',marginBottom:'5%'}}>
                    <Text style={[styles.textcontent,{flex:1}]}>
                       Payment amount
                    </Text>
                    <Text style={[styles.textcontent,{flex:1}]}>
                       {this.state.price}
                    </Text>
                  </View>
                  <View style={{flexDirection:'row',width:'100%',marginBottom:'5%'}}>
                    <Text style={[styles.textcontent,{flex:1}]}>
                       Booking Date
                    </Text>
                    <Text style={[styles.textcontent,{flex:1}]}>
                       {this.state.date}
                    </Text>
                  </View>
                  <View style={{flexDirection:'row',width:'100%',marginBottom:'5%'}}>
                    <Text style={[styles.textcontent,{flex:1}]}>
                       Payment Method
                    </Text>
                    <Text style={[styles.textcontent,{flex:1}]}>
                       {this.state.status}
                    </Text>
                  </View>

                  <Text style={styles.tccontent}>
                    Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industrys standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.
                   </Text>
                </ScrollView>
                <CheckBox
                  style={styles.checkboxSection}
                  checkBoxColor={"#000000"}
                  onClick={()=>{this.setState({paytc:!this.state.paytc}),this.validationAndApiParameter('buyService')}}
                  isChecked={this.state.paytc}
                  rightText={"I ACCEPT TERMS AND CONDITIONS"}
                  rightTextStyle={{color:"#000000"}}
                />
              </View>
            </View>
          </Modal>
        :
          null
        }
        {this.state.Modal_Visibility?
          <Modal
            visible={this.state.Modal_Visibility}
            onRequestClose={()=> this.setState({Modal_Visibility:false})}
            animationType={"fade"}>
            <View style={{flex:1,padding:15,backgroundColor:'#fff'}}>
              <Text style={{fontFamily:'400',color:'#000',fontSize:AppSizes.ResponsiveSize.Sizes(25),textAlign:'center',marginBottom:20,marginTop:10}}>Terms & Conditions</Text>
              <ScrollView>
                <Text style={styles.tccontent}>
                  Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industrys standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.
                 </Text>
              </ScrollView>
              <CheckBox
                style={styles.checkboxSection}
                checkBoxColor={"#000000"}
                onClick={()=>{this.setState({tc:!this.state.tc})}}
                isChecked={this.state.tc}
                rightText={"I ACCEPT TERMS AND CONDITIONS"}
                rightTextStyle={{color:"#000000"}}
              />
              <View style={styles.okBtn}>
              {this.state.tc?
                <TouchableOpacity onPress={() => {this.setState({Modal_Visibility:false}),this.validationAndApiParameter('buyService')}} style={styles.SectionStyle}>
                  <CommonButton label='Confirm' width='100%'/>
                </TouchableOpacity>
              :
                null
              }
              </View>
            </View>
          </Modal>
        :
          null
        }

        <View style={styles.container3}>
          <TouchableOpacity onPress={this.submit_click}>
            <View style={[styles.SectionStyle,{justifyContent:'flex-end'}]}>
              <CommonButton label='Next' width='100%'/>
            </View>
          </TouchableOpacity>
        </View>
        <Spinner visible={this.state.isVisible}  />

      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    paddingTop:  0,
    backgroundColor:'#fff'
  },
  content: {
      flex: 1,
      padding:'5%',
      paddingBottom:0,
      //backgroundColor:'green'

  },
    servicebox1:{
      marginBottom:20
    },
  servicebox:{
    padding:AppSizes.ResponsiveSize.Sizes(15),
    borderRadius: 5,
    marginBottom:15,
    width:'90%',
    alignSelf:'center'

  },
    shadow:{
     borderWidth:1,
     borderRadius: 2,
     borderColor: 'red',
     justifyContent:'center',
     backgroundColor:'#fff',
     borderColor: '#ddd',
     borderBottomWidth: 1,
     shadowColor: '#999',
     shadowOffset: { width: 0, height: 2 },
     shadowOpacity: 0.8,
     shadowRadius: 2,
     elevation: 2,
     //backgroundColor:'red',
   },
   title:{
     fontSize:AppSizes.ResponsiveSize.Sizes(18),
     paddingBottom: AppSizes.ResponsiveSize.Sizes(5),
     color:'#3995f7',
     fontWeight:'700',
     marginTop:10,
        marginBottom:5,
     fontFamily:Fonts.RobotoBold
   },
   border:{
     borderWidth:1.5,
     borderColor:'#333',
     width:30,
     marginBottom:10,
   },
   smalltitle:{
     fontSize:AppSizes.ResponsiveSize.Sizes(15),
     paddingBottom: AppSizes.ResponsiveSize.Sizes(3),
     color:'#333',
     fontWeight:'700',
     fontFamily:Fonts.RobotoBold,
     marginTop:AppSizes.ResponsiveSize.Padding(2)
   },
   description:{
     fontSize:AppSizes.ResponsiveSize.Sizes(11),
     paddingBottom: AppSizes.ResponsiveSize.Sizes(3),
     color:'#9b9b9b',
     fontWeight:'500',
     fontFamily:Fonts.RobotoRegular
   },
   pikbox:{
     marginTop:AppSizes.ResponsiveSize.Padding(2)
   },
   valueText: {
     fontSize: 18,
     marginBottom: 50,
   },
  container3: {
    height:AppSizes.screen.width/7.6,
    alignItems: 'center',
    justifyContent: 'flex-end',
    //backgroundColor:'red'
  },
  timePicker:{
    flexDirection:'row',
    alignItems:'center',
    justifyContent:'center',
    backgroundColor:'#eee',
    height:40,
    borderWidth:1,
    borderColor:'#bdc3c7',
    marginTop:AppSizes.ResponsiveSize.Padding(5),
  },
  smaions:{
    height:'50%',
    width:'7.5%',
    position:'absolute',
    right:10
  },

  contentContainer: {
    paddingBottom: 200
  },
  ImageStyle:{
    height:AppSizes.screen.height/10,
    width:AppSizes.screen.width/2,
    marginLeft:AppSizes.ResponsiveSize.Padding(2)
  },
  methodbox:{
    marginTop:10,
    backgroundColor:'#eee',
    alignItems:'center',
    borderWidth: 1,
    borderColor: '#000',
    borderRadius: 5 ,
  },
  SectionRadioView: {
    borderBottomWidth:1,
    borderBottomColor:'#ffffff',
    borderRadius:5,
    width:'100%',
    marginBottom: (Platform.OS === 'ios') ? AppSizes.ResponsiveSize.Padding(1) :AppSizes.ResponsiveSize.Padding(2)
  },
    timePicker:{
      flexDirection:'row',
      alignItems:'center',
      justifyContent:'center',
      backgroundColor:'#eee',
      height:40,
      borderWidth:1,
      borderColor:'#bdc3c7',
      marginTop:AppSizes.ResponsiveSize.Padding(2),
      marginBottom:AppSizes.ResponsiveSize.Padding(2),
      position:'relative'
    },
    inputtext:{marginBottom:10,
      marginTop:AppSizes.ResponsiveSize.Padding(3),
      fontSize:AppSizes.ResponsiveSize.Sizes(14),
      fontWeight:'600',
      fontFamily:Fonts.RobotoBold,
      marginBottom:AppSizes.ResponsiveSize.Padding(1)
    },
    smaions:{
      height:'50%',
      width:'7.5%',
      position:'absolute',
      right:10
    },
  field: {
    width: 300,
    color: '#449aeb',
    borderColor: '#000000',
    borderWidth: 1,
    borderRadius: 5,
    backgroundColor: '#FFFFFF',
    overflow: 'hidden',
  },
  checkboxSection :{
    marginBottom: AppSizes.ResponsiveSize.Padding(2),
    height:AppSizes.screen.width/15,
  },
  tccontent:{
    color:AppColors.contentColor,
    textAlign:'center',
    fontSize:AppSizes.ResponsiveSize.Sizes(13),
    lineHeight: AppSizes.ResponsiveSize.Sizes(13 * 1.70),
    fontWeight:'300',
    fontFamily:Fonts.RobotoRegular,
 },
 textcontent:{
   color:AppColors.contentColor,
   fontSize:AppSizes.ResponsiveSize.Sizes(13),
   lineHeight: AppSizes.ResponsiveSize.Sizes(13 * 1.70),
   fontWeight:'600',
   fontFamily:Fonts.RobotoBold,
 },
});
