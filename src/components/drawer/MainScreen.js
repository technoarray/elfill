import React, { Component } from 'react';
import {
    AppRegistry,
    StyleSheet,
    Text,
    View,
    Image,
    TouchableHighlight,
    ScrollView,
    Dimensions,
    TouchableOpacity,
    Alert,
    NetInfo,
    Platform,
    AsyncStorage,
    StatusBar,
    ImageBackground
} from 'react-native';
import { NavigationActions } from 'react-navigation'
import { AppStyles, AppSizes, AppColors } from '../../themes/'
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view'
import Spinner from 'react-native-loading-spinner-overlay';
import LinearGradient from 'react-native-linear-gradient';
//import { LoginManager,LoginButton,AccessToken,GraphRequest,GraphRequestManager} from 'react-native-fbsdk';
import Spacer from '../common/Spacer'
import * as commonFunctions from '../../utils/CommonFunctions'
import CommonButton from '../common/CommonButton'

import { Fonts } from '../../utils/Fonts';



const logo = require('../../themes/Images/main_logo.png')
const bg_image = require('../../themes/Images/bg_image.png')
const location = require('../../themes/Images/location.png')
const fb_icon = require('../../themes/Images/facebook-icon.png')
const google_icon = require('../../themes/Images/google-icon.png')
const email_icon = require('../../themes/Images/mail-icon.png')

let _that

const MyStatusBar = ({backgroundColor, ...props}) => (
  <View style={[styles.statusBar, { backgroundColor }]}>
    <StatusBar translucent backgroundColor={backgroundColor} {...props} />
  </View>
);
export default class LoginScreen extends Component {
    constructor(props) {
        super(props)
        this.state = {
          name: '',
          email: '',
          isVisible: false,
        },
        _that = this;
    }

    componentWillMount() {
      AsyncStorage.getItem('loggedIn').then((value) => {
            var loggedIn = JSON.parse(value);
            if(loggedIn){
              const resetAction = NavigationActions.navigate({ routeName: 'StartScreen'})
              _that.props.navigation.dispatch(resetAction);
            }
        })
      AsyncStorage.getItem('onScreen').then((value) => {
        var cscreen =value;
        console.log('screen',cscreen);
        if (data=='AcceptedTc') {
          _that.props.navigation.navigate('VerifyOtpScreen')
        }
      })
    }

    _fbAuth() {
      /*LoginManager.logInWithReadPermissions(['public_profile','email']).then(function (result) {
         if (result.isCancelled) {
           alert('Error','Something went wrong, Please try again!');
         } else {
           AccessToken.getCurrentAccessToken().then((data) => {
               let accessToken = data.accessToken
               // console.log(accessToken.toString()) // access token

               const responseInfoCallback = (error, result) => {
                 if (error) {
                   console.log(error)
                   alert('Error fetching data: ' + error.toString());
                 } else {
                   console.log(result);
                   var data = {
                       device_token: "",
                       device_type: "",
                       username: result.first_name+" "+result.last_name,
                       email: result.email,
                       facebook_id: result.id,
                   };

                   console.log(data);

                   _that.setState({
                       isVisible: true
                   });
                   _that.postToApiCalling('POST', 'login', Constant.URL_socialLogin, data);
                 }
               }

               const infoRequest = new GraphRequest('/me', {
                 accessToken: accessToken,
                 parameters: {
                   fields: {
                     string: 'email,name,first_name,middle_name,last_name'
                   }
                 }
               }, responseInfoCallback);

               // Start the graph request.
               new GraphRequestManager()
                 .addRequest(infoRequest)
                 .start()

             })
         }
        }, function (error) {
         alert('Login fail with error: ' + error);
       });*/
    }

  userLogin() {
    _that.props.navigation.navigate('LoginScreen');
  }

  userSignup() {
    _that.props.navigation.navigate('SignupScreen');
  }



    static navigationOptions = {
        header: null,
    }


    render() {
        return (
          <View style={styles.wrapper}>
            <MyStatusBar barStyle="light-content"  backgroundColor="#3995f7"/>
            <ImageBackground source={bg_image} style={{width: '100%', height: '100%'}}>
              <View style={styles.container1}/>

              <View style={styles.container2}>
                <View style={{width:AppSizes.screen.width-180,height:AppSizes.screen.width/4,justifyContent: 'center', marginLeft: AppSizes.ResponsiveSize.Padding(5)}}>
                        <Image source={logo} style={{flexGrow:1,height:null,width:null,alignItems: 'center',justifyContent:'center',resizeMode:'contain',}} />
                </View>
              </View>

              <View style={styles.container3}>
              </View>

              <View style={styles.container4}>
                <View style={{width:AppSizes.screen.width,height:AppSizes.screen.width/4,justifyContent: 'center',}}>
                        <Image source={location} style={{flexGrow:1,height:null,width:null,alignItems: 'center',justifyContent:'center',resizeMode:'contain',}} />
                </View>
              </View>
              <View style={styles.container5}>

              <TouchableOpacity activeOpacity={.6} onPress={this._fbAuth} style={[styles.buttonContainer,{backgroundColor:'#ffffff'}]}>
                <View style={styles.buttonView}>
                   <View style={styles.buttonimgContainer}>
                           <Image source={fb_icon} style={styles.btnimg} />
                   </View>
                   <View style={styles.buttontextContainer}>
                      <Text style={styles.textlogin}>Login with Facebook</Text>
                    </View>
                  </View>
              </TouchableOpacity>

              <TouchableOpacity activeOpacity={.6} onPress={this._fbAuth} style={[styles.buttonContainer,{backgroundColor:'#ffffff'}]}>
              <View style={styles.buttonView}>
                <View style={styles.buttonimgContainer}>
                    <Image source={google_icon} style={styles.btnimg} />
                </View>
                <View style={styles.buttontextContainer}>
                  <Text style={styles.textlogin}>Login with Google</Text>
                </View>
               </View>
              </TouchableOpacity>

              <TouchableOpacity activeOpacity={.6} onPress={this.userLogin} style={[styles.buttonContainer,{backgroundColor:'#ffffff'}]}>
                <View style={styles.buttonView}>
                 <View style={styles.buttonimgContainer}>
                     <Image source={email_icon} style={styles.btnimg} />
                 </View>
                 <View style={styles.buttontextContainer}>
                      <Text style={styles.textlogin}>Login with Email</Text>
                 </View>
              </View>
              </TouchableOpacity>

              <TouchableOpacity activeOpacity={.6} onPress={this.userSignup} style={styles.buttonContainer}>
                <View style={styles.buttonView}>
                <View style={styles.buttonimgContainer}>

                </View>
                <View style={styles.buttontextContainer}>
                    <Text style={styles.textcreateAccount}>Create account</Text>
                </View>
             </View>
            </TouchableOpacity>
             </View>
            </ImageBackground>
          </View>
        );
    }
}

const styles = {
  statusBar: {
    height: AppSizes.statusBarHeight,
  },
  appBar: {
      height: AppSizes.navbarHeight,
      justifyContent:'center',
    },
  wrapper: {
    flex: 1,
    backgroundColor:'#ffffff'
  },
  container1: {
    flex: 2,
    alignItems:'center',
    //backgroundColor:'#36c4d8'

  },
  container2: {
    flex: 2,
    flexDirection: 'column',
    justifyContent: 'center',
    //backgroundColor:'red'
  },
  container3: {
    flex: 1,
    flexDirection: 'column',
    alignItems: 'center',//replace with flex-end or center
    justifyContent: 'center',
    //backgroundColor:'#36c4d8'
  },

  container4: {
    flex: 3,
    flexDirection: 'column',
    alignItems: 'center',//replace with flex-end or center
    justifyContent: 'center',
    marginBottom: AppSizes.ResponsiveSize.Padding(5),
    //backgroundColor:'blue'
  },
  container5: {
    flex: 10,
    flexDirection: 'column',
    alignItems: 'center',//replace with flex-end or center
    justifyContent: 'flex-start',
    //backgroundColor:'yellow'
  },
buttonContainer:{
   justifyContent: 'center',
   marginBottom:AppSizes.ResponsiveSize.Padding(3),
   width: '90%',
   height:AppSizes.screen.width/8,
   borderWidth:1,
   borderColor:'#ffffff',
},
 buttonView:{
   flexDirection: 'row',
   alignItems: 'center',
   justifyContent:'center',
 },
 buttonimgContainer:{
   width: '15%',
   height:AppSizes.screen.width/15,
   //backgroundColor:'red',
   position:'absolute',
   left:0,
   //top:'10%'
 },
 buttontextContainer:{
   width: '75%',
   //justifyContent:'center',
   alignItems: 'center',
   //backgroundColor:'green',
 },
textcreateAccount:{
  color:'#ffffff',
  fontSize:AppSizes.ResponsiveSize.Sizes(14),
  fontWeight:'500',
  fontFamily:Fonts.RobotoRegular
},
textlogin:{
  color:AppColors.primary,
  fontSize:AppSizes.ResponsiveSize.Sizes(14),
  fontWeight:'500',
  color:'#3995f7',
fontFamily:Fonts.RobotoRegular
},
btnimg:{
  flex: 1,
  width: undefined,
  height: undefined,
  resizeMode:'contain',
},
imageWrapper: {
  width:'100%',
  height:AppSizes.screen.width/8,
},
image: {
  flex: 1,
  width: undefined,
  height: undefined,
  resizeMode:'contain'
},
}
