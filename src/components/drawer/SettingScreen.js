import React, { Component } from 'react';
import {StyleSheet,Text,View,Image,TouchableOpacity,Platform,Alert,ScrollView} from 'react-native';
import { AppStyles, AppSizes, AppColors } from '../../themes/'
import { NavigationActions } from 'react-navigation'
import Spacer from '../common/Spacer'
import * as commonFunctions from '../../utils/CommonFunctions'
import Header from '../common/HeaderWithBackOnly'
import FlipToggle from 'react-native-flip-toggle-button';
var Constant = require('../../api/ApiRules').Constant;
import { Fonts } from '../../utils/Fonts';
/* Images */
var map = require('../../themes/Images/password.png')
var left= require( '../../themes/Images/right-black.png')
var logo = require('../../themes/Images/logoc.png')

let _that
export default class SettingScreen extends Component {
  constructor (props) {
      super(props)
      this.state = {
      qus1:false,
      qus2:false,
      isVisible: false,
      Modal_Visibility: false
    },
    _that = this;
  }


  profileBtn=()=>{
     _that.props.navigation.navigate('EditProfile');
  }



  render() {
    const headerProp = {
      title: 'Setting',
      screens: 'SettingScreen',
    };
    return (
      <View style={styles.container}>
          <Header info={headerProp} navigation={_that.props.navigation}/>

            <View style={styles.content}>
                <View style={styles.container1}>
                  <View style={styles.logobox}>
                        <Image style={styles.logo} source={logo} />
                  </View>
                  <TouchableOpacity activeOpacity={.6} onPress={this.profileBtn} style={styles.questionbox}>
                        <View style={{width:'75%'}}>
                            <Text style={styles.questionheading}>
                                Edit  Profile
                            </Text>
                        </View>
                        <View style={{width:'25%',alignItems:'flex-end',paddingRight:AppSizes.ResponsiveSize.Padding(8)}}>
                            <View style={styles.sideicon}>
                                <Image source={left} style={[styles.ImageStyle,styles.sidearrow]} />
                            </View>
                        </View>
                    </TouchableOpacity>

                    <View style={styles.questionbox}>
                          <View style={{width:'75%'}}>
                              <Text style={styles.questionheading}>
                                    Push Notification
                              </Text>
                          </View>
                          <View style={{width:'25%'}}>
                              <FlipToggle
                                value={this.state.qus1}
                                buttonOnColor={"#3995f7"}
                                buttonOffColor={"#9e9e9e"}
                                sliderOnColor={"#ffffff"}
                                sliderOffColor={"#ffffff"}
                                buttonWidth={AppSizes.screen.width/5}
                                buttonHeight={AppSizes.screen.width/15}
                                buttonRadius={50}
                                onLabel={'On'}
                                offLabel={'Off'}
                                onToggle={(value) => {
                                  this.setState({ qus1: value });
                                  console.log(value);
                                }}
                                changeToggleStateOnLongPress={false}
                                onToggleLongPress={() => {
                                  console.log('Long Press');
                                }}
                              />
                          </View>
                      </View>

                </View>
            </View>
        </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    paddingTop:  0,
  },

  content: {
      flex: 1,
      flexDirection: 'column',
      height:'70%',
      //paddingTop:AppSizes.ResponsiveSize.Padding(3),
      backgroundColor:'#ffffff'
  },
  container1:{
    height:AppSizes.screen.height/4,
    //backgroundColor:'red',

  },
  questionbox:{
  //  backgroundColor:'green',
  marginTop:AppSizes.ResponsiveSize.Padding(3),
    height:50,
    flexDirection:'row',
    borderBottomWidth:1,
    borderBottomColor:'#999',
    justifyContent:'center',
    paddingLeft:AppSizes.ResponsiveSize.Padding(3),
    paddingTop:AppSizes.ResponsiveSize.Padding(2),
    padding:AppSizes.ResponsiveSize.Padding(2)
  },
  questionheading:{
    fontSize:AppSizes.ResponsiveSize.Sizes(14),
    fontWeight:'300',
    fontFamily:Fonts.RobotoRegular,
    //textTransform:'uppercase'
  },
  sideicon:{
      width:'10%',
      alignItems: 'center',
      paddingTop:AppSizes.ResponsiveSize.Padding(2),
      //backgroundColor:'red'
  },
  ImageStyle: {
    height: AppSizes.screen.width/18,
    width: AppSizes.screen.width/18,
    resizeMode : 'contain',
  },
  logobox:{
    width:'100%',
    backgroundColor:'#ddd',
    alignItems:'center',
    justifyContent:'center',
    height:AppSizes.screen.height/4
  },
  logo:{
    width:AppSizes.screen.width/2,
    height:AppSizes.screen.height/10

  }
});
