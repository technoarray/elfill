import React, { Component } from 'react';
import {StyleSheet,Text,View,Image,TouchableOpacity,Platform,Alert,ScrollView,TextInput} from 'react-native';
import { AppStyles, AppSizes, AppColors } from '../../themes/'
import { NavigationActions } from 'react-navigation'
import Spacer from '../common/Spacer'
import * as commonFunctions from '../../utils/CommonFunctions'
import Header from '../common/Header'
import CommonButton from '../common/CommonButton'
import LinearGradient from 'react-native-linear-gradient';
var Constant = require('../../api/ApiRules').Constant;
import { Fonts } from '../../utils/Fonts';
import CalendarPicker from 'react-native-calendar-picker';

/* Images */
const paypal = require('../../themes/Images/paypal.png')
var stripe= require( '../../themes/Images/stripe.png')
//var paypal= require( '../../themes/Images/payments.png')
//var service= require( '../../themes/Images/services.png')
//var servicestation= require( '../../themes/Images/service-station.png')

export default class AccountScreen extends Component {
  constructor (props) {
      super(props)
      this.state = {
      selectedStartDate: null,
      };
      this.onDateChange = this.onDateChange.bind(this);
    _that = this;
  }

  onDateChange(date) {
    this.setState({
      selectedStartDate: date,
    });
  }

  render() {
    const headerProp = {
      title: 'Dashboard',
      screens: 'SideMenu',
    };
    const { selectedStartDate } = this.state;
    const startDate = selectedStartDate ? selectedStartDate.toString() : '';

    return (
      <View style={styles.container}>
      <Header info={headerProp} navigation={_that.props.navigation}/>

            <View style={styles.content}>
                <ScrollView contentContainerStyle={styles.contentContainer}>
                    <View style={styles.container1}>
                      <TouchableOpacity activeOpacity={.6}  style={[styles.servicebox,styles.shadow1]}>
                      <CalendarPicker
                          onDateChange={this.onDateChange}
                          style={styles.calendarbox}
                        />

                        <View>
                          <Text>SELECTED DATE:{ startDate }</Text>
                        </View>

                        </TouchableOpacity>
                        <TouchableOpacity activeOpacity={.6}  style={[styles.servicebox1,styles.shadow,]}>

                                <Text style={styles.heading}>
                                    PREVIOUS
                                </Text>
                                <ScrollView style={styles.heightbox}>
                                <View style={styles.linebox}>

                                    <View style={styles.Valuebox}>
                                        <Text style={styles.value}>
                                            20
                                        </Text>
                                    </View>
                                    <View style={styles.Valueboxb}>
                                        <Text style={styles.textbox}>
                                              Car Station
                                        </Text>
                                    </View>
                                </View>
                                <View style={styles.linebox}>
                                    <View style={styles.Valuebox}>
                                        <Text style={styles.value}>
                                            20
                                        </Text>
                                    </View>
                                    <View style={styles.Valueboxb}>
                                        <Text style={styles.textbox}>
                                              Car Station
                                        </Text>
                                    </View>
                                </View>
                                <View style={styles.linebox}>
                                    <View style={styles.Valuebox}>
                                        <Text style={styles.value}>
                                            20
                                        </Text>
                                    </View>
                                    <View style={styles.Valueboxb}>
                                        <Text style={styles.textbox}>
                                              Car Station
                                        </Text>
                                    </View>
                                </View>
                                <View style={styles.linebox}>
                                    <View style={styles.Valuebox}>
                                        <Text style={styles.value}>
                                            20
                                        </Text>
                                    </View>
                                    <View style={styles.Valueboxb}>
                                        <Text style={styles.textbox}>
                                              Car Station
                                        </Text>
                                    </View>
                                </View>
                                <View style={styles.linebox}>
                                    <View style={styles.Valuebox}>
                                        <Text style={styles.value}>
                                            20
                                        </Text>
                                    </View>
                                    <View style={styles.Valueboxb}>
                                        <Text style={styles.textbox}>
                                              Car Station
                                        </Text>
                                    </View>
                                </View>
                                <View style={styles.linebox}>
                                    <View style={styles.Valuebox}>
                                        <Text style={styles.value}>
                                            20
                                        </Text>
                                    </View>
                                    <View style={styles.Valueboxb}>
                                        <Text style={styles.textbox}>
                                              Car Station
                                        </Text>
                                    </View>
                                </View>
                                <View style={styles.linebox}>
                                    <View style={styles.Valuebox}>
                                        <Text style={styles.value}>
                                            20
                                        </Text>
                                    </View>
                                    <View style={styles.Valueboxb}>
                                        <Text style={styles.textbox}>
                                              Car Station
                                        </Text>
                                    </View>
                                </View>
                                </ScrollView>

                        </TouchableOpacity>
                        <TouchableOpacity activeOpacity={.6}  style={[styles.servicebox1,styles.shadow,]}>

                                <Text style={styles.heading}>
                                    NEXT
                                </Text>
                                <ScrollView style={styles.heightbox}>
                                <View style={styles.linebox}>

                                    <View style={styles.Valuebox}>
                                        <Text style={styles.value}>
                                            20
                                        </Text>
                                    </View>
                                    <View style={styles.Valueboxb}>
                                        <Text style={styles.textbox}>
                                              Car Station
                                        </Text>
                                    </View>
                                </View>
                                <View style={styles.linebox}>
                                    <View style={styles.Valuebox}>
                                        <Text style={styles.value}>
                                            20
                                        </Text>
                                    </View>
                                    <View style={styles.Valueboxb}>
                                        <Text style={styles.textbox}>
                                              Car Station
                                        </Text>
                                    </View>
                                </View>
                                <View style={styles.linebox}>
                                    <View style={styles.Valuebox}>
                                        <Text style={styles.value}>
                                            20
                                        </Text>
                                    </View>
                                    <View style={styles.Valueboxb}>
                                        <Text style={styles.textbox}>
                                              Car Station
                                        </Text>
                                    </View>
                                </View>
                                <View style={styles.linebox}>
                                    <View style={styles.Valuebox}>
                                        <Text style={styles.value}>
                                            20
                                        </Text>
                                    </View>
                                    <View style={styles.Valueboxb}>
                                        <Text style={styles.textbox}>
                                              Car Station
                                        </Text>
                                    </View>
                                </View>
                                <View style={styles.linebox}>
                                    <View style={styles.Valuebox}>
                                        <Text style={styles.value}>
                                            20
                                        </Text>
                                    </View>
                                    <View style={styles.Valueboxb}>
                                        <Text style={styles.textbox}>
                                              Car Station
                                        </Text>
                                    </View>
                                </View>
                                <View style={styles.linebox}>
                                    <View style={styles.Valuebox}>
                                        <Text style={styles.value}>
                                            20
                                        </Text>
                                    </View>
                                    <View style={styles.Valueboxb}>
                                        <Text style={styles.textbox}>
                                              Car Station
                                        </Text>
                                    </View>
                                </View>
                                <View style={styles.linebox}>
                                    <View style={styles.Valuebox}>
                                        <Text style={styles.value}>
                                            20
                                        </Text>
                                    </View>
                                    <View style={styles.Valueboxb}>
                                        <Text style={styles.textbox}>
                                              Car Station
                                        </Text>
                                    </View>
                                </View>
                                </ScrollView>

                        </TouchableOpacity>




                    </View>
                  </ScrollView>

              </View>
        </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    paddingTop:  0,
    backgroundColor:'#fff'
  },

  content: {
      flex: 1,
      padding:'5%',
      paddingBottom:0,
      //backgroundColor:'#000'
      paddingLeft:0.

  },
  container1: {
    flex: 1,
    //backgroundColor:'green',
    flexDirection: 'column',
  },
  servicebox:{
    flexDirection:'column',
    padding:AppSizes.ResponsiveSize.Sizes(5),
    borderRadius: 5,
    marginBottom:AppSizes.ResponsiveSize.Padding(2),
    //backgroundColor:'red',
    //flex:1,
    width:'100%',
justifyContent:'space-between'
  },
  servicebox1:{
    flexDirection:'column',
    borderRadius: 5,
    marginBottom:AppSizes.ResponsiveSize.Padding(2),
    //backgroundColor:'red',
    //flex:1,
    width:'100%',

  },

    shadow:{
     borderWidth:1,
     borderRadius: 2,
     borderColor: '#fff',
     justifyContent:'center',
     backgroundColor:'#fff',
     borderColor: '#ddd',
     borderBottomWidth: 1,
     shadowColor: '#999',
     shadowOffset: { width: 0, height: 2 },
     shadowOpacity: 0.8,
     shadowRadius: 2,
     elevation: 0,
     //backgroundColor:'red',

    },
contentContainer:{
  flex:1
},
title:{
  fontSize:AppSizes.ResponsiveSize.Sizes(18),
  paddingBottom: AppSizes.ResponsiveSize.Sizes(5),
  color:'#333',
  fontWeight:'700',
  fontFamily:Fonts.RobotoBold,
},



    SectionRadioView: {
      borderBottomWidth: 1,
      borderBottomColor: '#ffffff',
      borderRadius: 5 ,
      width:AppSizes.screen.width*90/90,
      //marginLeft: AppSizes.ResponsiveSize.Padding(5),
      //marginRight: AppSizes.ResponsiveSize.Padding(5),
      marginBottom: (Platform.OS === 'ios') ? AppSizes.ResponsiveSize.Padding(1) :AppSizes.ResponsiveSize.Padding(2),

    },
heading:{
    backgroundColor:'#492c59',
    padding:AppSizes.ResponsiveSize.Padding(2),
    color:'#fff',
    fontFamily:Fonts.RobotoRegular,
},
linebox:{
  //padding:AppSizes.ResponsiveSize.Padding(2),
  flexDirection:'row',
  //backgroundColor:'green',
  borderBottomColor:'#999',
  borderBottomWidth:1,
  padding:AppSizes.ResponsiveSize.Padding(1)
},

Valuebox:{
  backgroundColor:'#492c59',
  //padding:AppSizes.ResponsiveSize.Padding(2),
  borderRadius:50,
  padding:AppSizes.ResponsiveSize.Padding(2),
  height:40,
  //width:45,
  //justifyContent:'center',
  alignItems:'center',

},
Valueboxb:{
    width:'100%',
    //backgroundColor:'gray',
    justifyContent:'center',
    paddingLeft:AppSizes.ResponsiveSize.Padding(3)
},

value:{
  fontSize:AppSizes.ResponsiveSize.Sizes(12),
  color:'#fff',
  fontWeight:'400',
fontFamily:Fonts.RobotoRegular,
  //backgroundColor:'yellow'
},
textbox:{
  fontWeight:'400',
  fontFamily:Fonts.RobotoRegular,
  fontSize:AppSizes.ResponsiveSize.Sizes(14),
  marginTop:AppSizes.ResponsiveSize.Padding(-5)
},
heightbox:{
  height:200
}
});
