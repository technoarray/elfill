import React, { Component } from 'react';
import {StyleSheet,Text,View,Image,TouchableOpacity,Platform,Alert,ScrollView,TextInput} from 'react-native';
import { AppStyles, AppSizes, AppColors } from '../../themes/'
import { NavigationActions } from 'react-navigation'
import Spacer from '../common/Spacer'
import * as commonFunctions from '../../utils/CommonFunctions'
import Header from '../common/Header'
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view'
import CommonButton from '../common/CommonButton'
import { Fonts } from '../../utils/Fonts';
var Constant = require('../../api/ApiRules').Constant;

/* Images */
const bg_image = require('../../themes/Images/bg_image.png')
const left_arrow = require('../../themes/Images/left-arrow.png')
const profile_icon = require('../../themes/Images/profile.png')
const email_icon = require('../../themes/Images/email.png')
const password_icon = require('../../themes/Images/password.png')
const device_icon = require('../../themes/Images/car.png')
const modal_icon = require('../../themes/Images/modal.png')
const mobile_icon = require('../../themes/Images/mobile.png')

export default class AccountScreen extends Component {
  constructor (props) {
      super(props)
      this.state = {
      isVisible: false,
      Modal_Visibility: false
    },
    _that = this;
  }

  updateAlert = () => {
        this.setState({Modal_Visibility: !this.state.Modal_Visibility});
  }

  website_link() {
    Alert.alert(
        'elFill',
        'You will be redirected to your default browser, to visit our Website',
        [
          {text: 'Cancel', onPress: () => console.log('Cancel Button Pressed'), style: 'cancel'},
          {text: 'OK', onPress: () => Linking.openURL(Constant.SITE_URL)},

        ]

      )
  }

  render() {
    const headerProp = {
      title: 'Account',
      screens: 'AccountScreen',
    };
    return (
      <View style={styles.container}>
      <Header info={headerProp} navigation={_that.props.navigation}/>

        <View style={styles.content}>
        <KeyboardAwareScrollView innerRef={() => {return [this.refs.name,this.refs.email, this.refs.password]}} >

          <View style={styles.container2}>

            <View style={styles.SectionStyle}>
                <Image source={profile_icon} style={styles.ImageStyle} />
                <TextInput
                  autoCapitalize={'none'}
                  autoCorrect={false}
                  underlineColorAndroid="transparent"
                  returnKeyType={ "next"}
                  selectionColor={"#000000"}
                  autoFocus={ false}
                  placeholder="Full Name"
                  placeholderTextColor="#000000"
                  style={styles.textInput}
                  ref="name"
                  keyboardType={ 'email-address'}
                  onChangeText={name=> this.setState({name})}
                />
            </View>

            <View style={styles.SectionStyle}>
                <Image source={email_icon} style={styles.ImageStyle} />
                <TextInput
                  autoCapitalize={'none'}
                  autoCorrect={false}
                  underlineColorAndroid="transparent"
                  returnKeyType={ "next"}
                  selectionColor={"#000000"}
                  autoFocus={ false}
                  placeholder="Email Address"
                  placeholderTextColor="#000000"
                  style={styles.textInput}
                  ref="email"
                  keyboardType={ 'email-address'}
                  onChangeText={email=> this.setState({email})}
                />
            </View>

            <View style={styles.SectionStyle}>
                <Image source={password_icon} style={styles.ImageStyle} />
                <TextInput
                  autoCapitalize={'none'}
                  autoCorrect={false}
                  underlineColorAndroid="transparent"
                  returnKeyType={ "next"}
                  selectionColor={"#000000"}
                  autoFocus={ false}
                  placeholder="Password"
                  placeholderTextColor="#000000"
                  style={styles.textInput}
                  ref="password"
                  secureTextEntry={true}
                  keyboardType={ 'email-address'}
                  onChangeText={password=> this.setState({password})}
                />
            </View>

            <View style={styles.SectionStyle}>
                <Image source={mobile_icon} style={styles.ImageStyle} />
                <TextInput
                  autoCapitalize={'none'}
                  autoCorrect={false}
                  underlineColorAndroid="transparent"
                  returnKeyType={ "next"}
                  selectionColor={"#000000"}
                  autoFocus={ false}
                  placeholder="Mobile Number"
                  placeholderTextColor="#000000"
                  style={styles.textInput}
                  ref="mobile"
                  keyboardType={ 'number-pad'}
                  onChangeText={mobile=> this.setState({mobile})}
                />
            </View>
            <View style={styles.SectionStyle}>
                <Image source={profile_icon} style={styles.ImageStyle} />
                <TextInput
                  autoCapitalize={'none'}
                  autoCorrect={false}
                  underlineColorAndroid="transparent"
                  returnKeyType={ "next"}
                  selectionColor={"#000000"}
                  autoFocus={ false}
                  placeholder="Buyer or seller"
                  placeholderTextColor="#000000"
                  style={styles.textInput}
                  ref="user_type"
                  keyboardType={ 'email-address'}
                  onChangeText={user_type=> this.setState({user_type})}
                />
            </View>
            <View style={styles.SectionStyle}>
                <Image source={device_icon} style={styles.ImageStyle} />
                <TextInput
                  autoCapitalize={'none'}
                  autoCorrect={false}
                  underlineColorAndroid="transparent"
                  returnKeyType={ "next"}
                  selectionColor={"#000000"}
                  autoFocus={ false}
                  placeholder="Your Device"
                  placeholderTextColor="#000000"
                  style={styles.textInput}
                  ref="device"
                  keyboardType={ 'email-address'}
                  onChangeText={device=> this.setState({device})}
                />
            </View>

            <View style={styles.SectionStyle}>
                <Image source={modal_icon} style={styles.ImageStyle} />
                <TextInput
                  autoCapitalize={'none'}
                  autoCorrect={false}
                  underlineColorAndroid="transparent"
                  returnKeyType={ "next"}
                  selectionColor={"#000000"}
                  autoFocus={ false}
                  placeholder="Modal Number"
                  placeholderTextColor="#000000"
                  style={styles.textInput}
                  ref="modal"
                  keyboardType={ 'email-address'}
                  onChangeText={modal=> this.setState({modal})}
                />
            </View>

                <TouchableOpacity onPress={this.submit_click}>
                <View style={[styles.SectionStyle,{marginTop:'5%',borderBottomColor: '#4b088c',}]}>
                  <CommonButton label='Edit Profile' width='100%'/>
                  </View>
                </TouchableOpacity>

            </View>

            <View style={styles.container3}>

            </View>
            </KeyboardAwareScrollView>

        </View>
    </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    paddingTop:  0,
  },

  content: {
      flex: 1,
      flexDirection: 'column',
      height:'70%',
      padding:'5%',
      backgroundColor:'#ffffff'
  },
  container1: {
    flex: 3,
    height:AppSizes.screen.width/4,
    paddingLeft:AppSizes.ResponsiveSize.Padding(5),
    //backgroundColor:'red'
  },

  container2: {
    flex: 10,
    height:'70%',
    flexDirection: 'column',
    alignItems: 'center',//replace with flex-end or center
    justifyContent: 'center',
    //backgroundColor:'blue'
  },
  container3: {
    flex: 3,
    height:AppSizes.screen.width/8,
    alignItems: 'center',//replace with flex-end or center
    justifyContent: 'flex-end',
    //backgroundColor:'red'
  },

headerTitle:{
  fontSize:AppSizes.ResponsiveSize.Sizes(25),
  color:'#000000',
  fontWeight:'bold',
  paddingBottom:AppSizes.ResponsiveSize.Padding(1),
  fontFamily:Fonts.RobotoBold,
},
headersubTitle:{
  fontSize:AppSizes.ResponsiveSize.Sizes(14),
  color:'#000000',
  fontWeight:'600',
  paddingTop:AppSizes.ResponsiveSize.Padding(2),
  fontFamily:Fonts.RobotoBold,
},
headerBorder :{
  borderBottomColor: '#000000',
  borderBottomWidth: 3,
  width:'10%',
},
footertext :{
 color :'#000000',
 fontSize:AppSizes.ResponsiveSize.Sizes(12),
 fontWeight:'400',
 fontFamily:Fonts.RobotoBold,
},
SectionStyle: {
  flexDirection: 'row',
  justifyContent: 'center',
  alignItems: 'center',
  borderBottomWidth: 1,
  borderBottomColor: '#000000',
  borderRadius: 5 ,
  marginLeft: AppSizes.ResponsiveSize.Padding(5),
  marginRight: AppSizes.ResponsiveSize.Padding(5),
  marginBottom: (Platform.OS === 'ios') ? AppSizes.ResponsiveSize.Padding(1) :AppSizes.ResponsiveSize.Padding(2),
  height:40,
},

ImageStyle: {
  height: AppSizes.screen.width/21,
  width: AppSizes.screen.width/21,
  resizeMode : 'contain',
},
textInput: {
flex:1,
marginLeft: AppSizes.ResponsiveSize.Padding(2),
fontSize: AppSizes.ResponsiveSize.Sizes(12),
color:'#000000',
fontFamily:Fonts.RobotoRegular,
},

});
