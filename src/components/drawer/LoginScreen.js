import React, { Component } from 'react';
import {AsyncStorage,StyleSheet,Text,View,Image,TouchableOpacity,Platform,ScrollView,Alert,ImageBackground,StatusBar,TextInput} from 'react-native';
import { AppStyles, AppSizes, AppColors } from '../../themes/'
import { NavigationActions, StackActions  } from 'react-navigation';
import Spacer from '../common/Spacer'
import Spinner from 'react-native-loading-spinner-overlay';

import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view'
import Header from '../common/HeaderBeforeLogin'
import CommonButton from '../common/CommonButton1'
import * as commonFunctions from '../../utils/CommonFunctions'
import { Fonts } from '../../utils/Fonts';

var Constant = require('../../api/ApiRules').Constant;
var WebServices = require('../../api/ApiRules').WebServices;

const bg_image = require('../../themes/Images/bg_image.png')
const left_arrow = require('../../themes/Images/left-arrow.png')
const password_icon = require('../../themes/Images/password.png')
const email_icon = require('../../themes/Images/email.png')

const MyStatusBar = ({backgroundColor, ...props}) => (
  <View style={[styles.statusBar, { backgroundColor }]}>
    <StatusBar translucent backgroundColor={backgroundColor} {...props} />
  </View>
);

let _that
export default class Login extends Component {
  constructor (props) {
    super(props)
    this.state = {
    isVisible: false,
    Modal_Visibility: false,
    email : '',
    password : '',
    emailerr:'',
    pwderr:'',
    devicetype:''
  },
  _that = this;
}

arrow_click = () => {
      _that.props.navigation.navigate('MainScreen');
}
signup_click=() =>{
  _that.props.navigation.navigate('SignupScreen');
}
userVerify=() =>{
  _that.props.navigation.navigate('VerifyOtpScreen');
}

submit_click=() =>{
  _that.validationAndApiParameter()

}

componentWillMount(){
  if(Platform.OS === 'ios'){
    this.setState({devicetype:'1'})
  }
  else{
    this.setState({devicetype:'0'})
  }
}

validationAndApiParameter() {
      const { method, devicetoken, devicetype, email, password, isVisible } = this.state

      var error=0;

      if ((email.indexOf(' ') >= 0 || email.length <= 0)) {
        _that.setState({emailerr:'Please enter Email address'})
        error=1;
      }else if (!commonFunctions.validateEmail(email)) {
        _that.setState({emailerr:'Please enter valid Email address'})
        error=1;
      }
      else{
          _that.setState({emailerr:''})
      }

      if ((password.indexOf(' ') >= 0 || password.length <= 0)) {
        _that.setState({pwderr:'Please enter password'})
        error=1;
      }
      else{
          _that.setState({pwderr:''})
      }

      if(error==0){
        const data = new FormData();
        data.append('device_token', devicetoken);
        data.append('device_type', devicetype);
        data.append('email', email);
        data.append('password', password);

        console.log(data);

        _that.setState({isVisible: true});
        this.postToApiCalling('POST', 'login', Constant.URL_login, data);
      }
    }

postToApiCalling(method, apiKey, apiUrl, data) {

   new Promise(function(resolve, reject) {
        if (method == 'POST') {
            resolve(WebServices.callWebService(apiUrl, data));
        } else {
            resolve(WebServices.callWebService_GET(apiUrl, data));
        }
    }).then((jsonRes) => {
      _that.setState({ isVisible: false })

        if ((!jsonRes) || (jsonRes.code == 0)) {

        setTimeout(()=>{
            Alert.alert('Login Error',jsonRes.message);
        },200);

        } else {
            if (jsonRes.code == 1) {
                _that.apiSuccessfullResponse(apiKey, jsonRes)
            }
        }
    }).catch((error) => {
        console.log("ERROR" + error);
        _that.setState({ isVisible: false })

        setTimeout(()=>{
            Alert.alert("Server issue");
        },200);
    });
}

apiSuccessfullResponse(apiKey, jsonRes) {
    if (apiKey == 'login') {
        jdata=jsonRes.data;

        AsyncStorage.setItem("loggedIn", JSON.stringify(true)).done();
        AsyncStorage.setItem('UserData', JSON.stringify(jdata));
        AsyncStorage.setItem("user_type", JSON.stringify(jdata.user_type));
            const resetAction = StackActions.reset({
                  index: 0,
                  actions: [NavigationActions.navigate({ routeName: 'StartScreen' })],
              });
          _that.props.navigation.dispatch(resetAction);

    }
}

  userForgetPassword() {
    _that.props.navigation.navigate('ForgetPasswordScreen');

  }

  render() {

    return (
      <View style={styles.wrapper}>
      <MyStatusBar barStyle="light-content"  backgroundColor="#00469a"/>

        <ImageBackground source={bg_image} style={{width: '100%', height: '100%'}}>
        <View style={styles.appBar} >
              <View style={styles.imageContainer}>
                <TouchableOpacity style={styles.menuWrapper} onPress={this.arrow_click}>
                  <Image style={styles.image} source={left_arrow}/>
                  </TouchableOpacity>
              </View>

        </View>
        <View style={{flex:1}}>
        <KeyboardAwareScrollView innerRef={() => {return [this.refs.email, this.refs.password]}} >

        <Text style={styles.text}> { this.state.getValue } </Text>
          <View style={styles.container1}>
            <Text style={styles.headerTitle}>Login</Text>
            <View style={styles.headerBorder}/>
          </View>

          <View style={styles.container2}>

            <View style={styles.SectionStyle}>
                <Image source={email_icon} style={styles.ImageStyle} />
                <TextInput
                  autoCapitalize={'none'}
                  autoCorrect={false}
                  underlineColorAndroid="transparent"
                  returnKeyType={ "next"}
                  selectionColor={"#FFFFFF"}
                  autoFocus={ false}
                  placeholder="Email Address"
                  placeholderTextColor="#fff"
                  style={styles.textInput}
                  ref="email"
                  keyboardType={ 'email-address'}
                  onFocus={ () => this.setState({emailerr:''}) }
                  onChangeText={email=> this.setState({email})}
                />
            </View>
            { !(this.state.emailerr) ? null :
              <Text style={styles.error}>{this.state.emailerr}</Text>
            }
            <View style={styles.SectionStyle}>
                <Image source={password_icon} style={styles.ImageStyle} />
                <TextInput
                  autoCapitalize={'none'}
                  autoCorrect={false}
                  underlineColorAndroid="transparent"
                  returnKeyType={ "next"}
                  selectionColor={"#FFFFFF"}
                  autoFocus={ false}
                  placeholder="Password"
                  placeholderTextColor="#fff"
                  style={styles.textInput}
                  ref="password"
                  onFocus={ () => this.setState({pwderr:''}) }
                  secureTextEntry={true}
                  onChangeText={password=> this.setState({password})}
                />
            </View>
            { !(this.state.pwderr) ? null :
              <Text style={styles.error}>{this.state.pwderr}</Text>
            }

            <TouchableOpacity onPress={this.submit_click} style={styles.btncon}>
              <CommonButton label='Login' width='100%'/>
            </TouchableOpacity>

            <View style={styles.forgetContainer}>
               <TouchableOpacity activeOpacity={.6} onPress={this.userForgetPassword}>
                 <Text style={styles.textforget}>Forget Password ?</Text>
               </TouchableOpacity>
            </View>

          {/*  <View style={styles.verifyContainer}>
               <TouchableOpacity activeOpacity={.6} onPress={this.userVerify}>
                 <Text style={styles.textverify}>OTP Verification</Text>
                 </TouchableOpacity>
             </View>*/}

          </View>
          </KeyboardAwareScrollView>
          </View>
          <View style={styles.container3}>
            <TouchableOpacity onPress={this.signup_click}>
              <Text style={styles.footertext}>Dont have an account?
              <Text style={{fontWeight:'bold'}}> Sign Up </Text></Text>
            </TouchableOpacity>
          </View>

        </ImageBackground>
        <Spinner visible={this.state.isVisible}  />

     </View>

    );
  }
}

const styles = StyleSheet.create({
  wrapper: {
    flex: 1,
    backgroundColor:'#ffffff'
  },
  statusBar: {
    height: AppSizes.statusBarHeight,
  },

  appBar: {
      height: AppSizes.navbarHeight,
      justifyContent:'flex-end',
      alignItems:'flex-start',
      paddingLeft:AppSizes.ResponsiveSize.Padding(5),
    },
    imageContainer:{
      width:'20%'
    },
    menuWrapper: {
      width:'60%',
      height:'60%',
    },
   image: {
      flex: 1,
      width: undefined,
      height: undefined,
      resizeMode:'contain'
    },

  container1: {
    flex: 3,
    height:AppSizes.screen.width/4,
    paddingLeft:AppSizes.ResponsiveSize.Padding(5),
    //backgroundColor:'red'
  },

  container2: {
    flex: 10,
    height:AppSizes.screen.width+20,
    flexDirection: 'column',
    alignItems: 'center',//replace with flex-end or center
    justifyContent: 'center',
  //  backgroundColor:'blue'
  },
  container3: {
    flex: .1,
  //  height:AppSizes.screen.width/5,
    alignItems: 'center',//replace with flex-end or center
    justifyContent: 'center',
    //backgroundColor:'red'
  },
  headerTitle:{
    fontSize:AppSizes.ResponsiveSize.Sizes(35),
    color:'#ffffff',
    fontWeight:'bold',
    paddingBottom:AppSizes.ResponsiveSize.Padding(3),
    fontFamily:Fonts.RobotoBold
  },
  headersubTitle:{
    fontSize:AppSizes.ResponsiveSize.Sizes(14),
    color:'#ffffff',
    fontWeight:'600',
    paddingTop:AppSizes.ResponsiveSize.Padding(2),
    fontFamily:Fonts.RobotoBold
  },
  headerBorder :{
    borderBottomColor: '#ffffff',
    borderBottomWidth: 3,
    width:'10%',
 },
 footertext :{
   color :'#ffffff',
   fontSize:AppSizes.ResponsiveSize.Sizes(12),
   fontWeight:'400',
   fontFamily:Fonts.RobotoRegular
 },
 SectionStyle: {
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    borderBottomWidth: 1,
    borderBottomColor: '#ffffff',
    borderRadius: 5 ,
    marginLeft: AppSizes.ResponsiveSize.Padding(10),
    marginRight: AppSizes.ResponsiveSize.Padding(10),
    marginBottom: AppSizes.ResponsiveSize.Padding(3),
    height:40,
},

ImageStyle: {
    height: AppSizes.screen.width/21,
    width: AppSizes.screen.width/21,
    resizeMode : 'contain',
},
textInput: {
  flex:1,
  marginLeft: AppSizes.ResponsiveSize.Padding(2),
  fontSize: AppSizes.ResponsiveSize.Sizes(14),
  color:'#ffffff',
  fontFamily:Fonts.RobotoRegular
  },
btncon:{
  marginLeft: AppSizes.ResponsiveSize.Padding(10),
  marginRight: AppSizes.ResponsiveSize.Padding(10),
  marginTop: AppSizes.ResponsiveSize.Padding(5),
},
error:{
  color:'red',
  fontFamily:Fonts.RobotoRegular
},
verifyContainer:{
  marginTop: AppSizes.ResponsiveSize.Padding(5),
  width:'80%',
},
textverify:{
 color:'#ffffff',
 fontSize:AppSizes.ResponsiveSize.Sizes(12),
 textAlign:'right',
 fontFamily:Fonts.RobotoRegular
},
forgetContainer:{
  width:'80%',
  marginLeft: AppSizes.ResponsiveSize.Padding(10),
  marginRight: AppSizes.ResponsiveSize.Padding(10),
  marginTop: AppSizes.ResponsiveSize.Padding(5),
},
textforget:{
 color:'#ffffff',
 fontSize:AppSizes.ResponsiveSize.Sizes(12),
 textAlign:'right',
 fontFamily:Fonts.RobotoRegular
},
});
