import React, { Component } from 'react';
import {AsyncStorage,StyleSheet,Text,View,Image,TouchableOpacity,Platform,Alert,ScrollView,TextInput} from 'react-native';
import { Calendar, CalendarList, Agenda } from 'react-native-calendars';
import { AppStyles, AppSizes, AppColors } from '../../themes/'
import { NavigationActions } from 'react-navigation'
import Spinner from 'react-native-loading-spinner-overlay';
import Header from '../common/HeaderWithBack'
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view'
import LinearGradient from 'react-native-linear-gradient';
import { Fonts } from '../../utils/Fonts';
var Constant = require('../../api/ApiRules').Constant;
var WebServices = require('../../api/ApiRules').WebServices;

let _that
export default class MyBookings extends Component {

  constructor(props){
    super(props)
    this.state = {
      isVisible: false,
      uid:'',
      dataSource: [],
    },
    _that=this
  }

  componentWillMount(){
      AsyncStorage.getItem('UserData').then((UserData) => {
      var data = JSON.parse(UserData);
        uid=data.id
        if(uid != ''){
          _that.setState({uid:uid})
          _that.validationAndApiParameter('mybooking')
        }
    })
  }

  validationAndApiParameter(apiKey) {
    const { uid,isVisible } = this.state
       if(apiKey=='mybooking'){
         const data = new FormData();
         data.append('uid', uid);
          console.log(data);
         _that.setState({isVisible: true});

        _that.postToApiCalling('POST', apiKey, Constant.URL_mybooking, data);
      }
    }

    postToApiCalling(method, apiKey, apiUrl, data) {
       new Promise(function(resolve, reject) {
            if (method == 'POST') {
                resolve(WebServices.callWebService(apiUrl, data));
            } else {
                resolve(WebServices.callWebService_GET(apiUrl, data));
            }
        }).then((jsonRes) => {
          _that.setState({ isVisible: false })
            if ((!jsonRes) || (jsonRes.code == 0)) {
            setTimeout(()=>{
                Alert.alert('Error',jsonRes.message);
            },200);
            } else {
                _that.apiSuccessfullResponse(apiKey, jsonRes)
            }
        }).catch((error) => {
            console.log("ERROR" + error);
            _that.setState({ isVisible: false })

            // setTimeout(()=>{
            //     Alert.alert("Server issue");
            // },200);
        });
    }

    apiSuccessfullResponse(apiKey, jsonRes) {
        if (apiKey == 'mybooking') {
          //console.log(jsonRes)
          if(jsonRes.code!=2){
            mybooking=jsonRes.result
            console.log(mybooking)
            _that.setState({
               dataSource: mybooking,
             });
          }

        }

    }


  viewBtn=(item)=>{
    console.log(item);
     _that.props.navigation.navigate('BookingDetails',{item:item});
  }
  render(){
    const headerProp = {
      title: 'My Bookings',
      screens: 'MyBookings',
        type:''
    };

    return(
      <View style={styles.container}>
      <Header info={headerProp} navigation={_that.props.navigation}/>
          <View style={styles.content}>
          {this.state.dataSource && this.state.dataSource.length > 0 ?
            <ScrollView style={{height:'100%'}}>
            {
              this.state.dataSource.map((item, index) => (
                <TouchableOpacity key={item.order_time} style={styles.container1} onPress={() => this.viewBtn(item)} activeOpacity={.6}>
                  <LinearGradient colors={['#fff','#fff','#fff','#fff']} style={[styles.linearGradient,styles.servicebox,styles.shadow]}>
                    <View  style={styles.titleblk}>
                      <View style={styles.titlesec}>
                        <Text style={styles.title}>{item.name}</Text>
                        <Text style={styles.price}>${item.price}</Text>
                      </View>
                        <Text style={styles.stationname}>{item.station_name}</Text>
                        <Text style={styles.smalltext}>Order Date : {item.order_date} {item.order_time}</Text>
                    </View>
                  </LinearGradient>
                </TouchableOpacity>
              ))
           }

            </ScrollView>
            :
            <Text style={{textAlign:'center'}}>No Record Found</Text>
           }
          </View>
          <Spinner visible={this.state.isVisible}  />

      </View>
    )
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    paddingTop:  0,
  },
  titleblk:{
    width:'100%',
    height:'100%',
    paddingLeft:13
  },
  title:{
    fontSize:AppSizes.ResponsiveSize.Sizes(20),
    paddingBottom: AppSizes.ResponsiveSize.Sizes(2),
    color:'#3995f7',
    fontWeight:'800',
    width:'70%',
    fontFamily:Fonts.RobotoBold
  },
  smalltext:{
    fontSize:AppSizes.ResponsiveSize.Sizes(10),
    color:'#000',
    fontWeight:'400',
    fontFamily:Fonts.RobotoRegular
  },
  content: {
    flex: 1,
    paddingLeft:'2%',
    paddingRight:'2%',
    paddingTop:'2%',
  //  backgroundColor:'#fff',
    //backgroundColor:'red'
  },
  container1: {
    height:AppSizes.screen.height/7,
    flexDirection: 'column',
  //  backgroundColor:'gray'
  },
  servicebox:{
    flexDirection:'row',
    padding:AppSizes.ResponsiveSize.Sizes(15),
    borderRadius: 5,
    marginBottom:AppSizes.ResponsiveSize.Padding(2),
  },
titlesec:{
  flexDirection:'row'
},
price:{
  width:'30%',
// backgroundColor:'red',
  textAlign:'center',
  color:'#3995f7',
  fontSize:AppSizes.ResponsiveSize.Sizes(17),
  fontWeight:'600',
  marginTop:2,
  fontFamily:Fonts.RobotoBold
},
stationname:{
  fontSize:AppSizes.ResponsiveSize.Sizes(13),
  color:'#000',
  fontWeight:'400',
  fontFamily:Fonts.RobotoRegular
},
shadow:{
 borderWidth:1,
 borderRadius: 2,
 borderColor: '#fff',
 justifyContent:'center',
 backgroundColor:'#fff',
 borderColor: '#ddd',
 borderBottomWidth: 1,
 shadowColor: '#999',
 shadowOffset: { width: 0, height: 2 },
 shadowOpacity: 0.8,
 shadowRadius: 2,
 elevation: 0,
 //backgroundColor:'red',

},



})
