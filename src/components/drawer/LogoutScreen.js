import React, { Component } from 'react';
import { Fonts } from '../../utils/Fonts';
import {
    View, AsyncStorage, Alert
} from 'react-native';
import { NavigationActions } from 'react-navigation'

var _that = ''
class LogoutScreen extends Component {

    constructor(props) {
        super(props)
        _that = this
    }

    componentDidMount() {
      AsyncStorage.removeItem('loggedIn');
      AsyncStorage.removeItem('user_type');
        AsyncStorage.removeItem('UserData', function () {
            //Alert.alert("You are logout successfully.")

            setTimeout(function() {
              //const resetAction = NavigationActions.navigate({ routeName: 'MainScreen'})
              _that.props.navigation.navigate('MainScreen');
            }, 2000);
        });
    }

    static navigationOptions = {
        header: null,
    }
    render() {
        return (
        	<View></View>
        );
    }
}


export default LogoutScreen;
