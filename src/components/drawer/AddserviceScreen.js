import React, { Component } from 'react';
import {AsyncStorage,StyleSheet,Text,View,Image,PixelRatio,TouchableOpacity,Platform,Alert,ScrollView,TextInput} from 'react-native';
import { AppStyles, AppSizes, AppColors } from '../../themes/'
import { NavigationActions } from 'react-navigation'
import Spinner from 'react-native-loading-spinner-overlay';
import * as commonFunctions from '../../utils/CommonFunctions'
import Header from '../common/HeaderWithBack'
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view'
import CommonButton from '../common/CommonButton'
import ImagePicker from 'react-native-image-picker';
import Picker from 'react-native-q-picker';
import {RadioGroup, RadioButton} from 'react-native-flexi-radio-button';
import { pickerData } from '../data/pickerData';
import CheckBox from 'react-native-check-box';
import { Fonts } from '../../utils/Fonts';

var Constant = require('../../api/ApiRules').Constant;
var WebServices = require('../../api/ApiRules').WebServices;

/* Images */
const bg_image = require('../../themes/Images/bg_image.png')
const left_arrow = require('../../themes/Images/left-arrow.png')
const arrow_right = require('../../themes/Images/right_arrow.png')
const arrow_down = require('../../themes/Images/down_arrow.png')
const profile_icon = require('../../themes/Images/profile.png')
const email_icon = require('../../themes/Images/email.png')
const password_icon = require('../../themes/Images/password.png')
const device_icon = require('../../themes/Images/car.png')
const modal_icon = require('../../themes/Images/modal.png')
const mobile_icon = require('../../themes/Images/mobile.png')

var numberchargeitems = [];

for( var i = 1; i <=20; i++){
  var numbers = {
      name: i+"",
      id: i
  };
  numberchargeitems.push(numbers);
}

let _that
export default class AddserviceScreen extends Component {
  constructor (props) {
      super(props)
      this.state = {
      isVisible: false,
      uid:'',
      station_id:this.props.navigation.state.params.item.id,
    },
    _that = this;
  }

  componentWillMount(){
    AsyncStorage.getItem('UserData').then((UserData) => {
      var data = JSON.parse(UserData);
        uid=data.id
        if(uid != ''){
          _that.setState({
            uid:uid,
          })
        }
    })
  }

  handleOnPress(){
    console.log(this.state.station_id);
    var selected=[]
    if(this.state.el_car_charge){
      selected.push('el_car_charge')
    }
    if(this.state.el_car_park){
      selected.push('el_car_park')
    }
    if(this.state.el_car_bed){
      selected.push('el_car_bed')
    }
    if(this.state.el_car_cable){
      selected.push('el_car_cable')
    }
    if(this.state.el_car_rent){
      selected.push('el_car_rent')
    }
    if(this.state.el_bike_charge){
      selected.push('el_bike_charge')
    }
    if(this.state.el_bike_bed){
      selected.push('el_bike_bed')
    }
    if(this.state.el_bike_park){
      selected.push('el_bike_park')
    }
    if(this.state.el_bike_rent){
      selected.push('el_bike_rent')
    }
    if(this.state.el_bike_cable){
      selected.push('el_bike_cable')
    }
    if(this.state.gadget_charge){
      selected.push('gadget_charge')
    }
    if(this.state.gadget_bed){
      selected.push('gadget_bed')
    }
    if(this.state.gadget_rent){
      selected.push('gadget_rent')
    }
    if(this.state.gadget_cable){
      selected.push('gadget_cable')
    }

    console.log(selected.length);

    if(selected.indexOf('el_car_charge')!=-1){
      selected.splice(selected.indexOf('el_car_charge'), 1);
      _that.props.navigation.navigate('El_car_charge',{selected:selected,station_id:this.state.station_id})
    }
    else if (selected.indexOf('el_car_park')!=-1) {
      selected.splice(selected.indexOf('el_car_park'), 1);
      _that.props.navigation.navigate('El_car_park',{selected:selected,station_id:this.state.station_id})
    }
    else if (selected.indexOf('el_car_bed')!=-1) {
      selected.splice(selected.indexOf('el_car_bed'), 1);
      _that.props.navigation.navigate('El_car_bed',{selected:selected,station_id:this.state.station_id})
    }
    else if (selected.indexOf('el_car_cable')!=-1) {
      selected.splice(selected.indexOf('el_car_cable'), 1);
      _that.props.navigation.navigate('El_car_cable',{selected:selected,station_id:this.state.station_id})
    }
    else if (selected.indexOf('el_car_rent')!=-1) {
      selected.splice(selected.indexOf('el_car_rent'), 1);
      _that.props.navigation.navigate('El_car_rent',{selected:selected,station_id:this.state.station_id})
    }
    else if (selected.indexOf('el_bike_charge')!=-1) {
      selected.splice(selected.indexOf('el_bike_charge'), 1);
      _that.props.navigation.navigate('El_bike_charge',{selected:selected,station_id:this.state.station_id})
    }
    else if (selected.indexOf('el_bike_park')!=-1) {
      selected.splice(selected.indexOf('el_bike_park'), 1);
      _that.props.navigation.navigate('El_bike_park',{selected:selected,station_id:this.state.station_id})
    }
    else if (selected.indexOf('el_bike_bed')!=-1) {
      selected.splice(selected.indexOf('el_bike_bed'), 1);
      _that.props.navigation.navigate('El_bike_bed',{selected:selected,station_id:this.state.station_id})
    }
    else if (selected.indexOf('el_bike_cable')!=-1) {
      selected.splice(selected.indexOf('el_bike_cable'), 1);
      _that.props.navigation.navigate('El_bike_cable',{selected:selected,station_id:this.state.station_id})
    }
    else if (selected.indexOf('el_bike_rent')!=-1) {
      selected.splice(selected.indexOf('el_bike_rent'), 1);
      _that.props.navigation.navigate('El_bike_rent',{selected:selected,station_id:this.state.station_id})
    }
    else if (selected.indexOf('gadget_charge')!=-1) {
      selected.splice(selected.indexOf('gadget_charge'), 1);
      _that.props.navigation.navigate('Gadget_charge',{selected:selected,station_id:this.state.station_id})
    }
    else if (selected.indexOf('gadget_bed')!=-1) {
      selected.splice(selected.indexOf('gadget_bed'), 1);
      _that.props.navigation.navigate('Gadget_bed',{selected:selected,station_id:this.state.station_id})
    }
    else if (selected.indexOf('gadget_cable')!=-1) {
      selected.splice(selected.indexOf('gadget_cable'), 1);
      _that.props.navigation.navigate('Gadget_cable',{selected:selected,station_id:this.state.station_id})
    }
    else if (selected.indexOf('gadget_rent')!=-1) {
      selected.splice(selected.indexOf('gadget_rent'), 1);
      _that.props.navigation.navigate('Gadget_rent',{selected:selected,station_id:this.state.station_id})
    }
  }

  render() {
    const headerProp = {
      title: 'Add Service',
      screens: 'Serviceslistseller',
      type:''
    };

    return (
      <View style={styles.container}>
        <Header info={headerProp} navigation={_that.props.navigation}/>

        <View style={styles.content}>
          <KeyboardAwareScrollView innerRef={() => {return [this.refs.name,this.refs.email, this.refs.password]}} >
            <ScrollView>
              <View style={styles.container2}>

                <View style={[styles.SectionStyle,{marginTop:15,marginBottom:10}]}>
                    <View style={styles.textHead}>
                        <Text style={styles.inputtext}>El-Car</Text>
                    </View>
                </View>
                <View style={{padding:10}}>
                  <View style={[styles.SectionStyle,{marginBottom:10}]}>
                      <View style={styles.textSubHead}>
                        <CheckBox
                          style={{width:'100%'}}
                          onClick={()=>{this.setState({el_car_charge:!this.state.el_car_charge})}}
                          isChecked={this.state.el_car_charge}
                          leftText={"Charging"}
                          checkedCheckBoxColor={'#3995f7'}
                          uncheckedCheckBoxColor={'#3995f7'}
                        />
                      </View>
                  </View>
                  <View style={[styles.SectionStyle,{marginBottom:10}]}>
                    <View style={styles.textSubHead}>
                      <CheckBox
                        style={{width:'100%'}}
                        onClick={()=>{this.setState({el_car_park:!this.state.el_car_park})}}
                        isChecked={this.state.el_car_park}
                        leftText={"Parking"}
                        checkedCheckBoxColor={'#3995f7'}
                        uncheckedCheckBoxColor={'#3995f7'}
                      />
                    </View>
                  </View>
                  <View style={[styles.SectionStyle,{marginBottom:10}]}>
                    <View style={styles.textSubHead} >
                      <CheckBox
                        style={{width:'100%'}}
                        onClick={()=>{this.setState({el_car_bed:!this.state.el_car_bed})}}
                        isChecked={this.state.el_car_bed}
                        leftText={"Accomodation"}
                        checkedCheckBoxColor={'#3995f7'}
                        uncheckedCheckBoxColor={'#3995f7'}
                      />
                    </View>
                  </View>
                  <View style={[styles.SectionStyle,{marginBottom:10}]}>
                    <View style={styles.textSubHead} >
                      <CheckBox
                        style={{width:'100%'}}
                        onClick={()=>{this.setState({el_car_cable:!this.state.el_car_cable})}}
                        isChecked={this.state.el_car_cable}
                        leftText={"Cable"}
                        checkedCheckBoxColor={'#3995f7'}
                        uncheckedCheckBoxColor={'#3995f7'}
                      />
                    </View>
                  </View>
                  <View style={[styles.SectionStyle,{marginBottom:10}]}>
                    <View style={styles.textSubHead} >
                      <CheckBox
                        style={{width:'100%'}}
                        onClick={()=>{this.setState({el_car_rent:!this.state.el_car_rent})}}
                        isChecked={this.state.el_car_rent}
                        leftText={"Rent"}
                        checkedCheckBoxColor={'#3995f7'}
                        uncheckedCheckBoxColor={'#3995f7'}
                      />
                    </View>
                  </View>
                </View>

                <View style={[styles.SectionStyle,{marginBottom:10}]}>
                    <View style={styles.textHead}>
                        <Text style={styles.inputtext}>El-Bike</Text>
                    </View>
                </View>

                <View style={{padding:10}}>
                  <View style={[styles.SectionStyle,{marginBottom:10}]}>
                      <View style={styles.textSubHead}>
                        <CheckBox
                          style={{width:'100%'}}
                          onClick={()=>{this.setState({el_bike_charge:!this.state.el_bike_charge})}}
                          isChecked={this.state.el_bike_charge}
                          leftText={"Charging"}
                          checkedCheckBoxColor={'#3995f7'}
                          uncheckedCheckBoxColor={'#3995f7'}
                        />
                      </View>
                  </View>
                  <View style={[styles.SectionStyle,{marginBottom:10}]}>
                    <View style={styles.textSubHead}>
                      <CheckBox
                        style={{width:'100%'}}
                        onClick={()=>{this.setState({el_bike_park:!this.state.el_bike_park})}}
                        isChecked={this.state.el_bike_park}
                        leftText={"Parking"}
                        checkedCheckBoxColor={'#3995f7'}
                        uncheckedCheckBoxColor={'#3995f7'}
                      />
                    </View>
                  </View>
                  <View style={[styles.SectionStyle,{marginBottom:10}]}>
                    <View style={styles.textSubHead} >
                      <CheckBox
                        style={{width:'100%'}}
                        onClick={()=>{this.setState({el_bike_bed:!this.state.el_bike_bed})}}
                        isChecked={this.state.el_bike_bed}
                        leftText={"Accomodation"}
                        checkedCheckBoxColor={'#3995f7'}
                        uncheckedCheckBoxColor={'#3995f7'}
                      />
                    </View>
                  </View>
                  <View style={[styles.SectionStyle,{marginBottom:10}]}>
                    <View style={styles.textSubHead} >
                      <CheckBox
                        style={{width:'100%'}}
                        onClick={()=>{this.setState({el_bike_cable:!this.state.el_bike_cable})}}
                        isChecked={this.state.el_bike_cable}
                        leftText={"Cable"}
                        checkedCheckBoxColor={'#3995f7'}
                        uncheckedCheckBoxColor={'#3995f7'}
                      />
                    </View>
                  </View>
                  <View style={[styles.SectionStyle,{marginBottom:10}]}>
                    <View style={styles.textSubHead} >
                      <CheckBox
                        style={{width:'100%'}}
                        onClick={()=>{this.setState({el_bike_rent:!this.state.el_bike_rent})}}
                        isChecked={this.state.el_bike_rent}
                        leftText={"Rent"}
                        checkedCheckBoxColor={'#3995f7'}
                        uncheckedCheckBoxColor={'#3995f7'}
                      />
                    </View>
                  </View>
                </View>

                <View style={[styles.SectionStyle,{marginBottom:20}]}>
                    <View  style={styles.textHead}>
                        <Text style={styles.inputtext}>Gadgets</Text>
                    </View>
                </View>
                <View style={{padding:10}}>
                  <View style={[styles.SectionStyle,{marginBottom:10}]}>
                      <View style={styles.textSubHead}>
                        <CheckBox
                          style={{width:'100%'}}
                          onClick={()=>{this.setState({gadget_charge:!this.state.gadget_charge})}}
                          isChecked={this.state.gadget_charge}
                          leftText={"Charging"}
                          checkedCheckBoxColor={'#3995f7'}
                          uncheckedCheckBoxColor={'#3995f7'}
                        />
                      </View>
                  </View>
                  <View style={[styles.SectionStyle,{marginBottom:10}]}>
                    <View style={styles.textSubHead} >
                      <CheckBox
                        style={{width:'100%'}}
                        onClick={()=>{this.setState({gadget_bed:!this.state.gadget_bed})}}
                        isChecked={this.state.gadget_bed}
                        leftText={"Accomodation"}
                        checkedCheckBoxColor={'#3995f7'}
                        uncheckedCheckBoxColor={'#3995f7'}
                      />
                    </View>
                  </View>
                  <View style={[styles.SectionStyle,{marginBottom:10}]}>
                    <View style={styles.textSubHead} >
                      <CheckBox
                        style={{width:'100%'}}
                        onClick={()=>{this.setState({gadget_cable:!this.state.gadget_cable})}}
                        isChecked={this.state.gadget_cable}
                        leftText={"Cable"}
                        checkedCheckBoxColor={'#3995f7'}
                        uncheckedCheckBoxColor={'#3995f7'}
                      />
                    </View>
                  </View>
                  <View style={[styles.SectionStyle,{marginBottom:10}]}>
                    <View style={styles.textSubHead} >
                      <CheckBox
                        style={{width:'100%'}}
                        onClick={()=>{this.setState({gadget_rent:!this.state.gadget_rent})}}
                        isChecked={this.state.gadget_rent}
                        leftText={"Rent"}
                        checkedCheckBoxColor={'#3995f7'}
                        uncheckedCheckBoxColor={'#3995f7'}
                      />
                    </View>
                  </View>
                </View>

                {/*<View style={styles.SectionStyle}>
                    <View style={styles.inputfield}>
                        <Text style={styles.inputtext}>Description</Text>
                        <TextInput
                          autoCapitalize={'none'}
                          autoCorrect={false}
                          value={this.state.description}
                          multiline={true}
                          numberOfLines={4}
                          underlineColorAndroid="transparent"
                          returnKeyType={ "next"}
                          selectionColor={"#000000"}
                          autoFocus={ false}
                          placeholder="Description"
                          placeholderTextColor="#808080"
                          style={[styles.textInput,{height:80}]}
                          ref="selecteddescription"
                          keyboardType={ 'default'}
                          onFocus={ () => this.setState({descriptionError:''}) }
                          onChangeText={selecteddescription=> this.setState({selecteddescription})}
                        />
                    </View>
                    <Text style={styles.error}>{this.state.descriptionError}</Text>
                </View>*/}

                {/*<View style={styles.SectionStyle}>
                  <View style={styles.inputfield}>
                    <Text style={styles.inputtext}>Status</Text>
                      <RadioGroup
                        color='#000000'
                        selectedIndex={1}
                        style={styles.SectionRadioGroup}
                        onSelect = {(index, value) => this.onSelect(index, value)}
                      >
                        <RadioButton value={'0'}>
                          <Text style={{color:'#000000'}}>InActive</Text>
                        </RadioButton>

                        <RadioButton value={'1'}>
                          <Text style={{color:'#000000'}}>Active</Text>
                        </RadioButton>
                      </RadioGroup>
                    </View>
                </View>*/}
              </View>
            </ScrollView>
          </KeyboardAwareScrollView>

          <TouchableOpacity style={[styles.SectionStyle,{marginTop:AppSizes.ResponsiveSize.Padding(3)}]} onPress={()=>this.handleOnPress()}>
            <CommonButton label='Next' width='100%'/>
          </TouchableOpacity>
        </View>

        <Spinner visible={this.state.isVisible}  />
      </View>
    );
  }
}


const styles = StyleSheet.create({
  container: {
    flex: 1,
    paddingTop:  0,
  },
  content: {
      flex: 1,
      flexDirection: 'column',
      height:'70%',
      backgroundColor:'#ffffff'
  },
  container1: {
    flex: 3,
    height:AppSizes.screen.width/4,
    paddingLeft:AppSizes.ResponsiveSize.Padding(5),
    //backgroundColor:'red'
  },
  container2: {
    flex: 10,
    height:'70%',
    paddingLeft:'4%',
    paddingRight:'4%',
    flexDirection: 'column',
    //backgroundColor:'blue'
  },
  container3: {
    flex: 3,
    height:AppSizes.screen.width/8,
    alignItems: 'center',//replace with flex-end or center
    justifyContent: 'flex-end',
  },
  avatarContainer: {
     borderColor: '#9B9B9B',
     borderWidth: 1 / PixelRatio.get(),
     justifyContent: 'center',
     alignItems: 'center',
  },
  avatar: {
   borderRadius: 75,
   width: 120,
   height: 120,
  },
  headerTitle:{
   fontSize:AppSizes.ResponsiveSize.Sizes(25),
   color:'#000000',
   fontWeight:'bold',
   fontFamily:Fonts.RobotoBold,
   paddingBottom:AppSizes.ResponsiveSize.Padding(1),
  },
  headersubTitle:{
   fontSize:AppSizes.ResponsiveSize.Sizes(14),
   color:'#000000',
   fontWeight:'600',
   fontFamily:Fonts.RobotoBold,
   paddingTop:AppSizes.ResponsiveSize.Padding(2),
  },
  headerBorder :{
   borderBottomColor: '#000000',
   borderBottomWidth: 3,
   width:'10%',
  },
  footertext :{
   color :'#000000',
   fontSize:AppSizes.ResponsiveSize.Sizes(12),
   fontWeight:'400',
   fontFamily:Fonts.RobotoRegular,
  },
  SectionStyle: {
   width:AppSizes.ResponsiveSize.width,
  },
  ImageStyle: {
   height: AppSizes.screen.width/21,
   width: AppSizes.screen.width/21,
   resizeMode : 'contain',
  },
  textInput: {
   flex:1,
   paddingLeft:AppSizes.ResponsiveSize.Padding(3),
   fontSize: AppSizes.ResponsiveSize.Sizes(12),
   color:'#000000',
   backgroundColor:'#eee',
   height:40,
  },
  inputfield:{
   width:AppSizes.ResponsiveSize.width,
  },
  inputtext:{marginBottom:10,
   color:'#fff',
   fontSize:AppSizes.ResponsiveSize.Sizes(14),
   fontWeight:'600',
   fontFamily:Fonts.RobotoBold,
  },

  error:{
   color:'red',
   fontSize:AppSizes.ResponsiveSize.Sizes(12),
   fontFamily:Fonts.RobotoRegular,
  },
  SectionRadioGroup:{
    flexDirection:'row',
  },
  facilityView:{
    alignItems:'center',
    margin:AppSizes.ResponsiveSize.Padding(2)
  },
  facilityText:{
    fontSize:25,
    color:'#000',
    fontFamily:Fonts.RobotoRegular,
  },
  checkboxSection :{
    marginBottom: AppSizes.ResponsiveSize.Padding(2),
    height:AppSizes.screen.width/15,
    width:AppSizes.screen.width/3
    //backgroundColor:'red',
  },
  SectionRadioView: {
    borderBottomWidth: 1,
    borderBottomColor: '#ffffff',
    borderRadius: 5 ,
    width:AppSizes.screen.width*90/100,
    marginLeft: AppSizes.ResponsiveSize.Padding(5),
    marginRight: AppSizes.ResponsiveSize.Padding(5),
    marginBottom: (Platform.OS === 'ios') ? AppSizes.ResponsiveSize.Padding(1) :AppSizes.ResponsiveSize.Padding(2),

  },
  textHead:{
    width:'100%',
    flexDirection:'row',
    justifyContent:'space-between',
    backgroundColor:'#3995f7',
    padding:10,
  },
  textSubHead:{
    width:'100%',
    flexDirection:'row',
    justifyContent:'space-between',
  }
});
