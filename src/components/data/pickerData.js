export const pickerData = {

  "bike_outlet":
  [
    {
        name: 'Outdoor Outlet',
        id: 1
    },
    {
        name: 'Indoor Outlet',
        id: 2
    },
    {
        name: 'Extension Cord',
        id: 3
    }
  ],

  "chargedata":
  [
    {
        name:'Fast charge',
        id:1
    },
    {
        name:'Regular Power Outlet(Long Charge)',
        id:2
    }
  ],


  "accomodation":
  [
    {
        name:'Single Bed',
        id:1
    },
    {
        name:'Double Bed',
        id:2
    },
    {
        name:'Kingsize Bed',
        id:3
    },
    {
        name:'House/Flat',
        id:4
    },
  ],

  "car_outlet":
  [
    {
        name:'CHAdeMO',
        id:1
    },
    {
        name:'CSS',
        id:2
    },
    {
        name:'Tesla SC',
        id:3
    },
    {
        name:'AC',
        id:4
    },
  ],

  "effect":
  [
    {
      name:'3.3kW',
      id:1
    },
    {
      name:'3.6kW',
      id:2
    },
    {
      name:'3.7kW',
      id:3
    },
    {
      name:'6.6kW',
      id:4
    },
    {
      name:'7.4kW',
      id:5
    },
    {
      name:'11kW',
      id:6
    },
    {
      name:'16.5kW',
      id:7
    },
    {
      name:'22kW',
      id:8
    },
    {
      name:'43kW',
      id:9
    },
  ],

  "phase":
  [
    {
      name:'1',
      id:1
    },
    {
      name:'3',
      id:2
    }
  ],

  "ampere":
  [
    {
      name:'14.5A',
      id:1
    },
    {
      name:'16A',
      id:2
    },
    {
      name:'24A',
      id:3
    },
    {
      name:'28A',
      id:4
    },
    {
      name:'30A',
      id:5
    },
    {
      name:'32A',
      id:6
    },
    {
      name:'63A',
      id:7
    }
  ],

  "ellectric":
  [
    {
      name:'Car',
      id:'1'
    },
    {
      name:'Bike',
      id:'2'
    },
    {
      name:'Gadgets',
      id:'5'
    },
  ],

  "rentals":
  [
    {
      name:'Car',
      id:'3'
    },
    {
      name:'Bike',
      id:'4'
    },
    {
      name:'Gadgets',
      id:'6'
    },
  ],


  "parking":
  [
    {
      name:'Car',
      id:'7'
    },
    {
      name:'Bike',
      id:'8'
    },
  ],


  "el_bikes":
  [
    {
      name:'elbike',
      id:'5'
    },
    {
      name:'Scooters',
      id:'1'
    },
    {
      name:'Skateboards',
      id:'3'
    },
    {
      name:'3wheelers',
      id:'6'
    },
  ],

  "coffee":
  [
    {
      name: 'Yes',
      id:1
    },
    {
      name:'No',
      id:2
    }
  ],

  "rent_bikes":
  [
    {
      name:'elbike',
      id:'7'
    },
    {
      name:'Scooters',
      id:'2'
    },
    {
      name:'Skateboards',
      id:'4'
    },
    {
      name:'3wheelers',
      id:'13'
    },
  ],

  "parking_bikes":
  [
    {
      name:'elbike',
      id:'16'
    },
    {
      name:'Scooters',
      id:'15'
    },
    {
      name:'Skateboards',
      id:'14'
    },
    {
      name:'3wheelers',
      id:'17'
    },
  ],

  "el_gadgets":
  [
    {
      name:'Laptops',
      id:'18'
    },
    {
      name:'Drone',
      id:'19'
    },
    {
      name:'Mobile Phones',
      id:'20'
    },
    {
      name:'PC',
      id:'21'
    },
    {
      name:'Filling for drones',
      id:'22'
    }
  ],

  "rent_gadgets":
  [
    {
      name:'Laptops',
      id:'24'
    },
    {
      name:'Drone',
      id:'27'
    },
    {
      name:'Mobile Phones',
      id:'25'
    },
    {
      name:'PC',
      id:'26'
    },
    {
      name:'Filling for drones',
      id:'23'
    }
  ]
}
