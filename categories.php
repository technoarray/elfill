<?php
	//defined('BASEPATH') OR exit('No direct script access allowed');

	class categories extends CI_Controller
	{

		public function __construct(){
			parent::__construct();
			if(!$this->session->has_userdata('is_admin_login'))
			{
				redirect('admin/auth/login');
			}
			$this->load->model('admin/categories_model', 'categories_model');
		}

    public function index()
	{
		$data['page_title'] = 'categories - '.$this->config->item('PROJNAME');
		$data['view'] = 'admin/categories/index';
		$this->load->view('admin/layout', $data);
	}

	public function category_list()
	{
		$data['page_title'] = 'Categories - '.$this->config->item('PROJNAME');
		$data['view'] = 'admin/categories/sell_category';
		$this->load->view('admin/layout', $data);
	}


	/* public function sell_category()
	{
		$data['page_title'] = 'Sell Categories - '.$this->config->item('PROJNAME');
		$data['view'] = 'admin/categories/sell_category';
		$this->load->view('admin/layout', $data);
	}

	public function rent_category()
	{
		$data['page_title'] = 'Rent Categories - '.$this->config->item('PROJNAME');
		$data['view'] = 'admin/categories/rent_category';
		$this->load->view('admin/layout', $data);
	} */


    public function sell_datatable_json(){
			$columns = array(
							0 =>'categories.cat_id ',
							1 =>'categories.cat_name',
							2 =>'categories.cat_type',
							3 =>'categories.status',
						);

			$limit = $this->input->post('length');
			$start = $this->input->post('start');
			$order = $columns[$this->input->post('order')[0]['column']];
			$dir = $this->input->post('order')[0]['dir'];
			//$type = 'sell';
			//$totalData = $this->categories_model->alldata_count($type);
			$totalData = $this->categories_model->alldata_count();

			$totalFiltered = $totalData;

			if(empty($this->input->post('search')['value']))
			{
				//$members = $this->categories_model->alldata($limit,$start,$order,$dir,$type);
				$members = $this->categories_model->alldata($limit,$start,$order,$dir);
			}
			else {
				$search = $this->input->post('search')['value'];

				//$members =  $this->categories_model->data_search($limit,$start,$search,$order,$dir,$type);
				$members =  $this->categories_model->data_search($limit,$start,$search,$order,$dir);

				//$totalFiltered = $this->categories_model->data_search_count($search,$type);
				$totalFiltered = $this->categories_model->data_search_count($search);
			}

			$data = array();
			if(!empty($members))
			{
				foreach($members as $row)
				{

					$sub_array = array();
					$sub_array[] = $row->cat_id;
					$sub_array[] = $row->cat_name;
					$sub_array[] = $row->cat_type_value;

					$sub_array[] = $cat_type;
					if( $row->status ==1)
					{
						//$status='active';
						$status='<span class="btn btn-success btn-flat btn-xs" title="status">Active</span>';
						//$status='<span class="btn btn-success btn-flat btn-xs" title="status"><a href="'.base_url().'admin/categories/cat_status/sell_category/'.$row->cat_id.'/1">Active</a></span>';
					}else{
						$status='<span class="btn btn-success btn-flat btn-xs" title="status">Inactive</span>';
						//$status='<span class="btn btn-success btn-flat btn-xs" title="status"><a href="'.base_url().'admin/categories/cat_status/sell_category/'.$row->cat_id.'/0">Deactive</a></span>';
					}
				  $sub_array[] = $status;

							$sub_array[] = '
				  <a rel="tooltip" class="edit_record btn btn-success btn-just-icon btn-sm" data-id="'.$row->cat_id.'" data-cat_name="'.$row->cat_name.'" data-cat_type="'.$row->cat_type.'" data-cat_type="'.$row->cat_type.'" data-status="'.$row->status.'">
					  <i class="material-icons"><i class="fa fa-pencil" aria-hidden="true"></i></i>
				  </a>
				  <a class="btn btn-danger btn-just-icon btn-sm" title="Delete" data-href="'.base_url().'admin/categories/del/sell_category/'.$row->cat_id.'" data-toggle="modal" data-target="#confirm-delete">
					  <i class="material-icons"><i class="fa fa-trash" aria-hidden="true"></i></i>
				  </a>
					';

					$data[] = $sub_array;

				}
			}

			$json_data = array(
						"draw"            => intval($this->input->post('draw')),
						"recordsTotal"    => intval($totalData),
						"recordsFiltered" => intval($totalFiltered),
						"data"            => $data
						);

			echo json_encode($json_data);
          exit();

		}

    public function rent_datatable_json(){
			$columns = array(
							0 =>'categories.cat_id ',
              1 =>'categories.cat_name',
							2 =>'categories.status',
      );

			$limit = $this->input->post('length');
			$start = $this->input->post('start');
			$order = $columns[$this->input->post('order')[0]['column']];
			$dir = $this->input->post('order')[0]['dir'];
      $type = 'rent';

			$totalData = $this->categories_model->alldata_count($type);

			$totalFiltered = $totalData;
			if(empty($this->input->post('search')['value']))
			{
				$members = $this->categories_model->alldata($limit,$start,$order,$dir,$type);
			}
			else {
				$search = $this->input->post('search')['value'];

				$members =  $this->categories_model->data_search($limit,$start,$search,$order,$dir,$type);

				$totalFiltered = $this->categories_model->data_search_count($search,$type);
			}

			$data = array();
			if(!empty($members))
			{
				foreach($members as $row)
				{

					$sub_array = array();
					$sub_array[] = $row->cat_id;
					$sub_array[] = $row->cat_name;
          if( $row->status ==1)
					{
						$status='<span class="btn btn-success btn-flat btn-xs" title="status"><a href="'.base_url().'admin/categories/cat_status/rent_category/'.$row->cat_id.'/1">Active</a></span>';
					}else{
						$status='<span class="btn btn-success btn-flat btn-xs" title="status"><a href="'.base_url().'admin/categories/cat_status/rent_category/'.$row->cat_id.'/0">Deactive</a></span>';
					}
          $sub_array[] = $status;

					$sub_array[] = '
          <a rel="tooltip" class="edit_record btn btn-success btn-just-icon btn-sm" data-id="'.$row->cat_id.'" data-cat_name="'.$row->cat_name.'" data-status="'.$row->status.'">
              <i class="material-icons"><i class="fa fa-pencil" aria-hidden="true"></i></i>
          </a>
          <a class="btn btn-danger btn-just-icon btn-sm" title="Delete" data-href="'.base_url().'admin/categories/del/rent_category/'.$row->cat_id.'" data-toggle="modal" data-target="#confirm-delete">
              <i class="material-icons"><i class="fa fa-trash" aria-hidden="true"></i></i>
          </a>
					';

					$data[] = $sub_array;

				}
			}

			$json_data = array(
						"draw"            => intval($this->input->post('draw')),
						"recordsTotal"    => intval($totalData),
						"recordsFiltered" => intval($totalFiltered),
						"data"            => $data
						);

			echo json_encode($json_data);
          exit();

		}

    public function cat_status($type,$id = FALSE, $status=false){
			if($status==1){
				$dstatus=0;
				$dmsg="This Category is deactivated";
			}
			else
			{
				$dstatus=1;
				$dmsg="This Category is activated";
			}
			$data = array(
							'status' => $dstatus,
							'modify_date' => date('Y-m-d h:i A'),
						);
						$data = $this->security->xss_clean($data);
						$result = $this->categories_model->edit_data($data, $id);
						if($result){
							$this->session->set_flashdata('msg', $dmsg);
							redirect(base_url('admin/categories/'.$type));
						}
		}

		public function add_categories(){
			$data = array(
				'cat_name' => $this->input->post('cat_name'),
				'cat_type' => $this->input->post('category_type'),
				'cat_type_value' => $this->input->post('cat_type_value'),
				'status' => $this->input->post('status'),
				'created_date' => date('Y-m-d : h:m:s'),
				'modify_date' => date('Y-m-d : h:m:s'),
			);
			$data = $this->security->xss_clean($data);
			$result = $this->categories_model->add_data($data);
			if($result){
				$this->session->set_flashdata('msg', 'Record is Added Successfully!');
				$json_data = array(
					"code"   => 1,
					"msg"    => "Record added successfully"
				);
			}
			else{
				$json_data = array(
					"code"   => 0,
					"msg"    => "Error Occured, Please try again!"
				);
			}


			echo json_encode($json_data);
          exit();
		}

		public function edit_categories(){
			$data = array(
				'cat_name' => $this->input->post('cat_name'),
				'cat_type' => $this->input->post('category_type'),
				'cat_type_value' => $this->input->post('cat_type_value'),
				'status' => $this->input->post('status'),
				'modify_date' => date('Y-m-d : h:m:s'),
			);
			$id=$this->input->post('id');
			$data = $this->security->xss_clean($data);
			$result = $this->categories_model->edit_data($data, $id);
			if($result){
				$this->session->set_flashdata('msg', 'Record is updated Successfully!');
				$json_data = array(
					"code"   => 1,
					"msg"    => "Record updated successfully"
				);
			}
			else{
				$json_data = array(
					"code"   => 0,
					"msg"    => "Error Occured, Please try again!"
				);
			}


			echo json_encode($json_data);
          exit();
		}

    public function del($type,$id = 0){
			$data=$this->categories_model->get_data_by_id($id);
      // echo $type . $id;

			$this->db->delete('charging_specification', array('cat_id' => $id));
			$this->db->delete('categories', array('cat_id' => $id));


			$this->session->set_flashdata('msg', 'Record is Deleted Successfully!');
			redirect(base_url('admin/categories/category_list'));
		}


}
